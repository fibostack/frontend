import { Divider } from 'antd'
import { LanguageSelector, ProfileMenu, TenantSelector } from 'components'
import React from 'react'
import styles from './styles.module.scss'

interface TopRightProps {}

const TopRight: React.FC<TopRightProps> = () => {
  return (
    <div className={styles.container}>
      <Divider type="vertical" />
      <TenantSelector />
      <Divider type="vertical" />
      <LanguageSelector />
      <Divider type="vertical" />
      <ProfileMenu />
    </div>
  )
}

export default TopRight
