/* eslint-disable camelcase */
/* eslint-disable no-unused-expressions */
import {
  AppstoreFilled,
  CodeFilled,
  DatabaseFilled,
  GoldFilled,
  LockFilled,
  MacCommandFilled,
  SettingFilled,
} from '@ant-design/icons'
import ProLayout, { MenuDataItem, Settings } from '@ant-design/pro-layout'
import logo from 'assets/logo.svg'
import { PoweredBy } from 'components'
import defaultSettings from 'configs'
import { ReduxInterface, UserInterface } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'
import { useSelector } from 'react-redux'
import { Link, RouteComponentProps, useHistory, withRouter } from 'react-router-dom'
import styles from './styles.module.scss'
import RightContent from './TopRight'

interface MainLayoutProps extends RouteComponentProps<any> {}

const MainLayout: React.FC<MainLayoutProps> = ({ children, ...props }) => {
  const intl = useIntl()
  const history = useHistory()
  const [collapsed, setCollapsed] = useState<boolean>(false)
  const { role, is_admin } = useSelector<ReduxInterface, UserInterface>((state: ReduxInterface) => state.UserReducer)
  const [settings] = useState<Partial<Settings>>({
    ...defaultSettings,
    fixedHeader: true,
    fixSiderbar: true,
  })

  const menuDataRender = () => {
    const menu: MenuDataItem[] = [
      {
        key: 'dashboard',
        name: intl.formatMessage({ id: 'menu.dashboard' }),
        path: '/dashboard',
        icon: <AppstoreFilled style={{ fontSize: 16 }} />,
      },
      {
        key: 'fg-compute',
        path: '/fg-compute',
        name: intl.formatMessage({ id: 'menu.compute' }),
        icon: <CodeFilled style={{ fontSize: 16 }} />,
        children: [
          {
            icon: null,
            key: 'instaces',
            name: intl.formatMessage({ id: 'menu.instaces' }),
            path: '/fg-compute/instances',
            parentKeys: ['compute'],
          },
          {
            icon: null,
            key: 'key-pairs',
            name: intl.formatMessage({ id: 'menu.key_pairs' }),
            path: '/fg-compute/key-pairs',
            parentKeys: ['compute'],
          },
        ],
      },
      {
        key: 'fg-storage',
        path: '/fg-storage',
        name: intl.formatMessage({ id: 'menu.storage' }),
        icon: <DatabaseFilled style={{ fontSize: 16 }} />,
        children: [
          {
            icon: null,
            key: 'volumes',
            name: intl.formatMessage({ id: 'menu.volumes' }),
            path: '/fg-storage/volumes',
            parentKeys: ['storage'],
          },
          {
            icon: null,
            key: 'snapshots',
            name: intl.formatMessage({ id: 'menu.snapshots' }),
            path: '/fg-storage/snapshots',
            parentKeys: ['storage'],
          },
        ],
      },
      {
        key: 'fg-network',
        path: '/fg-network',
        name: intl.formatMessage({ id: 'menu.network' }),
        icon: <GoldFilled style={{ fontSize: 16 }} />,
        children: [
          {
            icon: null,
            key: 'networks',
            name: intl.formatMessage({ id: 'menu.networks' }),
            path: '/fg-network/networks',
            parentKeys: ['network'],
          },
          {
            icon: null,
            key: 'security-groups',
            name: intl.formatMessage({ id: 'menu.security_groups' }),
            path: '/fg-network/security-groups',
            parentKeys: ['network'],
          },
          {
            icon: null,
            key: 'router',
            name: intl.formatMessage({ id: 'menu.routers' }),
            path: '/fg-network/routers',
            parentKeys: ['network'],
          },
          {
            icon: null,
            key: 'floating_ip',
            name: intl.formatMessage({ id: 'menu.floating_ips' }),
            path: '/fg-network/floating-ips',
            parentKeys: ['network'],
          },
        ],
      },
    ]

    if (role && is_admin) {
      menu.length > 2 &&
        menu[1].children &&
        menu[1].children.push(
          {
            icon: null,
            key: 'images',
            name: intl.formatMessage({ id: 'menu.images' }),
            path: '/fg-compute/images',
            parentKeys: ['compute'],
          },
          {
            icon: null,
            key: 'flavors',
            name: intl.formatMessage({ id: 'menu.flavors' }),
            path: '/fg-compute/flavors',
            parentKeys: ['compute'],
          }
        )
      menu.push(
        {
          key: 'fg-identity',
          path: '/fg-identity',
          name: intl.formatMessage({ id: 'menu.identity' }),
          icon: <LockFilled style={{ fontSize: 16 }} />,
          children: [
            {
              icon: null,
              key: 'projects',
              name: intl.formatMessage({ id: 'menu.projects' }),
              path: '/fg-identity/projects',
              parentKeys: ['identity'],
            },
            {
              icon: null,
              key: 'users',
              name: intl.formatMessage({ id: 'menu.users' }),
              path: '/fg-identity/users',
              parentKeys: ['identity'],
            },
            {
              icon: null,
              key: 'roles',
              name: intl.formatMessage({ id: 'menu.roles' }),
              path: '/fg-identity/roles',
              parentKeys: ['identity'],
            },
            {
              icon: null,
              key: 'permissions',
              name: intl.formatMessage({ id: 'menu.permissions' }),
              path: '/fg-identity/permissions',
              parentKeys: ['identity'],
            },
          ],
        },
        {
          key: 'fg-system',
          path: '/fg-system',
          name: intl.formatMessage({ id: 'menu.system' }),
          icon: <SettingFilled style={{ fontSize: 16 }} />,
          children: [
            {
              icon: null,
              key: 'information',
              name: intl.formatMessage({ id: 'menu.system_information' }),
              path: '/fg-system/information',
              parentKeys: ['system'],
            },
            {
              icon: null,
              key: 'hypervisor',
              name: intl.formatMessage({ id: 'menu.hypervisor' }),
              path: '/fg-system/hypervisor',
              parentKeys: ['system'],
            },
          ],
        }
      )
    }

    menu.push({
      key: 'fg-action_log',
      name: intl.formatMessage({ id: 'menu.action_log' }),
      path: '/fg-action_log',
      icon: <MacCommandFilled style={{ fontSize: 16 }} />,
    })

    return menu
  }

  return (
    <ProLayout
      logo={logo}
      siderWidth={280}
      disableMobile={false}
      onMenuHeaderClick={() => history.push('/')}
      menuHeaderRender={(logoDom, titleDom) => (
        <Link to="/" className="left-logo">
          {logoDom}
          {titleDom}
        </Link>
      )}
      collapsed={collapsed}
      onCollapse={setCollapsed}
      menuDataRender={menuDataRender}
      menuItemRender={(menuItemProps, defaultDom) => {
        return menuItemProps.isUrl ? defaultDom : <Link to={menuItemProps.path || '/'}>{defaultDom}</Link>
      }}
      rightContentRender={() => <RightContent />}
      footerRender={() => (
        <div className={styles.footer}>
          <PoweredBy color="black" />
        </div>
      )}
      contentStyle={{ overflow: 'auto', height: 'calc(100vh - 125px)', padding: 12 }}
      {...props}
      {...settings}
    >
      {children}
    </ProLayout>
  )
}

export default withRouter(MainLayout)
