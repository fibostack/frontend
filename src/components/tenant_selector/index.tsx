/* eslint-disable camelcase */
import { Input, notification, Select, Tag } from 'antd'
import { listUserProjects, updateUser } from 'api'
import { CloseAwaitMS } from 'configs'
import { Project, ReduxInterface, UserInterface } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux'
import { FormattedMessage, useIntl } from 'react-intl'
import { FireTwoTone, SyncOutlined } from '@ant-design/icons'

const TenantSelector = () => {
  const intl = useIntl()
  const _isMounted = useRef<boolean>(true)
  const [fLoading, setFLoading] = useState<boolean>(true)
  const [loading, setLoading] = useState<boolean>(false)
  const [projects, setProjects] = useState<Project[]>([])
  const { os_tenant_id, os_user_id } = useSelector<ReduxInterface, UserInterface>(
    (state: ReduxInterface) => state.UserReducer
  )

  useEffect(() => {
    fetchProject()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchProject = async () => {
    setFLoading(true)
    const tmpProjects = (await listUserProjects({})) as Project[]
    if (_isMounted.current) {
      setProjects(tmpProjects)
      setFLoading(false)
    }
  }

  const handleChangeProject = async (value: string) => {
    setLoading(true)
    const success = await updateUser({
      data: { os_user_id, project_id: value, status: true },
    })
    setLoading(false)
    if (success && _isMounted.current) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${projects.find((project) => project.id === value)?.name} project changed.`,
      })
      setTimeout(() => {
        window.location.reload(false)
      }, CloseAwaitMS)
    }
  }

  return (
    <Input.Group compact>
      <strong style={{ padding: '2px 0' }}>
        <FormattedMessage id="project" />:
      </strong>
      {fLoading ? (
        <Tag color="red" className="m_l_1">
          <SyncOutlined spin />
        </Tag>
      ) : (
        <Select
          bordered={false}
          size="small"
          loading={loading}
          style={{ minWidth: 100, color: '#CE201F' }}
          suffixIcon={loading ? <SyncOutlined spin /> : <FireTwoTone twoToneColor="#CE201F" />}
          defaultValue={os_tenant_id}
          onChange={handleChangeProject}
        >
          {projects.map((project) => (
            <Select.Option key={project.name} value={project.id}>
              {project.name}
            </Select.Option>
          ))}
        </Select>
      )}
    </Input.Group>
  )
}

export default TenantSelector
