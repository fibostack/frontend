import { Button, Input, Select } from 'antd'
import { listProject } from 'api'
import { Project } from 'models'
import CreateModal from 'pages/identity/project/create'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'

interface Props {
  value?: string
  onChange?: (value: string) => void
}

const Selector: React.FC<Props> = React.forwardRef<any, Props>(({ value, onChange }, ref) => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState<boolean>(true)
  const [name, setName] = useState<string | undefined>(value)
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [projects, setProjects] = useState<Project[]>([])

  useEffect(() => {
    fetchProject()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchProject = async () => {
    setLoading(true)
    const ps = (await listProject({})) as Project[]
    if (_isMounted.current) {
      setProjects(ps)
      setLoading(false)
    }
  }

  const handleChange = (val: string) => {
    setName(val)
    if (onChange) {
      onChange(val)
    }
  }

  return (
    <>
      {createVisible && (
        <CreateModal
          visible={createVisible}
          onOk={(newName: string) => {
            fetchProject()
            handleChange(newName)
            setCreateVisible(false)
          }}
          onCancel={() => {
            setCreateVisible(false)
          }}
        />
      )}
      <Input.Group compact className="flex row">
        <Select
          ref={ref}
          showSearch
          value={name}
          style={{ flex: 1 }}
          onChange={handleChange}
          placeholder="Project"
          optionFilterProp="children"
          filterOption={(input, option) =>
            option ? option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 : false
          }
        >
          {projects.map((project) => {
            return (
              <Select.Option key={project.id} value={project.id}>
                {project.name}
              </Select.Option>
            )
          })}
        </Select>
        <Button
          loading={loading}
          style={{ borderLeftWidth: 0 }}
          onClick={() => {
            setCreateVisible(true)
          }}
        >
          {intl.formatMessage({ id: 'project.create_title' })}
        </Button>
      </Input.Group>
    </>
  )
})

export default Selector
