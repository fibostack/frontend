import { Button, Input, Select } from 'antd'
import { listKeypair } from 'api'
import { KeyPair } from 'models'
import CreateModal from 'pages/compute/key_pair/create'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'

interface Props {
  value?: string
  onChange?: (value: string) => void
}

const Selector: React.FC<Props> = React.forwardRef<any, Props>(({ value, onChange }, ref) => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState<boolean>(true)
  const [name, setName] = useState<string | undefined>(value)
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [keyPairs, setKeyPairs] = useState<KeyPair[]>([])

  useEffect(() => {
    fetchKeyPair()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchKeyPair = async () => {
    setLoading(true)
    const kps = (await listKeypair({})) as KeyPair[]
    if (_isMounted.current) {
      setKeyPairs(kps)
      setLoading(false)
    }
  }

  const handleChange = (val: string) => {
    setName(val)
    if (onChange) {
      onChange(val)
    }
  }

  return (
    <>
      {createVisible && (
        <CreateModal
          keyPairs={keyPairs}
          visible={createVisible}
          onOk={(newName: string) => {
            fetchKeyPair()
            handleChange(newName)
            setCreateVisible(false)
          }}
          onCancel={() => {
            setCreateVisible(false)
          }}
        />
      )}
      <Input.Group compact className="flex row">
        <Select
          ref={ref}
          showSearch
          value={name}
          style={{ flex: 1 }}
          onChange={handleChange}
          placeholder="Keypair"
          optionFilterProp="children"
          filterOption={(input, option) =>
            option ? option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 : false
          }
        >
          {keyPairs.map((keyPair) => {
            return (
              <Select.Option key={keyPair.fingerprint} value={keyPair.name}>
                {keyPair.name}
              </Select.Option>
            )
          })}
        </Select>
        <Button
          loading={loading}
          style={{ borderLeft: 'none' }}
          onClick={() => {
            setCreateVisible(true)
          }}
        >
          {intl.formatMessage({ id: 'keypair.create_title' })}
        </Button>
      </Input.Group>
    </>
  )
})

export default Selector
