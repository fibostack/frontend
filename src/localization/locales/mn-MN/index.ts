import Basic from './basic.json'
import Dashboard from './dashboard.json'
import Instance from './instance.json'
import Keypair from './keypair.json'
import SecurityGroup from './security_group.json'
import Snapshot from './snapshot.json'
import Valid from './valid.json'
import Volume from './volume.json'
import Menu from './menu.json'
import Project from './project.json'
import User from './user.json'
import Image from './image.json'
import Network from './network.json'
import Router from './router.json'
import System from './system.json'
import Role from './role.json'
import Permission from './permission.json'
import Credential from './credential.json'
import FloatingIP from './floating_ip.json'
import Billing from './billing.json'

export default {
  ...Basic,
  ...Dashboard,
  ...Instance,
  ...Keypair,
  ...Volume,
  ...Snapshot,
  ...Valid,
  ...SecurityGroup,
  ...Menu,
  ...Project,
  ...User,
  ...Image,
  ...Network,
  ...Router,
  ...System,
  ...Role,
  ...Permission,
  ...Credential,
  ...FloatingIP,
  ...Billing,
}
