import { Col, Row } from 'antd'
import { statisticsHypervisor } from 'api'
import { CircleProgress, PageLayout } from 'components'
import { HypervisorTotal } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { formatMB } from 'utils'

interface Props {}

const Hypervisor: React.FC<Props> = () => {
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [hypervisor, setHypervisor] = useState<HypervisorTotal>()

  useEffect(() => {
    fetchDatas()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchDatas = async () => {
    setLoading(true)
    const hyps = (await statisticsHypervisor({})) as HypervisorTotal
    if (_isMounted.current) {
      setHypervisor(hyps)
      setLoading(false)
    }
  }

  return (
    <PageLayout className="p_t_0" title={<FormattedMessage id="menu.hypervisor" />} loading={loading}>
      {hypervisor && (
        <Row justify="center" gutter={48}>
          <Col>
            <CircleProgress used={hypervisor.count} all={hypervisor.count} title="menu.hypervisor" />
          </Col>
          <Col>
            <CircleProgress used={hypervisor.vcpus_used} all={hypervisor.vcpus} title="total_vcpu" />
          </Col>
          <Col>
            <CircleProgress
              used={hypervisor.memory_mb_used}
              all={hypervisor.memory_mb}
              title="total_ram"
              format={formatMB}
            />
          </Col>
          {/* <Col>
                <CircleProgress
                  used={hypervisor.local_gb_used}
                  all={hypervisor.local_gb}
                  title="total_storage"
                  format={formatGB}
                />
              </Col> */}
          <Col>
            <CircleProgress used={hypervisor.running_vms} all={hypervisor.running_vms} title="instance" />
          </Col>
        </Row>
      )}
    </PageLayout>
  )
}

export default Hypervisor
