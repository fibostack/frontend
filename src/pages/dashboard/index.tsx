/* eslint-disable camelcase */
import { Avatar, Col, Row } from 'antd'
import cssClass from 'classnames'
import { Card, KeyValue } from 'components'
import { ReduxInterface, UserInterface } from 'models'
import React from 'react'
import { FormattedMessage } from 'react-intl'
import { useSelector } from 'react-redux'
import { Hypervisor, Quota } from './components'

const Dashboard: React.FC<any> = () => {
  const { firstname, lastname, email, created_at, role } = useSelector<ReduxInterface, UserInterface>(
    (state: ReduxInterface) => state.UserReducer
  )

  const avatar = (firstname && lastname && firstname.substring(0, 1) + lastname.substring(0, 1)) || 'AU'

  return (
    <Row gutter={24}>
      <Col xs={24} sm={24} xl={6}>
        <Card className="m_b">
          <div className="flex col jc ac m_2">
            <Avatar className={cssClass('noselect')} shape="circle" size={75}>
              {avatar}
            </Avatar>
            <h3>
              {firstname} {lastname}
            </h3>
            <KeyValue title={<FormattedMessage id="role" />} value={role} />
            <KeyValue title={<FormattedMessage id="email" />} value={email} />
            <KeyValue
              title={<FormattedMessage id="create_at" />}
              value={created_at && new Date(created_at).toDateString()}
            />
          </div>
        </Card>
      </Col>
      <Col xs={24} sm={24} xl={18} className="m_b">
        <Hypervisor />
      </Col>
      <Col xs={24} sm={24} xl={24} className="m_b">
        <Quota />
      </Col>
    </Row>
  )
}

export default Dashboard
