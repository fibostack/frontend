import { DetailModal, KeyValue } from 'components'
import { CloseAwaitMS } from 'configs'
import { KeyPair } from 'models'
import React, { useState } from 'react'
import { FormattedMessage } from 'react-intl'

interface KeypairDetailProps {
  visible: boolean
  onOk: Function
  keyPair: KeyPair
}

const KeypairDetail: React.FC<KeypairDetailProps> = ({ visible, onOk, keyPair }) => {
  const [vis, setVis] = useState<boolean>(visible)
  return (
    <DetailModal
      visible={vis}
      title={<FormattedMessage id="keypair.detail_title" values={{ name: keyPair.name }} />}
      okText={<FormattedMessage id="ok" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onOk()
        }, CloseAwaitMS)
      }}
    >
      <KeyValue
        vertical
        title={<FormattedMessage id="keypair.fingerprint" />}
        value={<pre>{keyPair.fingerprint}</pre>}
      />
      <KeyValue vertical title={<FormattedMessage id="keypair.public_key" />} value={<pre>{keyPair.public_key}</pre>} />
    </DetailModal>
  )
}

export default KeypairDetail
