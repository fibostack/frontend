import { Col, Form, notification, Row } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { importKeypair } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { useWindow } from 'hooks'
import { KeyPair } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { calcModalWidth } from 'utils'
import { ImportOutlined } from '@ant-design/icons'

interface KeypairImportProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  keyPairs: KeyPair[]
}

const KeypairImport: React.FC<KeypairImportProps> = ({ visible, onOk, onCancel, keyPairs }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [width] = useWindow()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await importKeypair({ data: values })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${values.keyPairName} keypair is imported.`,
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      width={calcModalWidth(width)}
      visible={vis}
      loading={loading}
      okIcon={<ImportOutlined />}
      title={<FormattedMessage id="keypair.import_title" />}
      formName="keypair_import_form"
      okText={<FormattedMessage id="import" />}
      cancelText={<FormattedMessage id="cancel" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Row gutter={24}>
        <Col xs={24} sm={24} xl={12} className="m_b_2">
          <Form form={form} name="keypair_import_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
            <FormInput
              required
              hasLabel
              name="keyPairName"
              type="input"
              label={<FormattedMessage id="name" />}
              placeholder={intl.formatMessage({ id: 'name' })}
              rules={[
                {
                  validator: async (_rule, value: string) => {
                    if (keyPairs.find((item: KeyPair) => item.name === value)) {
                      throw new Error(intl.formatMessage({ id: 'valid.name_exist' }))
                    } else {
                      Promise.resolve()
                    }
                  },
                },
              ]}
            />
            <FormInput
              required
              hasLabel
              rows={11}
              name="publicKey"
              type="textarea"
              className="form_last_item"
              label={<FormattedMessage id="keypair.public_key" />}
              placeholder={intl.formatMessage({ id: 'keypair.public_key' })}
            />
          </Form>
        </Col>
        <Col xs={24} sm={24} xl={12}>
          <p>
            <FormattedMessage id="keypair.import_info_1" />
          </p>
          <p>
            <FormattedMessage id="keypair.import_info_2" />
          </p>
          <p>
            <FormattedMessage id="keypair.import_info_3" />:
          </p>
          <pre>ssh-keygen -t rsa -f cloud.key</pre>
          <p>
            <FormattedMessage id="keypair.import_info_4" />
          </p>
          <p>
            <FormattedMessage id="keypair.import_info_5" />:
          </p>
          <pre>{`ssh -i cloud.key <username>@<instance_ip>`}</pre>
        </Col>
      </Row>
    </ModalForm>
  )
}

export default KeypairImport
