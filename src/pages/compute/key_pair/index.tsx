import { DeleteOutlined, ImportOutlined } from '@ant-design/icons'
import { Button } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listKeypair } from 'api'
import { PageLayout, Table, Ligther } from 'components'
import { KeyPair, MenuAction } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { findInString } from 'utils'
import CreateModal from './create'
import DeleteModal from './delete'
import DetailModal from './detail'
import ImportModal from './import'

interface KeyPairProps {}

const KeyPairList: React.FC<KeyPairProps> = () => {
  const _isMounted = useRef(true)
  const [keyword, setKeyword] = useState<string>('')
  const [loading, setLoading] = useState<boolean>(true)
  const [keyPairs, setKeyPairs] = useState<Array<KeyPair>>([])
  const [selectedRows, setSelectedRows] = useState<KeyPair[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [deleteVisible, setDeleteVisible] = useState<boolean>(false)
  const [importVisible, setImportVisible] = useState<boolean>(false)
  const [detailVisible, setDetailVisible] = useState<KeyPair>()

  useEffect(() => {
    fetchKeypairs()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchKeypairs = async () => {
    setLoading(true)
    const keypairs = (await listKeypair({})) as KeyPair[]
    if (_isMounted.current) {
      setKeyPairs(keypairs)
      setLoading(false)
    }
  }

  const columns: ColumnType<KeyPair>[] = [
    {
      title: <FormattedMessage id="name" />,
      dataIndex: 'name',
      key: 'name',
      width: 300,
      ellipsis: true,
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (value: string, keypair: KeyPair) => (
        <Button
          type="link"
          onClick={() => {
            setDetailVisible(keypair)
          }}
        >
          <Ligther keywords={[keyword]} source={value} />
        </Button>
      ),
    },
    {
      title: <FormattedMessage id="keypair.fingerprint" />,
      dataIndex: 'fingerprint',
      key: 'fingerprint',
      width: 500,
      ellipsis: true,
      sorter: (a, b) => a.fingerprint.localeCompare(b.fingerprint),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
  ]

  const actions: MenuAction[] = [
    {
      name: <FormattedMessage id="delete" />,
      disabled: selectedRows.length === 0,
      icon: <DeleteOutlined />,
      action: () => {
        setDeleteVisible(true)
      },
    },
  ]

  const importButton = (
    <Button onClick={() => setImportVisible(true)}>
      <ImportOutlined />
      <FormattedMessage id="import" />
    </Button>
  )

  return (
    <PageLayout
      loading={loading}
      title={<FormattedMessage id="keypairs" />}
      fetchAction={fetchKeypairs}
      extraElement={importButton}
      createAction={() => {
        setCreateVisible(true)
      }}
      actions={actions}
    >
      <>
        {createVisible && (
          <CreateModal
            keyPairs={keyPairs}
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchKeypairs()
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {importVisible && (
          <ImportModal
            keyPairs={keyPairs}
            visible={importVisible}
            onOk={() => {
              setImportVisible(false)
              fetchKeypairs()
            }}
            onCancel={() => {
              setImportVisible(false)
            }}
          />
        )}
        {deleteVisible && selectedRows.length > 0 && (
          <DeleteModal
            keyPairs={selectedRows}
            visible={deleteVisible}
            onOk={() => {
              setDeleteVisible(false)
              fetchKeypairs()
            }}
            onCancel={() => {
              setDeleteVisible(false)
            }}
          />
        )}
        {!!detailVisible && (
          <DetailModal
            keyPair={detailVisible}
            visible={!!detailVisible}
            onOk={() => {
              setDetailVisible(undefined)
            }}
          />
        )}
        <Table
          rowKey="fingerprint"
          loading={loading}
          columns={columns}
          onRowSelected={setSelectedRows}
          dataCount={keyPairs.length}
          dataSource={
            keyword
              ? keyPairs.filter(
                  (keyPair) => findInString(keyPair.name, keyword) || findInString(keyPair.fingerprint, keyword)
                )
              : keyPairs
          }
          onSearch={(value) => {
            setKeyword(value)
          }}
        />
      </>
    </PageLayout>
  )
}

export default KeyPairList
