import { Alert, Form, notification } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createKeypair } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { KeyPair } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

interface KeypairCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  keyPairs: KeyPair[]
}

const KeypairCreate: React.FC<KeypairCreateProps> = ({ visible, onOk, onCancel, keyPairs }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await createKeypair({
      data: values,
    })
    setLoading(false)
    if (success) {
      const element = window.document.createElement('a')
      element.setAttribute('href', `data:text/plain;charset=utf-8,${encodeURIComponent(success.private_key)}`)
      element.setAttribute('download', `${success.name}.pem`)
      element.style.display = 'none'
      window.document.body.appendChild(element)
      element.click()
      window.document.body.removeChild(element)
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${values.keyPairName} keypair is created.`,
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.keyPairName)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="keypair.create_title" />}
      formName="keypair_create_form"
      okText={<FormattedMessage id="create" />}
      cancelText={<FormattedMessage id="cancel" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="keypair_create_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <Alert type="warning" style={{ marginBottom: 12 }} message={<FormattedMessage id="keypair.create_warning" />} />
        <FormInput
          required
          hasLabel
          name="keyPairName"
          type="input"
          className="form_last_item"
          label={<FormattedMessage id="name" />}
          placeholder={intl.formatMessage({ id: 'name' })}
          rules={[
            {
              validator: async (_rule, value: string) => {
                if (keyPairs.find((item: KeyPair) => item.name === value)) {
                  throw new Error(intl.formatMessage({ id: 'valid.name_exist' }))
                } else {
                  Promise.resolve()
                }
              },
            },
          ]}
        />
      </Form>
    </ModalForm>
  )
}

export default KeypairCreate
