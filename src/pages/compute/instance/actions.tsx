import { notification } from 'antd'
import {
  offWAFInstance,
  onWAFInstance,
  rebootInstance,
  resumeInstance,
  startInstance,
  stopInstance,
  suspendInstance,
} from 'api'
import { ConfirmModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { Instance, InstanceAction } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

interface InstanceActionsProps {
  actionName: InstanceAction
  visible: boolean
  onOk: Function
  okIcon?: React.ReactNode
  onCancel: Function
  instances: Instance[]
}

const InstanceActions: React.FC<InstanceActionsProps> = ({
  actionName,
  visible,
  onOk,
  okIcon,
  onCancel,
  instances,
}) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [vis, setVis] = useState<boolean>(visible)

  const ids = instances.reduce<string[]>((acc: string[], instance: Instance) => {
    return [...acc, instance.id]
  }, [])

  let names = instances.reduce<string>((acc: string, instance: Instance) => {
    return `${acc}${instance.name}, `
  }, '')
  names = names.substring(0, names.length - 2)

  const onActions = async () => {
    setLoading(true)
    let success = false
    switch (actionName) {
      case 'Soft Reboot':
        success = await rebootInstance({
          data: { server_ids: ids, type: 'soft' },
        })
        break
      case 'Hard Reboot':
        success = await rebootInstance({
          data: { server_ids: ids, type: 'hard' },
        })
        break
      case 'Start':
        success = await startInstance({
          data: { serverID: ids },
        })
        break
      case 'Stop':
        success = await stopInstance({
          data: { serverID: ids },
        })
        break
      case 'Resume':
        success = await resumeInstance({
          data: { serverID: ids },
        })
        break
      case 'Suspend':
        success = await suspendInstance({
          data: { serverID: ids },
        })
        break
      case 'On WAF':
        success = await onWAFInstance({
          data: { serverID: ids[0] },
        })
        break
      case 'Off WAF':
        success = await offWAFInstance({
          data: { serverID: ids[0] },
        })
        break
      default:
        break
    }
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${names} instances is ${actionName.toLowerCase()}.`,
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ConfirmModal
      visible={vis}
      okIcon={okIcon}
      loading={loading}
      okText={actionName}
      selectedNames={names}
      title={`${actionName} Instances`}
      cancelText={<FormattedMessage id="cancel" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onConfirm={() => {
        onActions()
      }}
    />
  )
}

export default InstanceActions
