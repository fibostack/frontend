import { Descriptions, Divider, Form, notification, Select } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { listFlavor, updateInstance } from 'api'
import { FormInput, Loader, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { Flavor, Instance } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

interface InstanceUpdateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  instance: Instance
}

const InstanceUpdate: React.FC<InstanceUpdateProps> = ({ visible, onOk, onCancel, instance }) => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(true)
  const [finishLoading, setFinishLoading] = useState<boolean>(false)
  const [flavors, setFlavors] = useState<Flavor[]>([])

  useEffect(() => {
    fetchData()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchData = async () => {
    setLoading(true)
    const fs = await listFlavor({})
    if (_isMounted.current) {
      if (fs) {
        setFlavors(fs ? (fs.sort((a: Flavor, b: Flavor) => a.ram - b.ram) as Flavor[]) : ([] as Flavor[]))
      }
      setLoading(false)
    }
  }

  const handleFinish = async (values: Store) => {
    setFinishLoading(true)
    values.serverID = instance.id
    const success = await updateInstance({ data: values })
    setFinishLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${instance.name} instance is updated.`,
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.keyPairName)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={finishLoading}
      title={<FormattedMessage id="instance.update_title" values={{ name: instance.name }} />}
      okText={<FormattedMessage id="update" />}
      formName="aws_instance_update_form"
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Loader fill loading={loading}>
        <Descriptions
          size="small"
          layout="horizontal"
          title={<FormattedMessage id="current" />}
          column={{ xs: 2, sm: 2 }}
        >
          <Descriptions.Item
            span={2}
            label={
              <strong>
                <FormattedMessage id="instance.machine_type" />
              </strong>
            }
          >
            {instance.flavor.name || '-'}
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="vcpu" />
              </strong>
            }
          >
            {instance.flavor.cpu || '-'}
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="ram" />
              </strong>
            }
          >
            {`${instance.flavor.ram / 1024}GB` || '-'}
          </Descriptions.Item>
        </Descriptions>
        <Divider />
        <Form
          form={form}
          name="aws_instance_update_form"
          layout="vertical"
          labelAlign="left"
          onFinish={handleFinish}
          initialValues={{ newflavorID: instance.flavor.id }}
        >
          <FormInput required hasLabel label={<FormattedMessage id="instance.machine_type" />} name="newflavorID">
            <Select placeholder={<FormattedMessage id="instance.machine_type" />}>
              {flavors.map((flavor) => (
                <Select.Option key={flavor.name + flavor.id} value={flavor.id}>
                  {flavor.name} (
                  <strong>
                    <FormattedMessage id="vcpu" />:
                  </strong>{' '}
                  {flavor.vcpus}
                  {' | '}
                  <strong>
                    <FormattedMessage id="ram" />:
                  </strong>{' '}
                  {`${(flavor.ram || 0) / 1024}GB`})
                </Select.Option>
              ))}
            </Select>
          </FormInput>
        </Form>
      </Loader>
    </ModalForm>
  )
}

export default InstanceUpdate
