import { EyeOutlined } from '@ant-design/icons'
import { Button } from 'antd'
import { remoteInstance } from 'api'
import { DetailModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { Instance, RemoteConnectionResponse } from 'models'
import React, { useState } from 'react'
import { FormattedMessage } from 'react-intl'

interface ConnectionProps {
  visible: boolean
  onOk: Function
  instance: Instance
}

const Connection = ({ instance, onOk, visible }: ConnectionProps) => {
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleConnect = async () => {
    setLoading(true)
    const response = (await remoteInstance({
      data: { serverID: instance.id },
    })) as RemoteConnectionResponse
    if (response) {
      setLoading(false)
      const realUrl = response.url.split('/')
      window.open(process.env.REACT_APP_REMOTE_URL + realUrl[realUrl.length - 1], '_blank')
    }
  }

  return (
    <DetailModal
      visible={vis}
      title={<FormattedMessage id="instance.connection_title" values={{ name: instance.name }} />}
      okText={<FormattedMessage id="ok" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onOk()
        }, CloseAwaitMS)
      }}
    >
      <>
        <strong>
          <FormattedMessage id="instance.connect_username_and_password" />:
        </strong>
        <br />
        <Button loading={loading} icon={<EyeOutlined />} onClick={handleConnect}>
          <FormattedMessage id="instance.connect_with_console" />
        </Button>
        <br />
        <br />
        {instance.key_name && (
          <>
            <strong>
              <FormattedMessage id="instance.connect_keypair" />:
            </strong>
            <ol>
              <li>
                <FormattedMessage id="instance.connect_keypair_1" />
              </li>
              <li>
                <FormattedMessage id="instance.connect_keypair_2" values={{ name: instance.key_name }} />
              </li>
              <li>
                <FormattedMessage id="instance.connect_keypair_3" />
                <pre>chmod 400 {instance.key_name}.pem </pre>
              </li>
              <li>
                <FormattedMessage id="instance.connect_keypair_4" />
                <pre>
                  {Object.keys(instance.addresses).map((key: string, index: number) => {
                    return (
                      index === 0 &&
                      instance.addresses[key] &&
                      instance.addresses[key].length > 0 &&
                      instance.addresses[key][0].addr
                    )
                  })}
                </pre>
              </li>
            </ol>
            <strong>
              <FormattedMessage id="example" />:
            </strong>
            <pre>
              ssh -i &quot;{instance.key_name}.pem&quot; username@
              {Object.keys(instance.addresses).map((key: string, index: number) => {
                return (
                  index === 0 &&
                  instance.addresses[key] &&
                  instance.addresses[key].length > 0 &&
                  instance.addresses[key][0].addr
                )
              })}
            </pre>
          </>
        )}
      </>
    </DetailModal>
  )
}

export default Connection
