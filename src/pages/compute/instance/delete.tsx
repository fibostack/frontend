import { notification } from 'antd'
import { deleteInstance } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { Instance } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

interface InstanceDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  instances: Instance[]
}

const InstanceDelete: React.FC<InstanceDeleteProps> = ({ visible, onOk, onCancel, instances }) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [vis, setVis] = useState<boolean>(visible)

  const deleteIDs = instances.reduce<string[]>((acc: string[], instance: Instance) => {
    return [...acc, instance.id]
  }, [])

  let deleteNames = instances.reduce<string>((acc: string, instance: Instance) => {
    return `${acc}${instance.name}, `
  }, '')
  deleteNames = deleteNames.substring(0, deleteNames.length - 2)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteInstance({
      data: { serverID: deleteIDs },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${deleteNames} instance is deleted.`,
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="instance.delete_instance" />}
      selectedNames={deleteNames}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default InstanceDelete
