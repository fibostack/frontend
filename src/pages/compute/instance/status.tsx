import {
  CheckCircleTwoTone,
  CloseCircleTwoTone,
  LoadingOutlined,
  PauseCircleTwoTone,
  ShrinkOutlined,
  StopTwoTone,
} from '@ant-design/icons'
import { AvailableStatus, DownStatus } from 'configs'
import { Instance } from 'models'
import React, { useState } from 'react'
import { Tooltip, Popconfirm, Button } from 'antd'
import { FormattedMessage } from 'react-intl'
import { confirmUpdateInstance, revertUpdateInstance } from 'api'

const stateIcons = new Map()
stateIcons.set('SHUTOFF', <StopTwoTone twoToneColor={DownStatus} />)
stateIcons.set('ACTIVE', <CheckCircleTwoTone twoToneColor={AvailableStatus} />)
stateIcons.set('SUSPENDED', <PauseCircleTwoTone twoToneColor={DownStatus} />)
stateIcons.set('ERROR', <CloseCircleTwoTone twoToneColor={DownStatus} />)
stateIcons.set('VERIFY_RESIZE', <ShrinkOutlined />)
stateIcons.set('BUILD', <LoadingOutlined />)
stateIcons.set('REBOOT', <LoadingOutlined />)
stateIcons.set('HARD_REBOOT', <LoadingOutlined />)
stateIcons.set('REVERT_RESIZE', <LoadingOutlined />)
stateIcons.set('STARTING', <LoadingOutlined />)
stateIcons.set('STOPING', <LoadingOutlined />)
stateIcons.set('DELETING', <LoadingOutlined />)
stateIcons.set('SUSPENDING', <LoadingOutlined />)
stateIcons.set('RESUMING', <LoadingOutlined />)
stateIcons.set('RESIZE', <LoadingOutlined />)

const StatusComponent: React.FC<{ instance: Instance; refresh?: Function }> = ({ instance, refresh, children }) => {
  const [loading, setLoading] = useState<boolean>(false)

  const handleResizeConfirm = async () => {
    setLoading(true)
    await confirmUpdateInstance({ data: { serverID: instance.id } })
    setLoading(false)
    if (refresh) refresh()
  }

  const handleResizeRevert = async () => {
    setLoading(true)
    await revertUpdateInstance({ data: { serverID: instance.id } })
    setLoading(false)
    if (refresh) refresh()
  }

  const renderStatus = () => {
    switch (instance.status) {
      case 'VERIFY_RESIZE':
        return (
          <Popconfirm
            placement="bottom"
            title={<FormattedMessage id="instance.verify_resize" />}
            okText={<FormattedMessage id="confirm" />}
            onConfirm={handleResizeConfirm}
            onCancel={handleResizeRevert}
          >
            <Button loading={loading} size="small" type="primary" icon={stateIcons.get(instance.status)}>
              <FormattedMessage id="instance.verify_resize" />
            </Button>
          </Popconfirm>
        )
      case 'ERROR':
        return (
          <Tooltip placement="topLeft" title={instance.fault.message}>
            <span>
              {stateIcons.get(instance.status)} {children || instance.status}
            </span>
          </Tooltip>
        )
      default:
        return (
          <span>
            {stateIcons.get(instance.status)} {children || instance.status}
          </span>
        )
    }
  }

  if (stateIcons.get(instance.status)) {
    return renderStatus()
  }
  return <span>{children || instance.status}</span>
}

export default StatusComponent
