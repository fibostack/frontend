/* eslint-disable camelcase */

import { Form, notification, Select } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { associateFIP, disassociatedFIP, listFloatingIP, listPort } from 'api'
import { FormInput, Loader, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { FloatingIP, Instance, InstanceAddresses, Port } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

interface Props {
  visible: boolean
  onOk: Function
  onCancel: Function
  instance: Instance
}

const Associations = ({ instance, visible, onOk, onCancel }: Props) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const _isMounted = useRef(true)
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(true)
  const [deFloatingIPs, setDeFloatingIPs] = useState<FloatingIP>()
  const [availableFIPs, setAvailableFIPs] = useState<FloatingIP[]>([])
  const [ports, setPorts] = useState<Port[]>([])

  useEffect(() => {
    return () => {
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    fetchFloatingIPs(instance.addresses)
    fetchPorts()
  }, [instance])

  const fetchFloatingIPs = async (addresses: InstanceAddresses) => {
    setLoading(true)
    const flIPs = (await listFloatingIP({})) as FloatingIP[]
    if (_isMounted.current) {
      setAvailableFIPs(flIPs.filter((ip) => ip.status === 'DOWN'))
      setDeFloatingIPs(
        flIPs.find((eachFip) => {
          return (
            Object.keys(addresses).findIndex((key) => {
              return (
                addresses[key].findIndex((padd) => {
                  if (eachFip.floating_ip_address === padd.addr) {
                    return true
                  }
                  return false
                }) >= 0
              )
            }) >= 0
          )
        })
      )
      setLoading(false)
    }
  }

  const fetchPorts = async () => {
    setLoading(true)
    const ps = (await listPort({ data: { network_id: '' } })) as Port[]
    if (_isMounted.current) {
      setPorts(ps)
      setLoading(false)
    }
  }

  const handleAttach = async (values: Store) => {
    setLoading(true)
    const tmpPort = ports.find((eachPort) => {
      if (eachPort.fixed_ips.length !== 0) {
        return (
          Object.keys(instance.addresses).findIndex((key) => {
            return (
              instance.addresses[key].findIndex((padd) => {
                if (eachPort.fixed_ips[0].ip_address === padd.addr) {
                  return true
                }
                return false
              }) >= 0
            )
          }) >= 0
        )
      }
      return false
    })

    if (tmpPort) {
      const success = await associateFIP({
        data: {
          fipID: values.floating_ip,
          portID: tmpPort.id,
        },
      })
      if (success) {
        notification.success({
          message: intl.formatMessage({ id: 'successful' }),
          description: intl.formatMessage({ id: 'instance.floating_ip_associating' }, { name: values.floating_ip }),
        })
        form.resetFields()
        setVis(false)
        setTimeout(() => {
          onOk()
        }, CloseAwaitMS)
      }
    }
    setLoading(false)
  }

  const handleDeattach = async () => {
    setLoading(true)
    if (deFloatingIPs) {
      const success = await disassociatedFIP({
        data: { floating_ip: deFloatingIPs.floating_ip_address },
      })
      if (success) {
        notification.success({
          message: intl.formatMessage({ id: 'successful' }),
          description: intl.formatMessage({ id: 'instance.floating_ip_deassociating' }, { name: deFloatingIPs.id }),
        })
        form.resetFields()
        setVis(false)
        setTimeout(() => {
          onOk()
        }, CloseAwaitMS)
      }
    }
    setLoading(false)
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={
        deFloatingIPs
          ? intl.formatMessage({ id: 'instance.floating_ip_deassociate' })
          : intl.formatMessage({ id: 'instance.floating_ip_associate' })
      }
      formName={deFloatingIPs ? 'floating_ip_deattach_from' : 'floating_ip_attach_from'}
      okText={deFloatingIPs ? intl.formatMessage({ id: 'deassociate' }) : intl.formatMessage({ id: 'associate' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Loader fill loading={loading}>
        {deFloatingIPs ? (
          <Form
            form={form}
            name="floating_ip_deattach_from"
            layout="vertical"
            labelAlign="left"
            onFinish={handleDeattach}
          >
            {intl.formatMessage(
              { id: 'instance.floating_ip_deassociate_description' },
              { ip: deFloatingIPs.floating_ip_address }
            )}
          </Form>
        ) : (
          <Form form={form} name="floating_ip_attach_from" layout="vertical" labelAlign="left" onFinish={handleAttach}>
            <FormInput required hasLabel label={intl.formatMessage({ id: 'instance.ip_addresses' })} name="floating_ip">
              <Select placeholder={<FormattedMessage id="instance.ip_addresses" />}>
                {availableFIPs.map((ip) => {
                  return (
                    <Select.Option key={ip.id} value={ip.id}>
                      {ip.floating_ip_address}: {ip.description}
                    </Select.Option>
                  )
                })}
              </Select>
            </FormInput>
          </Form>
        )}
      </Loader>
    </ModalForm>
  )
}

export default Associations
