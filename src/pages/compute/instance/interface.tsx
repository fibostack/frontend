import { CloseOutlined, PlusCircleOutlined, QuestionCircleOutlined } from '@ant-design/icons'
import { Button, Form, Input, notification, Popconfirm, Select } from 'antd'
import { Store } from 'antd/es/form/interface'
import { attachInterface, detachInterface, listIntanceInterfaces, listNetwork } from 'api'
import { DetailModal, Loader, Table } from 'components'
import { CloseAwaitMS } from 'configs'
import { FixedIp, Instance, InstanceInterface, Network } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { isEmptyString } from 'utils'

interface Props {
  visible: boolean
  instance: Instance
  onOk: Function
  onCancel: Function
}

const AttachInterface: React.FC<Props> = ({ instance, visible, onOk, onCancel }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const _isMounted = useRef(true)
  const [vis, setVis] = useState<boolean>(visible)
  const [interfaces, setInterfaces] = useState<InstanceInterface[]>([])
  const [networks, setNetworks] = useState<Network[]>([])
  const [netLoading, setNetLoading] = useState<boolean>(true)
  const [loading, setLoading] = useState<boolean>(true)

  useEffect(() => {
    fetchNetwork()
    return () => {
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    fetchData(instance.id)
  }, [instance.id])

  const fetchData = async (id: string) => {
    setLoading(true)
    const ifs = await listIntanceInterfaces({ data: { serverID: id } })
    if (_isMounted.current) {
      if (ifs) {
        setInterfaces(ifs as InstanceInterface[])
      }
      setLoading(false)
    }
  }

  const fetchNetwork = async () => {
    setNetLoading(true)
    const nets = await listNetwork({})
    if (_isMounted.current) {
      if (nets) {
        setNetworks(nets as Network[])
      }
      setNetLoading(false)
    }
  }

  const handleAttach = async (values: Store) => {
    setLoading(true)
    const success = await attachInterface({
      data: { serverID: instance.id, networkID: values.networkID },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage(
          { id: 'instance.interface_attaching' },
          {
            instance: instance.name,
          }
        ),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  const handleDetach = async (inter: InstanceInterface) => {
    setLoading(true)
    const success = await detachInterface({
      data: { serverID: instance.id, portID: inter.port_id },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage(
          { id: 'instance.interface_detaching' },
          {
            instance: instance.name,
            interface:
              (inter.fixed_ips.length && inter.fixed_ips.length > 0 && inter.fixed_ips[0].ip_address) || inter.port_id,
          }
        ),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DetailModal
      visible={vis}
      title={
        <FormattedMessage
          id="instance.interface_manage"
          values={{
            name: isEmptyString(instance.name) ? instance.name : instance.id,
          }}
        />
      }
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Loader fill loading={loading}>
        <Table
          rowKey="port_id"
          className="w-fill"
          footer={() => (
            <Form
              form={form}
              name="instance_interface_attach_form"
              layout="vertical"
              labelAlign="left"
              onFinish={handleAttach}
            >
              <Input.Group compact className="flex row">
                <Form.Item
                  name="networkID"
                  style={{ flex: 1 }}
                  rules={[
                    {
                      required: true,
                      message: <FormattedMessage id="valid.required" />,
                    },
                  ]}
                >
                  <Select loading={netLoading} placeholder={intl.formatMessage({ id: 'network' })}>
                    {networks.map((ntw) => {
                      return (
                        <Select.Option key={ntw.id} value={ntw.id}>
                          {ntw.name}
                        </Select.Option>
                      )
                    })}
                  </Select>
                </Form.Item>
                <Popconfirm
                  title={intl.formatMessage({ id: 'instance.interface_attach' })}
                  icon={<QuestionCircleOutlined style={{ color: 'green' }} />}
                  onConfirm={() => {
                    form.submit()
                  }}
                >
                  <Button type="primary" icon={<PlusCircleOutlined />}>
                    <FormattedMessage id="add" />
                  </Button>
                </Popconfirm>
              </Input.Group>
            </Form>
          )}
          size="small"
          pagination={false}
          columns={[
            {
              key: 'index',
              width: '10',
              title: '№',
              render: (_record, _row, index: number) => index + 1,
            },
            {
              width: '90%',
              key: 'fixed-ips',
              dataIndex: 'fixed_ips',
              title: intl.formatMessage({ id: 'address' }),
              render: (record: FixedIp[]) => (record.length && record.length > 0 && record[0].ip_address) || '-',
            },
            {
              key: 'action',
              title: intl.formatMessage({ id: 'action' }),
              render: (record: InstanceInterface) => (
                <Popconfirm
                  title={intl.formatMessage(
                    { id: 'instance.interface_detach' },
                    {
                      interface:
                        (record.fixed_ips.length && record.fixed_ips.length > 0 && record.fixed_ips[0].ip_address) ||
                        record.port_id,
                    }
                  )}
                  placement="topRight"
                  icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                  onConfirm={() => {
                    handleDetach(record)
                  }}
                >
                  <Button size="small" type="primary" className="onlyIcon" icon={<CloseOutlined />} />
                </Popconfirm>
              ),
            },
          ]}
          dataSource={interfaces}
        />
      </Loader>
    </DetailModal>
  )
}

export default AttachInterface
