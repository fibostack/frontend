import {
  ApiOutlined,
  CloseCircleOutlined,
  CompressOutlined,
  DeleteOutlined,
  InteractionOutlined,
  LaptopOutlined,
  LoginOutlined,
  PauseCircleOutlined,
  PlayCircleOutlined,
  ReloadOutlined,
  RetweetOutlined,
  SafetyCertificateFilled,
  SafetyOutlined,
  StopOutlined,
} from '@ant-design/icons'
import { Button, Tooltip } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listInstance } from 'api'
import { Ligther, PageLayout, Table } from 'components'
import { useQuery } from 'hooks'
import { Instance, InstanceAction, InstanceAddresses, InstanceFlavor, MenuAction, Volume } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { Link, useLocation } from 'react-router-dom'
import { findInString, mbToGb } from 'utils'
import ActionModal from './actions'
import VolumeModal from './volume'
import FloatingIPModal from './floating_ip'
import ConnectionModal from './connection'
import InterfaceModal from './interface'
import CreateModal from './create'
import DeleteModal from './delete'
import DetailModal from './detail'
import Status from './status'
import UpdateModal from './update'

interface InstanceProps {}

const InstanceList: React.FC<InstanceProps> = () => {
  const _isMounted = useRef<boolean>(true)
  const _canRedirect = useRef<boolean>(true)
  const id = useQuery(useLocation().search).get('id')
  const [keyword, setKeyword] = useState<string>('')
  const [loading, setLoading] = useState<boolean>(true)
  const [miniLoading, setMiniLoading] = useState<boolean>(true)
  const [instances, setInstances] = useState<Array<Instance>>([])
  const [selectedRows, setSelectedRows] = useState<Instance[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [updateVisible, setUpdateVisible] = useState<Instance>()
  const [deleteVisible, setDeleteVisible] = useState<boolean>(false)
  const [actionVisible, setActionVisible] = useState<InstanceAction>()
  const [connectVisible, setConnectVisible] = useState<Instance>()
  const [detailVisible, setDetailVisible] = useState<Instance>()
  const [volumeVisible, setVolumeVisible] = useState<Instance>()
  const [floatingIPVisible, setFloatingIPVisible] = useState<Instance>()
  const [interfaceVisible, setInterfaceVisible] = useState<Instance>()

  useEffect(() => {
    setLoading(true)
    fetchInstances()
    return () => {
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    if (
      instances.findIndex((item) => {
        if (
          item.status === 'BUILD' ||
          item.status === 'REBOOT' ||
          item.status === 'HARD_REBOOT' ||
          item.status === 'REVERT_RESIZE' ||
          item.status === 'STARTING' ||
          item.status === 'STOPING' ||
          item.status === 'DELETING' ||
          item.status === 'SUSPENDING' ||
          item.status === 'RESUMING' ||
          item.status === 'RESIZE'
        ) {
          return true
        }
        return false
      }, false) >= 0
    ) {
      setTimeout(() => {
        fetchInstances()
      }, 3000)
    }
    const instance = instances.find((item) => item.id === id)
    if (instance && _canRedirect.current) {
      setDetailVisible(instance)
      _canRedirect.current = false
    }
  }, [id, instances])

  const fetchInstances = async () => {
    setMiniLoading(true)
    setSelectedRows([])
    const is = await listInstance({})
    if (_isMounted.current) {
      if (is) {
        setInstances(is as Instance[])
      }
      setLoading(false)
      setMiniLoading(false)
    }
  }

  const columns: ColumnType<Instance>[] = [
    {
      title: () => <FormattedMessage id="name" />,
      key: 'name',
      width: 300,
      ellipsis: true,
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (instance: Instance) => (
        <Button
          type="link"
          onClick={() => {
            setDetailVisible(instance)
          }}
          icon={instance.has_waf && <SafetyCertificateFilled style={{ color: 'green' }} />}
        >
          {keyword ? <Ligther keywords={[keyword]} source={instance.name} /> : instance.name}
        </Button>
      ),
    },
    {
      title: () => <FormattedMessage id="connect" />,
      key: 'connect',
      width: 200,
      ellipsis: true,
      render: (instance: Instance) => (
        <Button
          type="primary"
          size="small"
          icon={<LaptopOutlined />}
          disabled={instance.status !== 'ACTIVE'}
          onClick={() => {
            setConnectVisible(instance)
          }}
        >
          <FormattedMessage id="connect" />
        </Button>
      ),
    },
    {
      title: () => <FormattedMessage id="addresses" />,
      key: 'addresses',
      width: 150,
      ellipsis: true,
      render: (record: Instance) => (
        <div>
          {record.addresses && (
            <ul className="nostyle">
              {Object.keys(record.addresses).map((key) => {
                return record.addresses[key].map((padd) => {
                  return (
                    <li key={padd.addr}>{keyword ? <Ligther keywords={[keyword]} source={padd.addr} /> : padd.addr}</li>
                  )
                })
              })}
            </ul>
          )}
          {record.has_waf && record.waf_addresses && (
            <ul className="nostyle">
              {Object.keys(record.waf_addresses).map((key) => {
                if (record.waf_addresses[key].length > 1) {
                  return record.waf_addresses[key].map((padd) => {
                    return (
                      padd['OS-EXT-IPS:type'] === 'floating' && (
                        <li key={padd.addr}>
                          <SafetyCertificateFilled style={{ color: 'green' }} />{' '}
                          {keyword ? <Ligther keywords={[keyword]} source={padd.addr} /> : padd.addr}
                        </li>
                      )
                    )
                  })
                }
                return record.waf_addresses[key].map((padd) => {
                  return (
                    <li key={padd.addr}>
                      <SafetyCertificateFilled style={{ color: 'green' }} />{' '}
                      {keyword ? <Ligther keywords={[keyword]} source={padd.addr} /> : padd.addr}
                    </li>
                  )
                })
              })}
            </ul>
          )}
        </div>
      ),
    },
    {
      title: () => <FormattedMessage id="status" />,
      key: 'status',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.status.localeCompare(b.status),
      render: (value: Instance) => (
        <Status instance={value} refresh={fetchInstances}>
          {keyword ? <Ligther keywords={[keyword]} source={value.status} /> : value.status}
        </Status>
      ),
    },
    {
      title: () => <FormattedMessage id="flavor" />,
      key: 'flavor',
      dataIndex: 'flavor',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.flavor.name.localeCompare(b.flavor.name),
      render: (flavor: InstanceFlavor) => (
        <Tooltip
          placement="top"
          title={
            <>
              <FormattedMessage id="vcpu" />: {flavor.cpu}, <FormattedMessage id="ram" />: {mbToGb(flavor.ram)}GB
            </>
          }
        >
          <span>{keyword ? <Ligther keywords={[keyword]} source={flavor.name} /> : flavor.name}</span>
        </Tooltip>
      ),
    },
    {
      title: () => <FormattedMessage id="disk" />,
      key: 'volumes',
      dataIndex: 'volumes',
      width: 200,
      ellipsis: true,
      render: (value: Volume[]) =>
        value && value.length > 0 ? (
          <ul className="nostyle">
            {value.map((volume, volIndex: number) => {
              return (
                <li key={volume.size.toString() + volIndex.toString()}>
                  {volume.size}GB{' '}
                  <Link to={`/storage/volumes?id=${volume.id}`}>
                    <strong>{volume.attachments.length > 0 && volume.attachments[0].device}</strong>
                  </Link>
                </li>
              )
            })}
          </ul>
        ) : (
          '-'
        ),
    },
  ]

  const actions: MenuAction[] = [
    {
      key: 'Soft Reboot',
      name: <FormattedMessage id="instance.soft_reboot" />,
      disabled: selectedRows.length === 0 || !!selectedRows.find((i) => i.status !== 'ACTIVE'),
      icon: <ReloadOutlined />,
      action: () => {
        setActionVisible('Soft Reboot')
      },
    },
    {
      key: 'Hard Reboot',
      name: <FormattedMessage id="instance.hard_reboot" />,
      disabled: selectedRows.length === 0 || !!selectedRows.find((i) => i.status !== 'ACTIVE'),
      icon: <RetweetOutlined />,
      action: () => {
        setActionVisible('Hard Reboot')
      },
    },
    {
      key: 'Start',
      name: <FormattedMessage id="instance.start" />,
      disabled: selectedRows.length === 0 || !!selectedRows.find((i) => i.status !== 'SHUTOFF'),
      icon: <PlayCircleOutlined />,
      action: () => {
        setActionVisible('Start')
      },
    },
    {
      key: 'Stop',
      name: <FormattedMessage id="instance.stop" />,
      disabled: selectedRows.length === 0 || !!selectedRows.find((i) => i.status !== 'ACTIVE'),
      icon: <StopOutlined />,
      action: () => {
        setActionVisible('Stop')
      },
    },
    {
      key: 'Resume',
      name: <FormattedMessage id="instance.resume" />,
      disabled: selectedRows.length === 0 || !!selectedRows.find((i) => i.status !== 'SUSPENDED'),
      icon: <LoginOutlined />,
      action: () => {
        setActionVisible('Resume')
      },
    },
    {
      key: 'Suspend',
      name: <FormattedMessage id="instance.suspend" />,
      disabled: selectedRows.length === 0 || !!selectedRows.find((i) => i.status !== 'ACTIVE'),
      icon: <PauseCircleOutlined />,
      action: () => {
        setActionVisible('Suspend')
      },
    },
    {
      key: 'Delete',
      name: <FormattedMessage id="delete" />,
      disabled: selectedRows.length === 0,
      icon: <DeleteOutlined />,
      action: () => {
        setDeleteVisible(true)
      },
    },
    {
      key: 'Volume',
      name: <FormattedMessage id="instance.volume_manage" />,
      disabled: selectedRows.length !== 1 || !!selectedRows.find((i) => i.status === 'ERROR'),
      icon: <ApiOutlined />,
      action: () => {
        setVolumeVisible(selectedRows[0])
      },
    },
    {
      key: 'Floating IP Manage',
      name: <FormattedMessage id="instance.floating_ip_manage" />,
      disabled: selectedRows.length !== 1 || !!selectedRows.find((i) => i.status === 'ERROR'),
      icon: <ApiOutlined />,
      action: () => {
        setFloatingIPVisible(selectedRows[0])
      },
    },
    {
      key: 'Interface Manage',
      name: <FormattedMessage id="instance.interface_manage" />,
      disabled: selectedRows.length !== 1 || !!selectedRows.find((i) => i.status === 'ERROR'),
      icon: <InteractionOutlined />,
      action: () => {
        setInterfaceVisible(selectedRows[0])
      },
    },
    {
      key: 'Resize',
      name: <FormattedMessage id="instance.resize" />,
      disabled: selectedRows.length !== 1 || selectedRows[0].status !== 'SHUTOFF',
      icon: <CompressOutlined />,
      action: () => {
        setUpdateVisible(selectedRows[0])
      },
    },
    selectedRows.length === 1 && selectedRows[0].has_waf
      ? {
          key: 'Off WAF',
          name: <FormattedMessage id="instance.off_waf" />,
          disabled: selectedRows.length !== 1 || !!selectedRows.find((i) => i.status === 'ERROR'),
          icon: <CloseCircleOutlined />,
          action: () => {
            setActionVisible('Off WAF')
          },
        }
      : {
          key: 'On WAF',
          name: <FormattedMessage id="instance.on_waf" />,
          disabled: selectedRows.length !== 1 || !!selectedRows.find((i) => i.status === 'ERROR'),
          icon: <SafetyOutlined />,
          action: () => {
            setActionVisible('On WAF')
          },
        },
  ]

  return (
    <PageLayout
      loading={miniLoading}
      title={<FormattedMessage id="instances" />}
      fetchAction={fetchInstances}
      createAction={() => {
        setCreateVisible(true)
      }}
      actions={actions}
    >
      <>
        {createVisible && (
          <CreateModal
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchInstances()
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {!!updateVisible && (
          <UpdateModal
            instance={updateVisible}
            visible={!!updateVisible}
            onOk={() => {
              setUpdateVisible(undefined)
              fetchInstances()
            }}
            onCancel={() => {
              setUpdateVisible(undefined)
            }}
          />
        )}
        {!!connectVisible && (
          <ConnectionModal
            visible={!!connectVisible}
            instance={connectVisible}
            onOk={() => {
              setConnectVisible(undefined)
            }}
          />
        )}
        {!!detailVisible && (
          <DetailModal
            visible={!!detailVisible}
            instance={detailVisible}
            onOk={() => {
              setDetailVisible(undefined)
            }}
          />
        )}
        {!!actionVisible && selectedRows.length > 0 && (
          <ActionModal
            actionName={actionVisible}
            instances={selectedRows}
            okIcon={actions.find((action) => action.key === actionVisible)?.icon}
            visible={!!actionVisible}
            onOk={() => {
              setActionVisible(undefined)
              fetchInstances()
            }}
            onCancel={() => {
              setActionVisible(undefined)
            }}
          />
        )}
        {!!volumeVisible && selectedRows.length === 1 && (
          <VolumeModal
            instance={volumeVisible}
            visible={!!volumeVisible}
            onOk={() => {
              setVolumeVisible(undefined)
              fetchInstances()
            }}
          />
        )}
        {!!floatingIPVisible && selectedRows.length === 1 && (
          <FloatingIPModal
            instance={floatingIPVisible}
            visible={!!floatingIPVisible}
            onOk={() => {
              setFloatingIPVisible(undefined)
              fetchInstances()
            }}
            onCancel={() => {
              setFloatingIPVisible(undefined)
            }}
          />
        )}
        {!!interfaceVisible && selectedRows.length === 1 && (
          <InterfaceModal
            instance={interfaceVisible}
            visible={!!interfaceVisible}
            onOk={() => {
              setInterfaceVisible(undefined)
              fetchInstances()
            }}
            onCancel={() => {
              setInterfaceVisible(undefined)
            }}
          />
        )}
        {deleteVisible && selectedRows.length > 0 && (
          <DeleteModal
            instances={selectedRows}
            visible={deleteVisible}
            onOk={() => {
              setDeleteVisible(false)
              fetchInstances()
            }}
            onCancel={() => {
              setDeleteVisible(false)
            }}
          />
        )}
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          onRowSelected={setSelectedRows}
          dataCount={instances.length}
          dataSource={
            instances
              ? instances.filter(
                  (instance) =>
                    findInString(instance.name, keyword) ||
                    findInString(instance.status, keyword) ||
                    findInString(instance.flavor.name, keyword) ||
                    !!Object.keys(instance.addresses).find((key) => {
                      return !!instance.addresses[key].find((padd) => {
                        return findInString(padd.addr, keyword)
                      })
                    })
                )
              : instances
          }
          onSearch={(value) => {
            setKeyword(value)
          }}
        />
      </>
    </PageLayout>
  )
}

export default InstanceList
