import React, { useEffect, useState, useRef } from 'react'
import { Instance } from 'models'
import { consoleLogInstance } from 'api'
import { Form, Select, Row, Col } from 'antd'
import { Loader } from 'components'
import { useIntl } from 'react-intl'

interface Props {
  instance: Instance
}

interface ConsoleLogResponse {
  output: string
}

const lengths = [25, 50, 75, 100, 150, 200]

const ConsoleLog = ({ instance }: Props) => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [logs, setLogs] = useState<string[]>([])
  const [loading, setLoading] = useState<boolean>(true)

  useEffect(() => {
    return () => {
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    fetchConsoleLog(instance.id)
  }, [instance.id])

  const fetchConsoleLog = async (instanceID: string, length: number = 35) => {
    setLoading(true)
    const consoleLogs = (await consoleLogInstance({
      data: { serverID: instanceID, length },
    })) as ConsoleLogResponse
    if (_isMounted.current) {
      if (consoleLogs) {
        setLogs(consoleLogs.output.split(/\r?\n/))
      }
      setLoading(false)
    }
  }

  return (
    <div>
      <Row>
        <Col>{intl.formatMessage({ id: 'instance.line_count' })}</Col>
        <Col>
          <Select
            defaultValue={35}
            onChange={(value) => {
              fetchConsoleLog(instance.id, value)
            }}
          >
            {lengths.map((length) => (
              <Select.Option key={length} value={length}>
                {length}
              </Select.Option>
            ))}
          </Select>
        </Col>
      </Row>

      <Loader loading={loading}>
        <pre>
          {logs.map((log, index) => (
            <>
              <strong>{index + 1}: </strong>
              {` ${log.trim()}\n`}
            </>
          ))}
        </pre>
      </Loader>
    </div>
  )
}

export default ConsoleLog
