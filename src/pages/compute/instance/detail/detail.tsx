import React from 'react'
import { KeyValue } from 'components'
import { FormattedMessage } from 'react-intl'
import { DateFormat, mbToGb } from 'utils'
import { Divider } from 'antd'
import { Link } from 'react-router-dom'
import { Instance } from 'models'
import Status from '../status'

interface Props {
  instance: Instance
}

const Detail = ({ instance }: Props) => {
  return (
    <>
      <KeyValue title={<FormattedMessage id="id" />} value={instance.id} />
      <KeyValue title={<FormattedMessage id="name" />} value={instance.name} />
      <KeyValue title={<FormattedMessage id="status" />} value={<Status instance={instance} />} />
      <KeyValue title={<FormattedMessage id="updated" />} value={DateFormat(instance.updated)} />
      <Divider>
        <FormattedMessage id="status" />
      </Divider>
      <KeyValue title={<FormattedMessage id="instance.flavor_id" />} value={instance.flavor.id} />
      <KeyValue title={<FormattedMessage id="instance.flavor_name" />} value={instance.flavor.name} />
      <KeyValue title={<FormattedMessage id="vcpu" />} value={`${instance.flavor.cpu} vCPU`} />
      <KeyValue title={<FormattedMessage id="ram" />} value={`${mbToGb(instance.flavor.ram)} GB`} />
      <KeyValue
        title={<FormattedMessage id="disk" />}
        value={`${instance.volumes?.reduce<number>((acc, volume) => acc + volume.size, 0)} GB`}
      />
      <Divider>
        <FormattedMessage id="instance.ip_addresses" />
      </Divider>
      {Object.keys(instance.addresses).map((keyName: string) => (
        <KeyValue
          key={keyName + 1}
          title={keyName}
          value={instance.addresses[keyName].map((network) => {
            return (
              <p className="mb-0" key={network.addr}>
                {network.addr}
              </p>
            )
          })}
        />
      ))}
      <Divider>
        <FormattedMessage id="metadata" />
      </Divider>
      <KeyValue title={<FormattedMessage id="keypair" />} value={instance.key_name} />
      <Divider>
        <FormattedMessage id="instance.attached_volumes" />
      </Divider>
      {instance.volumes?.map((volume) => {
        return (
          <KeyValue
            key={volume.id + volume.name}
            title={<FormattedMessage id="instance.attached_to" />}
            value={volume.attachments.map((attachment) => {
              return (
                <span key={attachment.id}>
                  <Link to={`/storage/volumes/?id=${attachment.id}`}>{attachment.id}</Link> on {attachment.device}
                </span>
              )
            })}
          />
        )
      })}
    </>
  )
}

export default Detail
