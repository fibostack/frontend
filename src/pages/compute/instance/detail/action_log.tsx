/* eslint-disable camelcase */
import React, { useEffect, useState, useRef } from 'react'
import { Instance } from 'models'
import { actionLogInstance } from 'api'
import { Table, Ligther } from 'components'
import { ColumnType } from 'antd/lib/table'
import { useIntl } from 'react-intl'
import { DateFormat, findInString, dateSort } from 'utils'

interface Props {
  instance: Instance
}

export interface ActionLogResponse {
  action: string
  instance_uuid: string
  message: string
  project_id: string
  request_id: string
  user_id: string
  start_time: Date
}

const ActionLog = ({ instance }: Props) => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [keyword, setKeyword] = useState<string>('')
  const [logs, setLogs] = useState<ActionLogResponse[]>([])
  const [loading, setLoading] = useState<boolean>(true)

  useEffect(() => {
    const fetchActionLog = async () => {
      setLoading(true)
      const actionLogs = (await actionLogInstance({
        data: { serverID: instance.id },
      })) as ActionLogResponse[]
      if (_isMounted.current) {
        setLogs(actionLogs)
        setLoading(false)
      }
    }

    fetchActionLog()
    return () => {
      _isMounted.current = false
    }
  }, [instance])

  const columns: ColumnType<ActionLogResponse>[] = [
    {
      width: 200,
      title: intl.formatMessage({ id: 'instance.action' }),
      key: 'action',
      dataIndex: 'action',
      ellipsis: true,
      sorter: (a, b) => a.action.localeCompare(b.action),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      width: 200,
      title: intl.formatMessage({ id: 'instance.start_time' }),
      key: 'start_time',
      dataIndex: 'start_time',
      ellipsis: true,
      sorter: (a, b) => dateSort(a.start_time, b.start_time),
      render: (value: Date) => {
        const tmpVal = DateFormat(value)
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
    {
      title: intl.formatMessage({ id: 'instance.message' }),
      key: 'message',
      dataIndex: 'message',
      width: 300,
      ellipsis: true,
      sorter: (a, b) => a.message.localeCompare(b.message),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
  ]

  return (
    <Table
      rowKey="request_id"
      loading={loading}
      columns={columns}
      dataCount={logs.length}
      dataSource={
        logs
          ? logs.filter(
              (log) =>
                findInString(log.action, keyword) ||
                findInString(DateFormat(log.start_time), keyword) ||
                findInString(log.message, keyword)
            )
          : logs
      }
      onSearch={(value) => {
        setKeyword(value)
      }}
    />
  )
}

export default ActionLog
