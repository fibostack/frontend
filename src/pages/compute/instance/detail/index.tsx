import { CodeOutlined, CoffeeOutlined, ReadOutlined } from '@ant-design/icons'
import { Tabs } from 'antd'
import { DetailModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { useWindow } from 'hooks'
import { Instance } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { calcModalWidth, isEmptyString } from 'utils'
import ActionLog from './action_log'
import ConsoleLog from './console_log'
import Detail from './detail'

interface InstanceDetailProps {
  visible: boolean
  onOk: Function
  instance: Instance
}

const InstanceDetail: React.FC<InstanceDetailProps> = ({ visible, onOk, instance }) => {
  const intl = useIntl()
  const [width] = useWindow()
  const [vis, setVis] = useState<boolean>(visible)

  return (
    <DetailModal
      visible={vis}
      width={calcModalWidth(width)}
      title={
        <FormattedMessage
          id="instance.detail_title"
          values={{
            name: isEmptyString(instance.name) ? instance.name : instance.id,
          }}
        />
      }
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onOk()
        }, CloseAwaitMS)
      }}
    >
      <Tabs defaultActiveKey="detail" style={{ marginTop: -24 }}>
        <Tabs.TabPane
          tab={
            <>
              <ReadOutlined />
              {intl.formatMessage({ id: 'detail' })}
            </>
          }
          key="detail"
          className="p_t_1"
        >
          <Detail instance={instance} />
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <>
              <CodeOutlined />
              {intl.formatMessage({ id: 'instance.console_log' })}
            </>
          }
          key="console_log"
          className="p_t_1"
        >
          <ConsoleLog instance={instance} />
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <>
              <CoffeeOutlined />
              {intl.formatMessage({ id: 'instance.action_log' })}
            </>
          }
          key="action_log"
          className="p_t_1"
        >
          <ActionLog instance={instance} />
        </Tabs.TabPane>
      </Tabs>
    </DetailModal>
  )
}

export default InstanceDetail
