import { Alert, Col, Divider, Form, notification, Radio, Row, Select, Switch } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createInstance, listAvailabilityZones, listFlavor, listImage, listNetwork, listSecurityGroup } from 'api'
import cssClass from 'classnames'
import { FormInput, KeypairSelector, KeyValue, Loader, ModalForm, OSImage, SizeInput } from 'components'
import { CloseAwaitMS, UsernameBlackList } from 'configs'
import { useWindow } from 'hooks'
import { AvailabilityZone, Flavor, Image, ImageListResponse, Network, SecurityGroup } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { mbToGb } from 'utils'
import styles from './styles.module.scss'

interface InstanceCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
}

interface FormTitleProps {
  title: React.ReactNode
  switchName?: string
  switchTitle?: React.ReactNode
}

const FormTitle = ({ title, switchName, switchTitle }: FormTitleProps) => {
  return (
    <Divider>
      <span className="flex row jc ac">
        <span className="m_r_1">{title}</span>
        {switchName && (
          <>
            <Form.Item name={switchName} className="form_last_item" valuePropName="checked">
              <Switch />
            </Form.Item>
            <span style={{ fontSize: 12, fontWeight: 800, marginLeft: 4 }}>{switchTitle}</span>
          </>
        )}
      </span>
    </Divider>
  )
}

const InstanceCreate: React.FC<InstanceCreateProps> = ({ visible, onOk, onCancel }) => {
  const intl = useIntl()
  const [width] = useWindow()
  const _isMounted = useRef(true)
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(true)
  const [finishLoading, setFinishLoading] = useState<boolean>(false)
  const [images, setImages] = useState<Image[]>([])
  const [flavors, setFlavors] = useState<Flavor[]>([])
  const [networks, setNetworks] = useState<Network[]>([])
  const [securityGroups, setSecurityGroups] = useState<SecurityGroup[]>([])
  const [availabilityZones, setAvailabilityZones] = useState<AvailabilityZone[]>([])
  const [selectedImage, setSelectedImage] = useState<Image>()

  useEffect(() => {
    fetchData()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchData = async () => {
    setLoading(true)
    const [flavorResponse, networkResponse, SGResponse, azResponse, imageResponse] = await Promise.all([
      listFlavor({}),
      listNetwork({}),
      listSecurityGroup({}),
      listAvailabilityZones({}),
      listImage({}),
    ])
    const fs = flavorResponse as Flavor[]
    const ns = networkResponse as Network[]
    const sgs = SGResponse as SecurityGroup[]
    const azs = azResponse as AvailabilityZone[]
    const imageListResponse = imageResponse as ImageListResponse
    if (_isMounted.current) {
      if (ns) setNetworks(ns)
      if (sgs) setSecurityGroups(sgs)
      if (azs) setAvailabilityZones(azs)
      if (imageListResponse)
        setImages(imageListResponse.publicImages?.sort((a, b) => a.name.localeCompare(b.name)) || [])
      if (fs) setFlavors(fs.sort((a, b) => a.ram - b.ram))
      setLoading(false)
    }
  }

  const handleFinish = async (values: Store) => {
    setFinishLoading(true)
    values.script_id = ''
    values.custom_flavor = values.custom_flavor || {}
    values.flavor_id = values.flavor_id || ''
    values.availability_zone = availabilityZones.length > 0 ? availabilityZones[0].zoneName : ''
    values.network_id = values.network_id || ''
    values.keypair_name = values.keypair_name || ''
    values.username = values.username || ''
    values.password = values.password || ''
    const success = await createInstance({ data: values })
    setFinishLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${values.name} instance is created.`,
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.keyPairName)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      width={width > 1200 ? '80%' : '100%'}
      visible={vis}
      loading={finishLoading}
      title={<FormattedMessage id="instance.create_title" />}
      formName="instance_create_form"
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Loader fill loading={loading}>
        <Form
          form={form}
          name="instance_create_form"
          layout="vertical"
          labelAlign="left"
          onFinish={handleFinish}
          initialValues={{
            is_custom: false,
            is_password: false,
            // delete_on_termination: true,
            auto_assign_ip: true,
          }}
        >
          <Row gutter={32}>
            <Col xs={24} sm={24} md={12}>
              <FormTitle title={<FormattedMessage id="instance.image_type" />} />
              <Form.Item
                name="image_id"
                rules={[
                  {
                    required: true,
                    message: <FormattedMessage id="valid.required" />,
                  },
                ]}
              >
                <Radio.Group
                  buttonStyle="solid"
                  onChange={(value) => {
                    const imageID = value.target.value as string
                    const image = images.find((item) => item.id === imageID)
                    setSelectedImage(image)
                  }}
                >
                  <Row gutter={12}>
                    {images.map((image) => {
                      return (
                        <Col key={image.name + image.id} className="m_b_1">
                          <Radio.Button key={image.name + image.id} value={image.id} className="h-fill">
                            <span className="flex col jc ac p_1">
                              <OSImage name={image.name} />
                              <strong className={cssClass('noselect', styles.imageName)}>{image.name}</strong>
                            </span>
                          </Radio.Button>
                        </Col>
                      )
                    })}
                  </Row>
                </Radio.Group>
              </Form.Item>
              <FormTitle
                title={<FormattedMessage id="instance.machine_type" />}
                switchName="is_custom"
                switchTitle={<FormattedMessage id="custom" />}
              />
              <Form.Item
                required
                noStyle
                shouldUpdate={(oldValue, newValue) => oldValue.is_custom !== newValue.is_custom}
              >
                {({ getFieldValue }) => {
                  const custom = getFieldValue('is_custom') as boolean
                  return custom ? (
                    <>
                      <Form.Item
                        name={['custom_flavor', 'vcpu']}
                        label={<FormattedMessage id="instance.virtual_cpu" />}
                        rules={[
                          {
                            required: true,
                            message: <FormattedMessage id="valid.required" />,
                          },
                        ]}
                      >
                        <SizeInput
                          dots
                          suffix="vCPU"
                          min={(selectedImage && Math.ceil(selectedImage.min_ram / 2)) || 1}
                          max={20}
                        />
                      </Form.Item>
                      <Form.Item
                        name={['custom_flavor', 'ram']}
                        label={<FormattedMessage id="ram" />}
                        rules={[
                          {
                            required: true,
                            message: <FormattedMessage id="valid.required" />,
                          },
                        ]}
                      >
                        <SizeInput dots min={(selectedImage && selectedImage.min_ram) || 1} max={50} />
                      </Form.Item>
                      <Alert type="warning" message={<FormattedMessage id="instance.custom_warning" />} />
                    </>
                  ) : (
                    <Form.Item
                      name="flavor_id"
                      rules={[
                        {
                          required: true,
                          message: <FormattedMessage id="valid.required" />,
                        },
                      ]}
                    >
                      <Radio.Group buttonStyle="solid">
                        <Row gutter={12}>
                          {flavors.map((flavor) => {
                            return (
                              <Col key={flavor.name + flavor.id} className="m_b_1">
                                <Radio.Button
                                  disabled={
                                    selectedImage &&
                                    selectedImage.name.toLowerCase().indexOf('windows') >= 0 &&
                                    flavor.ram < selectedImage.min_ram * 3
                                  }
                                  key={flavor.name + flavor.id}
                                  value={flavor.id}
                                  className="h-fill"
                                >
                                  <span className="flex col jc ac p_1">
                                    <strong className={cssClass('noselect', styles.flavorName)}>{flavor.name}</strong>
                                    <Divider className="m_1" />
                                    <KeyValue
                                      title={<FormattedMessage id="vcpu" />}
                                      value={flavor.vcpus.toString()}
                                      className="lh_0"
                                    />
                                    <KeyValue
                                      title={<FormattedMessage id="ram" />}
                                      value={`${mbToGb(flavor.ram)}GB`}
                                      className="lh_0"
                                    />
                                  </span>
                                </Radio.Button>
                              </Col>
                            )
                          })}
                        </Row>
                      </Radio.Group>
                    </Form.Item>
                  )
                }}
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={12}>
              <FormTitle title={<FormattedMessage id="instance.instance_name" />} />
              <FormInput
                required
                name="name"
                type="input"
                label={intl.formatMessage({ id: 'instance.instance_name' })}
                placeholder={intl.formatMessage({ id: 'instance.instance_name' })}
                rules={[
                  {
                    validator: async (_rules, value) => {
                      if (value && UsernameBlackList.includes(value.toLowerCase())) {
                        throw Error(intl.formatMessage({ id: 'valid.instance_name' }, { value }))
                      }
                      return Promise.resolve()
                    },
                  },
                ]}
              />
              <FormTitle
                title={<FormattedMessage id="instance.authentication" />}
                switchName="is_password"
                switchTitle={<FormattedMessage id="keypair" />}
              />
              <Form.Item
                required
                noStyle
                shouldUpdate={(oldValue, newValue) => oldValue.is_password !== newValue.is_password}
              >
                {({ getFieldValue }) => {
                  const isPassword = !getFieldValue('is_password') as boolean
                  return isPassword ? (
                    <Row gutter={12}>
                      <Col xs={24} sm={24} md={12}>
                        <FormInput
                          required
                          hasLabel
                          name="username"
                          type="input"
                          label={intl.formatMessage({ id: 'username' })}
                          placeholder={intl.formatMessage({ id: 'username' })}
                        />
                      </Col>
                      <Col xs={24} sm={24} md={12}>
                        <FormInput
                          required
                          hasLabel
                          name="password"
                          type="password"
                          label={intl.formatMessage({ id: 'password' })}
                          placeholder={intl.formatMessage({ id: 'password' })}
                        />
                      </Col>
                    </Row>
                  ) : (
                    <FormInput required name="keypair_name" label="keypair_name">
                      <KeypairSelector />
                    </FormInput>
                  )
                }}
              </Form.Item>
              <FormTitle
                title={<FormattedMessage id="volume_size" />}
                // switchName="delete_on_termination"
                // switchTitle={<FormattedMessage id="instance.delete_on_termination" />}
              />
              <FormInput required name="disk_size" label="disk_size">
                <SizeInput min={(selectedImage && selectedImage.min_disk) || 10} max={1024} />
              </FormInput>
              <FormTitle
                title={<FormattedMessage id="network" />}
                switchName="auto_assign_ip"
                switchTitle={<FormattedMessage id="instance.auto_assign_ip" />}
              />
              <Form.Item
                required
                noStyle
                shouldUpdate={(oldValue, newValue) => oldValue.auto_assign_ip !== newValue.auto_assign_ip}
              >
                {({ getFieldValue }) => {
                  const autoAssign = getFieldValue('auto_assign_ip') as boolean
                  return (
                    !autoAssign && (
                      <FormInput required name="network_id" label="network_id">
                        <Select placeholder={<FormattedMessage id="network" />}>
                          {networks.map((network) => {
                            return (
                              <Select.Option key={network.name + network.id} value={network.id}>
                                {network.name}
                              </Select.Option>
                            )
                          })}
                        </Select>
                      </FormInput>
                    )
                  )
                }}
              </Form.Item>
              <FormTitle title={<FormattedMessage id="instance.security_groups" />} />
              <FormInput required name="secgroup_ids" label="secgroup_ids">
                <Select mode="multiple" placeholder={<FormattedMessage id="instance.security_groups" />}>
                  {securityGroups.map((securitGroup) => {
                    return (
                      <Select.Option key={securitGroup.name + securitGroup.ID} value={securitGroup.ID}>
                        {securitGroup.name}
                      </Select.Option>
                    )
                  })}
                </Select>
              </FormInput>
            </Col>
          </Row>
        </Form>
      </Loader>
    </ModalForm>
  )
}

export default InstanceCreate
