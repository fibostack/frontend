import { MinusOutlined, PlusOutlined } from '@ant-design/icons'
import { Button, notification } from 'antd'
import { attachVolume, detachVolume, listVolume } from 'api'
import { DetailModal, Loader, Table } from 'components'
import { CloseAwaitMS } from 'configs'
import { Instance, Volume } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { isEmptyString } from 'utils'

interface Props {
  visible: boolean
  onOk: Function
  instance: Instance
}

const AttachVolume: React.FC<Props> = ({ instance, visible, onOk }) => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [vis, setVis] = useState<boolean>(visible)
  const [attachedVolumes, setAttachedVolumes] = useState<Volume[]>([])
  const [availableVolumes, setAvailableVolumes] = useState<Volume[]>([])
  const [loading, setLoading] = useState<boolean>(true)

  useEffect(() => {
    return () => {
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    fetchVolumes(instance.id)
  }, [instance.id])

  const fetchVolumes = async (id: string) => {
    setLoading(true)
    const volumes = (await listVolume({})) as Volume[]
    if (_isMounted.current) {
      setAttachedVolumes(
        volumes.filter(
          (volume) =>
            volume.attachments.length > 0 && volume.attachments[0].server_id === id && volume.bootable === 'false'
        )
      )
      setAvailableVolumes(
        volumes.filter(
          (volume) => volume.attachments.length === 0 && volume.bootable === 'false' && volume.status === 'available'
        )
      )
      setLoading(false)
    }
  }

  const handleAttach = async (volume: Volume) => {
    setLoading(true)
    const success = await attachVolume({
      data: { serverID: instance.id, volumeID: volume.id },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage(
          { id: 'instance.attaching_message' },
          { volumeName: volume.name || volume.id, instanceName: instance.name }
        ),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  const handleDetach = async (volume: Volume) => {
    setLoading(true)
    const success = await detachVolume({
      data: { serverID: instance.id, attachmentID: volume.id },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage(
          { id: 'instance.detaching_message' },
          { volumeName: volume.name || volume.id, instanceName: instance.name }
        ),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DetailModal
      visible={vis}
      title={
        <FormattedMessage
          id="instance.volume_manage"
          values={{
            name: isEmptyString(instance.name) ? instance.name : instance.id,
          }}
        />
      }
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onOk()
        }, CloseAwaitMS)
      }}
    >
      <Loader fill loading={loading}>
        <div className="flex col jc ac">
          <Table
            rowKey="id"
            className="w-fill"
            title={() => (
              <h3>
                <strong>
                  <FormattedMessage id="instance.attached_volumes" />
                </strong>
              </h3>
            )}
            size="small"
            showHeader={false}
            pagination={false}
            columns={[
              {
                key: 'index',
                width: '10',
                render: (_record, _row, index: number) => index + 1,
              },
              {
                key: 'name',
                width: '30%',
                render: (record: Volume) => record.name || record.id,
              },
              {
                key: 'size',
                width: '20%',
                dataIndex: 'size',
                render: (size) => `${size} GB`,
              },
              {
                key: 'attachments',
                width: '30%',
                dataIndex: 'attachments',
                render: (attachments) => attachments.map((attachment: any) => attachment.device),
              },
              {
                key: 'action',
                render: (record: Volume) => (
                  <Button
                    size="small"
                    type="primary"
                    className="onlyIcon"
                    icon={<MinusOutlined />}
                    onClick={() => {
                      handleDetach(record)
                    }}
                  />
                ),
              },
            ]}
            dataSource={attachedVolumes}
          />
          <Table
            rowKey="id"
            className="w-fill m_t_2"
            title={() => (
              <h3>
                <strong>
                  <FormattedMessage id="instance.available_volumes" />
                </strong>
              </h3>
            )}
            size="small"
            showHeader={false}
            pagination={false}
            columns={[
              {
                key: 'index',
                width: '10',
                render: (_record, _row, index: number) => index + 1,
              },
              {
                key: 'name',
                width: '30%',
                render: (record: Volume) => record.name || record.id,
              },
              {
                key: 'size',
                width: '50%',
                dataIndex: 'size',
                render: (size) => `${size} GB`,
              },
              {
                key: 'action',
                render: (record: Volume) => (
                  <Button
                    size="small"
                    type="primary"
                    className="onlyIcon"
                    icon={<PlusOutlined />}
                    onClick={() => {
                      handleAttach(record)
                    }}
                  />
                ),
              },
            ]}
            dataSource={availableVolumes}
          />
        </div>
      </Loader>
    </DetailModal>
  )
}

export default AttachVolume
