import { Form, notification, Radio, Row, Col } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createFlavor } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface FlavorCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
}

const FlavorCreate: React.FC<FlavorCreateProps> = ({ visible, onOk, onCancel }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await createFlavor({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'flavor.created' }, { name: values.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.name)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'flavor.create_title' })}
      formName="flavor_create_form"
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form
        form={form}
        name="flavor_create_form"
        layout="vertical"
        labelAlign="left"
        onFinish={handleFinish}
        initialValues={{ is_public: false }}
      >
        <FormInput
          required
          hasLabel
          name="name"
          label={intl.formatMessage({ id: 'name' })}
          type="input"
          placeholder={intl.formatMessage({ id: 'name' })}
        />
        <Row gutter={12}>
          <Col xs={24} sm={24} md={12}>
            <FormInput
              required
              hasLabel
              min={0}
              name="ram"
              className="w-fill"
              label={intl.formatMessage({ id: 'ram' })}
              type="number"
              placeholder={intl.formatMessage({ id: 'ram' })}
            />
          </Col>
          <Col xs={24} sm={24} md={12}>
            <FormInput
              required
              hasLabel
              min={0}
              name="vcpu"
              className="w-fill"
              label={intl.formatMessage({ id: 'vcpu' })}
              placeholder={intl.formatMessage({ id: 'vcpu' })}
              type="number"
            />
          </Col>
        </Row>
        <FormInput required hasLabel name="is_public" label={intl.formatMessage({ id: 'public' })}>
          <Radio.Group buttonStyle="solid">
            <Radio.Button value>{intl.formatMessage({ id: 'yes' })}</Radio.Button>
            <Radio.Button value={false}>{intl.formatMessage({ id: 'no' })}</Radio.Button>
          </Radio.Group>
        </FormInput>
      </Form>
    </ModalForm>
  )
}

export default FlavorCreate
