import { DeleteOutlined } from '@ant-design/icons'
import { ColumnType } from 'antd/es/table'
import { listFlavor } from 'api'
import { Ligther, PageLayout, Table } from 'components'
import { Flavor, MenuAction } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import { findInString, formatMB } from 'utils'
import CreateModal from './create'
import DeleteModal from './delete'

interface FlavorProps {}

const FlavorList: React.FC<FlavorProps> = () => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [flavors, setFlavors] = useState<Flavor[]>([])
  const [keyword, setKeyword] = useState<string>('')
  const [selectedRows, setSelectedRows] = useState<Flavor[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  // const [updateVisible, setUpdateVisible] = useState<Flavor>()
  const [deleteVisible, setDeleteVisible] = useState<boolean>(false)

  useEffect(() => {
    fetchFlavors()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchFlavors = async () => {
    setLoading(true)
    const pers = (await listFlavor({})) as Flavor[]
    if (_isMounted.current) {
      setFlavors(pers.sort((a, b) => a.ram - b.ram))
      setLoading(false)
    }
  }

  const columns: ColumnType<Flavor>[] = [
    {
      title: intl.formatMessage({ id: 'name' }),
      key: 'name',
      width: 200,
      ellipsis: true,
      dataIndex: 'name',
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (value: string) => {
        return <Ligther keywords={[keyword]} source={value} />
      },
    },
    {
      title: intl.formatMessage({ id: 'vcpu' }),
      dataIndex: 'vcpus',
      key: 'vcpus',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.vcpus - b.vcpus,
      render: (value: number) => (
        <Ligther keywords={[keyword]} source={`${value}${intl.formatMessage({ id: 'vcpu' })}`} />
      ),
    },
    {
      title: intl.formatMessage({ id: 'ram' }),
      dataIndex: 'ram',
      key: 'ram',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.ram - b.ram,
      render: (value: number) => <Ligther keywords={[keyword]} source={formatMB(value)} />,
    },
    // {
    //   title: intl.formatMessage({ id: 'action' }),
    //   key: 'action',
    //   width: 150,
    //   render: (_value, record) => (
    //     <Tooltip placement="topRight" title={intl.formatMessage({ id: 'update' })}>
    //       <Button
    //         type="primary"
    //         className="onlyIcon"
    //         icon={<EditOutlined />}
    //         onClick={() => {
    //           setUpdateVisible(record)
    //         }}
    //       />
    //     </Tooltip>
    //   ),
    // },
  ]

  const actions: MenuAction[] = [
    {
      name: intl.formatMessage({ id: 'delete' }),
      disabled: selectedRows.length === 0,
      icon: <DeleteOutlined />,
      action: () => {
        setDeleteVisible(true)
      },
    },
  ]

  return (
    <PageLayout
      loading={loading}
      title={intl.formatMessage({ id: 'menu.flavors' })}
      fetchAction={() => {
        fetchFlavors()
      }}
      actions={actions}
      createAction={() => {
        setCreateVisible(true)
      }}
    >
      <>
        {createVisible && (
          <CreateModal
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchFlavors()
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {deleteVisible && selectedRows.length > 0 && (
          <DeleteModal
            flavors={selectedRows}
            visible={deleteVisible}
            onOk={() => {
              setDeleteVisible(false)
              fetchFlavors()
            }}
            onCancel={() => {
              setDeleteVisible(false)
            }}
          />
        )}
        {/* {!!updateVisible && (
          <UpdateModal
            visible={!!updateVisible}
            flavor={updateVisible}
            onOk={() => {
              setUpdateVisible(undefined)
              fetchFlavors()
            }}
            onCancel={() => {
              setUpdateVisible(undefined)
            }}
          />
        )} */}
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          onRowSelected={setSelectedRows}
          dataCount={flavors.length}
          dataSource={
            keyword
              ? flavors.filter(
                  (flavor) =>
                    findInString(flavor.name, keyword) ||
                    findInString(formatMB(flavor.ram), keyword) ||
                    findInString(`${flavor.vcpus}${intl.formatMessage({ id: 'vcpu' })}`, keyword)
                )
              : flavors
          }
          onSearch={(value) => {
            setKeyword(value)
          }}
        />
      </>
    </PageLayout>
  )
}

export default FlavorList
