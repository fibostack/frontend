import { notification } from 'antd'
import { deleteFlavor } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { Flavor } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface FlavorDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  flavors: Flavor[]
}

const FlavorDelete: React.FC<FlavorDeleteProps> = ({ visible, onOk, onCancel, flavors }) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [vis, setVis] = useState<boolean>(visible)

  const deleteIDs = flavors.reduce<string[]>((acc: string[], flavor: Flavor) => {
    return [...acc, flavor.id]
  }, [])

  let deleteNames = flavors.reduce<string>((acc: string, flavor: Flavor) => {
    return `${acc}${flavor.name}, `
  }, '')
  deleteNames = deleteNames.substring(0, deleteNames.length - 2)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteFlavor({
      data: { flavor_ids: deleteIDs },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'flavor.deleted' }, { name: deleteNames }),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'flavor.delete_title' })}
      selectedNames={deleteNames}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default FlavorDelete
