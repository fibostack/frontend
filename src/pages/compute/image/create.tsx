import { UploadOutlined } from '@ant-design/icons'
import { Button, Form, notification, Radio, Select, Upload } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createImageService, listImageServiceData } from 'api'
import cssClass from 'classnames'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { ImageServiceData, ImageServiceDiskFormat } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

interface ImageCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
}

const ImageCreate: React.FC<ImageCreateProps> = ({ visible, onOk, onCancel }) => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)
  const [visibles, setVisibles] = useState<string[]>([])
  const [containerFormats, setContainerFormats] = useState<string[]>([])
  const [diskFormats, setDiskFormats] = useState<ImageServiceDiskFormat[]>([])
  const [file, setFile] = useState<any>(null)

  useEffect(() => {
    fetchData()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchData = async () => {
    setLoading(true)
    const response = (await listImageServiceData({})) as ImageServiceData
    if (_isMounted.current) {
      setVisibles(response.visible)
      setDiskFormats(response.disk_formats)
      setContainerFormats(response.container_formats)
      setLoading(false)
    }
  }

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const data = new window.FormData()
    Object.keys(values).forEach((value: string) => {
      data.append(value, values[value])
    })
    data.set('image_file', file)
    const success = await createImageService({ data })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${values.name} image is created.`,
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.keyPairName)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="image.create_title" />}
      formName="image_create_form"
      okText={<FormattedMessage id="create" />}
      cancelText={<FormattedMessage id="cancel" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="image_create_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <FormInput
          required
          hasLabel
          name="name"
          type="input"
          label={<FormattedMessage id="name" />}
          placeholder={intl.formatMessage({ id: 'name' })}
        />
        <FormInput
          required
          hasLabel
          min={0}
          name="min_disk"
          type="number"
          className="w-fill"
          label={<FormattedMessage id="image.min_disk" />}
          placeholder={intl.formatMessage({ id: 'min_disk' })}
        />
        <FormInput
          required
          hasLabel
          min={0}
          name="min_ram"
          type="number"
          className="w-fill"
          label={<FormattedMessage id="image.min_ram" />}
          placeholder={intl.formatMessage({ id: 'image.min_ram' })}
        />
        <FormInput required hasLabel name="disk_format" label={intl.formatMessage({ id: 'image.disk_format' })}>
          <Select placeholder={intl.formatMessage({ id: 'image.disk_format' })}>
            {diskFormats.map((format) => (
              <Select.Option key={format.key} value={format.key}>
                <span className={cssClass('bold uppercase')}>{format.key}</span> {format.value && `(${format.value})`}
              </Select.Option>
            ))}
          </Select>
        </FormInput>
        <FormInput
          required
          hasLabel
          name="container_format"
          label={intl.formatMessage({ id: 'image.container_format' })}
        >
          <Select placeholder={intl.formatMessage({ id: 'image.container_format' })}>
            {containerFormats.map((format) => (
              <Select.Option key={format} value={format}>
                <span className={cssClass('uppercase')}>{format}</span>
              </Select.Option>
            ))}
          </Select>
        </FormInput>
        <FormInput required hasLabel name="visible" label={intl.formatMessage({ id: 'image.visibility' })}>
          <Radio.Group buttonStyle="solid">
            {visibles.map((item) => (
              <Radio.Button key={item} value={item}>
                <span className={cssClass('capitalize')}>{item}</span>
              </Radio.Button>
            ))}
          </Radio.Group>
        </FormInput>
        <FormInput
          required
          hasLabel
          name="image_file"
          valuePropName="fileList"
          label={intl.formatMessage({ id: 'image.image_file' })}
        >
          <Upload
            multiple={false}
            showUploadList={false}
            beforeUpload={(value) => {
              setFile(value)
              return false
            }}
          >
            <Button icon={<UploadOutlined />}>{file ? file.name : intl.formatMessage({ id: 'upload' })}</Button>
          </Upload>
        </FormInput>
      </Form>
    </ModalForm>
  )
}

export default ImageCreate
