import { Tag } from 'antd'
import { AvailableStatus, DownStatus } from 'configs'
import React from 'react'

const Status: React.FC<{ status: string }> = ({ status, children }) => {
  let color = DownStatus
  switch (status) {
    case 'active':
      color = AvailableStatus
      break
    case 'disable':
      color = DownStatus
      break
    default:
      break
  }
  return (
    <Tag color={color} className="capitalize">
      {children || status}
    </Tag>
  )
}

export default Status
