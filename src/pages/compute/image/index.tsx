// import { DeleteOutlined } from "@ant-design/icons";
// import { Button } from "antd";
import { ColumnType } from 'antd/es/table'
import { listImageService } from 'api'
import { Ligther, PageLayout, Table } from 'components'
import { Image, MenuAction } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { findInString } from 'utils'
import CreateModal from './create'
// import DeleteModal from "./delete";
// import DetailModal from "./detail";
import Status from './status'

interface ImageProps {}

const ImageList: React.FC<ImageProps> = () => {
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState<boolean>(true)
  const [keyPairs, setImages] = useState<Array<Image>>([])
  const [, setSelectedRows] = useState<Image[]>([])
  const [keyword, setKeyword] = useState<string>('')
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  // const [deleteVisible, setDeleteVisible] = useState<boolean>(false);
  // const [detailVisible, setDetailVisible] = useState<Image>();

  useEffect(() => {
    fetchImages()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchImages = async () => {
    setLoading(true)
    const images = (await listImageService({})) as Image[]
    if (_isMounted.current) {
      setImages(images)
      setLoading(false)
    }
  }

  const columns: ColumnType<Image>[] = [
    {
      title: <FormattedMessage id="name" />,
      dataIndex: 'name',
      key: 'name',
      width: 300,
      ellipsis: true,
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: <FormattedMessage id="image.disk_format" />,
      dataIndex: 'disk_format',
      key: 'disk_format',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.disk_format.localeCompare(b.disk_format),
      render: (value: string) => (
        <span className="uppercase">
          <Ligther keywords={[keyword]} source={value} />
        </span>
      ),
    },
    {
      title: <FormattedMessage id="image.container_format" />,
      dataIndex: 'container_format',
      key: 'container_format',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.container_format.localeCompare(b.container_format),
      render: (value: string) => (
        <span className="uppercase">
          <Ligther keywords={[keyword]} source={value} />
        </span>
      ),
    },
    {
      title: <FormattedMessage id="image.visibility" />,
      dataIndex: 'visibility',
      key: 'visibility',
      width: 150,
      ellipsis: true,
      sorter: (a, b) => a.visibility.localeCompare(b.visibility),
      render: (value: string) => (
        <span className="capitalize">
          <Ligther keywords={[keyword]} source={value} />
        </span>
      ),
    },
    {
      title: <FormattedMessage id="image.min_disk" />,
      dataIndex: 'min_disk',
      key: 'min_disk',
      width: 150,
      ellipsis: true,
      sorter: (a, b) => a.min_disk - b.min_disk,
      render: (value: number) => (keyword ? <Ligther keywords={[keyword]} source={`${value}GB`} /> : `${value}GB`),
    },
    {
      title: <FormattedMessage id="image.min_ram" />,
      dataIndex: 'min_ram',
      key: 'min_ram',
      width: 150,
      ellipsis: true,
      sorter: (a, b) => a.min_ram - b.min_ram,
      render: (value: number) => (keyword ? <Ligther keywords={[keyword]} source={`${value}GB`} /> : `${value}GB`),
    },
    {
      title: <FormattedMessage id="status" />,
      dataIndex: 'status',
      width: 100,
      key: 'status',
      ellipsis: true,
      sorter: (a, b) => a.status.localeCompare(b.status),
      render: (status: string) => (
        <Status status={status}>{keyword ? <Ligther keywords={[keyword]} source={status} /> : status}</Status>
      ),
    },
  ]

  const actions: MenuAction[] = [
    // {
    //   name: <FormattedMessage id="delete" />,
    //   disabled: selectedRows.length === 0,
    //   icon: <DeleteOutlined />,
    //   action: () => {
    //     setDeleteVisible(true);
    //   },
    // },
  ]

  return (
    <PageLayout
      loading={loading}
      title={<FormattedMessage id="images" />}
      fetchAction={fetchImages}
      createAction={() => {
        setCreateVisible(true)
      }}
      actions={actions}
    >
      <>
        {createVisible && (
          <CreateModal
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchImages()
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {/* {deleteVisible && selectedRows.length > 0 && (
          <DeleteModal
            keyPairs={selectedRows}
            visible={deleteVisible}
            onOk={() => {
              setDeleteVisible(false);
              fetchKeypairs();
            }}
            onCancel={() => {
              setDeleteVisible(false);
            }}
          />
        )}
        {!!detailVisible && (
          <DetailModal
            keyPair={detailVisible}
            visible={!!detailVisible}
            onOk={() => {
              setDetailVisible(undefined);
            }}
          />
        )} */}
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          onRowSelected={setSelectedRows}
          dataCount={keyPairs.length}
          dataSource={
            keyword
              ? keyPairs.filter(
                  (keyPair) =>
                    findInString(keyPair.name, keyword) ||
                    findInString(keyPair.disk_format, keyword) ||
                    findInString(`${keyPair.min_disk}GB`, keyword) ||
                    findInString(`${keyPair.min_ram}GB`, keyword) ||
                    findInString(keyPair.visibility, keyword) ||
                    findInString(keyPair.status, keyword) ||
                    findInString(keyPair.container_format, keyword)
                )
              : keyPairs
          }
          onSearch={(value) => {
            setKeyword(value)
          }}
        />
      </>
    </PageLayout>
  )
}

export default ImageList
