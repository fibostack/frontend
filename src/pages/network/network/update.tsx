import { Form, notification, Radio } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { updateNetwork } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { Network } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface NetworkUpdateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  network: Network
}

const NetworkUpdate: React.FC<NetworkUpdateProps> = ({ visible, onOk, onCancel, network }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.network_id = network.id
    if (!values.desc) {
      values.desc = ''
    }
    const success = await updateNetwork({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'network.updated' }, { name: values.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.keyPairName)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'network.update_title' }, { name: network.name })}
      formName="network_update_form"
      okText={intl.formatMessage({ id: 'update' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form
        form={form}
        name="network_update_form"
        layout="vertical"
        labelAlign="left"
        onFinish={handleFinish}
        initialValues={{
          name: network.name,
          desc: network.description,
          is_shared: network.shared,
          state: network.admin_state_up,
        }}
      >
        <FormInput
          required
          hasLabel
          name="name"
          type="input"
          label={intl.formatMessage({ id: 'name' })}
          placeholder={intl.formatMessage({ id: 'name' })}
        />
        <FormInput
          required
          hasLabel
          name="desc"
          label={intl.formatMessage({ id: 'description' })}
          type="textarea"
          placeholder={intl.formatMessage({ id: 'description' })}
        />
        <FormInput required hasLabel name="is_shared" label={intl.formatMessage({ id: 'network.shared' })}>
          <Radio.Group buttonStyle="solid">
            <Radio.Button value>{intl.formatMessage({ id: 'yes' })}</Radio.Button>
            <Radio.Button value={false}>{intl.formatMessage({ id: 'no' })}</Radio.Button>
          </Radio.Group>
        </FormInput>
        <FormInput required hasLabel name="state" label={intl.formatMessage({ id: 'network.admin_state_up' })}>
          <Radio.Group buttonStyle="solid">
            <Radio.Button value>{intl.formatMessage({ id: 'yes' })}</Radio.Button>
            <Radio.Button value={false}>{intl.formatMessage({ id: 'no' })}</Radio.Button>
          </Radio.Group>
        </FormInput>
      </Form>
    </ModalForm>
  )
}

export default NetworkUpdate
