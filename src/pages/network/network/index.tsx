import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import { Button, Col, Row, Tooltip } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listNetwork } from 'api'
import { BooleanStatus, PageLayout, Table } from 'components'
import { Network } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import CreateModal from './create'
import DeleteModal from './delete'
import DetailModal from './detail'
import Status from './status'
import UpdateModal from './update'

interface NetworkProps {}

const NetworkList: React.FC<NetworkProps> = () => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [networks, setNetworks] = useState<Network[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [updateVisible, setUpdateVisible] = useState<Network>()
  const [deleteVisible, setDeleteVisible] = useState<Network>()
  const [detailVisible, setDetailVisible] = useState<Network>()

  useEffect(() => {
    fetchNetworks()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchNetworks = async () => {
    setLoading(true)
    const ns = await listNetwork({})
    if (_isMounted.current) {
      if (ns) setNetworks(ns as Network[])
      setLoading(false)
    }
  }

  const columns: ColumnType<Network>[] = [
    {
      key: 'name',
      width: 200,
      title: intl.formatMessage({ id: 'name' }),
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (network: Network) => (
        <Button
          type="link"
          onClick={() => {
            setDetailVisible(network)
          }}
        >
          {network.name}
        </Button>
      ),
    },
    {
      key: 'description',
      width: 300,
      title: intl.formatMessage({ id: 'description' }),
      dataIndex: 'description',
      render: (val) => val || '-',
    },
    {
      key: 'subnets',
      width: 300,
      dataIndex: 'subnets',
      title: intl.formatMessage({ id: 'network.subnets' }),
      render: (value: string[]) =>
        (value && value.length) > 0 ? (
          <ul className="nostyle">
            {value.map((val) => (
              <li key={val}>{val}</li>
            ))}
          </ul>
        ) : (
          '-'
        ),
    },
    {
      title: intl.formatMessage({ id: 'network.shared' }),
      dataIndex: 'shared',
      key: 'shared',
      width: 150,
      render: (value: boolean) => <BooleanStatus status={value} />,
    },
    {
      title: intl.formatMessage({ id: 'status' }),
      dataIndex: 'status',
      key: 'status',
      width: 150,
      render: (value: string) => <Status status={value} />,
    },
    {
      title: intl.formatMessage({ id: 'network.admin_state_up' }),
      dataIndex: 'admin_state_up',
      key: 'admin_state_up',
      width: 150,
      render: (value: boolean) => <BooleanStatus status={value} />,
    },
    {
      title: intl.formatMessage({ id: 'action' }),
      key: 'action',
      width: 120,
      render: (_value, record) => (
        <Row gutter={8}>
          <Col>
            <Tooltip placement="topRight" title={intl.formatMessage({ id: 'update' })}>
              <Button
                type="primary"
                className="onlyIcon"
                icon={<EditOutlined />}
                onClick={() => {
                  setUpdateVisible(record)
                }}
              />
            </Tooltip>
          </Col>
          <Col>
            <Tooltip placement="topRight" title={intl.formatMessage({ id: 'delete' })}>
              <Button
                danger
                className="onlyIcon"
                icon={<DeleteOutlined />}
                onClick={() => {
                  setDeleteVisible(record)
                }}
              />
            </Tooltip>
          </Col>
        </Row>
      ),
    },
  ]

  return (
    <PageLayout
      loading={loading}
      title={intl.formatMessage({ id: 'menu.networks' })}
      fetchAction={fetchNetworks}
      createAction={() => {
        setCreateVisible(true)
      }}
    >
      <>
        {createVisible && (
          <CreateModal
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchNetworks()
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {!!updateVisible && (
          <UpdateModal
            network={updateVisible}
            visible={!!updateVisible}
            onOk={() => {
              setUpdateVisible(undefined)
              fetchNetworks()
            }}
            onCancel={() => {
              setUpdateVisible(undefined)
            }}
          />
        )}
        {!!deleteVisible && (
          <DeleteModal
            network={deleteVisible}
            visible={!!deleteVisible}
            onOk={() => {
              setDeleteVisible(undefined)
              fetchNetworks()
            }}
            onCancel={() => {
              setDeleteVisible(undefined)
            }}
          />
        )}
        {!!detailVisible && (
          <DetailModal
            network={detailVisible}
            visible={!!detailVisible}
            onOk={() => {
              setDetailVisible(undefined)
            }}
          />
        )}
        <Table rowKey="id" loading={loading} columns={columns} dataSource={networks} />
      </>
    </PageLayout>
  )
}

export default NetworkList
