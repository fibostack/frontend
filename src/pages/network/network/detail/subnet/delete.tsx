import { notification } from 'antd'
import { deleteSubnet } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { Subnet } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface SubnetDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  subnets: Subnet[]
}

const SubnetDelete: React.FC<SubnetDeleteProps> = ({ visible, onOk, onCancel, subnets }) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [vis, setVis] = useState<boolean>(visible)

  const deleteIDs = subnets.reduce<string[]>((acc: string[], subnet: Subnet) => {
    return [...acc, subnet.id]
  }, [])

  let deleteNames = subnets.reduce<string>((acc: string, subnet: Subnet) => {
    return `${acc}${subnet.name}, `
  }, '')
  deleteNames = deleteNames.substring(0, deleteNames.length - 2)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteSubnet({
      data: { subnet_ids: deleteIDs },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'network.subnet_deleted' }, { name: deleteNames }),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'network.subnet_delete_title' })}
      okText={intl.formatMessage({ id: 'delete' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      selectedNames={deleteNames}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default SubnetDelete
