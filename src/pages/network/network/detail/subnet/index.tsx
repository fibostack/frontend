import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import { Button, Descriptions, Tooltip } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listSubnet } from 'api'
import { BooleanStatus, PageLayout, Table } from 'components'
import { MenuAction, Subnet } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import CreateModal from './create'
import DeleteModal from './delete'
import UpdateModal from './update'

interface SubnetProps {
  networkID: string
}

const SubnetList: React.FC<SubnetProps> = ({ networkID }) => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [subnets, setSubnets] = useState<Subnet[]>([])
  const [selectedRows, setSelectedRows] = useState<Subnet[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [updateVisible, setUpdateVisible] = useState<Subnet>()
  const [deleteVisible, setDeleteVisible] = useState<boolean>(false)

  useEffect(() => {
    return () => {
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    fetchSubnets(networkID)
  }, [networkID])

  const fetchSubnets = async (nID: string) => {
    setLoading(true)
    const ss = (await listSubnet({ data: { network_id: nID } })) as Subnet[]
    if (_isMounted.current) {
      setSubnets(ss)
      setLoading(false)
    }
  }

  const columns: ColumnType<Subnet>[] = [
    {
      key: 'name',
      dataIndex: 'name',
      width: 300,
      title: intl.formatMessage({ id: 'name' }),
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (value: string) => value || '-',
    },
    {
      key: 'description',
      dataIndex: 'description',
      title: intl.formatMessage({ id: 'description' }),
      width: 400,
      render: (value: string) => value || '-',
    },
    {
      key: 'cidr',
      width: 200,
      dataIndex: 'cidr',
      title: intl.formatMessage({ id: 'network.address' }),
      render: (value: string) => value || '-',
    },
    {
      title: intl.formatMessage({ id: 'network.ip_version' }),
      dataIndex: 'ip_version',
      width: 200,
      key: 'ip_version',
      render: (value: number) => `IPv${value}`,
    },
    {
      key: 'dns_nameservers',
      width: 200,
      dataIndex: 'dns_nameservers',
      title: intl.formatMessage({ id: 'network.dns_name_servers' }),
      render: (value: string[]) =>
        (value && value.length) > 0 ? (
          <ul className="nostyle">
            {value.map((val) => (
              <li key={val}>{val}</li>
            ))}
          </ul>
        ) : (
          '-'
        ),
    },
    {
      title: intl.formatMessage({ id: 'network.gateway_ip' }),
      dataIndex: 'gateway_ip',
      width: 200,
      key: 'gateway_ip',
      render: (value: string) => value || '-',
    },
    {
      title: intl.formatMessage({ id: 'action' }),
      width: 80,
      key: 'action',
      render: (_value, record) => (
        <Tooltip placement="topRight" title={intl.formatMessage({ id: 'update' })}>
          <Button
            type="primary"
            className="onlyIcon"
            icon={<EditOutlined />}
            onClick={() => {
              setUpdateVisible(record)
            }}
          />
        </Tooltip>
      ),
    },
  ]

  const actions: MenuAction[] = [
    {
      name: intl.formatMessage({ id: 'delete' }),
      disabled: selectedRows.length === 0,
      icon: <DeleteOutlined />,
      action: () => {
        setDeleteVisible(true)
      },
    },
  ]

  return (
    <PageLayout
      noCard
      loading={loading}
      title={intl.formatMessage({ id: 'network.subnets' })}
      fetchAction={() => {
        fetchSubnets(networkID)
      }}
      actions={actions}
      createAction={() => {
        setCreateVisible(true)
      }}
    >
      <>
        {createVisible && (
          <CreateModal
            networkID={networkID}
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchSubnets(networkID)
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {!!updateVisible && (
          <UpdateModal
            subnet={updateVisible}
            visible={!!updateVisible}
            onOk={() => {
              setUpdateVisible(undefined)
              fetchSubnets(networkID)
            }}
            onCancel={() => {
              setUpdateVisible(undefined)
            }}
          />
        )}
        {deleteVisible && selectedRows.length > 0 && (
          <DeleteModal
            subnets={selectedRows}
            visible={deleteVisible}
            onOk={() => {
              setDeleteVisible(false)
              fetchSubnets(networkID)
            }}
            onCancel={() => {
              setDeleteVisible(false)
            }}
          />
        )}
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          dataSource={subnets}
          onRowSelected={setSelectedRows}
          expandable={{
            expandedRowRender: (record: Subnet) => (
              <Descriptions size="small" layout="vertical" column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="name" />
                    </strong>
                  }
                >
                  {record.name || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="id" />
                    </strong>
                  }
                >
                  {record.id || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="project_id" />
                    </strong>
                  }
                >
                  {record.project_id || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.network_id" />
                    </strong>
                  }
                >
                  {record.network_id || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.subnetpool_id" />
                    </strong>
                  }
                >
                  {record.subnetpool_id || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.ip_version" />
                    </strong>
                  }
                >
                  {record.ip_version || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.address" />
                    </strong>
                  }
                >
                  {record.cidr || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.allocation_pools" />
                    </strong>
                  }
                >
                  <ul className="nostyle">
                    {record.allocation_pools.map((val) => (
                      <li key={val.start + val.end}>{`Start: ${val.start} ~ End: ${val.end}`}</li>
                    ))}
                  </ul>
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.gateway_ip" />
                    </strong>
                  }
                >
                  {record.gateway_ip || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.dns_name_servers" />
                    </strong>
                  }
                >
                  <ul className="nostyle">
                    {record.dns_nameservers.map((val) => (
                      <li key={val}>{val}</li>
                    ))}
                  </ul>
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.enable_dhcp" />
                    </strong>
                  }
                >
                  <BooleanStatus status={record.enable_dhcp} />
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="description" />
                    </strong>
                  }
                >
                  {record.description || '-'}
                </Descriptions.Item>
              </Descriptions>
            ),
          }}
        />
      </>
    </PageLayout>
  )
}

export default SubnetList
