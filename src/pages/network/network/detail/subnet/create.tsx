import { Form, Input, notification, Select, Space, Button } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createSubnet } from 'api'
import cssClass from 'classnames'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons'

interface SubnetCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  networkID: string
}

const SubnetCreate: React.FC<SubnetCreateProps> = ({ visible, onOk, onCancel, networkID }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.network_id = networkID
    if (!values.desc) values.desc = ''
    if (!values.gateway_ip) values.gateway_ip = ''
    if (!values.allocation_pools) values.allocation_pools = []
    if (!values.dns_name_servers) values.dns_name_servers = []
    if (!values.host_routes) values.host_routes = []
    const success = await createSubnet({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'network.subnet_created' }, { name: values.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'network.subnet_create_title' })}
      formName="network_subnet_create_form"
      okText={intl.formatMessage({ id: 'create' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="network_subnet_create_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <FormInput
          required
          hasLabel
          name="name"
          type="input"
          label={intl.formatMessage({ id: 'name' })}
          placeholder={intl.formatMessage({ id: 'name' })}
        />
        <FormInput
          hasLabel
          name="desc"
          label={intl.formatMessage({ id: 'description' })}
          type="textarea"
          placeholder={intl.formatMessage({ id: 'description' })}
        />
        <FormInput
          required
          hasLabel
          name="cidr"
          label={intl.formatMessage({ id: 'network.address' })}
          placeholder={intl.formatMessage({ id: 'network.address' })}
          type="input"
        />
        <FormInput
          hasLabel
          name="gateway_ip"
          label={intl.formatMessage({ id: 'network.gateway_ip' })}
          placeholder={intl.formatMessage({ id: 'network.gateway_ip' })}
          type="input"
        />
        <Form.Item label={intl.formatMessage({ id: 'network.allocation_pools' })}>
          <Form.List name="allocation_pools">
            {(fields, { add, remove }) => {
              return (
                <div>
                  <div className="m_b_1">
                    {fields.map((field) => (
                      <Space key={field.key} align="center">
                        <Input.Group compact>
                          <Form.Item
                            noStyle
                            fieldKey={[field.fieldKey, 'start']}
                            name={[field.name, 'start']}
                            rules={[
                              {
                                required: true,
                                message: intl.formatMessage({ id: 'valid.required' }),
                                whitespace: false,
                              },
                            ]}
                          >
                            <Input placeholder="Start" className="range_input range_left_input" />
                          </Form.Item>
                          <Form.Item noStyle fieldKey={[field.fieldKey, 'pool_div']}>
                            <Input
                              style={{
                                width: '10%',
                                borderLeft: 0,
                                borderRight: 0,
                                pointerEvents: 'none',
                                backgroundColor: '#fff',
                              }}
                              placeholder="~"
                              disabled
                            />
                          </Form.Item>
                          <Form.Item
                            noStyle
                            fieldKey={[field.fieldKey, 'end']}
                            name={[field.name, 'end']}
                            rules={[
                              {
                                required: true,
                                message: intl.formatMessage({ id: 'valid.required' }),
                                whitespace: false,
                              },
                            ]}
                          >
                            <Input
                              className={cssClass('site-input-right range_right_input range_input')}
                              placeholder="End"
                            />
                          </Form.Item>
                        </Input.Group>

                        <Button
                          danger
                          type="link"
                          className="onlyIcon"
                          icon={<MinusCircleOutlined />}
                          onClick={() => {
                            remove(field.name)
                          }}
                        />
                      </Space>
                    ))}
                  </div>

                  <Form.Item>
                    <Button
                      block
                      type="dashed"
                      icon={<PlusOutlined />}
                      onClick={() => {
                        add()
                      }}
                    >
                      {intl.formatMessage({ id: 'add' })}
                    </Button>
                  </Form.Item>
                </div>
              )
            }}
          </Form.List>
        </Form.Item>
        <FormInput hasLabel name="dns_name_servers" label={intl.formatMessage({ id: 'network.dns_name_servers' })}>
          <Select mode="tags" />
        </FormInput>
        <Form.Item label={intl.formatMessage({ id: 'network.host_routes' })}>
          <Form.List name="host_routes">
            {(fields, { add, remove }) => {
              return (
                <div>
                  <div className="m_b_1">
                    {fields.map((field) => (
                      <Space key={field.key} align="center">
                        <Input.Group compact>
                          <Form.Item
                            noStyle
                            fieldKey={[field.fieldKey, 'destination']}
                            name={[field.name, 'destination']}
                            rules={[
                              {
                                required: true,
                                message: intl.formatMessage({ id: 'valid.required' }),
                                whitespace: false,
                              },
                            ]}
                          >
                            <Input placeholder="Destination" className="range_input range_left_input" />
                          </Form.Item>
                          <Form.Item noStyle fieldKey={[field.fieldKey, 'route_div']}>
                            <Input
                              style={{
                                width: '10%',
                                borderLeft: 0,
                                borderRight: 0,
                                pointerEvents: 'none',
                                backgroundColor: '#fff',
                              }}
                              placeholder="~"
                              disabled
                            />
                          </Form.Item>
                          <Form.Item
                            noStyle
                            fieldKey={[field.fieldKey, 'nexthop']}
                            name={[field.name, 'nexthop']}
                            rules={[
                              {
                                required: true,
                                message: intl.formatMessage({ id: 'valid.required' }),
                                whitespace: false,
                              },
                            ]}
                          >
                            <Input
                              className={cssClass('site-input-right range_right_input range_input')}
                              placeholder="Next hop"
                            />
                          </Form.Item>
                        </Input.Group>

                        <Button
                          danger
                          type="link"
                          className="onlyIcon"
                          icon={<MinusCircleOutlined />}
                          onClick={() => {
                            remove(field.name)
                          }}
                        />
                      </Space>
                    ))}
                  </div>

                  <Form.Item>
                    <Button
                      block
                      type="dashed"
                      icon={<PlusOutlined />}
                      onClick={() => {
                        add()
                      }}
                    >
                      {intl.formatMessage({ id: 'add' })}
                    </Button>
                  </Form.Item>
                </div>
              )
            }}
          </Form.List>
        </Form.Item>
      </Form>
    </ModalForm>
  )
}

export default SubnetCreate
