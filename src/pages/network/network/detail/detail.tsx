import { Descriptions } from 'antd'
import { getNetwork } from 'api'
import { BooleanStatus, Loader } from 'components'
import { Network } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { DateFormat } from 'utils'
import Status from '../status'

interface Props {
  network: Network
}

const Detail = ({ network }: Props) => {
  const _isMounted = useRef(true)
  const [networkDetail, setNetworkDetail] = useState<Network>()
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    return () => {
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    fetchNetworks(network.id)
  }, [network.id])

  const fetchNetworks = async (netID: string) => {
    setLoading(true)
    const net = (await getNetwork({ data: { network_id: netID } })) as Network
    if (_isMounted.current) {
      setNetworkDetail(net)
      setLoading(false)
    }
  }

  return (
    <Loader loading={loading}>
      {networkDetail && (
        <Descriptions column={{ sm: 2, xs: 1 }}>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="name" />
              </strong>
            }
          >
            {networkDetail.name || '-'}
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="id" />
              </strong>
            }
          >
            {networkDetail.id || '-'}
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="create_at" />
              </strong>
            }
          >
            {DateFormat(networkDetail.created_at) || '-'}
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="updated_at" />
              </strong>
            }
          >
            {DateFormat(networkDetail.updated_at) || '-'}
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="project_id" />
              </strong>
            }
          >
            {networkDetail.project_id || '-'}
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="status" />
              </strong>
            }
          >
            <Status status={networkDetail.status} />
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="network.admin_state_up" />
              </strong>
            }
          >
            <BooleanStatus status={networkDetail.admin_state_up} />
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="network.shared" />
              </strong>
            }
          >
            <BooleanStatus status={networkDetail.shared} />
          </Descriptions.Item>
        </Descriptions>
      )}
    </Loader>
  )
}

export default Detail
