import { notification } from 'antd'
import { deletePort } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { Port } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface PortDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  ports: Port[]
}

const PortDelete: React.FC<PortDeleteProps> = ({ visible, onOk, onCancel, ports }) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [vis, setVis] = useState<boolean>(visible)

  const deleteIDs = ports.reduce<string[]>((acc: string[], port: Port) => {
    return [...acc, port.id]
  }, [])

  let deleteNames = ports.reduce<string>((acc: string, port: Port) => {
    return `${acc}${port.name}, `
  }, '')
  deleteNames = deleteNames.substring(0, deleteNames.length - 2)

  const onDelete = async () => {
    setLoading(true)
    const success = await deletePort({
      data: { port_ids: deleteIDs },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'network.port_deleted' }, { name: deleteNames }),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'network.port_delete_title' })}
      okText={intl.formatMessage({ id: 'delete' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      selectedNames={deleteNames}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default PortDelete
