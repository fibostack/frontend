import { Form, notification, Select } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createPort } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'
import { SecurityGroup } from 'models'

interface PortCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  sgs: SecurityGroup[]
  networkId: string
}

const PortCreate: React.FC<PortCreateProps> = ({ visible, onOk, onCancel, sgs, networkId }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.network_id = networkId
    if (!values.desc) {
      values.desc = ''
    }
    const success = await createPort({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'network.port_created' }, { name: values.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'network.port_create_title' })}
      formName="network_port_create_form"
      okText={intl.formatMessage({ id: 'create' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="network_port_create_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <FormInput
          required
          hasLabel
          name="name"
          type="input"
          label={intl.formatMessage({ id: 'name' })}
          placeholder={intl.formatMessage({ id: 'name' })}
        />
        <FormInput
          hasLabel
          name="desc"
          label={intl.formatMessage({ id: 'description' })}
          type="textarea"
          placeholder={intl.formatMessage({ id: 'description' })}
        />
        <FormInput
          required
          hasLabel
          label={intl.formatMessage({ id: 'instance.security_groups' })}
          name="sec_group_ids"
        >
          <Select mode="multiple" placeholder={<FormattedMessage id="instance.security_groups" />}>
            {sgs.map((securitGroup) => {
              return (
                <Select.Option key={securitGroup.name + securitGroup.ID} value={securitGroup.ID}>
                  {securitGroup.name}
                </Select.Option>
              )
            })}
          </Select>
        </FormInput>
      </Form>
    </ModalForm>
  )
}

export default PortCreate
