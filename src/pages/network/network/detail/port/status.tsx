import { Tag } from 'antd'
import { AvailableStatus, DownStatus, InUseStatus } from 'configs'
import React from 'react'

const Status = ({ status }: { status: string }) => {
  let color = DownStatus
  switch (status) {
    case 'ACTIVE':
      color = AvailableStatus
      break
    case 'DISABLE':
      color = InUseStatus
      break
    case 'SHUTOFF':
      color = DownStatus
      break
    default:
      break
  }
  return (
    <Tag color={color} className="capitalize">
      {status}
    </Tag>
  )
}

export default Status
