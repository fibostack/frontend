import { Form, notification, Select, Radio } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { updatePort } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { Port, SecurityGroup } from 'models'
import React, { useState } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'

interface PortUpdateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  port: Port
  sgs: SecurityGroup[]
}

const PortUpdate: React.FC<PortUpdateProps> = ({ visible, onOk, onCancel, port, sgs }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.port_id = port.id
    if (!values.desc) {
      values.desc = ''
    }
    const success = await updatePort({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'network.port_updated' }, { name: values.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.keyPairName)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'network.port_update_title' }, { name: port.name })}
      formName="network_port_update_form"
      okText={intl.formatMessage({ id: 'update' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form
        form={form}
        name="network_port_update_form"
        layout="vertical"
        labelAlign="left"
        onFinish={handleFinish}
        initialValues={{
          name: port.name,
          desc: port.description,
          sec_group_ids: port.security_groups,
          state: port.admin_state_up,
        }}
      >
        <FormInput
          required
          hasLabel
          name="name"
          type="input"
          label={intl.formatMessage({ id: 'name' })}
          placeholder={intl.formatMessage({ id: 'name' })}
        />
        <FormInput
          hasLabel
          name="desc"
          label={intl.formatMessage({ id: 'description' })}
          type="textarea"
          placeholder={intl.formatMessage({ id: 'description' })}
        />
        <FormInput
          required
          hasLabel
          label={intl.formatMessage({ id: 'instance.security_groups' })}
          name="sec_group_ids"
        >
          <Select mode="multiple" placeholder={<FormattedMessage id="instance.security_groups" />}>
            {sgs.map((securitGroup) => {
              return (
                <Select.Option key={securitGroup.name + securitGroup.ID} value={securitGroup.ID}>
                  {securitGroup.name}
                </Select.Option>
              )
            })}
          </Select>
        </FormInput>
        <FormInput required hasLabel name="state" label={intl.formatMessage({ id: 'network.admin_state_up' })}>
          <Radio.Group buttonStyle="solid">
            <Radio.Button value>{intl.formatMessage({ id: 'yes' })}</Radio.Button>
            <Radio.Button value={false}>{intl.formatMessage({ id: 'no' })}</Radio.Button>
          </Radio.Group>
        </FormInput>
      </Form>
    </ModalForm>
  )
}

export default PortUpdate
