import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import { Button, Descriptions, Tooltip, Divider } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listPort, listSecurityGroup } from 'api'
import { BooleanStatus, PageLayout, Table } from 'components'
import { MenuAction, Port, SecurityGroup } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import CreateModal from './create'
import DeleteModal from './delete'
import Status from './status'
import UpdateModal from './update'

interface PortProps {
  networkID: string
}

const PortList: React.FC<PortProps> = ({ networkID }) => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [ports, setPorts] = useState<Port[]>([])
  const [SGs, setSGs] = useState<SecurityGroup[]>([])
  const [selectedRows, setSelectedRows] = useState<Port[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [updateVisible, setUpdateVisible] = useState<Port>()
  const [deleteVisible, setDeleteVisible] = useState<boolean>(false)

  useEffect(() => {
    fetchSG()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchSG = async () => {
    setLoading(true)
    const sgs = (await listSecurityGroup({})) as SecurityGroup[]
    if (_isMounted.current) {
      setSGs(sgs)
      setLoading(false)
    }
  }

  useEffect(() => {
    fetchPorts(networkID)
  }, [networkID])

  const fetchPorts = async (nID: string) => {
    setLoading(true)
    const ps = (await listPort({ data: { network_id: nID } })) as Port[]
    if (_isMounted.current) {
      setPorts(ps)
      setLoading(false)
    }
  }

  const columns: ColumnType<Port>[] = [
    {
      key: 'name',
      width: 300,
      title: intl.formatMessage({ id: 'name' }),
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (record: Port) => record.name || record.id,
    },
    {
      key: 'description',
      dataIndex: 'description',
      title: intl.formatMessage({ id: 'description' }),
      width: 400,
      render: (value: string) => value || '-',
    },
    {
      title: intl.formatMessage({ id: 'network.mac_address' }),
      dataIndex: 'mac_address',
      key: 'mac_address',
      width: 200,
      render: (value: string) => value || '-',
    },
    {
      title: intl.formatMessage({ id: 'network.device_owner' }),
      dataIndex: 'device_owner',
      key: 'device_owner',
      width: 300,
      render: (value: string) => value || '-',
    },
    {
      title: intl.formatMessage({ id: 'status' }),
      dataIndex: 'status',
      key: 'status',
      width: 150,
      render: (value: string) => <Status status={value} />,
    },
    {
      title: intl.formatMessage({ id: 'network.admin_state_up' }),
      dataIndex: 'admin_state_up',
      key: 'admin_state_up',
      width: 150,
      render: (value: boolean) => <BooleanStatus status={value} />,
    },
    {
      title: intl.formatMessage({ id: 'action' }),
      key: 'action',
      width: 80,
      render: (_value, record) => (
        <Tooltip placement="topRight" title={intl.formatMessage({ id: 'update' })}>
          <Button
            type="primary"
            className="onlyIcon"
            icon={<EditOutlined />}
            onClick={() => {
              setUpdateVisible(record)
            }}
          />
        </Tooltip>
      ),
    },
  ]

  const actions: MenuAction[] = [
    {
      name: intl.formatMessage({ id: 'delete' }),
      disabled: selectedRows.length === 0,
      icon: <DeleteOutlined />,
      action: () => {
        setDeleteVisible(true)
      },
    },
  ]

  return (
    <PageLayout
      noCard
      loading={loading}
      title={intl.formatMessage({ id: 'network.ports' })}
      fetchAction={() => {
        fetchPorts(networkID)
      }}
      actions={actions}
      createAction={() => {
        setCreateVisible(true)
      }}
    >
      <>
        {createVisible && (
          <CreateModal
            networkId={networkID}
            sgs={SGs}
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchPorts(networkID)
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {!!updateVisible && (
          <UpdateModal
            sgs={SGs}
            port={updateVisible}
            visible={!!updateVisible}
            onOk={() => {
              setUpdateVisible(undefined)
              fetchPorts(networkID)
            }}
            onCancel={() => {
              setUpdateVisible(undefined)
            }}
          />
        )}
        {deleteVisible && selectedRows.length > 0 && (
          <DeleteModal
            ports={selectedRows}
            visible={deleteVisible}
            onOk={() => {
              setDeleteVisible(false)
              fetchPorts(networkID)
            }}
            onCancel={() => {
              setDeleteVisible(false)
            }}
          />
        )}
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          dataSource={ports}
          onRowSelected={setSelectedRows}
          expandable={{
            expandedRowRender: (record: Port) => (
              <Descriptions size="small" layout="vertical" column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="name" />
                    </strong>
                  }
                >
                  {record.name || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="id" />
                    </strong>
                  }
                >
                  {record.id || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="project_id" />
                    </strong>
                  }
                >
                  {record.project_id || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.network_id" />
                    </strong>
                  }
                >
                  {record.network_id || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.mac_address" />
                    </strong>
                  }
                >
                  {record.mac_address || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="status" />
                    </strong>
                  }
                >
                  <Status status={record.status} />
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.admin_state_up" />
                    </strong>
                  }
                >
                  <BooleanStatus status={record.admin_state_up} />
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.device_id" />
                    </strong>
                  }
                >
                  {record.device_id || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.device_owner" />
                    </strong>
                  }
                >
                  {record.device_owner || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="instance.security_groups" />
                    </strong>
                  }
                >
                  <ul className="nostyle">
                    {record.security_groups.map((val) => (
                      <li key={val}>{val}</li>
                    ))}
                  </ul>
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.binding:vnic_type" />
                    </strong>
                  }
                >
                  {record['binding:vnic_type'] || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.binding:host_id" />
                    </strong>
                  }
                >
                  {record['binding:host_id'] || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.binding:profile" />
                    </strong>
                  }
                >
                  {JSON.stringify(record['binding:profile']) || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.binding:vif_type" />
                    </strong>
                  }
                >
                  {record['binding:vif_type'] || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.binding:vif_details.connectivity" />
                    </strong>
                  }
                >
                  {record['binding:vif_details'].connectivity || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.binding:vif_details.port_filter" />
                    </strong>
                  }
                >
                  <BooleanStatus status={record['binding:vif_details'].port_filter} />
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.binding:vif_details.ovs_hybrid_plug" />
                    </strong>
                  }
                >
                  <BooleanStatus status={record['binding:vif_details'].ovs_hybrid_plug} />
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.binding:vif_details.datapath_type" />
                    </strong>
                  }
                >
                  {record['binding:vif_details'].datapath_type || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    <strong>
                      <FormattedMessage id="network.binding:vif_details.bridge_name" />
                    </strong>
                  }
                >
                  {record['binding:vif_details'].bridge_name || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  span={2}
                  label={
                    <strong>
                      <FormattedMessage id="description" />
                    </strong>
                  }
                >
                  {record.description || '-'}
                </Descriptions.Item>
                <Descriptions.Item
                  span={2}
                  label={
                    <strong>
                      <FormattedMessage id="network.fixed_ips" />
                    </strong>
                  }
                >
                  <ul className="nostyle">
                    {record.fixed_ips.map((val) => (
                      <li key={val.ip_address + val.subnet_id}>
                        <>
                          <strong>
                            <FormattedMessage id="network.address" />:{' '}
                          </strong>
                          {val.ip_address}
                          {' ~ '}
                          <strong>
                            <FormattedMessage id="network.subnet_id" />:{' '}
                          </strong>
                          {val.subnet_id}
                        </>
                      </li>
                    ))}
                  </ul>
                </Descriptions.Item>
              </Descriptions>
            ),
          }}
        />
      </>
    </PageLayout>
  )
}

export default PortList
