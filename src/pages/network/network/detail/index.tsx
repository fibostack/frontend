import { HeatMapOutlined, InfoCircleOutlined, ReadOutlined } from '@ant-design/icons'
import { Tabs } from 'antd'
import { DetailModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { useWindow } from 'hooks'
import { Network } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { isEmptyString } from 'utils'
import Detail from './detail'
import Port from './port'
import Subnet from './subnet'

interface NetworkDetailProps {
  visible: boolean
  onOk: Function
  network: Network
}

const NetworkDetail: React.FC<NetworkDetailProps> = ({ visible, onOk, network }) => {
  const intl = useIntl()
  const [width] = useWindow()
  const [vis, setVis] = useState<boolean>(visible)

  return (
    <DetailModal
      visible={vis}
      width={width > 1200 ? '80%' : '100%'}
      title={
        <FormattedMessage
          id="network.detail_title"
          values={{
            name: isEmptyString(network.name) ? network.name : network.id,
          }}
        />
      }
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onOk()
        }, CloseAwaitMS)
      }}
    >
      <Tabs defaultActiveKey="detail" style={{ marginTop: -24 }}>
        <Tabs.TabPane
          tab={
            <>
              <ReadOutlined />
              {intl.formatMessage({ id: 'detail' })}
            </>
          }
          key="detail"
          className="p_t_1"
        >
          <Detail network={network} />
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <>
              <InfoCircleOutlined />
              {intl.formatMessage({ id: 'network.subnets' })}
            </>
          }
          key="subnets"
          className="p_t_1"
        >
          <Subnet networkID={network.id} />
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <>
              <HeatMapOutlined />
              {intl.formatMessage({ id: 'network.ports' })}
            </>
          }
          key="ports"
          className="p_t_1"
        >
          <Port networkID={network.id} />
        </Tabs.TabPane>
      </Tabs>
    </DetailModal>
  )
}

export default NetworkDetail
