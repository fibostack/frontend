import { notification } from 'antd'
import { deleteNetwork } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { Network } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface NetworkDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  network: Network
}

const NetworkDelete: React.FC<NetworkDeleteProps> = ({ visible, onOk, onCancel, network }) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [vis, setVis] = useState<boolean>(visible)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteNetwork({
      data: { network_id: network.id },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'network.deleted' }, { name: network.name }),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'network.delete_title' })}
      okText={intl.formatMessage({ id: 'delete' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      selectedNames={network.name}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default NetworkDelete
