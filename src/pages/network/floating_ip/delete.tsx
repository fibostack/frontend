import { notification } from 'antd'
import { deleteFloatingIP } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { FloatingIP } from 'models'
import React, { useState } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'

interface FloatingIPDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  floatingIP: FloatingIP
}

const FloatingIPDelete: React.FC<FloatingIPDeleteProps> = ({ onOk, visible, onCancel, floatingIP }) => {
  const intl = useIntl()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteFloatingIP({
      data: { fipID: floatingIP.id },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'floating_ip.deleted' }, { name: floatingIP.description }),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="floating_ip.delete_title" />}
      selectedNames={floatingIP.floating_ip_address}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default FloatingIPDelete
