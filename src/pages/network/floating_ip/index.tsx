import { CloseOutlined, DeleteOutlined } from '@ant-design/icons'
import { Button, Col, Row, Tag } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listFloatingIP } from 'api'
import { Ligther, PageLayout, Table } from 'components'
import { AvailableStatus, DownStatus, InUseStatus } from 'configs'
import { FloatingIP } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import { findInString } from 'utils'
import CreateModal from './create'
import DeleteModal from './delete'

const Status = ({ status }: { status: string }) => {
  let color = InUseStatus
  switch (status) {
    case 'ACTIVE':
      color = AvailableStatus
      break
    case 'DOWN':
      color = DownStatus
      break
    default:
      break
  }
  return (
    <Tag color={color} className="capitalize">
      {status}
    </Tag>
  )
}

interface FloatingIPProps {}

const FloatingIPList: React.FC<FloatingIPProps> = () => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [keyword, setKeyword] = useState<string>('')
  const [floatingIPs, setFloatingIPs] = useState<FloatingIP[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [deleteVisible, setDeleteVisible] = useState<FloatingIP>()

  useEffect(() => {
    fetchFloatingIPs()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchFloatingIPs = async () => {
    setLoading(true)
    const flIPs = (await listFloatingIP({})) as FloatingIP[]
    if (_isMounted.current) {
      setFloatingIPs(flIPs)
      setLoading(false)
    }
  }

  const columns: ColumnType<FloatingIP>[] = [
    {
      title: 'Floating IP Address',
      dataIndex: 'floating_ip_address',
      key: 'floating_ip_address',
      width: 100,
      ellipsis: true,
      sorter: (a, b) => a.floating_ip_address.localeCompare(b.floating_ip_address),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: 'Associated IP Address',
      dataIndex: 'fixed_ip_address',
      key: 'fixed_ip_address',
      width: 100,
      ellipsis: true,
      sorter: (a, b) => a.fixed_ip_address.localeCompare(b.fixed_ip_address),
      render: (value: string) => {
        if (value) {
          return (
            <Row gutter={12}>
              <Col>
                <Ligther keywords={[keyword]} source={value} />
              </Col>
              <Col>
                <Button
                  danger
                  disabled
                  size="small"
                  type="primary"
                  className="onlyIcon"
                  icon={<CloseOutlined />}
                  onClick={() => {}}
                />
              </Col>
            </Row>
          )
        }
        return (
          <Button disabled size="small" type="dashed" onClick={() => {}}>
            Associate
          </Button>
        )
      },
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      width: 100,
      render: (value: string) => <Status status={value} />,
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
      width: 200,
    },
    {
      title: 'Delete',
      key: 'delete',
      width: 50,
      render: (floatingIP: FloatingIP) => (
        <Button
          danger
          className="onlyIcon"
          onClick={() => {
            setDeleteVisible(floatingIP)
          }}
          icon={<DeleteOutlined />}
        />
      ),
    },
  ]

  return (
    <PageLayout
      loading={loading}
      title={intl.formatMessage({ id: 'menu.floating_ips' })}
      createAction={() => {
        setCreateVisible(true)
      }}
      fetchAction={fetchFloatingIPs}
    >
      <>
        {createVisible && (
          <CreateModal
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchFloatingIPs()
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {!!deleteVisible && (
          <DeleteModal
            visible={!!deleteVisible}
            floatingIP={deleteVisible}
            onOk={() => {
              setDeleteVisible(undefined)
              fetchFloatingIPs()
            }}
            onCancel={() => {
              setDeleteVisible(undefined)
            }}
          />
        )}
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          dataCount={floatingIPs.length}
          dataSource={
            floatingIPs
              ? floatingIPs.filter(
                  (floatingIP) =>
                    findInString(floatingIP.floating_ip_address, keyword) ||
                    findInString(floatingIP.fixed_ip_address, keyword) ||
                    findInString(floatingIP.description, keyword)
                )
              : floatingIPs
          }
          onSearch={(value) => {
            setKeyword(value)
          }}
        />
      </>
    </PageLayout>
  )
}

export default FloatingIPList
