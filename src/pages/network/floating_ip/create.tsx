import { Form, notification } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createFloatingIP } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'

interface FloatingIPCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
}

const FloatingIPCreate: React.FC<FloatingIPCreateProps> = ({ onOk, visible, onCancel }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await createFloatingIP({ data: values })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'floating_ip.created' }, { name: values.description }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="floating_ip.create_title" />}
      formName="floating_ip_create_form"
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="floating_ip_create_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <FormInput
          required
          hasLabel
          name="description"
          type="textarea"
          rows={3}
          className="form_last_item"
          label={intl.formatMessage({ id: 'description' })}
          placeholder={intl.formatMessage({ id: 'description' })}
        />
      </Form>
    </ModalForm>
  )
}

export default FloatingIPCreate
