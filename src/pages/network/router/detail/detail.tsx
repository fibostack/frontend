import { Descriptions } from 'antd'
import { getRouter } from 'api'
import { BooleanStatus, Loader } from 'components'
import { Router } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { DateFormat } from 'utils'
import Status from '../status'

interface Props {
  router: Router
}

const Detail = ({ router }: Props) => {
  const _isMounted = useRef(true)
  const [routerDetail, setRouterDetail] = useState<Router>()
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    return () => {
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    fetchRouters(router.id)
  }, [router.id])

  const fetchRouters = async (netID: string) => {
    setLoading(true)
    const net = (await getRouter({ data: { router_id: netID } })) as Router
    if (_isMounted.current) {
      setRouterDetail(net)
      setLoading(false)
    }
  }

  return (
    <Loader loading={loading}>
      {routerDetail && (
        <Descriptions column={{ sm: 2, xs: 1 }}>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="name" />
              </strong>
            }
          >
            {routerDetail.name || '-'}
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="id" />
              </strong>
            }
          >
            {routerDetail.id || '-'}
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="create_at" />
              </strong>
            }
          >
            {DateFormat(routerDetail.created_at) || '-'}
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="updated_at" />
              </strong>
            }
          >
            {DateFormat(routerDetail.updated_at) || '-'}
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="project_id" />
              </strong>
            }
          >
            {routerDetail.project_id || '-'}
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="status" />
              </strong>
            }
          >
            <Status status={routerDetail.status} />
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <strong>
                <FormattedMessage id="router.admin_state_up" />
              </strong>
            }
          >
            <BooleanStatus status={routerDetail.admin_state_up} />
          </Descriptions.Item>
        </Descriptions>
      )}
    </Loader>
  )
}

export default Detail
