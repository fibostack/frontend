import { Form, notification, Select } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createInterface } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'
import { Subnet } from 'models'

interface InterfaceCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  sbs: Subnet[]
  routerId: string
}

const InterfaceCreate: React.FC<InterfaceCreateProps> = ({ visible, onOk, onCancel, sbs, routerId }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.router_id = routerId
    const success = await createInterface({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'router.interface_created' }, { name: values.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'router.interface_create_title' })}
      formName="router_interface_create_from"
      okText={intl.formatMessage({ id: 'create' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="router_interface_create_from" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <FormInput required hasLabel label={intl.formatMessage({ id: 'network.subnets' })} name="subnet_id">
          <Select placeholder={<FormattedMessage id="network.subnets" />}>
            {sbs.map((subnet) => {
              return (
                <Select.Option key={subnet.name + subnet.id} value={subnet.id}>
                  {subnet.name}: {subnet.cidr}
                </Select.Option>
              )
            })}
          </Select>
        </FormInput>
      </Form>
    </ModalForm>
  )
}

export default InterfaceCreate
