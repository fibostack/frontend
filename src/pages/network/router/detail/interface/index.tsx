import { DeleteOutlined } from '@ant-design/icons'
import { Button, Tooltip } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listAllSubnet, listInterface } from 'api'
import { BooleanStatus, PageLayout, Table } from 'components'
import { Interface, Subnet } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import CreateModal from './create'
import DeleteModal from './delete'
import Status from './status'

interface InterfaceProps {
  routerID: string
}

const InterfaceList: React.FC<InterfaceProps> = ({ routerID }) => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [interfaces, setInterfaces] = useState<Interface[]>([])
  const [subnets, setSubnets] = useState<Subnet[]>([])
  const [selectedRows, setSelectedRows] = useState<Interface[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [deleteVisible, setDeleteVisible] = useState<Interface>()

  useEffect(() => {
    return () => {
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    fetchDatas(routerID, true)
  }, [routerID])

  const fetchDatas = async (nID: string, init: boolean = false) => {
    setLoading(true)
    const [interfaceResponse, SubnetResponse] = await Promise.all([
      listInterface({ data: { router_id: nID } }),
      init && listAllSubnet(),
    ])
    const ps = interfaceResponse as Interface[]
    if (_isMounted.current) {
      setInterfaces(ps)
      if (init) {
        const sbs = SubnetResponse as Subnet[]
        setSubnets(sbs)
      }
      setLoading(false)
    }
  }

  const columns: ColumnType<Interface>[] = [
    {
      title: intl.formatMessage({ id: 'network.fixed_ips' }),
      dataIndex: 'fixed_ips',
      key: 'fixed_ips',
      width: 200,
      render: (value) => (
        <ul>
          {value.map((i: any) => (
            <li>{i.ip_address}</li>
          ))}
        </ul>
      ),
    },
    {
      title: intl.formatMessage({ id: 'status' }),
      dataIndex: 'status',
      key: 'status',
      width: 150,
      render: (value: string) => <Status status={value} />,
    },
    {
      title: intl.formatMessage({ id: 'network.admin_state_up' }),
      dataIndex: 'admin_state_up',
      key: 'admin_state_up',
      width: 150,
      render: (value: boolean) => <BooleanStatus status={value} />,
    },
    {
      title: intl.formatMessage({ id: 'action' }),
      key: 'action',
      width: 80,
      render: (_value, record) => (
        <Tooltip placement="topRight" title={intl.formatMessage({ id: 'delete' })}>
          <Button
            danger
            className="onlyIcon"
            icon={<DeleteOutlined />}
            onClick={() => {
              setDeleteVisible(record)
            }}
          />
        </Tooltip>
      ),
    },
  ]

  return (
    <PageLayout
      noCard
      loading={loading}
      title={intl.formatMessage({ id: 'router.interfaces' })}
      fetchAction={() => {
        fetchDatas(routerID)
      }}
      // actions={actions}
      createAction={() => {
        setCreateVisible(true)
      }}
    >
      <>
        {createVisible && (
          <CreateModal
            sbs={subnets}
            routerId={routerID}
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchDatas(routerID)
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {/* {!!updateVisible && (
          <UpdateModal
            // subnets={subnets}
            inter={updateVisible}
            visible={!!updateVisible}
            onOk={() => {
              setUpdateVisible(undefined)
              fetchDatas(routerID)
            }}
            onCancel={() => {
              setUpdateVisible(undefined)
            }}
          />
        )} */}
        {deleteVisible && (
          <DeleteModal
            routerId={routerID}
            interfaces={deleteVisible}
            visible={!!deleteVisible}
            onOk={() => {
              setDeleteVisible(undefined)
              fetchDatas(routerID)
            }}
            onCancel={() => {
              setDeleteVisible(undefined)
            }}
          />
        )}
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          dataSource={interfaces}
          onRowSelected={setSelectedRows}
        />
      </>
    </PageLayout>
  )
}

export default InterfaceList
