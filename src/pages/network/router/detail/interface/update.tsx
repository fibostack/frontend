import { Form, notification, Select, Radio } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { updateInterface } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { Interface, SecurityGroup } from 'models'
import React, { useState } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'

interface InterfaceUpdateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  inter: Interface
  sgs: SecurityGroup[]
}

const InterfaceUpdate: React.FC<InterfaceUpdateProps> = ({ visible, onOk, onCancel, inter, sgs }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.interface_id = inter.id
    if (!values.desc) {
      values.desc = ''
    }
    const success = await updateInterface({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'router.interface_updated' }, { name: values.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.keyPairName)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'router.interface_update_title' }, { name: inter.name })}
      formName="router_interface_update_from"
      okText={intl.formatMessage({ id: 'update' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form
        form={form}
        name="router_interface_update_from"
        layout="vertical"
        labelAlign="left"
        onFinish={handleFinish}
        initialValues={{
          name: inter.name,
          desc: inter.description,
          sec_group_ids: inter.security_groups,
          state: inter.admin_state_up,
        }}
      >
        <FormInput
          required
          hasLabel
          name="name"
          type="input"
          label={intl.formatMessage({ id: 'name' })}
          placeholder={intl.formatMessage({ id: 'name' })}
        />
        <FormInput
          hasLabel
          name="desc"
          label={intl.formatMessage({ id: 'description' })}
          type="textarea"
          placeholder={intl.formatMessage({ id: 'description' })}
        />
        <FormInput
          required
          hasLabel
          label={intl.formatMessage({ id: 'instance.security_groups' })}
          name="sec_group_ids"
        >
          <Select mode="multiple" placeholder={<FormattedMessage id="instance.security_groups" />}>
            {sgs.map((securitGroup) => {
              return (
                <Select.Option key={securitGroup.name + securitGroup.ID} value={securitGroup.ID}>
                  {securitGroup.name}
                </Select.Option>
              )
            })}
          </Select>
        </FormInput>
        <FormInput required hasLabel name="state" label={intl.formatMessage({ id: 'router.admin_state_up' })}>
          <Radio.Group buttonStyle="solid">
            <Radio.Button value>{intl.formatMessage({ id: 'yes' })}</Radio.Button>
            <Radio.Button value={false}>{intl.formatMessage({ id: 'no' })}</Radio.Button>
          </Radio.Group>
        </FormInput>
      </Form>
    </ModalForm>
  )
}

export default InterfaceUpdate
