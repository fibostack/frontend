import { notification } from 'antd'
import { deleteInterface } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { Interface } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface InterfaceDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  interfaces: Interface
  routerId: string
}

const InterfaceDelete: React.FC<InterfaceDeleteProps> = ({ visible, onOk, onCancel, interfaces, routerId }) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [vis, setVis] = useState<boolean>(visible)

  // let deleteNames = interface.cidr
  // deleteNames = deleteNames.substring(0, deleteNames.length - 2)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteInterface({
      data: { interface_id: interfaces.id, router_id: routerId },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'router.interface_deleted' }, { name: interfaces.id }),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'router.interface_delete_title' })}
      okText={intl.formatMessage({ id: 'delete' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      selectedNames={interfaces.id}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default InterfaceDelete
