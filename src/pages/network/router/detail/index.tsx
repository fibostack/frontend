import { OneToOneOutlined, ReadOutlined } from '@ant-design/icons'
import { Tabs } from 'antd'
import { DetailModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { useWindow } from 'hooks'
import { Router } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { isEmptyString } from 'utils'
import Detail from './detail'
import Interface from './interface'

interface RouterDetailProps {
  visible: boolean
  onOk: Function
  router: Router
}

const RouterDetail: React.FC<RouterDetailProps> = ({ visible, onOk, router }) => {
  const intl = useIntl()
  const [width] = useWindow()
  const [vis, setVis] = useState<boolean>(visible)

  return (
    <DetailModal
      visible={vis}
      width={width > 1200 ? '80%' : '100%'}
      title={
        <FormattedMessage
          id="router.detail_title"
          values={{
            name: isEmptyString(router.name) ? router.name : router.id,
          }}
        />
      }
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onOk()
        }, CloseAwaitMS)
      }}
    >
      <Tabs defaultActiveKey="detail" style={{ marginTop: -24 }}>
        <Tabs.TabPane
          tab={
            <>
              <ReadOutlined />
              {intl.formatMessage({ id: 'detail' })}
            </>
          }
          key="detail"
          className="p_t_1"
        >
          <Detail router={router} />
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <>
              <OneToOneOutlined />
              {intl.formatMessage({ id: 'router.interface' })}
            </>
          }
          key="interfaces"
          className="p_t_1"
        >
          <Interface routerID={router.id} />
        </Tabs.TabPane>
      </Tabs>
    </DetailModal>
  )
}

export default RouterDetail
