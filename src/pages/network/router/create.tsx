import { Form, notification, Select } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createRouter } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'
import { Network } from 'models'

interface RouterCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  networks: Network[]
}

const RouterCreate: React.FC<RouterCreateProps> = ({ visible, onOk, onCancel, networks }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    if (!values.desc) {
      values.desc = ''
    }
    const success = await createRouter({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'router.created' }, { name: values.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'router.create_title' })}
      formName="router_create_form"
      okText={intl.formatMessage({ id: 'create' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="router_create_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <FormInput
          required
          hasLabel
          name="name"
          type="input"
          label={intl.formatMessage({ id: 'name' })}
          placeholder={intl.formatMessage({ id: 'name' })}
        />
        <FormInput
          hasLabel
          name="desc"
          label={intl.formatMessage({ id: 'description' })}
          type="textarea"
          placeholder={intl.formatMessage({ id: 'description' })}
        />
        <FormInput required hasLabel name="ext_network_id" label={intl.formatMessage({ id: 'router.ext_network_id' })}>
          <Select placeholder={intl.formatMessage({ id: 'router.ext_network_id' })}>
            {networks.map((network) => (
              <Select.Option value={network.id}>{network.name}</Select.Option>
            ))}
          </Select>
        </FormInput>
      </Form>
    </ModalForm>
  )
}

export default RouterCreate
