import { notification } from 'antd'
import { deleteRouter } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { Router } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface RouterDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  router: Router
}

const RouterDelete: React.FC<RouterDeleteProps> = ({ visible, onOk, onCancel, router }) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [vis, setVis] = useState<boolean>(visible)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteRouter({
      data: { router_id: router.id },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'router.deleted' }, { name: router.name }),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'router.delete_title' })}
      okText={intl.formatMessage({ id: 'delete' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      selectedNames={router.name}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default RouterDelete
