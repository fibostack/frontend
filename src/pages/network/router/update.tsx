import { Form, notification, Radio, Select } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { updateRouter } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { Router, Network } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface RouterUpdateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  router: Router
  networks: Network[]
}

const RouterUpdate: React.FC<RouterUpdateProps> = ({ visible, onOk, onCancel, router, networks }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.router_id = router.id
    if (!values.desc) {
      values.desc = ''
    }
    const success = await updateRouter({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'router.updated' }, { name: values.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.keyPairName)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'router.update_title' }, { name: router.name })}
      formName="router_update_form"
      okText={intl.formatMessage({ id: 'update' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form
        form={form}
        name="router_update_form"
        layout="vertical"
        labelAlign="left"
        onFinish={handleFinish}
        initialValues={{
          name: router.name,
          desc: router.description,
          ext_network_id: router.external_gateway_info.network_id,
          distributed: router.distributed,
          state: router.admin_state_up,
        }}
      >
        <FormInput
          required
          hasLabel
          name="name"
          type="input"
          label={intl.formatMessage({ id: 'name' })}
          placeholder={intl.formatMessage({ id: 'name' })}
        />
        <FormInput
          required
          hasLabel
          name="desc"
          label={intl.formatMessage({ id: 'description' })}
          type="textarea"
          placeholder={intl.formatMessage({ id: 'description' })}
        />
        <FormInput required hasLabel name="ext_network_id" label={intl.formatMessage({ id: 'router.ext_network_id' })}>
          <Select placeholder={intl.formatMessage({ id: 'router.ext_network_id' })}>
            {networks.map((network) => (
              <Select.Option value={network.id}>{network.name}</Select.Option>
            ))}
          </Select>
        </FormInput>
        <FormInput required hasLabel name="distributed" label={intl.formatMessage({ id: 'router.distributed' })}>
          <Radio.Group buttonStyle="solid">
            <Radio.Button value>{intl.formatMessage({ id: 'yes' })}</Radio.Button>
            <Radio.Button value={false}>{intl.formatMessage({ id: 'no' })}</Radio.Button>
          </Radio.Group>
        </FormInput>
        <FormInput required hasLabel name="state" label={intl.formatMessage({ id: 'admin_state' })}>
          <Radio.Group buttonStyle="solid">
            <Radio.Button value>{intl.formatMessage({ id: 'yes' })}</Radio.Button>
            <Radio.Button value={false}>{intl.formatMessage({ id: 'no' })}</Radio.Button>
          </Radio.Group>
        </FormInput>
      </Form>
    </ModalForm>
  )
}

export default RouterUpdate
