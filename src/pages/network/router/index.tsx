import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import { Button, Col, Row, Tooltip } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listNetwork, listRouter } from 'api'
import { BooleanStatus, PageLayout, Table } from 'components'
import { ExternalGatewayInfo, Network, Router } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import CreateModal from './create'
import DeleteModal from './delete'
import DetailModal from './detail'
import Status from './status'
import UpdateModal from './update'

interface RouterProps {}

const RouterList: React.FC<RouterProps> = () => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [routers, setRouters] = useState<Router[]>([])
  const [networks, setNetworks] = useState<Network[]>([])
  const [selectedRows, setSelectedRows] = useState<Router[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [updateVisible, setUpdateVisible] = useState<Router>()
  const [deleteVisible, setDeleteVisible] = useState<Router>()
  const [detailVisible, setDetailVisible] = useState<Router>()

  useEffect(() => {
    fetchDatas(true)
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchDatas = async (init: boolean = false) => {
    setLoading(true)
    const [routerResponse, networkResponse] = await Promise.all([listRouter({}), init && listNetwork({})])
    const rs = routerResponse as Router[]
    if (_isMounted.current) {
      setRouters(rs)
      if (init) {
        const ns = networkResponse as Network[]
        setNetworks(ns)
      }
      setLoading(false)
    }
  }

  const columns: ColumnType<Router>[] = [
    {
      key: 'name',
      width: 300,
      title: intl.formatMessage({ id: 'name' }),
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (router: Router) => (
        <Button
          type="link"
          onClick={() => {
            setDetailVisible(router)
          }}
        >
          {router.name}
        </Button>
      ),
    },
    {
      key: 'description',
      width: 400,
      title: intl.formatMessage({ id: 'description' }),
      dataIndex: 'description',
      render: (val) => val || '-',
    },
    {
      title: intl.formatMessage({ id: 'router.ext_network_id' }),
      dataIndex: 'external_gateway_info',
      key: 'external_gateway_info',
      width: 200,
      render: (value: ExternalGatewayInfo) => networks.find((item) => item.id === value.network_id)?.name,
    },
    {
      title: intl.formatMessage({ id: 'status' }),
      dataIndex: 'status',
      key: 'status',
      width: 100,
      render: (value: string) => <Status status={value} />,
    },
    {
      title: intl.formatMessage({ id: 'admin_state' }),
      dataIndex: 'admin_state_up',
      key: 'admin_state_up',
      width: 100,
      render: (value: boolean) => <BooleanStatus status={value} />,
    },
    {
      title: intl.formatMessage({ id: 'action' }),
      key: 'action',
      width: 120,
      render: (_value, record) => (
        <Row gutter={8}>
          <Col>
            <Tooltip placement="topRight" title={intl.formatMessage({ id: 'update' })}>
              <Button
                type="primary"
                className="onlyIcon"
                icon={<EditOutlined />}
                onClick={() => {
                  setUpdateVisible(record)
                }}
              />
            </Tooltip>
          </Col>
          <Col>
            <Tooltip placement="topRight" title={intl.formatMessage({ id: 'delete' })}>
              <Button
                danger
                className="onlyIcon"
                icon={<DeleteOutlined />}
                onClick={() => {
                  setDeleteVisible(record)
                }}
              />
            </Tooltip>
          </Col>
        </Row>
      ),
    },
  ]

  return (
    <PageLayout
      loading={loading}
      title={intl.formatMessage({ id: 'menu.routers' })}
      fetchAction={() => {
        fetchDatas()
      }}
      createAction={() => {
        setCreateVisible(true)
      }}
    >
      <>
        {createVisible && (
          <CreateModal
            networks={networks}
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchDatas()
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {!!updateVisible && (
          <UpdateModal
            networks={networks}
            router={updateVisible}
            visible={!!updateVisible}
            onOk={() => {
              setUpdateVisible(undefined)
              fetchDatas()
            }}
            onCancel={() => {
              setUpdateVisible(undefined)
            }}
          />
        )}
        {!!deleteVisible && (
          <DeleteModal
            router={deleteVisible}
            visible={!!deleteVisible}
            onOk={() => {
              setDeleteVisible(undefined)
              fetchDatas()
            }}
            onCancel={() => {
              setDeleteVisible(undefined)
            }}
          />
        )}
        {!!detailVisible && (
          <DetailModal
            router={detailVisible}
            visible={!!detailVisible}
            onOk={() => {
              setDetailVisible(undefined)
            }}
          />
        )}
        <Table rowKey="id" loading={loading} columns={columns} dataSource={routers} onRowSelected={setSelectedRows} />
      </>
    </PageLayout>
  )
}

export default RouterList
