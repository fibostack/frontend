import { notification } from 'antd'
import { deleteSecurityGroup } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { SecurityGroup } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { isEmptyString } from 'utils'

interface SecurityGroupDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  securityGroups: SecurityGroup[]
}

const SecurityGroupDelete: React.FC<SecurityGroupDeleteProps> = ({ onOk, visible, onCancel, securityGroups }) => {
  const intl = useIntl()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const deleteIDs = securityGroups.reduce<string[]>((acc: string[], sg: SecurityGroup) => {
    return [...acc, sg.ID]
  }, [])

  let deleteNames = securityGroups.reduce<string>((acc: string, sg: SecurityGroup) => {
    return `${acc}${isEmptyString(sg.name) ? sg.name : sg.ID}, `
  }, '')
  deleteNames = deleteNames.substring(0, deleteNames.length - 2)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteSecurityGroup({
      data: { sgID: deleteIDs },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${deleteNames} security groups is deleted.`,
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="security_group.delete_title" />}
      selectedNames={deleteNames}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default SecurityGroupDelete
