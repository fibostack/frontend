import { notification } from 'antd'
import { deleteSecurityGroupRule } from 'api'
import { DeleteModal } from 'components'
import { Rule, RuleDirection } from 'models'
import React, { useState } from 'react'
import { isEmptyString } from 'utils'
import { CloseAwaitMS } from 'configs'
import { useIntl } from 'react-intl'

interface RuleDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  type: RuleDirection
  rules: Rule[]
}

const RuleDelete: React.FC<RuleDeleteProps> = ({ onOk, visible, onCancel, type, rules }) => {
  const intl = useIntl()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const deleteIDs = rules.reduce<string[]>((acc: string[], rule: Rule) => {
    return [...acc, rule.ID]
  }, [])

  let deleteNames = rules.reduce<string>((acc: string, rule: Rule) => {
    return `${acc}${isEmptyString(rule.description) ? rule.description : rule.ID}, `
  }, '')
  deleteNames = deleteNames.substring(0, deleteNames.length - 2)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteSecurityGroupRule({
      data: { rule_ids: deleteIDs },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${deleteNames} rules is deleted.`,
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={
        type === 'ingress'
          ? intl.formatMessage({ id: 'security_group.rules.in_delete_title' })
          : intl.formatMessage({ id: 'security_group.rules.out_delete_title' })
      }
      selectedNames={deleteNames}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default RuleDelete
