import { Protocol, ProtocolTempName } from 'models'

interface Protocols {
  name: ProtocolTempName
  protocol: Protocol
}

const protocols: Protocols[] = [
  {
    name: 'tcp',
    protocol: {
      protocol: 'tcp',
      portRangeMin: -1,
      portRangeMax: -1,
    },
  },
  {
    name: 'udp',
    protocol: {
      protocol: 'udp',
      portRangeMin: -1,
      portRangeMax: -1,
    },
  },
  {
    name: 'icmp',
    protocol: {
      protocol: 'icmp',
      portRangeMin: -1,
      portRangeMax: -1,
    },
  },
  {
    name: 'all_icmp',
    protocol: {
      protocol: 'icmp',
      portRangeMin: 0,
      portRangeMax: 0,
    },
  },
  {
    name: 'all_tcp',
    protocol: {
      protocol: 'tcp',
      portRangeMin: 0,
      portRangeMax: 0,
    },
  },
  {
    name: 'all_udp',
    protocol: {
      protocol: 'udp',
      portRangeMin: 0,
      portRangeMax: 0,
    },
  },
  {
    name: 'dns',
    protocol: {
      protocol: 'udp',
      portRangeMin: 53,
      portRangeMax: 53,
    },
  },
  {
    name: 'http',
    protocol: {
      protocol: 'tcp',
      portRangeMin: 80,
      portRangeMax: 80,
    },
  },
  {
    name: 'https',
    protocol: {
      protocol: 'tcp',
      portRangeMin: 443,
      portRangeMax: 443,
    },
  },
  {
    name: 'imap',
    protocol: {
      protocol: 'tcp',
      portRangeMin: 143,
      portRangeMax: 143,
    },
  },
  {
    name: 'imaps',
    protocol: {
      protocol: 'tcp',
      portRangeMin: 993,
      portRangeMax: 993,
    },
  },
  {
    name: 'ldap',
    protocol: {
      protocol: 'tcp',
      portRangeMin: 389,
      portRangeMax: 389,
    },
  },
  {
    name: 'ms_sql',
    protocol: {
      protocol: 'tcp',
      portRangeMin: 1433,
      portRangeMax: 1433,
    },
  },
  {
    name: 'mysql',
    protocol: {
      protocol: 'tcp',
      portRangeMin: 3306,
      portRangeMax: 3306,
    },
  },
  {
    name: 'pop3',
    protocol: {
      protocol: 'tcp',
      portRangeMin: 110,
      portRangeMax: 110,
    },
  },
  {
    name: 'pop3s',
    protocol: {
      protocol: 'tcp',
      portRangeMin: 995,
      portRangeMax: 995,
    },
  },
  {
    name: 'rdp',
    protocol: {
      protocol: 'tcp',
      portRangeMin: 3389,
      portRangeMax: 3389,
    },
  },
  {
    name: 'smtp',
    protocol: {
      protocol: 'tcp',
      portRangeMin: 25,
      portRangeMax: 25,
    },
  },
  {
    name: 'smtps',
    protocol: {
      protocol: 'tcp',
      portRangeMin: 465,
      portRangeMax: 465,
    },
  },
  {
    name: 'ssh',
    protocol: {
      protocol: 'tcp',
      portRangeMin: 22,
      portRangeMax: 22,
    },
  },
]

export default (name: string) => {
  return protocols.find((protocol) => protocol.name === name)?.protocol
}
