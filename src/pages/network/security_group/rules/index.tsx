import { DeleteOutlined, PlusOutlined } from '@ant-design/icons'
import { Button, Tabs } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listSecurityGroup } from 'api'
import { KeyValue, Ligther, Loader, PageLayout, Table } from 'components'
import { Rule, RuleDirection, SecurityGroup } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import { useParams } from 'react-router-dom'
import { findInString } from 'utils'
import CreateModal from './create'
import DeleteModal from './delete'
import styles from './styles.module.scss'

interface RuleProps {}

const RuleList: React.FC<RuleProps> = () => {
  const { id } = useParams()
  const intl = useIntl()
  const _isMounted = useRef<boolean>(true)
  const [keyword, setKeyword] = useState<string>('')
  const [activeTab, setActiveTab] = useState<string>('1')
  const [loading, setLoading] = useState<boolean>(true)
  const [securityGroup, setSecurityGroup] = useState<SecurityGroup>()
  const [securityGroups, setSecurityGroups] = useState<SecurityGroup[]>()
  const [outbounds, setOutbounds] = useState<Rule[]>([])
  const [inbounds, setInbounds] = useState<Rule[]>([])
  const [inSelectedRows, setInSelectedRows] = useState<Rule[]>([])
  const [outSelectedRows, setOutSelectedRows] = useState<Rule[]>([])
  const [createVisible, setCreateVisible] = useState<RuleDirection>()
  const [inDeleteVisible, setInDeleteVisible] = useState<boolean>(false)
  const [outDeleteVisible, setOutDeleteVisible] = useState<boolean>(false)

  useEffect(() => {
    fetchSecurityGroup(id)
    return () => {
      _isMounted.current = false
    }
  }, [id])

  const fetchSecurityGroup = async (sgID?: string) => {
    setLoading(true)
    const sgs = (await listSecurityGroup({})) as SecurityGroup[]
    const sg = sgs.find((item) => item.ID === sgID)
    if (_isMounted.current && sg) {
      setSecurityGroups(sgs)
      setOutbounds(sg.rules.filter((rule) => rule.Direction === 'egress'))
      setInbounds(sg.rules.filter((rule) => rule.Direction === 'ingress'))
      setSecurityGroup(sg)
      setLoading(false)
    }
  }

  const getPortRangeString = (rule: Rule) => {
    if (rule.port_range_max === 0) return 'All'
    if (rule.port_range_min === rule.port_range_max) return rule.port_range_min.toString()
    return `${rule.port_range_min} - ${rule.port_range_max}`
  }

  const getColumns = (type: RuleDirection) => {
    const tmp: ColumnType<Rule>[] = [
      {
        title: intl.formatMessage({ id: 'security_group.rules.ether_type' }),
        key: 'ethertype',
        filters: [
          { text: 'IPv4', value: 'IPv4' },
          { text: 'IPv6', value: 'IPv6' },
        ],
        onFilter: (value, record) => record.ethertype === value,
        dataIndex: 'ethertype',
        width: 150,
        ellipsis: true,
        sorter: (a, b) => a.ethertype.localeCompare(b.ethertype),
        render: (value: string) => <Ligther keywords={[keyword]} source={value} /> || '-',
      },
      {
        title: intl.formatMessage({ id: 'security_group.rules.protocol' }),
        key: 'protocol',
        filters: [
          { text: 'tcp', value: 'tcp' },
          { text: 'udp', value: 'udp' },
          { text: 'icmp', value: 'icmp' },
        ],
        onFilter: (value, record) => record.Protocol === value,
        dataIndex: 'Protocol',
        width: 150,
        ellipsis: true,
        sorter: (a, b) => a.Protocol.localeCompare(b.Protocol),
        render: (value: string) => <Ligther keywords={[keyword]} source={value} /> || '-',
      },
      {
        title: intl.formatMessage({ id: 'security_group.rules.port_range' }),
        key: 'port_range',
        width: 200,
        ellipsis: true,
        sorter: (a: Rule, b: Rule) => a.port_range_max - b.port_range_max,
        render: (rule: Rule) => {
          const tmpVal = getPortRangeString(rule)
          return <Ligther keywords={[keyword]} source={tmpVal} />
        },
      },
    ]

    if (type === 'ingress') {
      tmp.push({
        title: intl.formatMessage({ id: 'security_group.rules.source' }),
        key: 'CIDR',
        dataIndex: 'CIDR',
        width: 200,
        ellipsis: true,
        sorter: (a, b) => a.CIDR.localeCompare(b.CIDR),
        render: (value: string) => <Ligther keywords={[keyword]} source={value} /> || '-',
      })
    }

    tmp.push({
      title: intl.formatMessage({ id: 'description' }),
      key: 'description',
      dataIndex: 'description',
      width: 400,
      ellipsis: true,
      sorter: (a, b) => a.CIDR.localeCompare(b.CIDR),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} /> || '-',
    })
    return tmp
  }

  const renderTableTitle = (type: RuleDirection) => {
    return (
      <div className="flex row js ac">
        <Button
          type="primary"
          onClick={() => {
            setCreateVisible(type)
          }}
          className="item"
        >
          <PlusOutlined />
          {type === 'ingress'
            ? intl.formatMessage({ id: 'security_group.rules.in_create_title' })
            : intl.formatMessage({
                id: 'security_group.rules.out_create_title',
              })}
        </Button>
        <Button
          onClick={() => {
            if (type === 'ingress') {
              setInDeleteVisible(true)
            } else {
              setOutDeleteVisible(true)
            }
          }}
          disabled={type === 'ingress' ? inSelectedRows.length === 0 : outSelectedRows.length === 0}
          className="item"
        >
          <DeleteOutlined />
          {type === 'ingress'
            ? intl.formatMessage({ id: 'security_group.rules.in_delete_title' })
            : intl.formatMessage({
                id: 'security_group.rules.out_delete_title',
              })}
        </Button>
      </div>
    )
  }

  return (
    <PageLayout
      loading={loading}
      title={intl.formatMessage(
        { id: 'security_group.rules.title' },
        { name: securityGroup ? securityGroup.name : '' }
      )}
      fetchAction={() => fetchSecurityGroup(id)}
    >
      <>
        {!!createVisible && (
          <CreateModal
            id={id || ''}
            securityGroups={securityGroups || []}
            visible={!!createVisible}
            type={createVisible}
            onOk={() => {
              setCreateVisible(undefined)
              fetchSecurityGroup(id)
            }}
            onCancel={() => {
              setCreateVisible(undefined)
            }}
          />
        )}
        {inDeleteVisible && inSelectedRows.length > 0 && (
          <DeleteModal
            type="ingress"
            visible={inDeleteVisible}
            rules={inSelectedRows}
            onOk={() => {
              setInDeleteVisible(false)
              fetchSecurityGroup(id)
            }}
            onCancel={() => {
              setInDeleteVisible(false)
            }}
          />
        )}
        {outDeleteVisible && outSelectedRows.length > 0 && (
          <DeleteModal
            type="egress"
            visible={outDeleteVisible}
            rules={outSelectedRows}
            onOk={() => {
              setOutDeleteVisible(false)
              fetchSecurityGroup(id)
            }}
            onCancel={() => {
              setOutDeleteVisible(false)
            }}
          />
        )}
        <Loader loading={loading}>
          <div className={styles.detail_container}>
            <KeyValue title={intl.formatMessage({ id: 'description' })} value={securityGroup?.description} />
          </div>

          <Tabs
            type="card"
            activeKey={activeTab}
            className="u_tab"
            onChange={(val) => {
              setActiveTab(val)
              setKeyword('')
            }}
          >
            <Tabs.TabPane
              tab={`${intl.formatMessage({ id: 'security_group.rules.inbound' })} (${
                securityGroup?.rules.filter((rule) => rule.Direction === 'ingress').length
              })`}
              key="1"
              className={styles.tab_content}
            >
              <Table
                rowKey="ID"
                loading={loading}
                columns={getColumns('ingress')}
                onRowSelected={setInSelectedRows}
                title={() => renderTableTitle('ingress')}
                dataCount={inbounds.length}
                dataSource={
                  keyword
                    ? inbounds.filter(
                        (inbound) =>
                          findInString(inbound.description, keyword) ||
                          findInString(inbound.CIDR, keyword) ||
                          findInString(inbound.Protocol, keyword) ||
                          findInString(inbound.ethertype, keyword) ||
                          findInString(getPortRangeString(inbound), keyword)
                      )
                    : inbounds
                }
                onSearch={(value) => {
                  setKeyword(value)
                }}
              />
            </Tabs.TabPane>
            <Tabs.TabPane
              tab={`${intl.formatMessage({ id: 'security_group.rules.outbound' })} (${
                securityGroup?.rules.filter((rule) => rule.Direction === 'egress').length
              })`}
              key="2"
              className={styles.tab_content}
            >
              <Table
                rowKey="ID"
                loading={loading}
                columns={getColumns('egress')}
                onRowSelected={setOutSelectedRows}
                title={() => renderTableTitle('egress')}
                dataCount={outbounds.length}
                dataSource={
                  keyword
                    ? outbounds.filter(
                        (outbound) =>
                          findInString(outbound.description, keyword) ||
                          findInString(outbound.Protocol, keyword) ||
                          findInString(outbound.ethertype, keyword) ||
                          findInString(getPortRangeString(outbound), keyword)
                      )
                    : outbounds
                }
                onSearch={(value) => {
                  setKeyword(value)
                }}
              />
            </Tabs.TabPane>
          </Tabs>
        </Loader>
      </>
    </PageLayout>
  )
}

export default RuleList
