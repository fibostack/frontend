import { QuestionCircleOutlined } from '@ant-design/icons'
import { Col, Descriptions, Form, Input, InputNumber, notification, Row, Select, Tooltip } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createSecurityGroupRule } from 'api'
import cssClass from 'classnames'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { useWindow } from 'hooks'
import { RuleDirection, SecurityGroup } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'
import { calcModalWidth } from 'utils'
import CalcProtocol from './portocols'

interface RuleCreateProps {
  visible: boolean
  id: string
  type: RuleDirection
  onOk: Function
  onCancel: Function
  securityGroups: SecurityGroup[]
}

const RuleCreate: React.FC<RuleCreateProps> = ({ onOk, visible, type, id, onCancel, securityGroups }) => {
  const intl = useIntl()
  const [width] = useWindow()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)
  const [protocol, setProtocol] = useState<string>('tcp')
  const [portType, setPortType] = useState<string>('port')

  const handleFinish = async (values: Store) => {
    setLoading(true)
    let tempProtocol = values.protocol as string
    let portRangeMin = 0
    let portRangeMax = 0
    if (tempProtocol === 'icmp') {
      portRangeMin = values.code as number
      portRangeMax = values.type as number
    } else if (values.portType === 'all') {
      portRangeMin = 0
      portRangeMax = 0
    } else if (values.portType === 'range') {
      portRangeMin = values.fromPort as number
      portRangeMax = values.toPort as number
    } else if (values.portType === 'port') {
      portRangeMin = values.port as number
      portRangeMax = values.port as number
    }
    const tmp = CalcProtocol(tempProtocol)
    if (tmp) {
      tempProtocol = tmp.protocol
      if (tmp.portRangeMin >= 0) portRangeMin = tmp.portRangeMin
      if (tmp.portRangeMax >= 0) portRangeMax = tmp.portRangeMax
    }
    const request = {
      direction: type,
      secGroupID: id,
      description: values.description,
      portRangeMin,
      portRangeMax,
      protocol: tempProtocol,
      remoteGroupID: '',
    }
    const success = await createSecurityGroupRule({ data: request })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${values.description} rule is created.`,
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      width={calcModalWidth(width)}
      visible={vis}
      loading={loading}
      title={
        type === 'ingress'
          ? intl.formatMessage({ id: 'security_group.rules.in_create_title' })
          : intl.formatMessage({ id: 'security_group.rules.out_create_title' })
      }
      formName="rule_create_form"
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Row gutter={24}>
        <Col xs={24} sm={24} xl={12} className="m_b_2">
          <Form
            form={form}
            name="rule_create_form"
            layout="vertical"
            labelAlign="left"
            initialValues={{
              protocol: 'tcp',
              portType: 'port',
              Remote: 'cidr',
              cidr: '0.0.0.0/0',
              secID: [],
            }}
            onFinish={handleFinish}
          >
            <FormInput required hasLabel label={intl.formatMessage({ id: 'rule' })} name="protocol">
              <Select
                placeholder={intl.formatMessage({ id: 'rule' })}
                onChange={(value) => {
                  setProtocol(value as string)
                }}
              >
                <Select.Option value="tcp">Custom TCP Rule</Select.Option>
                <Select.Option value="udp">Custom UDP Rule</Select.Option>
                <Select.Option value="icmp">Custom ICMP Rule</Select.Option>
                <Select.Option value="all_icmp">All ICMP</Select.Option>
                <Select.Option value="all_tcp">All TCP</Select.Option>
                <Select.Option value="all_udp">All UDP</Select.Option>
                <Select.Option value="dns">DNS</Select.Option>
                <Select.Option value="http">HTTP</Select.Option>
                <Select.Option value="https">HTTPS</Select.Option>
                <Select.Option value="imap">IMAP</Select.Option>
                <Select.Option value="imaps">IMAPS</Select.Option>
                <Select.Option value="ldap">LDAP</Select.Option>
                <Select.Option value="ms_sql">MS SQL</Select.Option>
                <Select.Option value="mysql">MYSQL</Select.Option>
                <Select.Option value="pop3">POP3</Select.Option>
                <Select.Option value="pop3s">POP3S</Select.Option>
                <Select.Option value="rdp">RDP</Select.Option>
                <Select.Option value="smtp">SMTP</Select.Option>
                <Select.Option value="smtps">SMTPS</Select.Option>
                <Select.Option value="ssh">SSH</Select.Option>
              </Select>
            </FormInput>
            <FormInput
              required
              hasLabel
              name="description"
              type="textarea"
              label={intl.formatMessage({ id: 'description' })}
              rows={3}
            />
            {(protocol === 'tcp' || protocol === 'udp') && (
              <FormInput
                required
                hasLabel
                name="portType"
                label={intl.formatMessage({
                  id: 'security_group.rules.open_port',
                })}
              >
                <Select
                  placeholder={intl.formatMessage({ id: 'security_group.rules.open_port' })}
                  onChange={(value) => {
                    setPortType(value as string)
                  }}
                >
                  <Select.Option value="port">Port</Select.Option>
                  <Select.Option value="range">Port Range</Select.Option>
                  <Select.Option value="all">All Port</Select.Option>
                </Select>
              </FormInput>
            )}
            {protocol === 'icmp' && (
              <>
                <FormInput
                  required
                  hasLabel
                  name="type"
                  type="number"
                  className="w-fill"
                  placeholder={intl.formatMessage({ id: 'type' })}
                  label={
                    <span>
                      {intl.formatMessage({ id: 'type' })} &nbsp;
                      <Tooltip
                        title={intl.formatMessage({
                          id: 'security_group.rules.type_info',
                        })}
                      >
                        <QuestionCircleOutlined />
                      </Tooltip>
                    </span>
                  }
                />
                <FormInput
                  required
                  hasLabel
                  name="code"
                  type="number"
                  className="w-fill"
                  placeholder={intl.formatMessage({ id: 'security_group.rules.code' })}
                  label={
                    <span>
                      {intl.formatMessage({
                        id: 'security_group.rules.code',
                      })}{' '}
                      &nbsp;
                      <Tooltip
                        title={intl.formatMessage({
                          id: 'security_group.rules.code_info',
                        })}
                      >
                        <QuestionCircleOutlined />
                      </Tooltip>
                    </span>
                  }
                />
              </>
            )}

            {portType === 'port' && protocol !== 'icmp' && (protocol === 'tcp' || protocol === 'udp') && (
              <FormInput
                hasLabel
                name="port"
                type="number"
                className="w-fill"
                placeholder={intl.formatMessage({ id: 'security_group.rules.port' })}
                label={
                  <span>
                    {intl.formatMessage({ id: 'security_group.rules.port' })}
                    &nbsp;
                    <Tooltip title={intl.formatMessage({ id: 'valid.port' })}>
                      <QuestionCircleOutlined />
                    </Tooltip>
                  </span>
                }
                rules={[
                  {
                    min: 1,
                    max: 65535,
                    type: 'number',
                    message: intl.formatMessage({ id: 'valid.port' }),
                  },
                ]}
              />
            )}

            {portType === 'range' && (protocol === 'tcp' || protocol === 'udp' || protocol === 'icmp') && (
              <Form.Item
                label={
                  <>
                    <span>
                      {intl.formatMessage({
                        id: 'security_group.rules.port_range',
                      })}
                      &nbsp;
                      <Tooltip title={intl.formatMessage({ id: 'valid.port' })}>
                        <QuestionCircleOutlined />
                      </Tooltip>
                    </span>
                  </>
                }
                required
              >
                <Input.Group compact>
                  <Form.Item
                    noStyle
                    name="fromPort"
                    rules={[
                      {
                        required: true,
                        type: 'number',
                        min: 1,
                        max: 65535,
                        message: intl.formatMessage({ id: 'valid.port' }),
                      },
                    ]}
                  >
                    <InputNumber placeholder="From" className="range_input" />
                  </Form.Item>
                  <Form.Item noStyle>
                    <InputNumber
                      style={{
                        width: '10%',
                        borderLeft: 0,
                        borderRight: 0,
                        pointerEvents: 'none',
                        backgroundColor: '#fff',
                      }}
                      placeholder="~"
                      disabled
                    />
                  </Form.Item>
                  <Form.Item
                    noStyle
                    name="toPort"
                    rules={[
                      {
                        required: true,
                        type: 'number',
                        min: 1,
                        max: 65535,
                        message: intl.formatMessage({ id: 'valid.port' }),
                      },
                    ]}
                  >
                    <InputNumber
                      className={cssClass('site-input-right range_right_input range_input')}
                      placeholder="To"
                    />
                  </Form.Item>
                </Input.Group>
              </Form.Item>
            )}

            {/* <Form.Item
              name="remote"
              label={
                <span>
                  Remote&nbsp;
                  <Tooltip title="To specify an allowed IP range, select 'CIDR'. To allow access from all members of another security group select 'Security Group'.">
                    <QuestionCircleOutlined />
                  </Tooltip>
                </span>
              }
              rules={[RequiredText]}
            >
              <Select
                disabled
                onChange={(value) => {
                  setRemoteType(value as string);
                }}
              >
                <Select.Option value="cidr">CIDR</Select.Option>
                <Select.Option value="sg">Security Group</Select.Option>
              </Select>
            </Form.Item>

            {remoteType === "cidr" ? (
              <Form.Item
                name="cidr"
                label={
                  <span>
                    CIDR&nbsp;
                    <Tooltip title="Classless Inter-Domain Routing (e.g. 192.168.0.0/24, or 2001:db8::/128)'.">
                      <QuestionCircleOutlined />
                    </Tooltip>
                  </span>
                }
              >
                <Input disabled />
              </Form.Item>
            ) : (
              <Form.Item
                name="secID"
                label={<span>Security Group&nbsp;</span>}
              >
                <Select style={{ width: "100%" }}>
                  {securityGroups.map((sg) => {
                    return (
                      <Select.Option key={sg.ID} value={sg.ID}>
                        {sg.name}
                      </Select.Option>
                    );
                  })}
                </Select>
              </Form.Item>
            )} */}
          </Form>
        </Col>
        <Col xs={24} sm={24} xl={12}>
          <Descriptions column={1} title={intl.formatMessage({ id: 'description' })}>
            <Descriptions.Item>{intl.formatMessage({ id: 'security_group.rules.create_info_1' })}</Descriptions.Item>
            <Descriptions.Item label={<strong>{intl.formatMessage({ id: 'rule' })}</strong>}>
              {intl.formatMessage({ id: 'security_group.rules.create_info_2' })}
            </Descriptions.Item>
            <Descriptions.Item label={<strong>{intl.formatMessage({ id: 'rule' })}</strong>}>
              {intl.formatMessage({ id: 'security_group.rules.create_info_3' })}
            </Descriptions.Item>
            <Descriptions.Item label={<strong>{intl.formatMessage({ id: 'remote' })}</strong>}>
              {intl.formatMessage({ id: 'security_group.rules.create_info_4' })}
            </Descriptions.Item>
          </Descriptions>
        </Col>
      </Row>
    </ModalForm>
  )
}

export default RuleCreate
