import { DeleteOutlined } from '@ant-design/icons'
import { ColumnType } from 'antd/es/table'
import { listSecurityGroup } from 'api'
import { Ligther, PageLayout, Table } from 'components'
import { MenuAction, SecurityGroup } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { Link } from 'react-router-dom'
import { findInString } from 'utils'
import CreateModal from './create'
import DeleteModal from './delete'

interface SecurityGroupProps {}

const SecurityGroupList: React.FC<SecurityGroupProps> = () => {
  const intl = useIntl()
  const _isMounted = useRef<boolean>(true)
  const [keyword, setKeyword] = useState<string>('')
  const [loading, setLoading] = useState<boolean>(true)
  const [securityGroups, setSecurityGroups] = useState<SecurityGroup[]>([])
  const [selectedRows, setSelectedRows] = useState<SecurityGroup[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [deleteVisible, setDeleteVisible] = useState<boolean>(false)

  useEffect(() => {
    fetchSecurityGroups()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchSecurityGroups = async () => {
    setLoading(true)
    const sgs = (await listSecurityGroup({})) as SecurityGroup[]
    if (_isMounted.current) {
      setSecurityGroups(sgs)
      setLoading(false)
    }
  }

  const columns: ColumnType<SecurityGroup>[] = [
    {
      title: <FormattedMessage id="name" />,
      key: 'name',
      width: 300,
      ellipsis: true,
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (sg: SecurityGroup) => (
        <Link to={`/network/security-groups/${sg.ID}`}>
          {keyword ? <Ligther keywords={[keyword]} source={sg.name} /> : sg.name}
        </Link>
      ),
    },
    {
      title: <FormattedMessage id="description" />,
      key: 'description',
      dataIndex: 'description',
      width: 400,
      ellipsis: true,
      sorter: (a, b) => a.description.localeCompare(b.description),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: <FormattedMessage id="security_group.inbound_role_count" />,
      key: 'inbound_rules',
      width: 200,
      ellipsis: true,
      sorter: (a, b) =>
        a.rules.filter((rule) => rule.Direction === 'ingress').length -
        b.rules.filter((rule) => rule.Direction === 'ingress').length,
      render: (sg: SecurityGroup) => {
        const tmpVal = `${sg.rules.filter((rule) => rule.Direction === 'ingress').length} ${intl.formatMessage({
          id: 'rules',
        })}`
        return (
          <Link to={`/network/security-groups/${sg.ID}`}>
            <Ligther keywords={[keyword]} source={tmpVal} />
          </Link>
        )
      },
    },
    {
      title: <FormattedMessage id="security_group.outbound_role_count" />,
      key: 'outbound_rules',
      width: 200,
      ellipsis: true,
      sorter: (a, b) =>
        a.rules.filter((rule) => rule.Direction === 'egress').length -
        b.rules.filter((rule) => rule.Direction === 'egress').length,
      render: (sg: SecurityGroup) => {
        const tmpVal = `${sg.rules.filter((rule) => rule.Direction === 'egress').length} ${intl.formatMessage({
          id: 'rules',
        })}`
        return (
          <Link to={`/network/security-groups/${sg.ID}`}>
            <Ligther keywords={[keyword]} source={tmpVal} />
          </Link>
        )
      },
    },
  ]

  const actions: MenuAction[] = [
    {
      name: <FormattedMessage id="delete" />,
      disabled: selectedRows.length === 0,
      icon: <DeleteOutlined />,
      action: () => {
        setDeleteVisible(true)
      },
    },
  ]

  return (
    <PageLayout
      loading={loading}
      title={<FormattedMessage id="security_groups" />}
      createAction={() => {
        setCreateVisible(true)
      }}
      fetchAction={fetchSecurityGroups}
      actions={actions}
    >
      <>
        {deleteVisible && selectedRows.length > 0 && (
          <DeleteModal
            securityGroups={selectedRows}
            visible={deleteVisible}
            onOk={() => {
              setDeleteVisible(false)
              fetchSecurityGroups()
            }}
            onCancel={() => {
              setDeleteVisible(false)
            }}
          />
        )}
        {createVisible && (
          <CreateModal
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchSecurityGroups()
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        <Table
          rowKey="ID"
          loading={loading}
          columns={columns}
          onRowSelected={setSelectedRows}
          dataCount={securityGroups.length}
          dataSource={
            keyword
              ? securityGroups.filter(
                  (sg) =>
                    findInString(sg.name, keyword) ||
                    findInString(sg.description, keyword) ||
                    findInString(
                      `${sg.rules.filter((rule) => rule.Direction === 'ingress').length} ${intl.formatMessage({
                        id: 'rules',
                      })}`,
                      keyword
                    ) ||
                    findInString(
                      `${sg.rules.filter((rule) => rule.Direction === 'egress').length} ${intl.formatMessage({
                        id: 'rules',
                      })}`,
                      keyword
                    )
                )
              : securityGroups
          }
          onSearch={(value) => {
            setKeyword(value)
          }}
        />
      </>
    </PageLayout>
  )
}

export default SecurityGroupList
