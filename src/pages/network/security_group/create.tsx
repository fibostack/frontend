import { Form, notification } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createSecurityGroup } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

interface SecurityGroupCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
}

const SecurityGroupCreate: React.FC<SecurityGroupCreateProps> = ({ onOk, visible, onCancel }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await createSecurityGroup({ data: values })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${values.name} security group is created.`,
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="security_group.create_title" />}
      formName="security_group_create_form"
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="security_group_create_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <FormInput
          required
          hasLabel
          name="groupName"
          type="input"
          label={<FormattedMessage id="name" />}
          placeholder={intl.formatMessage({ id: 'name' })}
        />
        <FormInput
          required
          hasLabel
          name="description"
          type="textarea"
          className="form_last_item"
          label={intl.formatMessage({ id: 'description' })}
          placeholder={intl.formatMessage({ id: 'description' })}
        />
      </Form>
    </ModalForm>
  )
}

export default SecurityGroupCreate
