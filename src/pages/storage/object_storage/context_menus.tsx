import {
  BorderOutlined,
  CheckSquareOutlined,
  CloudDownloadOutlined,
  ControlOutlined,
  CopyOutlined,
  DeleteOutlined,
  EditOutlined,
  ExclamationCircleOutlined,
  EyeOutlined,
  FolderAddOutlined,
  ScissorOutlined,
  SnippetsOutlined,
  UploadOutlined,
} from '@ant-design/icons'
import { Button, Divider } from 'antd'
import React from 'react'
import { ContextMenu, MenuItem } from 'react-contextmenu'
import { FormattedMessage } from 'react-intl'

// #region [Item context menu]
interface ItemContextMenuProps {
  id: string
  handleAction: (action: string, id: string) => void
}

export const ItemContextMenu: React.FC<ItemContextMenuProps> = ({ id, handleAction }) => (
  <ContextMenu id={id} className="ant-dropdown-menu ant-dropdown-menu-light">
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('open', value)
      }}
    >
      <Button block type="text" icon={<EyeOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="open" />
        </span>
      </Button>
    </MenuItem>
    {/* <Divider className="m_0" />
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('download', value)
      }}
    >
      <Button block type="text" icon={<CloudDownloadOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="download" />
        </span>
      </Button>
    </MenuItem> */}
    <MenuItem
      disabled
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('rename', value)
      }}
    >
      <Button disabled block type="text" icon={<EditOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="rename" />
        </span>
      </Button>
    </MenuItem>
    <Divider className="m_0" />
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('copy', value)
      }}
    >
      <Button block type="text" icon={<CopyOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="copy" />
        </span>
      </Button>
    </MenuItem>
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('cut', value)
      }}
    >
      <Button block type="text" icon={<ScissorOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="cut" />
        </span>
      </Button>
    </MenuItem>
    <MenuItem
      disabled
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('paste', value)
      }}
    >
      <Button block disabled type="text" icon={<SnippetsOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="paste" />
        </span>
      </Button>
    </MenuItem>
    <Divider className="m_0" />
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('delete', value)
      }}
    >
      <Button block danger type="text" icon={<DeleteOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="delete" />
        </span>
      </Button>
    </MenuItem>
    <Divider className="m_0" />
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('details', value)
      }}
    >
      <Button block type="text" icon={<ExclamationCircleOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="details" />
        </span>
      </Button>
    </MenuItem>
  </ContextMenu>
)
// #endregion

// #region [Multi items context menu]
interface MultiItemsContextMenuProps {
  id: string
  handleAction: (action: string, id: string) => void
}

export const MultiItemsContextMenu: React.FC<MultiItemsContextMenuProps> = ({ id, handleAction }) => (
  <ContextMenu id={id} className="ant-dropdown-menu ant-dropdown-menu-light">
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('copy', value)
      }}
    >
      <Button block type="text" icon={<CopyOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="copy" />
        </span>
      </Button>
    </MenuItem>
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('cut', value)
      }}
    >
      <Button block type="text" icon={<ScissorOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="cut" />
        </span>
      </Button>
    </MenuItem>
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('paste', value)
      }}
    >
      <Button block disabled type="text" icon={<SnippetsOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="paste" />
        </span>
      </Button>
    </MenuItem>
    <Divider className="m_0" />
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('delete', value)
      }}
    >
      <Button block danger type="text" icon={<DeleteOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="delete" />
        </span>
      </Button>
    </MenuItem>
  </ContextMenu>
)
// #endregion

// #region [Folder context menu]
interface FolderContextMenuProps {
  id: string
  copied: boolean
  handleAction: (action: string, id: string) => void
}

export const FolderContextMenu: React.FC<FolderContextMenuProps> = ({ id, copied, handleAction }) => (
  <ContextMenu id={id} className="ant-dropdown-menu ant-dropdown-menu-light">
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('open_folder', value)
      }}
    >
      <Button block type="text" icon={<EyeOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="open" />
        </span>
      </Button>
    </MenuItem>
    <Divider className="m_0" />
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('rename', value)
      }}
    >
      <Button block type="text" icon={<EditOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="rename" />
        </span>
      </Button>
    </MenuItem>
    <Divider className="m_0" />
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('copy', value)
      }}
    >
      <Button block type="text" icon={<CopyOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="copy" />
        </span>
      </Button>
    </MenuItem>
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('cut', value)
      }}
    >
      <Button block type="text" icon={<ScissorOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="cut" />
        </span>
      </Button>
    </MenuItem>
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('paste', value)
      }}
    >
      <Button block disabled={!copied} type="text" icon={<SnippetsOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="paste" />
        </span>
      </Button>
    </MenuItem>
    <Divider className="m_0" />
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('delete', value)
      }}
    >
      <Button block danger type="text" icon={<DeleteOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="delete" />
        </span>
      </Button>
    </MenuItem>
    <Divider className="m_0" />
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        if (value) handleAction('details', value)
      }}
    >
      <Button block type="text" icon={<ExclamationCircleOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="details" />
        </span>
      </Button>
    </MenuItem>
  </ContextMenu>
)
// #endregion

// #region [Container context menu]
interface ContainerContextMenuProps {
  id: string
  copied: boolean
  allSelected: boolean
  handleAction: (action: string) => void
}

export const ContainerContextMenu: React.FC<ContainerContextMenuProps> = ({
  id,
  copied,
  allSelected,
  handleAction,
}) => (
  <ContextMenu id={id} className="ant-dropdown-menu ant-dropdown-menu-light">
    <MenuItem
      onClick={() => {
        handleAction(allSelected ? 'deselect_all' : 'select_all')
      }}
    >
      <Button block type="text" icon={allSelected ? <BorderOutlined /> : <CheckSquareOutlined />}>
        <span className="w-fill align_left">
          {allSelected ? <FormattedMessage id="deselect_all" /> : <FormattedMessage id="select_all" />}
        </span>
      </Button>
    </MenuItem>
    <Divider className="m_t_0 m_b_0" />
    <MenuItem
      onClick={() => {
        handleAction('paste')
      }}
    >
      <Button block disabled={!copied} type="text" icon={<SnippetsOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="paste" />
        </span>
      </Button>
    </MenuItem>
    <Divider className="m_t_0 m_b_0" />
    <MenuItem
      onClick={() => {
        handleAction('upload_file')
      }}
    >
      <Button block type="text" icon={<UploadOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="upload" />
        </span>
      </Button>
    </MenuItem>
    <MenuItem
      onClick={() => {
        handleAction('new_folder')
      }}
    >
      <Button block type="text" icon={<FolderAddOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="new_folder" />
        </span>
      </Button>
    </MenuItem>
  </ContextMenu>
)
// #endregion

// #region [Bucket context menu]
interface BucketContextMenuProps {
  id: string
  handleAction: (action: string, id: string, isCloud: boolean) => void
}

export const BucketContextMenu: React.FC<BucketContextMenuProps> = ({ id, handleAction }) => (
  <ContextMenu id={id} className="ant-dropdown-menu ant-dropdown-menu-light">
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        const isC = target.getAttribute('aria-valuenow')
        if (value) handleAction('policy', value, isC === '1')
      }}
    >
      <Button block type="text" icon={<ControlOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="policy" />
        </span>
      </Button>
    </MenuItem>
    <Divider className="m_t_0 m_b_0" />
    <MenuItem
      onClick={(_x, _y, target) => {
        const value = target.getAttribute('aria-valuetext')
        const isC = target.getAttribute('aria-valuenow')
        if (value) handleAction('delete', value, isC === '1')
      }}
    >
      <Button block type="text" icon={<DeleteOutlined />}>
        <span className="w-fill align_left">
          <FormattedMessage id="delete" />
        </span>
      </Button>
    </MenuItem>
  </ContextMenu>
)
// #endregion
