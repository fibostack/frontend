import { Col, Row, Table } from 'antd'
import { ColumnType } from 'antd/lib/table'
import { FileInterface } from 'models'
import React from 'react'
import { ContextMenuTrigger } from 'react-contextmenu'
import { FormattedMessage } from 'react-intl'
import { DateFormat, dateSort, findInString, formatByte, getContextType } from 'utils'
import { FileIcon } from '../file'
import RowItem from './rowItem'

interface Props {
  dataSource: FileInterface[]
  selected: React.Key[]
  handleAction: (action: string, id: string) => void
  onSelectedChange: React.Dispatch<React.SetStateAction<React.ReactText[]>>
}

const ListComponent: React.FC<Props> = ({ dataSource, selected, handleAction, onSelectedChange }) => {
  const columns: ColumnType<FileInterface>[] = [
    {
      title: <FormattedMessage id="name" />,
      key: 'showName',
      dataIndex: 'showName',
      ellipsis: true,
      width: 300,
      sorter: (a, b) => a.showName.localeCompare(b.showName),
      render: (value: string, file: FileInterface) => {
        const itemIsFolder = file.contentType === 'Folder'
        return (
          <ContextMenuTrigger
            id={getContextType(selected, itemIsFolder)}
            holdToDisplay={1000}
            attributes={{ 'aria-valuetext': file.name }}
          >
            <Row gutter={12} align="middle">
              <Col>
                <FileIcon type="list" file={file} />
              </Col>
              <Col>{itemIsFolder ? value.substring(0, value.length - 1) : value}</Col>
            </Row>
          </ContextMenuTrigger>
        )
      },
    },
    {
      title: <FormattedMessage id="type" />,
      key: 'contentType',
      dataIndex: 'contentType',
      width: 200,
      filters: [
        {
          text: 'Folder',
          value: 'folder',
        },
        {
          text: 'Image',
          value: 'image',
        },
        {
          text: 'Document',
          value: 'document',
        },
      ],
      onFilter: (value, record) =>
        value === 'folder' ? record.contentType === 'Folder' : findInString(record.contentType, value.toString()),
      sorter: (a, b) => a.contentType.localeCompare(b.contentType),
      render: (value: string, file: FileInterface) => {
        const itemIsFolder = file.contentType === 'Folder'
        return (
          <ContextMenuTrigger
            id={getContextType(selected, itemIsFolder)}
            holdToDisplay={1000}
            attributes={{ 'aria-valuetext': file.name }}
          >
            <div className="w-fill">{itemIsFolder ? 'Folder' : value || '-'}</div>
          </ContextMenuTrigger>
        )
      },
    },
    {
      title: <FormattedMessage id="size" />,
      key: 'size',
      dataIndex: 'size',
      width: 200,
      sorter: (a, b) => a.size - b.size,
      render: (value: number, file: FileInterface) => {
        const itemIsFolder = file.contentType === 'Folder'
        return (
          <ContextMenuTrigger
            id={getContextType(selected, itemIsFolder)}
            holdToDisplay={1000}
            attributes={{ 'aria-valuetext': file.name }}
          >
            <div className="w-fill">{formatByte(value) || '-'}</div>
          </ContextMenuTrigger>
        )
      },
    },
    {
      title: <FormattedMessage id="last_modified" />,
      key: 'lastModified',
      dataIndex: 'lastModified',
      width: 200,
      sorter: (a, b) => dateSort(a.lastModified, b.lastModified),
      render: (value: Date, file: FileInterface) => {
        const itemIsFolder = file.contentType === 'Folder'
        return (
          <ContextMenuTrigger
            id={getContextType(selected, itemIsFolder)}
            holdToDisplay={1000}
            attributes={{ 'aria-valuetext': file.name }}
          >
            <div className="w-fill">{DateFormat(value) || '-'}</div>
          </ContextMenuTrigger>
        )
      },
    },
  ]

  return (
    <Table
      rowKey="name"
      columns={columns}
      dataSource={dataSource}
      size="small"
      pagination={false}
      scroll={{ y: 'calc(60vh - 38px)' }}
      components={{ body: { row: RowItem } }}
      onRow={(record, index) => {
        return {
          onClick: () => {
            onSelectedChange((old) => {
              if (old.length > 1) {
                return [...old, record.name]
              }
              return [record.name]
            })
          },
          onDoubleClick: () => {
            if (record.contentType === 'Folder') {
              handleAction('open_folder', record.name)
            } else {
              handleAction('open', record.name)
            }
          },
          onContextMenu: () => {
            onSelectedChange((old) => {
              if (old.length > 1) {
                return [...old, record.name]
              }
              return [record.name]
            })
          },
        }
      }}
      rowSelection={{
        fixed: true,
        selectedRowKeys: selected,
        onChange: (selectRowKeys) => {
          onSelectedChange(selectRowKeys)
        },
      }}
    />
  )
}

export default ListComponent
