// /* eslint-disable react/destructuring-assignment */
import React, { useRef, HTMLAttributes } from 'react'
import { FileInterface } from 'models'
import { useDrop, useDrag } from 'react-dnd'

const type = 'file'

interface ChildInterface {
  key: string
  props: {
    className: string
    component: string
    prefixCls: string
    record: FileInterface
    index: number
    fixLeft: number
    lastFixLeft: boolean
    firstFixRight: boolean
    lastFixRight: boolean
    firstFixLeft: boolean
  }
}

interface Props {
  children: ChildInterface[]
  style: any
  [x: string]: any
}

const RowItem = (rps: Props) => {
  const currentRecord = rps.children && rps.children.length > 0 ? rps.children[0].props.record : undefined
  const ref = useRef<any>()
  const [{ isOver, dropClassName }, drop] = useDrop({
    accept: type,
    collect: (monitor) => {
      const { record } = monitor.getItem() || {}
      const tmpRecord = record as FileInterface
      if (currentRecord && tmpRecord && tmpRecord.name === currentRecord.name) {
        return {}
      }
      return {
        isOver: monitor.isOver(),
        dropClassName: record ? ' drop-over-downward' : ' drop-over-upward',
      }
    },
    drop: (item) => {
      // console.log(item, currentRecord)
    },
  })
  const [, drag] = useDrag({
    item: { type, record: currentRecord },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  })
  drop(drag(ref))
  return (
    <tr
      ref={ref}
      {...rps}
      className={`${rps.className}${isOver ? dropClassName : ''}`}
      style={{ cursor: 'move', ...rps.style }}
    />
  )
}

export default RowItem
