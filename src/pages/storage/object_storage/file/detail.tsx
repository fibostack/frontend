import { CheckCircleOutlined, CloseCircleOutlined, LoadingOutlined } from '@ant-design/icons'
import { Tooltip } from 'antd'
import { getObject, getObjectParts } from 'api'
import { DetailModal, Loader, Table } from 'components'
import { CloseAwaitMS } from 'configs'
import {
  CloudObjectPart,
  CloudObjectPartChunk,
  CloudObjectPartChunkCoderConfig,
  CloudObjectPartFrag,
  SelectedBucket,
} from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import { formatByte } from 'utils'

interface Props {
  visible: boolean
  bucket?: SelectedBucket
  onOk: Function
  fileName: React.Key
}

const FileDetail: React.FC<Props> = ({ onOk, visible, bucket, fileName }) => {
  const intl = useIntl()
  const _isMounted = useRef<boolean>(true)
  const [loading, setLoading] = useState<boolean>(true)
  const [detail, setDetail] = useState<any>()
  const [parts, setParts] = useState<CloudObjectPartChunk[]>([])
  const [vis, setVis] = useState<boolean>(visible)

  useEffect(() => {
    return () => {
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    if (bucket && fileName) fetchData(bucket, fileName)
  }, [bucket, fileName])

  const fetchData = async (selectedBucket: SelectedBucket, name: React.Key) => {
    setLoading(true)
    if (selectedBucket) {
      if (selectedBucket.isCloud) {
        const [dt, pr] = await Promise.all([
          getObject({
            data: { object_path: name, bucket_name: selectedBucket.name, is_cloud: selectedBucket.isCloud },
          }),
          getObjectParts({
            data: {
              bucket: selectedBucket.name,
              key: name,
            },
          }),
        ])
        if (dt && pr) {
          const tmpPr = pr as CloudObjectPart
          setParts(tmpPr.chunks || [])
          setDetail(dt)
        }
      } else {
        const dt = await getObject({
          data: { object_path: name, bucket_name: selectedBucket.name, is_cloud: selectedBucket.isCloud },
        })
        if (dt) {
          setDetail(dt)
        }
      }
    }
    setLoading(false)
  }

  const fileState = (file: CloudObjectPartChunk) => {
    if (file.is_accessible) {
      return (
        <Tooltip title="Healthy" placement="top">
          <CheckCircleOutlined style={{ fontSize: 18, marginRight: 4, color: 'green' }} />
        </Tooltip>
      )
    }
    if (file.is_building_blocks || file.is_building_frags) {
      return (
        <Tooltip title="Processing" placement="top">
          <LoadingOutlined style={{ fontSize: 18, marginRight: 4, color: 'blue' }} />
        </Tooltip>
      )
    }
    return (
      <Tooltip title="Unhealthy" placement="top">
        <CloseCircleOutlined style={{ fontSize: 18, marginRight: 4, color: 'red' }} />
      </Tooltip>
    )
  }

  return (
    <DetailModal
      visible={vis}
      title={intl.formatMessage({ id: 'object_storage.file_detail' }, { name: fileName })}
      okText={intl.formatMessage({ id: 'ok' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onOk()
        }, CloseAwaitMS)
      }}
    >
      <Loader loading={loading}>
        {bucket?.isCloud && (
          <Table
            rowKey="_id"
            title={() => (
              <h3>
                <strong>{intl.formatMessage({ id: 'object_storage.file_parts' })}</strong>
              </h3>
            )}
            columns={[
              {
                title: intl.formatMessage({ id: 'name' }),
                key: 'name',
                width: 100,
                ellipsis: true,
                render: (_a, record: CloudObjectPartChunk) => (
                  <span>
                    {fileState(record)} {`${intl.formatMessage({ id: 'object_storage.part' })} ${record.key}`}
                  </span>
                ),
              },
              {
                title: intl.formatMessage({ id: 'size' }),
                dataIndex: 'size',
                key: 'size',
                width: 100,
                ellipsis: true,
                render: (value: number) => formatByte(value),
              },
              {
                title: intl.formatMessage({ id: 'object_storage.replica' }),
                dataIndex: 'chunk_coder_config',
                key: 'replica',
                width: 100,
                ellipsis: true,
                render: (value: CloudObjectPartChunkCoderConfig) =>
                  `${value.replicas} ${intl.formatMessage({ id: 'object_storage.replica' })}`,
              },
              {
                title: intl.formatMessage({ id: 'object_storage.resource' }),
                dataIndex: 'frags',
                key: 'real_bucket',
                width: 100,
                ellipsis: true,
                render: (value: CloudObjectPartFrag[]) =>
                  (value.length > 0 && value[0].blocks.length > 0 && value[0].blocks[0].adminfo.pool_name) || '-',
              },
            ]}
            dataSource={parts.map((item, index) => {
              item.key = index + 1
              return item
            })}
            dataCount={parts.length}
          />
        )}
      </Loader>
    </DetailModal>
  )
}

export default FileDetail
