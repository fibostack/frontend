/* eslint-disable camelcase */
import { InboxOutlined } from '@ant-design/icons'
import { Col, List, Modal, notification, Progress, Row, Upload } from 'antd'
import { createObject, freshAxios } from 'api'
import { AxiosResponse } from 'axios'
import { CloseAwaitMS } from 'configs'
import { SelectedBucket } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { FixedNubmer } from 'utils'

interface Props {
  visible: boolean
  onOk: Function
  onCancel: Function
  bucket?: SelectedBucket
}

const UploadComponent: React.FC<Props> = ({ visible, bucket, onOk, onCancel }) => {
  const intl = useIntl()
  const [vis, setVis] = useState<boolean>(visible)
  const [files, setFiles] = useState<any[]>([])
  const [loading, setLoading] = useState<boolean>(false)
  const [percentage, setPercentage] = useState<Map<string, number>>(new Map())
  const [, setReload] = useState<Date>()

  const myUploadProgress = (name: string) => (progress: any) => {
    setPercentage((old) => {
      old.set(name, FixedNubmer((progress.loaded * 100) / progress.total))
      return old
    })
    setReload(new Date())
  }

  const handleFinish = async () => {
    if (bucket) {
      setLoading(true)
      const success = await createObject({
        data: {
          bucket_name: bucket.name,
          object_paths: files.reduce<string[]>((acc, item) => [...acc, item.name], []),
          is_cloud: bucket.isCloud,
        },
      })
      if (success && success.length > 0) {
        const requestArray = success.reduce<Promise<AxiosResponse<any>>[]>((acc, scc, sccInd) => {
          acc.push(
            freshAxios.put(scc.presign_url, files[sccInd], {
              onUploadProgress: myUploadProgress(scc.file_name),
            })
          )
          return acc
        }, [])
        const [...allRess] = await Promise.all([...requestArray])
        if (allRess.every((item) => item.status === 200)) {
          setLoading(false)
          notification.success({
            message: intl.formatMessage({ id: 'successful' }),
            description: intl.formatMessage({ id: 'object_storage.file_uploaded' }),
          })
          setVis(false)
          setTimeout(() => {
            onOk()
          }, CloseAwaitMS)
        }
      }
    }
  }

  return (
    <Modal
      width="50%"
      visible={vis}
      closable={!loading}
      maskClosable={!loading}
      title={<FormattedMessage id="upload" />}
      okButtonProps={{ loading }}
      onOk={handleFinish}
      onCancel={() => {
        if (!loading) {
          setVis(false)
          setTimeout(() => {
            onCancel()
          }, CloseAwaitMS)
        }
      }}
    >
      <>
        <Upload.Dragger
          multiple
          name="upload"
          showUploadList={false}
          beforeUpload={(file) => {
            setFiles((old) => [...old, file])
            return false
          }}
        >
          <p className="ant-upload-drag-icon">
            <InboxOutlined />
          </p>
          <p className="ant-upload-text">
            <FormattedMessage id="object_storage.upload_title" />
          </p>
          <p className="ant-upload-hint">
            <FormattedMessage id="object_storage.upload_description" />
          </p>
        </Upload.Dragger>
        {files.length > 0 && (
          <List
            size="small"
            bordered
            className="m_t_2"
            dataSource={files}
            renderItem={(file) => {
              const perc = percentage.get(file.name)
              return (
                <List.Item className="w-fill">
                  <Row gutter={4} className="w-fill" justify="center" align="middle">
                    <Col span={12}>{file.name}</Col>
                    <Col span={12}>
                      {perc && <Progress percent={perc} status={perc < 100 ? 'active' : 'success'} />}
                    </Col>
                  </Row>
                </List.Item>
              )
            }}
          />
        )}
      </>
    </Modal>
  )
}

export default UploadComponent
