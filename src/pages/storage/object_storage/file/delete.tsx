import { notification } from 'antd'
import { deleteObject } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { SelectedBucket } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface Props {
  visible: boolean
  bucket?: SelectedBucket
  onOk: Function
  onCancel: Function
  fileNames: React.Key[]
}

const FileDelete: React.FC<Props> = ({ visible, bucket, fileNames, onOk, onCancel }) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [vis, setVis] = useState<boolean>(visible)

  const onDelete = async () => {
    if (bucket) {
      setLoading(true)
      const success = await deleteObject({
        data: { bucket_name: bucket.name, is_cloud: bucket.isCloud, object_paths: fileNames },
      })
      setLoading(false)
      if (success) {
        notification.success({
          message: intl.formatMessage({ id: 'successful' }),
          description: intl.formatMessage({ id: 'object_storage.file_deleted' }, { name: fileNames }),
        })
        setVis(false)
        setTimeout(() => {
          onOk()
        }, CloseAwaitMS)
      }
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'delete' })}
      selectedNames={fileNames?.join(', ')}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default FileDelete
