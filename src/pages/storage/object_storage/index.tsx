/* eslint-disable no-undef */
import { Layout, notification, Spin, Modal, Tabs, Button } from 'antd'
import { KeyOutlined } from '@ant-design/icons'
import { copyObject, getAPIkey, getObject, linkObject, listObject, getObjectParts } from 'api'
import cssClass from 'classnames'
import { PageLayout, Table } from 'components'
import { FileInterface, FilemanagerViewType, SelectedBucket } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { ContextMenuTrigger } from 'react-contextmenu'
import { FormattedMessage, useIntl } from 'react-intl'
import { findInString, isFolder, formatByte } from 'utils'
import ApiModal from './api'
import { ContainerContextMenu, FolderContextMenu, ItemContextMenu, MultiItemsContextMenu } from './context_menus'
import { FileDeleteModal, FileExtension, FileUploadModal, FileDetailModal } from './file'
import { FolderCreateModal } from './folder'
import GridView from './grid'
import Header from './header'
import ListView from './list'
import Sidemenu from './sidemenu'
import styles from './styles.module.scss'

interface Props {}

interface ObjectLinkResponse {
  Scheme: string
  Opaque: string
  User?: any
  Host: string
  Path: string
  RawPath: string
  ForceQuery: boolean
  RawQuery: string
  Fragment: string
}

const ObjectStorage: React.FC<Props> = () => {
  const intl = useIntl()
  const _isMounted = useRef<boolean>(true)
  const [loading, setLoading] = useState<boolean>(false)
  const [keyword, setKeyword] = useState<string>('')
  const [files, setFiles] = useState<FileInterface[]>([])
  const [selectedFolder, setSelectedFolder] = useState<string>()
  const [selectedBucket, setSelectedBucket] = useState<SelectedBucket>()
  const [uploadVisible, setUploadVisible] = useState<boolean>(false)
  const [deleteVisible, setDeleteVisible] = useState<boolean>(false)
  const [detailVisible, setDetailVisible] = useState<React.Key>()
  const [createFolderVisible, setCreateFolderVisible] = useState<boolean>(false)
  const [apiShowVisible, setApiShowVisible] = useState<boolean>(false)
  const [selectedKeys, setSelectedKeys] = useState<React.Key[]>([])
  const [viewType, setViewType] = useState<FilemanagerViewType>('list')
  const [copied, setCopied] = useState<{ isMove: boolean; sourceBucket?: string; names: React.Key[] }>()

  useEffect(() => {
    return () => {
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    if (selectedBucket) fetchData(selectedBucket, selectedFolder || '')
  }, [selectedBucket, selectedFolder])

  const fetchData = async (bucketO: SelectedBucket, folderPath: string) => {
    setLoading(true)
    const obs = (await listObject({
      data: { bucket_name: bucketO.name, is_cloud: bucketO.isCloud, folder_path: folderPath },
    })) as FileInterface[]
    if (_isMounted.current) {
      if (obs) {
        obs.map((ob) => {
          if (isFolder(ob)) {
            ob.contentType = 'Folder'
            ob.extension = 'Folder'
            return ob
          }
          const nameExtenstion = ob.name.split('.').pop()
          ob.contentType = FileExtension(nameExtenstion)
          ob.extension = nameExtenstion || ''
          return ob
        })
        setFiles(obs.sort((a, b) => a.name.localeCompare(b.name)))
      }
      setLoading(false)
    }
  }

  const openFile = async (name: string) => {
    if (selectedBucket) {
      setLoading(true)
      const rsu = (await linkObject({
        data: { object_path: name, bucket_name: selectedBucket.name, is_cloud: selectedBucket.isCloud },
      })) as ObjectLinkResponse
      setLoading(false)
      window.open(decodeURI(`${rsu.Scheme}://${rsu.Host}${rsu.Path}?${rsu.RawQuery}`), '_blank')
    }
  }

  const pasteFiles = async (folderName?: string) => {
    if (selectedBucket && copied) {
      setLoading(true)
      const cpd = await copyObject({
        data: {
          source_bucket: copied.sourceBucket,
          object_paths: copied.names,
          desc_bucket: selectedBucket.name,
          desc_path: folderName || selectedFolder || '',
          is_move: copied.isMove,
          is_cloud: selectedBucket.isCloud,
        },
      })
      setLoading(false)
      if (cpd) {
        notification.success({
          message: intl.formatMessage({ id: 'successful' }),
          description: intl.formatMessage(
            { id: copied.isMove ? 'object_storage.file_moved' : 'object_storage.file_copied' },
            { name: copied.names.join(', '), target_name: folderName || selectedFolder || selectedBucket.name }
          ),
        })
        if (selectedBucket) fetchData(selectedBucket, selectedFolder || '')
      }
    }
  }

  const searchFilter = (fls: FileInterface[]) => fls.filter((item) => findInString(item.name, keyword))
  const pathFilter = (fls: FileInterface[]) =>
    fls.filter((item) => {
      const pathTest = new RegExp(/.\/./g)
      if (!selectedFolder) {
        return !pathTest.test(item.name)
      }
      if (item.name !== selectedFolder && item.name.indexOf(selectedFolder) === 0) {
        item.showName = item.name.replace(selectedFolder, '')
        return !pathTest.test(item.showName)
      }
      return false
    })
  const clearFileName = (fls: FileInterface[]) =>
    fls.map((item) => {
      if (selectedFolder) item.showName = item.name.replace(selectedFolder, '')
      else item.showName = item.name
      return item
    })
  const fileFilters = (fls: FileInterface[]) => clearFileName(pathFilter(searchFilter(fls)))

  const renderContent = () => {
    switch (viewType) {
      case 'grid':
        return (
          <GridView
            selected={selectedKeys}
            handleAction={fileHandleAction}
            onSelectedChange={setSelectedKeys}
            dataSource={fileFilters(files)}
          />
        )
      case 'list':
        return (
          <ListView
            selected={selectedKeys}
            handleAction={fileHandleAction}
            onSelectedChange={setSelectedKeys}
            dataSource={fileFilters(files)}
          />
        )
      default:
        return (
          <GridView
            selected={selectedKeys}
            handleAction={fileHandleAction}
            onSelectedChange={setSelectedKeys}
            dataSource={fileFilters(files)}
          />
        )
    }
  }

  const containerHandleAction = (action: string) => {
    switch (action) {
      case 'back':
        setSelectedFolder((old) => {
          const tmp = old?.split('/')
          if (tmp) {
            const aTmp = tmp.slice(0, tmp.length - 2)
            aTmp.push('')
            return aTmp.join('/')
          }
          return tmp
        })
        break
      case 'select_all':
        setSelectedKeys(files.reduce<string[]>((acc, item) => [...acc, item.name], []))
        break
      case 'deselect_all':
        setSelectedKeys([])
        break
      case 'paste':
        pasteFiles()
        break
      case 'upload_file':
        setUploadVisible(true)
        break
      case 'new_folder':
        setCreateFolderVisible(true)
        break
      case 'delete':
        setDeleteVisible(true)
        break
      default:
        break
    }
  }

  const fileHandleAction = (action: string, key: string) => {
    switch (action) {
      case 'open':
        setSelectedKeys([key])
        openFile(key)
        break
      case 'open_folder':
        setSelectedFolder(key)
        break
      case 'download':
        // openFile(key)
        break
      case 'copy':
        setSelectedKeys((old) => {
          if (old.includes(key)) {
            return [...old]
          }
          return [key]
        })
        setCopied({
          sourceBucket: selectedBucket?.name,
          names: selectedKeys.length > 0 ? selectedKeys : [key],
          isMove: false,
        })
        break
      case 'cut':
        setSelectedKeys((old) => {
          if (old.includes(key)) {
            return [...old]
          }
          return [key]
        })
        setCopied({
          sourceBucket: selectedBucket?.name,
          names: selectedKeys.length > 0 ? selectedKeys : [key],
          isMove: true,
        })
        break
      case 'paste':
        setSelectedKeys([key])
        pasteFiles()
        break
      case 'delete':
        setSelectedKeys((old) => {
          if (old.includes(key)) {
            return [...old]
          }
          return [key]
        })
        setDeleteVisible(true)
        break
      case 'details':
        setDetailVisible(key)
        break
      default:
        break
    }
  }

  const showApi = () => {
    setApiShowVisible(true)
  }
  const allSelected = files.length === selectedKeys.length
  const folderPaths = selectedFolder?.split('/') || []
  if (selectedBucket) {
    folderPaths.pop()
    folderPaths.unshift(selectedBucket.name)
  }

  return (
    <PageLayout
      className={styles.objectStorage}
      title={
        <div className={styles.titleDiv}>
          <FormattedMessage id="menu.object_storage" />
          <Button className={styles.showAPIBtn} type="primary" icon={<KeyOutlined />} onClick={showApi}>
            <FormattedMessage id="object_storage.show_api_key" />
          </Button>
        </div>
      }
    >
      <Layout className={cssClass('u-filemanager', styles.filemanager)}>
        <Layout>
          <Sidemenu
            onSelectedBucket={(value) => {
              setSelectedFolder(undefined)
              setSelectedBucket(value)
            }}
            handleAction={containerHandleAction}
          />
          <Layout.Content className={styles.content}>
            <Header
              loading={loading}
              selectedPath={folderPaths}
              fetchAction={() => {
                if (selectedBucket) fetchData(selectedBucket, selectedFolder || '')
              }}
              onClickBread={setSelectedFolder}
              viewType={viewType}
              onSearch={setKeyword}
              selected={selectedKeys}
              allSelected={allSelected}
              viewTypeChange={setViewType}
              handleAction={containerHandleAction}
            />
            <Spin spinning={loading}>
              <ContextMenuTrigger id="content_menu" holdToDisplay={1000}>
                <div className={styles.fileContainer}>{renderContent()}</div>
              </ContextMenuTrigger>
            </Spin>
            <ItemContextMenu id="file_item" handleAction={fileHandleAction} />
            <MultiItemsContextMenu id="multi_files_item" handleAction={fileHandleAction} />
            <FolderContextMenu id="folder_item" copied={!!copied} handleAction={fileHandleAction} />
            <ContainerContextMenu
              id="content_menu"
              allSelected={allSelected}
              copied={!!copied}
              handleAction={containerHandleAction}
            />
            {deleteVisible && (
              <FileDeleteModal
                visible={deleteVisible}
                bucket={selectedBucket}
                fileNames={selectedKeys}
                onOk={() => {
                  if (selectedBucket) fetchData(selectedBucket, selectedFolder || '')
                  setSelectedKeys([])
                  setDeleteVisible(false)
                }}
                onCancel={() => {
                  setDeleteVisible(false)
                }}
              />
            )}
            {uploadVisible && (
              <FileUploadModal
                visible={uploadVisible}
                bucket={selectedBucket}
                onOk={() => {
                  if (selectedBucket) fetchData(selectedBucket, selectedFolder || '')
                  setSelectedKeys([])
                  setUploadVisible(false)
                }}
                onCancel={() => {
                  setUploadVisible(false)
                }}
              />
            )}
            {!!detailVisible && (
              <FileDetailModal
                bucket={selectedBucket}
                visible={!!detailVisible}
                fileName={detailVisible}
                onOk={() => {
                  setDetailVisible(undefined)
                }}
              />
            )}
            {createFolderVisible && (
              <FolderCreateModal
                bucket={selectedBucket}
                visible={createFolderVisible}
                selectedFolder={selectedFolder}
                onOk={() => {
                  if (selectedBucket) fetchData(selectedBucket, selectedFolder || '')
                  setSelectedKeys([])
                  setCreateFolderVisible(false)
                }}
                onCancel={() => {
                  setCreateFolderVisible(false)
                }}
              />
            )}
            {apiShowVisible && (
              <ApiModal
                visible={apiShowVisible}
                onCancel={() => {
                  setApiShowVisible(false)
                }}
              />
            )}
          </Layout.Content>
        </Layout>
      </Layout>
    </PageLayout>
  )
}

export default ObjectStorage
