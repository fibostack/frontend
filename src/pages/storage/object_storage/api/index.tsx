import { Form, notification, Input, Button, Tooltip } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { generateAPIkey, getAPIkey } from 'api'
import { ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState, useEffect, useRef } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { CopyOutlined } from '@ant-design/icons'
import CopyToClipboard from 'react-copy-to-clipboard'

interface ApiModalProps {
  visible: boolean
  onCancel: Function
}

const ApiModal: React.FC<ApiModalProps> = ({ visible, onCancel }) => {
  const _isMounted = useRef<boolean>(true)
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)
  const [apiKey, setApiKey] = useState<string>('')

  useEffect(() => {
    return () => {
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    fetchApiKey()
  }, [])

  const fetchApiKey = async () => {
    setLoading(true)
    const api = await getAPIkey()
    setLoading(false)
    if (_isMounted.current && api) {
      setApiKey(api)
    }
  }

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await generateAPIkey({ data: {} })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'object_storage.generate_success' }),
      })
      fetchApiKey()
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="object_storage.show_api_text" />}
      formName="object_storage_show_api_form"
      okText={<FormattedMessage id="generate" />}
      cancelText={<FormattedMessage id="cancel" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="object_storage_show_api_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <strong>
          <FormattedMessage id="object_storage.your_api_key" />:
        </strong>
        <div style={{ display: `flex`, marginTop: `10px` }}>
          <Input disabled value={apiKey} />
          <CopyToClipboard text={apiKey}>
            <Tooltip title={intl.formatMessage({ id: 'copied' })} trigger={[`click`]}>
              <Button type="primary" icon={<CopyOutlined />} />
            </Tooltip>
          </CopyToClipboard>
        </div>
      </Form>
    </ModalForm>
  )
}

export default ApiModal
