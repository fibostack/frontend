import { Form, notification } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createFolder } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { SelectedBucket } from 'models'

interface FolderCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  selectedFolder?: string
  bucket?: SelectedBucket
}

const FolderCreate: React.FC<FolderCreateProps> = ({ onOk, visible, onCancel, selectedFolder = '', bucket }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.bucket_name = (bucket && bucket.name) || ''
    values.is_cloud = (bucket && bucket.isCloud) || false
    values.folder_name = `${selectedFolder}${values.folder_name}/`
    const success = await createFolder({ data: values })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'object_storage.folder_created' }, { name: values.folder_name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="object_storage.folder_create_title" />}
      formName="object_storage_folder_create_form"
      okText={<FormattedMessage id="create" />}
      cancelText={<FormattedMessage id="cancel" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form
        form={form}
        name="object_storage_folder_create_form"
        layout="vertical"
        labelAlign="left"
        onFinish={handleFinish}
      >
        <FormInput
          required
          hasLabel
          name="folder_name"
          label={<FormattedMessage id="name" />}
          type="input"
          placeholder={intl.formatMessage({ id: 'name' })}
        />
      </Form>
    </ModalForm>
  )
}

export default FolderCreate
