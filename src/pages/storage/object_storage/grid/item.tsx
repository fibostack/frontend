import { Checkbox, Col } from 'antd'
import cssClass from 'classnames'
import { FileInterface } from 'models'
import React from 'react'
import { ContextMenuTrigger } from 'react-contextmenu'
import { DragSourceMonitor, useDrag, useDrop } from 'react-dnd'
import { getContextType } from 'utils'
import { FileIcon } from '../file'
import styles from '../styles.module.scss'

interface Props {
  file: FileInterface
  selected: React.Key[]
  handleAdd: (val: number) => void
  handleAction: (action: string, id: string) => void
}

const Item: React.FC<Props> = ({ file, handleAction, selected }) => {
  const [{ canDrop, isOver }, drop] = useDrop({
    accept: 'file',
    drop: () => ({ name: file.name }),
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  })

  const [{ isDragging }, drag] = useDrag({
    item: { name: file.name, type: 'file' },
    end: (item: { name: string } | undefined, monitor: DragSourceMonitor) => {
      const dropResult = monitor.getDropResult()
      if (item && dropResult) {
        // console.log(`You dropped ${item.name} into ${dropResult.name}!`)
      }
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  })

  const isActive = canDrop && isOver
  const itemIsFolder = file.contentType === 'Folder'

  return (
    <Col className="m_b_1">
      <Checkbox value={file.name} className="flex row">
        <ContextMenuTrigger
          id={getContextType(selected, itemIsFolder)}
          holdToDisplay={1000}
          attributes={{ 'aria-valuetext': file.name }}
        >
          <div
            ref={(itemIsFolder && drop) || undefined}
            style={{ width: 100 }}
            className={cssClass(styles.dropDiv, isActive && styles.activeDrop)}
            onDoubleClick={() => {
              if (itemIsFolder) {
                handleAction('open_folder', file.name)
              } else {
                handleAction('open', file.name)
              }
            }}
          >
            <div ref={drag} style={{ opacity: isDragging ? 0.1 : 1 }} className="flex col jc ac">
              <FileIcon type="grid" file={file} />
              <span style={{ textAlign: 'center' }}>
                {itemIsFolder ? file.showName.substring(0, file.showName.length - 1) : file.showName}
              </span>
            </div>
          </div>
        </ContextMenuTrigger>
      </Checkbox>
    </Col>
  )
}

export default Item
