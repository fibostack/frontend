import { Checkbox, Row } from 'antd'
import { FileInterface } from 'models'
import React from 'react'
import Item from './item'

interface Props {
  dataSource: FileInterface[]
  selected: React.Key[]
  handleAction: (action: string, id: string) => void
  onSelectedChange: React.Dispatch<React.SetStateAction<React.ReactText[]>>
}

const Grid: React.FC<Props> = ({ dataSource, selected, onSelectedChange, handleAction }) => {
  const handleAddCheckValues = (adVal: number) => {
    onSelectedChange((old) => [...old, adVal])
  }

  return (
    <Checkbox.Group
      value={selected}
      onChange={(event) => {
        onSelectedChange(event as React.ReactText[])
      }}
    >
      <Row style={{ padding: '12px 24px' }}>
        {dataSource.map((file) => (
          <Item
            key={file.name}
            file={file}
            selected={selected}
            handleAdd={handleAddCheckValues}
            handleAction={handleAction}
          />
        ))}
      </Row>
    </Checkbox.Group>
  )
}

export default Grid
