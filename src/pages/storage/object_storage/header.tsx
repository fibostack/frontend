import {
  AppstoreFilled,
  BorderOutlined,
  CheckSquareOutlined,
  CloudDownloadOutlined,
  CopyOutlined,
  DeleteOutlined,
  DoubleLeftOutlined,
  DoubleRightOutlined,
  ScissorOutlined,
  SnippetsOutlined,
  SyncOutlined,
  UnorderedListOutlined,
} from '@ant-design/icons'
import { Breadcrumb, Button, Col, Divider, Input, Radio, Row, Tooltip } from 'antd'
import cssClass from 'classnames'
import { FilemanagerViewType } from 'models'
import React from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import styles from './styles.module.scss'

interface Props {
  loading: boolean
  selected: React.Key[]
  allSelected: boolean
  viewType: FilemanagerViewType
  selectedPath?: string[]
  onClickBread: (keyword: string) => void
  onSearch: (keyword: string) => void
  handleAction: (action: string) => void
  viewTypeChange: (value: FilemanagerViewType) => void
  fetchAction?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void
}

const HeaderComponent: React.FC<Props> = ({
  allSelected,
  viewType,
  selected,
  loading = false,
  selectedPath,
  onClickBread,
  onSearch,
  fetchAction,
  handleAction,
  viewTypeChange,
}) => {
  const intl = useIntl()
  return (
    <Row justify="space-between" className={cssClass(styles.header)}>
      <Col>
        <span className="flex row jc ac">
          <Tooltip placement="top" title={<FormattedMessage id="back" />}>
            <Button
              className="onlyIcon m_r"
              type="primary"
              onClick={() => {
                handleAction('back')
              }}
              icon={<DoubleLeftOutlined />}
            />
          </Tooltip>
          <Tooltip placement="top" title={<FormattedMessage id="refresh" />}>
            <Button className="onlyIcon" type="primary" onClick={fetchAction} icon={<SyncOutlined spin={loading} />} />
          </Tooltip>
        </span>
      </Col>
      <Col>
        <Breadcrumb separator={<DoubleRightOutlined />}>
          {selectedPath?.map((path, index) => (
            <Breadcrumb.Item
              key={path}
              onClick={() => {
                const tmp = selectedPath.slice(1, index + 1)
                tmp.push('')
                onClickBread(tmp.join('/'))
              }}
            >
              <Button type="link">{path}</Button>
            </Breadcrumb.Item>
          ))}
        </Breadcrumb>
      </Col>
      <Col>
        <Row justify="start" align="middle">
          {/* <Col>
            <span className="flex row jc ac">
              <Tooltip
                placement="top"
                title={allSelected ? <FormattedMessage id="deselect_all" /> : <FormattedMessage id="select_all" />}
              >
                <Button
                  className="onlyIcon"
                  type="primary"
                  icon={allSelected ? <BorderOutlined /> : <CheckSquareOutlined />}
                  onClick={() => {
                    handleAction(allSelected ? 'deselect_all' : 'select_all')
                  }}
                />
              </Tooltip>
              <Divider type="vertical" />
              <Tooltip placement="top" title={<FormattedMessage id="download" />}>
                <Button
                  disabled={selected.length === 0}
                  className="onlyIcon"
                  type="primary"
                  icon={<CloudDownloadOutlined />}
                />
              </Tooltip>
              <Divider type="vertical" />
              <Tooltip placement="top" title={<FormattedMessage id="copy" />}>
                <Button
                  disabled={selected.length === 0}
                  className="onlyIcon m_r"
                  type="primary"
                  icon={<CopyOutlined />}
                />
              </Tooltip>
              <Tooltip placement="top" title={<FormattedMessage id="cut" />}>
                <Button
                  disabled={selected.length === 0}
                  className="onlyIcon m_r"
                  type="primary"
                  icon={<ScissorOutlined />}
                />
              </Tooltip>
              <Tooltip placement="top" title={<FormattedMessage id="paste" />}>
                <Button disabled className="onlyIcon" type="primary" icon={<SnippetsOutlined />} />
              </Tooltip>
              <Divider type="vertical" />
              <Tooltip placement="top" title={<FormattedMessage id="delete" />}>
                <Button
                  disabled={selected.length === 0}
                  className="onlyIcon"
                  type="primary"
                  icon={<DeleteOutlined />}
                  onClick={() => {
                    handleAction('delete')
                  }}
                />
              </Tooltip>
            </span>
          </Col> */}
          {/* <Col>
            <Divider type="vertical" />
          </Col> */}
          <Col>
            <Input.Search
              placeholder={intl.formatMessage({ id: 'search' })}
              onChange={(event) => {
                onSearch(event.target.value)
              }}
              style={{ minWidth: 200 }}
            />
          </Col>
          <Col>
            <Divider type="vertical" />
          </Col>
          <Col>
            <Radio.Group
              size="small"
              className="flex row jc ac"
              defaultValue={viewType}
              buttonStyle="solid"
              onChange={(event) => {
                viewTypeChange(event.target.value)
              }}
            >
              <Radio.Button value="list">
                <Tooltip placement="top" title={<FormattedMessage id="list" />}>
                  <UnorderedListOutlined />
                </Tooltip>
              </Radio.Button>
              <Radio.Button value="grid">
                <Tooltip placement="top" title={<FormattedMessage id="grid" />}>
                  <AppstoreFilled />
                </Tooltip>
              </Radio.Button>
            </Radio.Group>
          </Col>
        </Row>
      </Col>
    </Row>
  )
}

export default HeaderComponent
