import { Form, notification, Spin } from 'antd'
import { FormInstance } from 'antd/lib/form'
import { Store } from 'antd/lib/form/interface'
import { getBucketPolicy, setBucketPolicy } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'

interface Props {
  visible: boolean
  onOk: Function
  onCancel: Function
  bucket: { id: string; isCloud: boolean }
}

const BucketPolicy: React.FC<Props> = ({ visible, bucket, onOk, onCancel }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const _isMounted = useRef(true)
  const [vis, setVis] = useState<boolean>(visible)
  const [fetchLoading, setFetchLoading] = useState<boolean>(true)
  const [submitLoading, setSubmitLoading] = useState<boolean>(false)

  useEffect(() => {
    fetchData(bucket, form)
    return () => {
      _isMounted.current = false
    }
  }, [bucket, form])

  const fetchData = async ({ id, isCloud }: { id: string; isCloud: boolean }, f: FormInstance) => {
    setFetchLoading(true)
    const pls = await getBucketPolicy({ data: { bucket_name: id, is_cloud: isCloud } })
    if (_isMounted.current) {
      f.setFieldsValue({ is_public: pls })
      setFetchLoading(false)
    }
  }

  const handleFinish = async (values: Store) => {
    setSubmitLoading(true)
    const success = await setBucketPolicy({
      data: { bucket_name: bucket.id, is_cloud: bucket.isCloud, ...values },
    })
    setSubmitLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'object_storage.bucket_policy_changed' }, { name: bucket.id }),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={submitLoading}
      title={intl.formatMessage({ id: 'object_storage.bucket_policy_title' })}
      formName="object_storage_bucket_policy_change_form"
      okText={intl.formatMessage({ id: 'update' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Spin spinning={fetchLoading}>
        <Form
          form={form}
          name="object_storage_bucket_policy_change_form"
          layout="inline"
          labelAlign="left"
          onFinish={handleFinish}
        >
          <FormInput
            required
            hasLabel
            type="switch"
            name="is_public"
            label={intl.formatMessage({ id: 'public' })}
            placeholder={intl.formatMessage({ id: 'public' })}
          />
        </Form>
      </Spin>
    </ModalForm>
  )
}

export default BucketPolicy
