import { notification } from 'antd'
import { deleteBucket } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface BucketDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  bucket: { id: string; isCloud: boolean }
}

const BucketDelete: React.FC<BucketDeleteProps> = ({ visible, bucket, onOk, onCancel }) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [vis, setVis] = useState<boolean>(visible)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteBucket({
      data: { bucket_name: bucket.id, is_cloud: bucket.isCloud },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'object_storage.bucket_deleted' }, { name: bucket.id }),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'delete' })}
      selectedNames={bucket.id}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default BucketDelete
