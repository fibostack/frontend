import { Form, notification, Radio, Tooltip, Row, Col } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createCloudBucket } from 'api'
import { FormInput } from 'components'
import React from 'react'
import { useIntl } from 'react-intl'
import { QuestionCircleOutlined } from '@ant-design/icons'
import ResourceInput from './resource_input'

interface Props {
  setLoading: React.Dispatch<React.SetStateAction<boolean>>
  handleOk: () => void
}

const Cloud: React.FC<Props> = ({ setLoading, handleOk }) => {
  const intl = useIntl()
  const [form] = Form.useForm()

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await createCloudBucket({ data: values })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'object_storage.bucket_created' }, { name: values.name }),
      })
      form.resetFields()
      handleOk()
    }
  }

  return (
    <Form
      form={form}
      name="object_storage_bucket_create_form"
      layout="vertical"
      labelAlign="left"
      onFinish={handleFinish}
      initialValues={{ data_placement: 'SPREAD' }}
    >
      <FormInput
        required
        hasLabel
        name="name"
        type="input"
        label={intl.formatMessage({ id: 'name' })}
        placeholder={intl.formatMessage({ id: 'name' })}
        rules={[
          {
            pattern: /(?!^(\d{1,3}\.){3}\d{1,3}$)(^[a-z0-9]([a-z0-9-]*(\.[a-z0-9])?)*$(?<!\-))/g,
            message: intl.formatMessage({ id: 'valid.s3_name' }),
          },
        ]}
      />
      <FormInput
        required
        hasLabel
        name="data_placement"
        label={
          <Row gutter={8}>
            <Col>{intl.formatMessage({ id: 'object_storage.policy_type' })}</Col>
            <Col>
              <Tooltip
                placement="rightTop"
                title={
                  <ul className="nostyle">
                    <li>
                      {intl.formatMessage({ id: 'object_storage.spread' })} -{' '}
                      {intl.formatMessage({ id: 'object_storage.spread_info' })}
                    </li>
                    <br />
                    <li>
                      {intl.formatMessage({ id: 'object_storage.mirror' })} -{' '}
                      {intl.formatMessage({ id: 'object_storage.mirror_info' })}
                    </li>
                  </ul>
                }
              >
                <QuestionCircleOutlined />
              </Tooltip>
            </Col>
          </Row>
        }
      >
        <Radio.Group buttonStyle="solid">
          <Radio.Button value="SPREAD">{intl.formatMessage({ id: 'object_storage.spread' })}</Radio.Button>
          <Radio.Button value="MIRROR">{intl.formatMessage({ id: 'object_storage.mirror' })}</Radio.Button>
        </Radio.Group>
      </FormInput>
      <Form.Item required noStyle shouldUpdate={(o, n) => o.data_placement !== n.data_placement}>
        {({ getFieldValue }) => {
          const type = getFieldValue('data_placement') as 'SPREAD' | 'MIRROR'
          return (
            <FormInput
              required
              hasLabel
              name="attached_pools"
              label={
                <Row gutter={8}>
                  <Col>{intl.formatMessage({ id: 'object_storage.resources' })}</Col>
                  <Col>
                    <Tooltip placement="rightTop" title={intl.formatMessage({ id: 'object_storage.resources_info' })}>
                      <QuestionCircleOutlined />
                    </Tooltip>
                  </Col>
                </Row>
              }
              rules={
                type === 'MIRROR'
                  ? [
                      {
                        type: 'array',
                        min: 2,
                        message: intl.formatMessage({ id: 'object_storage.mirror_resources_valid' }),
                      },
                    ]
                  : []
              }
            >
              <ResourceInput />
            </FormInput>
          )
        }}
      </Form.Item>
    </Form>
  )
}

export default Cloud
