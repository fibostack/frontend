import { CheckCircleOutlined, UploadOutlined } from '@ant-design/icons'
import { Alert, Button, Form, notification, Select, Upload } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { checkCloudConnection, createCloudConnection } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { fileToText } from 'utils'

interface Props {
  visible: boolean
  onOk: Function
  onCancel: Function
}

const serviceValues: { key: string; value: string; suggestEndpoint?: string }[] = [
  { key: 'AWS S3', value: 'AWS', suggestEndpoint: 'https://s3.amazonaws.com' },
  { key: 'Azure Blob', value: 'AZURE', suggestEndpoint: 'https://blob.core.windows.net' },
  { key: 'Google', value: 'GOOGLE', suggestEndpoint: 'www.googleapis.com' },
  { key: 'S3 V2 Compatible service', value: 'S3_V2_COMPATIBLE', suggestEndpoint: '' },
  { key: 'S3 V4 Compatible service', value: 'S3_V4_COMPATIBLE', suggestEndpoint: '' },
  { key: 'FlashBlade', value: 'FLASHBLADE', suggestEndpoint: '' },
  { key: 'NetStorage', value: 'NET_STORAGE', suggestEndpoint: '' },
  { key: 'IBM Cloud Object Storage', value: 'IBM_COS', suggestEndpoint: '' },
]

type ServiceType =
  | 'AWS'
  | 'AZURE'
  | 'S3_V2_COMPATIBLE'
  | 'S3_V4_COMPATIBLE'
  | 'GOOGLE'
  | 'FLASHBLADE'
  | 'NET_STORAGE'
  | 'IBM_COS'

interface CheckError {
  code: string
  message: string
}
interface CheckResponse {
  error: CheckError
  status: string
}

const ConnectionCreate: React.FC<Props> = ({ visible, onOk, onCancel }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [finishLoading, setFinishLoading] = useState<boolean>(false)
  const [checkLoading, setCheckLoading] = useState<boolean>(false)
  const [valid, setValid] = useState<boolean>(false)
  const [error, setError] = useState<CheckError>()
  const [GCPJsonfile, setGCPJsonfile] = useState<File>()

  const handleFinish = async (values: Store) => {
    setFinishLoading(true)
    if (values.service === 'GOOGLE') {
      if (GCPJsonfile) {
        const fileTxt = (await fileToText(GCPJsonfile)) as string
        const data = JSON.parse(fileTxt)
        values.endpoint = 'www.googleapis.com'
        values.identity = data.private_key_id
        values.secret = fileTxt
      }
    }
    const success = await createCloudConnection({
      data: values,
    })
    setFinishLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'object_storage.connection_created' }, { name: values.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.name)
      }, CloseAwaitMS)
    }
  }

  const handleCheck = async () => {
    const values = await form.validateFields()
    setCheckLoading(true)
    if (values.service === 'GOOGLE') {
      if (GCPJsonfile) {
        const fileTxt = (await fileToText(GCPJsonfile)) as string
        const data = JSON.parse(fileTxt)
        values.endpoint = 'www.googleapis.com'
        values.identity = data.private_key_id
        values.secret = fileTxt
      }
    }
    const check = await checkCloudConnection({
      data: values,
    })
    setCheckLoading(false)
    if (check) {
      const tmpRes = check as CheckResponse
      if (tmpRes.status === 'SUCCESS') {
        setValid(true)
        setError(undefined)
      } else {
        setValid(false)
        setError(tmpRes.error)
      }
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={finishLoading}
      title={<FormattedMessage id="object_storage.create_connection_title" />}
      formName="connection_create_form"
      okText={<FormattedMessage id="create" />}
      okDisable={!valid}
      cancelText={<FormattedMessage id="cancel" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form
        form={form}
        name="connection_create_form"
        layout="vertical"
        labelAlign="left"
        onFinish={handleFinish}
        initialValues={{ service: 'AWS' }}
      >
        <FormInput
          required
          hasLabel
          name="name"
          type="input"
          label={intl.formatMessage({ id: 'name' })}
          placeholder={intl.formatMessage({ id: 'name' })}
        />
        <FormInput required hasLabel label={<FormattedMessage id="object_storage.service" />} name="service">
          <Select placeholder={<FormattedMessage id="object_storage.service" />}>
            {serviceValues.map((ser) => (
              <Select.Option key={ser.value} value={ser.value}>
                {ser.key}
              </Select.Option>
            ))}
          </Select>
        </FormInput>
        <Form.Item required noStyle shouldUpdate={(o, n) => o.service !== n.service}>
          {({ getFieldValue, setFieldsValue }) => {
            const service = getFieldValue('service') as ServiceType
            setFieldsValue({
              endpoint: serviceValues.find((item) => item.value === service)?.suggestEndpoint || '',
            })
            switch (service) {
              case 'AZURE':
                return (
                  <>
                    <FormInput
                      required
                      hasLabel
                      name="endpoint"
                      type="input"
                      label={intl.formatMessage({ id: 'object_storage.endpoint' })}
                      placeholder={intl.formatMessage({ id: 'object_storage.endpoint' })}
                    />
                    <FormInput
                      required
                      hasLabel
                      name="identity"
                      type="input"
                      label={intl.formatMessage({ id: 'object_storage.account_name' })}
                      placeholder={intl.formatMessage({ id: 'object_storage.account_name' })}
                    />
                    <FormInput
                      required
                      hasLabel
                      name="secret"
                      type="input"
                      label={intl.formatMessage({ id: 'object_storage.account_key' })}
                      placeholder={intl.formatMessage({ id: 'object_storage.account_key' })}
                    />
                  </>
                )
              case 'GOOGLE':
                return (
                  <FormInput
                    required
                    hasLabel
                    valuePropName="fileList"
                    label={<FormattedMessage id="object_storage.service_account_keys" />}
                    name="service_account_keys"
                  >
                    <Upload
                      accept=".json"
                      multiple={false}
                      showUploadList={false}
                      name="service_account_keys"
                      beforeUpload={(value) => {
                        setGCPJsonfile(value)
                        return false
                      }}
                    >
                      <Button icon={<UploadOutlined />}>
                        {GCPJsonfile ? GCPJsonfile.name : intl.formatMessage({ id: 'upload' })}
                      </Button>
                    </Upload>
                  </FormInput>
                )
              default:
                return (
                  <>
                    <FormInput
                      required
                      hasLabel
                      name="endpoint"
                      type="input"
                      label={intl.formatMessage({ id: 'object_storage.endpoint' })}
                      placeholder={intl.formatMessage({ id: 'object_storage.endpoint' })}
                    />
                    <FormInput
                      required
                      hasLabel
                      name="identity"
                      type="input"
                      label={intl.formatMessage({ id: 'access_key' })}
                      placeholder={intl.formatMessage({ id: 'access_key' })}
                    />
                    <FormInput
                      required
                      hasLabel
                      name="secret"
                      type="input"
                      label={intl.formatMessage({ id: 'secret_key' })}
                      placeholder={intl.formatMessage({ id: 'secret_key' })}
                    />
                  </>
                )
            }
          }}
        </Form.Item>
        <Button
          type="primary"
          className="m_b_1"
          icon={<CheckCircleOutlined />}
          loading={checkLoading}
          onClick={() => {
            handleCheck()
          }}
        >
          <FormattedMessage id="object_storage.connection_check" />
        </Button>
        {error && <Alert message={error.message} type="error" showIcon />}
        {valid && (
          <Alert message={intl.formatMessage({ id: 'object_storage.connection_success' })} type="success" showIcon />
        )}
      </Form>
    </ModalForm>
  )
}

export default ConnectionCreate
