import { Form, notification, Select } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createCloudResource, getCloudBuckets } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import ConnectionInput from './connection'

interface Props {
  visible: boolean
  onOk: Function
  onCancel: Function
}

const ResourceCreate: React.FC<Props> = ({ visible, onOk, onCancel }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const _isMounted = useRef(true)
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)
  const [loadingTarget, setLoadingTarget] = useState<boolean>(false)
  const [targets, setTargets] = useState<{ name: string }[]>([])
  const [conName, setConName] = useState<string>()

  useEffect(() => {
    return () => {
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    if (conName) fetchTarget(conName)
  }, [conName])

  const fetchTarget = async (name: string) => {
    setLoadingTarget(true)
    const clds = await getCloudBuckets({}, name)
    if (_isMounted.current) {
      if (clds) setTargets(clds as { name: string }[])
      setLoadingTarget(false)
    }
  }

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await createCloudResource({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'object_storage.cloud_resource_created' }, { name: values.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.name)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="object_storage.create_resource_title" />}
      formName="resource_create_form"
      okText={<FormattedMessage id="create" />}
      cancelText={<FormattedMessage id="cancel" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="resource_create_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <FormInput
          required
          hasLabel
          label={<FormattedMessage id="object_storage.target_connection" />}
          name="connection"
        >
          <ConnectionInput onChange={setConName} />
        </FormInput>
        <FormInput required hasLabel label={<FormattedMessage id="object_storage.target" />} name="target_bucket">
          <Select placeholder={<FormattedMessage id="object_storage.target" />} loading={loadingTarget}>
            {targets.map((target) => (
              <Select.Option key={target.name} value={target.name}>
                {target.name}
              </Select.Option>
            ))}
          </Select>
        </FormInput>
        <FormInput
          required
          hasLabel
          name="name"
          type="input"
          label={<FormattedMessage id="name" />}
          placeholder={intl.formatMessage({ id: 'name' })}
        />
      </Form>
    </ModalForm>
  )
}

export default ResourceCreate
