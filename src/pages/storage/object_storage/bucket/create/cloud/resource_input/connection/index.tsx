import { PlusCircleOutlined } from '@ant-design/icons'
import { Button, Input, Select } from 'antd'
import { listCloudConnection } from 'api'
import { CloudConnection } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import CreateModal from './create'

interface Props {
  value?: string
  onChange?: (value: string) => void
}

const Selector: React.FC<Props> = React.forwardRef<any, Props>(({ value, onChange }, ref) => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState<boolean>(true)
  const [data, setData] = useState<string | undefined>(value)
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [connections, setConnections] = useState<CloudConnection[]>([])

  useEffect(() => {
    fetchData()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchData = async () => {
    setLoading(true)
    const cons = await listCloudConnection({})
    if (_isMounted.current) {
      if (cons && cons.connections) setConnections(cons.connections as CloudConnection[])
      setLoading(false)
    }
  }

  const handleChange = (val: string) => {
    setData(val)
    if (onChange) {
      onChange(val)
    }
  }

  return (
    <>
      {createVisible && (
        <CreateModal
          visible={createVisible}
          onOk={(newName: string) => {
            fetchData()
            handleChange(newName)
            setCreateVisible(false)
          }}
          onCancel={() => {
            setCreateVisible(false)
          }}
        />
      )}
      <Input.Group compact className="flex row">
        <Select
          ref={ref}
          showSearch
          value={data}
          style={{ flex: 1 }}
          onChange={handleChange}
          placeholder={intl.formatMessage({ id: 'object_storage.target_connection' })}
          optionFilterProp="children"
          filterOption={(input, option) =>
            option ? option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 : false
          }
        >
          {connections.map((connection) => {
            return (
              <Select.Option key={connection.identity} value={connection.name}>
                {connection.name}
              </Select.Option>
            )
          })}
        </Select>
        <Button
          loading={loading}
          style={{ borderLeft: 'none' }}
          icon={<PlusCircleOutlined />}
          onClick={() => {
            setCreateVisible(true)
          }}
        >
          {intl.formatMessage({ id: 'object_storage.create_connection' })}
        </Button>
      </Input.Group>
    </>
  )
})

export default Selector
