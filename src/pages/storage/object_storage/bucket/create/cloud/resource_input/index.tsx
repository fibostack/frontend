import { PlusCircleOutlined, CloseCircleOutlined, CheckCircleOutlined, WarningOutlined } from '@ant-design/icons'
import { Button, Table, Tooltip } from 'antd'
import { listCloudResource } from 'api'
import { CloudResource, CloudResourceState } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import CreateModal from './create'

interface Props {
  value?: React.ReactText[]
  onChange?: (value: React.ReactText[]) => void
}

const ResourceInput: React.FC<Props> = React.forwardRef<HTMLSpanElement, Props>(({ value = [], onChange }, ref) => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState<boolean>(true)
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [data, setData] = useState<CloudResource[]>([])

  useEffect(() => {
    fetchData()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchData = async () => {
    setLoading(true)
    const clds = await listCloudResource({})
    if (_isMounted.current) {
      if (clds) setData(clds as CloudResource[])
      setLoading(false)
    }
  }

  const cloudResourceState = (state: CloudResourceState) => {
    const renderStateIcon = () => {
      switch (state.css) {
        case 'error':
          return <CloseCircleOutlined style={{ fontSize: 18, marginRight: 4, color: 'red' }} />
        case 'success':
          return <CheckCircleOutlined style={{ fontSize: 18, marginRight: 4, color: 'green' }} />
        case 'warning':
          return <WarningOutlined style={{ fontSize: 18, marginRight: 4, color: 'orange' }} />
        default:
          return state.css
      }
    }

    return (
      <Tooltip title={state.tooltip} placement="top">
        <span>{renderStateIcon()}</span>
      </Tooltip>
    )
  }

  return (
    <span ref={ref}>
      {createVisible && (
        <CreateModal
          visible={createVisible}
          onOk={() => {
            setCreateVisible(false)
            fetchData()
          }}
          onCancel={() => {
            setCreateVisible(false)
          }}
        />
      )}
      <Table
        rowKey="name"
        loading={loading}
        pagination={false}
        size="small"
        columns={[
          {
            title: intl.formatMessage({ id: 'name' }),
            dataIndex: 'name',
            render: (val: string, record) => (
              <span>
                {cloudResourceState(record.state)} {val}
              </span>
            ),
          },
          {
            title: intl.formatMessage({ id: 'type' }),
            dataIndex: 'resource_type',
            render: (val: string, record) => (val === 'CLOUD' ? record.cloud_info.endpoint_type : val),
          },
        ]}
        rowSelection={{
          fixed: true,
          selectedRowKeys: value,
          onChange: (selectRowKeys) => {
            if (onChange) onChange(selectRowKeys)
          },
        }}
        dataSource={data}
        footer={() => (
          <Button
            type="primary"
            icon={<PlusCircleOutlined />}
            onClick={() => {
              setCreateVisible(true)
            }}
          >
            {intl.formatMessage({ id: 'object_storage.add_resource' })}
          </Button>
        )}
      />
    </span>
  )
})

export default ResourceInput
