import { Form, notification, Select, Spin } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { listCloudResource, setupCloudBucket } from 'api'
import { FormInput } from 'components'
import { BucketType, CloudResource } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'

interface Props {
  setType: React.Dispatch<React.SetStateAction<BucketType | undefined>>
  setLoading: React.Dispatch<React.SetStateAction<boolean>>
}

const FirstTimeCloud: React.FC<Props> = ({ setType, setLoading }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const _isMounted = useRef(true)
  const [fetchLoading, setFetchLoading] = useState<boolean>(true)
  const [resources, setResources] = useState<CloudResource[]>([])

  useEffect(() => {
    fetchData()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchData = async () => {
    setFetchLoading(true)
    const clds = await listCloudResource({})
    if (_isMounted.current) {
      if (clds) setResources(clds as CloudResource[])
      setFetchLoading(false)
    }
  }

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.default_pool = values.default_pool || ''
    const success = await setupCloudBucket({ data: values })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'object_storage.cloud_bucket_configured' }),
      })
      form.resetFields()
      setType('cloud')
    }
  }

  return (
    <Form
      form={form}
      name="object_storage_bucket_create_form"
      layout="vertical"
      labelAlign="left"
      onFinish={handleFinish}
    >
      <Spin spinning={fetchLoading}>
        <p>{intl.formatMessage({ id: 'object_storage.first_time_cloud_info_1' })}</p>
        <p>{intl.formatMessage({ id: 'object_storage.first_time_cloud_info_2' })}</p>
        <p>{intl.formatMessage({ id: 'object_storage.first_time_cloud_info_3' })}</p>
        <p>{intl.formatMessage({ id: 'object_storage.first_time_cloud_info_4' })}</p>
        <FormInput
          hasLabel
          required={resources.length > 0}
          label={intl.formatMessage({ id: 'object_storage.resources' })}
          name="default_pool"
        >
          <Select placeholder={intl.formatMessage({ id: 'object_storage.resources' })} loading={fetchLoading}>
            {resources.map((res) => (
              <Select.Option key={res.name} value={res.name}>
                {res.name}
              </Select.Option>
            ))}
          </Select>
        </FormInput>
      </Spin>
    </Form>
  )
}
export default FirstTimeCloud
