import Icon from '@ant-design/icons'
import { Button, Card, Spin } from 'antd'
import { checkAccessCloudBucket } from 'api'
import { ReactComponent as CloudIcon } from 'assets/icons/cloud.svg'
import { ReactComponent as LocalIcon } from 'assets/icons/local.svg'
import { BucketType } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'
import styles from './styles.module.scss'

interface Props {
  setType: React.Dispatch<React.SetStateAction<BucketType | undefined>>
}

const ChooseBucketType: React.FC<Props> = ({ setType }) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)

  const checkCloudBucketAccess = async () => {
    setLoading(true)
    const success = await checkAccessCloudBucket({})
    setLoading(false)
    if (success) {
      if (success.check) {
        setType('cloud')
      } else {
        setType('first_cloud')
      }
    }
  }

  return (
    <div>
      <Card.Grid className="w-fill m_b_1 p_0" style={{ borderColor: '#000' }}>
        <Button
          block
          type="default"
          className={styles.cardButton}
          icon={<Icon component={LocalIcon} style={{ fontSize: '2rem' }} />}
          onClick={() => {
            setType('onpremise')
          }}
        >
          <span className={styles.cardTitle}>{intl.formatMessage({ id: 'object_storage.on_premise_bucket' })}</span>
        </Button>
      </Card.Grid>
      <Card.Grid className="w-fill p_0">
        <Spin spinning={loading}>
          <Button
            block
            type="default"
            className={styles.cardButton}
            icon={<Icon component={CloudIcon} style={{ fontSize: '2rem' }} />}
            onClick={() => {
              checkCloudBucketAccess()
            }}
          >
            <span className={styles.cardTitle}>{intl.formatMessage({ id: 'object_storage.cloud_bucket' })}</span>
          </Button>
        </Spin>
      </Card.Grid>
    </div>
  )
}
export default ChooseBucketType
