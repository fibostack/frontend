import { SettingOutlined } from '@ant-design/icons'
import { ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { BucketType } from 'models'
import React, { useState } from 'react'
import { FormattedMessage } from 'react-intl'
import ChooseBucketType from './bucket_type'
import CreateCloud from './cloud'
import FirstTimeCloud from './first_time_cloud'
import CreateOnPremise from './on_premise'

interface BucketCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
}

const BucketCreate: React.FC<BucketCreateProps> = ({ onOk, visible, onCancel }) => {
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)
  const [type, setType] = useState<BucketType>()

  const renderNavigation = () => {
    if (!type) return <ChooseBucketType setType={setType} />
    if (type === 'onpremise') return <CreateOnPremise handleOk={handleOk} setLoading={setLoading} />
    if (type === 'cloud') return <CreateCloud handleOk={handleOk} setLoading={setLoading} />
    if (type === 'first_cloud') return <FirstTimeCloud setType={setType} setLoading={setLoading} />
    return <></>
  }

  const handleOk = () => {
    setVis(false)
    setTimeout(() => {
      onOk()
    }, CloseAwaitMS)
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="object_storage.bucket_create_title" />}
      formName="object_storage_bucket_create_form"
      okText={type === 'first_cloud' ? <FormattedMessage id="setup" /> : <FormattedMessage id="create" />}
      okIcon={type === 'first_cloud' && <SettingOutlined />}
      okDisable={!type}
      cancelText={<FormattedMessage id="cancel" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      {renderNavigation()}
    </ModalForm>
  )
}

export default BucketCreate
