import { Form, notification } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createBucket } from 'api'
import { FormInput } from 'components'
import React from 'react'
import { useIntl } from 'react-intl'

interface Props {
  setLoading: React.Dispatch<React.SetStateAction<boolean>>
  handleOk: () => void
}

const OnPremise: React.FC<Props> = ({ setLoading, handleOk }) => {
  const intl = useIntl()
  const [form] = Form.useForm()

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await createBucket({ data: values })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'object_storage.bucket_created' }, { name: values.bucket_name }),
      })
      form.resetFields()
      handleOk()
    }
  }

  return (
    <Form
      form={form}
      name="object_storage_bucket_create_form"
      layout="vertical"
      labelAlign="left"
      onFinish={handleFinish}
    >
      <FormInput
        required
        hasLabel
        name="bucket_name"
        type="input"
        label={intl.formatMessage({ id: 'name' })}
        placeholder={intl.formatMessage({ id: 'name' })}
        rules={[
          {
            pattern: /(?!^(\d{1,3}\.){3}\d{1,3}$)(^[a-z0-9]([a-z0-9-]*(\.[a-z0-9])?)*$(?<!\-))/g,
            message: intl.formatMessage({ id: 'valid.s3_name' }),
          },
        ]}
      />
    </Form>
  )
}

export default OnPremise
