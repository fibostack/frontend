import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  DatabaseOutlined,
  DatabaseTwoTone,
  FolderAddOutlined,
  LoadingOutlined,
  PlusOutlined,
  UploadOutlined,
  WarningOutlined,
} from '@ant-design/icons'
import { Button, Dropdown, Layout, Menu, Spin, Tooltip, Tree } from 'antd'
import { listBucket, listCloudBucket } from 'api'
import { Bucket, CloudBucket, CloudBucketState, SelectedBucket } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { ContextMenuTrigger } from 'react-contextmenu'
import { FormattedMessage } from 'react-intl'
import { BucketCreateModal, BucketDeleteModal, BucketPolicyModal } from './bucket'
import { BucketContextMenu } from './context_menus'
import styles from './styles.module.scss'

interface Props {
  handleAction: (action: string) => void
  onSelectedBucket: (value: SelectedBucket) => void
}

const Sidemenu: React.FC<Props> = ({ handleAction, onSelectedBucket }) => {
  const _isMounted = useRef<boolean>(true)
  const [onPremiseLoading, setOnPremiseLoading] = useState<boolean>(true)
  const [cloudLoading, setCloudLoading] = useState<boolean>(true)
  const [buckets, setBuckets] = useState<Bucket[]>([])
  const [cloudBuckets, setCloudBuckets] = useState<CloudBucket[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [policyVisible, setPolicyVisible] = useState<{ id: string; isCloud: boolean }>()
  const [deleteVisible, setDeleteVisible] = useState<{ id: string; isCloud: boolean }>()
  const [selectedFolder, setSelectedFolder] = useState<React.Key[]>(['0'])

  useEffect(() => {
    fetchOnPremise()
    fetchCloud()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchOnPremise = async () => {
    setOnPremiseLoading(true)
    const bks = await listBucket({ data: { is_cloud: false } })
    if (_isMounted.current) {
      if (bks) setBuckets(bks as Bucket[])
      setOnPremiseLoading(false)
    }
  }

  const fetchCloud = async () => {
    setCloudLoading(true)
    const cbks = await listCloudBucket({})
    if (_isMounted.current) {
      if (cbks) setCloudBuckets(cbks as CloudBucket[])
      setCloudLoading(false)
    }
  }

  const BucketHandleAction = (action: string, id: string, isCloud: boolean) => {
    switch (action) {
      case 'delete':
        setDeleteVisible({ id, isCloud })
        break
      case 'policy':
        setPolicyVisible({ id, isCloud })
        break
      default:
        break
    }
  }

  const folders = buckets
    ? buckets.reduce<any[]>(
        (acc, item) => [
          ...acc,
          {
            key: item.name,
            title: (
              <ContextMenuTrigger
                id="bucket_item"
                holdToDisplay={1000}
                attributes={{ 'aria-valuetext': item.name, 'aria-valuenow': 0 }}
              >
                <div className="flex row js ac">
                  <DatabaseTwoTone style={{ fontSize: 20, marginRight: 8 }} />
                  <strong>{item.name}</strong>
                </div>
              </ContextMenuTrigger>
            ),
          },
        ],
        []
      )
    : []

  const cloudBucketState = (state: CloudBucketState) => {
    const renderIcon = () => {
      switch (state.css) {
        case 'error':
          return <CloseCircleOutlined style={{ fontSize: 18, marginRight: 4, color: 'red' }} />
        case 'processing':
          return <LoadingOutlined style={{ fontSize: 18, marginRight: 4, color: 'blue' }} />
        case 'success':
          return <CheckCircleOutlined style={{ fontSize: 18, marginRight: 4, color: 'green' }} />
        case 'warning':
          return <WarningOutlined style={{ fontSize: 18, marginRight: 4, color: 'orange' }} />
        default:
          return state.css
      }
    }

    return (
      <Tooltip title={state.tooltip} placement="top">
        {renderIcon()}
      </Tooltip>
    )
  }

  const cloudFolders = cloudBuckets
    ? cloudBuckets.reduce<any[]>(
        (acc, item) => [
          ...acc,
          {
            key: item.name,
            title: (
              <ContextMenuTrigger
                id="bucket_item"
                holdToDisplay={1000}
                attributes={{ 'aria-valuetext': item.name, 'aria-valuenow': 1 }}
              >
                <div className="flex row js ac">
                  {cloudBucketState(item.state)}
                  <DatabaseTwoTone style={{ fontSize: 20, marginRight: 8 }} />
                  <strong>{item.name}</strong>
                </div>
              </ContextMenuTrigger>
            ),
          },
        ],
        []
      )
    : []

  return (
    <Layout.Sider theme="light" width={250} className={styles.sidemenu}>
      <div className={styles.upload}>
        <Dropdown
          overlay={
            <Menu>
              <Menu.Item
                key="upload_file"
                icon={<UploadOutlined />}
                onClick={() => {
                  handleAction('upload_file')
                }}
              >
                <FormattedMessage id="upload" />
              </Menu.Item>
              <Menu.Divider />
              <Menu.Item
                key="new_folder"
                icon={<FolderAddOutlined />}
                onClick={() => {
                  handleAction('new_folder')
                }}
              >
                <FormattedMessage id="new_folder" />
              </Menu.Item>
              <Menu.Divider />
              <Menu.Item
                key="new_bucket"
                icon={<DatabaseOutlined />}
                onClick={() => {
                  setCreateVisible(true)
                }}
              >
                <FormattedMessage id="new_bucket" />
              </Menu.Item>
            </Menu>
          }
          trigger={['click']}
          className="m_r_1"
        >
          <Button block type="primary" icon={<PlusOutlined />}>
            <FormattedMessage id="add" />
          </Button>
        </Dropdown>
        {createVisible && (
          <BucketCreateModal
            visible={createVisible}
            onOk={() => {
              fetchCloud()
              fetchOnPremise()
              setCreateVisible(false)
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {policyVisible && (
          <BucketPolicyModal
            bucket={policyVisible}
            visible={!!policyVisible}
            onOk={() => {
              setPolicyVisible(undefined)
              fetchCloud()
              fetchOnPremise()
            }}
            onCancel={() => {
              setPolicyVisible(undefined)
            }}
          />
        )}
        {deleteVisible && (
          <BucketDeleteModal
            bucket={deleteVisible}
            visible={!!deleteVisible}
            onOk={() => {
              fetchCloud()
              fetchOnPremise()
              setDeleteVisible(undefined)
            }}
            onCancel={() => {
              setDeleteVisible(undefined)
            }}
          />
        )}
      </div>
      <Spin spinning={onPremiseLoading}>
        <strong>
          <FormattedMessage id="object_storage.on_premise_storage" />
        </strong>
        <Tree
          blockNode
          draggable
          showLine={{ showLeafIcon: false }}
          selectedKeys={selectedFolder}
          onSelect={(key) => {
            if (key.length > 0) {
              setSelectedFolder(key)
              onSelectedBucket({ name: key[0].toString(), isCloud: false })
            }
          }}
          onDrop={console.log}
          treeData={folders}
        />
      </Spin>
      <Spin spinning={cloudLoading}>
        <strong>
          <FormattedMessage id="object_storage.cloud_storage" />
        </strong>
        <Tree
          blockNode
          draggable
          showLine={{ showLeafIcon: false }}
          selectedKeys={selectedFolder}
          onSelect={(key) => {
            if (key.length > 0) {
              setSelectedFolder(key)
              onSelectedBucket({ name: key[0].toString(), isCloud: true })
            }
          }}
          onDrop={console.log}
          treeData={cloudFolders}
        />
        <BucketContextMenu id="bucket_item" handleAction={BucketHandleAction} />
      </Spin>
    </Layout.Sider>
  )
}

export default Sidemenu
