import { Form, notification } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { extendVolume } from 'api'
import { ModalForm, SizeInput, FormInput } from 'components'
import { CloseAwaitMS } from 'configs'
import { Volume } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { isEmptyString } from 'utils'

interface VolumeExtendProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  volume: Volume
}

const VolumeExtend: React.FC<VolumeExtendProps> = ({ onOk, visible, onCancel, volume }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await extendVolume({
      data: { ...values, volumeID: volume.id },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${isEmptyString(volume.name) ? volume.name : volume.name} volume is extended.`,
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={
        <FormattedMessage
          id="volume.extend_title"
          values={{
            name: isEmptyString(volume.name) ? volume.name : volume.id,
            size: volume.size,
          }}
        />
      }
      formName="volume_extend_form"
      okText={<FormattedMessage id="volume.extend" />}
      cancelText={<FormattedMessage id="cancel" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="volume_extend_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <FormInput
          required
          hasLabel
          label={<FormattedMessage id="volume.new_size" />}
          name="newSize"
          className="form_last_item"
        >
          <SizeInput min={volume.size} max={1024} />
        </FormInput>
      </Form>
    </ModalForm>
  )
}

export default VolumeExtend
