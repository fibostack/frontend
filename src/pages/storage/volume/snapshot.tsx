import { Form, notification } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { snapshotVolume } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { Volume } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { isEmptyString } from 'utils'

interface VolumeSnapshotProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  volume: Volume
}

const VolumeSnapshot: React.FC<VolumeSnapshotProps> = ({ visible, onOk, onCancel, volume }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await snapshotVolume({
      data: { ...values, volumeID: volume.id },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${isEmptyString(volume.name) ? volume.name : volume.name} volume is created snapshot.`,
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={
        <FormattedMessage
          id="volume.snapshot_title"
          values={{
            name: isEmptyString(volume.name) ? volume.name : volume.id,
            size: volume.size,
          }}
        />
      }
      formName="volume_snapshot_form"
      okText={<FormattedMessage id="snapshot" />}
      cancelText={<FormattedMessage id="cancel" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="volume_snapshot_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <FormInput
          required
          hasLabel
          name="name"
          type="input"
          label={intl.formatMessage({ id: 'name' })}
          placeholder={intl.formatMessage({ id: 'name' })}
        />
      </Form>
    </ModalForm>
  )
}

export default VolumeSnapshot
