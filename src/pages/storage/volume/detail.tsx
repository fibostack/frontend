import { Divider } from 'antd'
import { BooleanStatus, DetailModal, KeyValue } from 'components'
import { CloseAwaitMS } from 'configs'
import { Volume } from 'models'
import React, { useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router-dom'
import { isEmptyString } from 'utils'
import Status from './status'

interface VolumeDetailProps {
  visible: boolean
  onOk: Function
  volume: Volume
}

const VolumeDetail: React.FC<VolumeDetailProps> = ({ onOk, visible, volume }) => {
  const [vis, setVis] = useState<boolean>(visible)
  return (
    <DetailModal
      visible={vis}
      title={
        <FormattedMessage
          id="volume.detail_title"
          values={{
            name: isEmptyString(volume.name) ? volume.name : volume.id,
          }}
        />
      }
      okText={<FormattedMessage id="ok" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onOk()
        }, CloseAwaitMS)
      }}
    >
      <KeyValue title={<FormattedMessage id="id" />} value={volume.id} />
      <KeyValue title={<FormattedMessage id="name" />} value={volume.name} />
      <KeyValue title={<FormattedMessage id="status" />} value={<Status status={volume.status} />} />
      <Divider>
        <FormattedMessage id="specs" />
      </Divider>
      <KeyValue title={<FormattedMessage id="size" />} value={`${volume.size} GB`} />
      <KeyValue
        title={<FormattedMessage id="volume.bootable" />}
        value={<BooleanStatus status={volume.bootable === 'true'} />}
      />
      <KeyValue
        title={<FormattedMessage id="volume.encrypted" />}
        value={<BooleanStatus status={volume.encrypted} />}
      />
      <Divider>
        <FormattedMessage id="volume.attachments" />
      </Divider>
      <KeyValue
        title={<FormattedMessage id="volume.attached_to" />}
        value={
          volume.status === 'in-use' ? (
            <>
              <Link to={`/compute/instances?id=${volume.attachments[0].server_id}`}>
                {volume.attachments[0].server_id}
              </Link>
              {` on ${volume.attachments[0].device}`}
            </>
          ) : (
            <BooleanStatus status={false} falseText="not attached" />
          )
        }
      />
      <Divider>
        <FormattedMessage id="volume.volume_source" />
      </Divider>
      <KeyValue title={<FormattedMessage id="volume.image" />} value={volume.volume_image_metadata.image_name} />
    </DetailModal>
  )
}

export default VolumeDetail
