import { Form, notification } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createVolume } from 'api'
import { FormInput, ModalForm, SizeInput } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

interface VolumeCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
}

const VolumeCreate: React.FC<VolumeCreateProps> = ({ onOk, visible, onCancel }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await createVolume({ data: values })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${values.name} volume is created.`,
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="volume.create_title" />}
      formName="volume_create_form"
      okText={<FormattedMessage id="create" />}
      cancelText={<FormattedMessage id="cancel" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="volume_create_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <FormInput
          required
          hasLabel
          name="name"
          type="input"
          label={intl.formatMessage({ id: 'name' })}
          placeholder={intl.formatMessage({ id: 'name' })}
        />
        <FormInput required hasLabel name="size" label={<FormattedMessage id="size" />}>
          <SizeInput min={20} max={1024} />
        </FormInput>
        <FormInput
          required
          hasLabel
          name="description"
          type="textarea"
          className="form_last_item"
          label={intl.formatMessage({ id: 'description' })}
          placeholder={intl.formatMessage({ id: 'description' })}
        />
      </Form>
    </ModalForm>
  )
}

export default VolumeCreate
