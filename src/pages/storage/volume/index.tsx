import { ArrowsAltOutlined, BlockOutlined, DeleteOutlined } from '@ant-design/icons'
import { Button } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listVolume } from 'api'
import { BooleanStatus, PageLayout, Table, Ligther } from 'components'
import { useQuery } from 'hooks'
import { MenuAction, Volume } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { useLocation } from 'react-router-dom'
import { isEmptyString, findInString } from 'utils'
import CreateModal from './create'
import DeleteModal from './delete'
import DetailModal from './detail'
import ExtendModal from './extend'
import SnapshotModal from './snapshot'
import Status from './status'

interface VolumeProps {}

const VolumeList: React.FC<VolumeProps> = () => {
  const id = useQuery(useLocation().search).get('id')
  const _canRedirect = useRef<boolean>(true)
  const _isMounted = useRef<boolean>(true)
  const timer = useRef<NodeJS.Timeout>()
  const [keyword, setKeyword] = useState<string>('')
  const [loading, setLoading] = useState<boolean>(true)
  const [volumes, setVolumes] = useState<Volume[]>([])
  const [selectedRows, setSelectedRows] = useState<Volume[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [deleteVisible, setDeleteVisible] = useState<boolean>(false)
  const [extendVisible, setExtendVisible] = useState<boolean>(false)
  const [snapshotVisible, setSnapshotVisible] = useState<boolean>(false)
  const [detailVisible, setDetailVisible] = useState<Volume>()

  useEffect(() => {
    fetchVolumes()
    return () => {
      if (timer.current) {
        clearTimeout(timer.current)
      }
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    const exist = volumes.findIndex((volume: Volume) => {
      const { length } = volume.status
      return volume.status.substring(length - 3, length) === 'ing'
    })
    if (exist !== -1)
      timer.current = setTimeout(() => {
        fetchVolumes()
      }, 3000)
  }, [volumes])

  useEffect(() => {
    const volume = volumes.find((vol) => vol.id === id)
    if (volume && _canRedirect.current) {
      setDetailVisible(volume)
      _canRedirect.current = false
    }
  }, [id, volumes])

  const fetchVolumes = async () => {
    setLoading(true)
    const vs = (await listVolume({})) as Volume[]
    if (_isMounted.current) {
      setVolumes(vs)
      setLoading(false)
    }
  }

  const columns: ColumnType<Volume>[] = [
    {
      title: <FormattedMessage id="name" />,
      dataIndex: 'name',
      key: 'name',
      width: 300,
      ellipsis: true,
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (value: string, volume: Volume) => {
        const tmpVal = isEmptyString(value) ? value : volume.id
        return (
          <Button
            type="link"
            onClick={() => {
              setDetailVisible(volume)
            }}
          >
            <Ligther keywords={[keyword]} source={tmpVal} />
          </Button>
        )
      },
    },
    {
      title: <FormattedMessage id="description" />,
      dataIndex: 'description',
      key: 'description',
      width: 400,
      ellipsis: true,
      sorter: (a, b) => a.description.localeCompare(b.description),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: <FormattedMessage id="size" />,
      dataIndex: 'size',
      key: 'size',
      width: 100,
      ellipsis: true,
      sorter: (a, b) => a.size - b.size,
      render: (value: number) => {
        const tmpVal = `${value} GB`
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
    {
      title: <FormattedMessage id="status" />,
      dataIndex: 'status',
      key: 'status',
      width: 100,
      ellipsis: true,
      sorter: (a, b) => a.status.localeCompare(b.status),
      render: (value: string) => (
        <Status status={value}>
          <Ligther keywords={[keyword]} source={value} />
        </Status>
      ),
    },
    {
      title: <FormattedMessage id="volume.bootable" />,
      dataIndex: 'bootable',
      key: 'bootable',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.status.localeCompare(b.status),
      render: (value: string) => <BooleanStatus status={isEmptyString(value) ? value === 'true' : false} />,
    },
    {
      title: <FormattedMessage id="volume.encrypted" />,
      dataIndex: 'encrypted',
      key: 'encrypted',
      width: 100,
      render: (value: boolean) => <BooleanStatus status={value} />,
    },
  ]

  const actions: MenuAction[] = [
    {
      name: <FormattedMessage id="snapshot" />,
      icon: <BlockOutlined />,
      disabled: selectedRows.length !== 1,
      action: () => {
        setSnapshotVisible(true)
      },
    },
    {
      name: <FormattedMessage id="volume.extend" />,
      icon: <ArrowsAltOutlined />,
      disabled: selectedRows.length !== 1,
      action: () => {
        setExtendVisible(true)
      },
    },
    {
      name: <FormattedMessage id="delete" />,
      disabled: selectedRows.length === 0,
      icon: <DeleteOutlined />,
      action: () => {
        setDeleteVisible(true)
      },
    },
  ]

  return (
    <PageLayout
      loading={loading}
      title={<FormattedMessage id="volumes" />}
      fetchAction={fetchVolumes}
      createAction={() => {
        setCreateVisible(true)
      }}
      actions={actions}
    >
      <>
        {createVisible && (
          <CreateModal
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchVolumes()
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {deleteVisible && selectedRows.length > 0 && (
          <DeleteModal
            volumes={selectedRows}
            visible={deleteVisible}
            onOk={() => {
              setDeleteVisible(false)
              fetchVolumes()
            }}
            onCancel={() => {
              setDeleteVisible(false)
            }}
          />
        )}
        {selectedRows.length === 1 && extendVisible && (
          <ExtendModal
            visible={extendVisible}
            volume={selectedRows[0]}
            onOk={() => {
              setExtendVisible(false)
              fetchVolumes()
            }}
            onCancel={() => {
              setExtendVisible(false)
            }}
          />
        )}
        {selectedRows.length === 1 && snapshotVisible && (
          <SnapshotModal
            visible={snapshotVisible}
            volume={selectedRows[0]}
            onOk={() => {
              setSnapshotVisible(false)
              fetchVolumes()
            }}
            onCancel={() => {
              setSnapshotVisible(false)
            }}
          />
        )}
        {!!detailVisible && (
          <DetailModal
            volume={detailVisible}
            visible={!!detailVisible}
            onOk={() => {
              setDetailVisible(undefined)
            }}
          />
        )}
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          onRowSelected={setSelectedRows}
          dataCount={volumes.length}
          dataSource={
            volumes
              ? volumes.filter(
                  (volume) =>
                    findInString(isEmptyString(volume.name) ? volume.name : volume.id, keyword) ||
                    findInString(volume.description, keyword) ||
                    findInString(`${volume.size} GB`, keyword) ||
                    findInString(volume.status, keyword)
                )
              : volumes
          }
          onSearch={(value) => {
            setKeyword(value)
          }}
        />
      </>
    </PageLayout>
  )
}

export default VolumeList
