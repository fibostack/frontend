import { Tag } from 'antd'
import { AvailableStatus, DownStatus, InUseStatus } from 'configs'
import React from 'react'

const Status: React.FC<{ status: string }> = ({ status, children }) => {
  let color = DownStatus
  switch (status) {
    case 'available':
      color = AvailableStatus
      break
    case 'in-use':
      color = InUseStatus
      break
    case 'SHUTOFF':
      color = DownStatus
      break
    default:
      break
  }
  return (
    <Tag color={color} className="capitalize">
      {children || status}
    </Tag>
  )
}

export default Status
