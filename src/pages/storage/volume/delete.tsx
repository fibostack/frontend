import { notification } from 'antd'
import { deleteVolume } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { Volume } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { isEmptyString } from 'utils'

interface VolumeDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  volumes: Volume[]
}

const VolumeDelete: React.FC<VolumeDeleteProps> = ({ onOk, visible, onCancel, volumes }) => {
  const intl = useIntl()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const deleteIDs = volumes.reduce<string[]>((acc: string[], volume: Volume) => {
    return [...acc, volume.id]
  }, [])

  let deleteNames = volumes.reduce<string>((acc: string, volume: Volume) => {
    return `${acc}${isEmptyString(volume.name) ? volume.name : volume.id}, `
  }, '')
  deleteNames = deleteNames.substring(0, deleteNames.length - 2)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteVolume({
      data: { vol_ids: deleteIDs },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${deleteNames} volume is deleted.`,
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="volume.delete_title" />}
      selectedNames={deleteNames}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default VolumeDelete
