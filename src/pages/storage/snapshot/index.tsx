import { DeleteOutlined } from '@ant-design/icons'
import { Tag } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listSnapshot } from 'api'
import { PageLayout, Table, Ligther } from 'components'
import { AvailableStatus, DownStatus, InUseStatus } from 'configs'
import { MenuAction, Snapshot } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router-dom'
import { goDateFormat, isEmptyString, findInString } from 'utils'
import DeleteModal from './delete'

const Status: React.FC<{ status: string }> = ({ status, children }) => {
  let color = DownStatus
  switch (status) {
    case 'available':
      color = AvailableStatus
      break
    case 'in-use':
      color = InUseStatus
      break
    case 'SHUTOFF':
      color = DownStatus
      break
    default:
      break
  }
  return (
    <Tag color={color} className="capitalize">
      {children || status}
    </Tag>
  )
}

interface SnapshotProps {}

const SnapshotList: React.FC<SnapshotProps> = () => {
  const _isMounted = useRef(true)
  const timer = useRef<NodeJS.Timeout>()
  const [loading, setLoading] = useState(true)
  const [keyword, setKeyword] = useState<string>('')
  const [snapshots, setSnapshots] = useState<Snapshot[]>([])
  const [selectedRows, setSelectedRows] = useState<Snapshot[]>([])
  const [deleteVisible, setDeleteVisible] = useState<boolean>(false)

  useEffect(() => {
    fetchSnapshots()
    return () => {
      if (timer.current) {
        clearTimeout(timer.current)
      }
      _isMounted.current = false
    }
  }, [])

  useEffect(() => {
    const exist = snapshots.findIndex((ss: Snapshot) => {
      const { length } = ss.status
      return ss.status.substring(length - 3, length) === 'ing'
    })
    if (exist !== -1)
      timer.current = setTimeout(() => {
        fetchSnapshots()
      }, 3000)
  }, [snapshots])

  const fetchSnapshots = async () => {
    setLoading(true)
    const sss = (await listSnapshot({})) as Snapshot[]
    if (_isMounted.current) {
      setSnapshots(sss)
      setLoading(false)
    }
  }

  const columns: ColumnType<Snapshot>[] = [
    {
      title: <FormattedMessage id="name" />,
      dataIndex: 'name',
      key: 'name',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (value: string, ss: Snapshot) => {
        const tmpVal = isEmptyString(value) ? value : ss.id
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
    {
      title: <FormattedMessage id="description" />,
      dataIndex: 'description',
      key: 'description',
      width: 400,
      ellipsis: true,
      sorter: (a, b) => a.description.localeCompare(b.description),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: <FormattedMessage id="size" />,
      dataIndex: 'size',
      key: 'size',
      width: 100,
      ellipsis: true,
      sorter: (a, b) => a.size - b.size,
      render: (value: number) => {
        const tmpVal = `${value} GB`
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
    {
      title: <FormattedMessage id="status" />,
      dataIndex: 'status',
      key: 'status',
      width: 100,
      ellipsis: true,
      sorter: (a, b) => a.status.localeCompare(b.status),
      render: (value: string) => (
        <Status status={value}>
          <Ligther keywords={[keyword]} source={value} />
        </Status>
      ),
    },
    {
      title: <FormattedMessage id="snapshot.volume_id" />,
      dataIndex: 'volume_id',
      key: 'volume_id',
      width: 300,
      ellipsis: true,
      sorter: (a, b) => a.volume_id.localeCompare(b.volume_id),
      render: (value: string) => (
        <Link to={`/storage/volumes?id=${value}`}>
          <Ligther keywords={[keyword]} source={value} />
        </Link>
      ),
    },
    {
      title: <FormattedMessage id="create_at" />,
      dataIndex: 'created_date',
      key: 'created_date',
      width: 150,
      ellipsis: true,
      sorter: (a, b) => a.created_date.localeCompare(b.created_date),
      render: (value: string) => {
        const tmpVal = goDateFormat(value)
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
  ]

  const actions: MenuAction[] = [
    {
      name: <FormattedMessage id="delete" />,
      disabled: selectedRows.length === 0,
      icon: <DeleteOutlined />,
      action: () => {
        setDeleteVisible(true)
      },
    },
  ]

  return (
    <PageLayout
      loading={loading}
      title={<FormattedMessage id="snapshots" />}
      fetchAction={fetchSnapshots}
      actions={actions}
    >
      <>
        {deleteVisible && selectedRows.length > 0 && (
          <DeleteModal
            snapshots={selectedRows}
            visible={deleteVisible}
            onOk={() => {
              setDeleteVisible(false)
              fetchSnapshots()
            }}
            onCancel={() => {
              setDeleteVisible(false)
            }}
          />
        )}
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          onRowSelected={setSelectedRows}
          dataCount={snapshots.length}
          dataSource={
            snapshots
              ? snapshots.filter(
                  (snapshot) =>
                    findInString(isEmptyString(snapshot.name) ? snapshot.name : snapshot.id, keyword) ||
                    findInString(snapshot.description, keyword) ||
                    findInString(`${snapshot.size} GB`, keyword) ||
                    findInString(snapshot.status, keyword) ||
                    findInString(snapshot.volume_id, keyword) ||
                    findInString(goDateFormat(snapshot.created_date), keyword)
                )
              : snapshots
          }
          onSearch={(value) => {
            setKeyword(value)
          }}
        />
      </>
    </PageLayout>
  )
}

export default SnapshotList
