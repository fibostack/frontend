import { notification } from 'antd'
import { deleteSnapshot } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { Snapshot } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { isEmptyString } from 'utils'

interface SnapshotDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  snapshots: Snapshot[]
}

const SnapshotDelete: React.FC<SnapshotDeleteProps> = ({ onOk, visible, onCancel, snapshots }) => {
  const intl = useIntl()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const deleteIDs = snapshots.reduce<string[]>((acc: string[], snapshot: Snapshot) => {
    return [...acc, snapshot.id]
  }, [])

  let deleteNames = snapshots.reduce<string>((acc: string, snapshot: Snapshot) => {
    return `${acc}${isEmptyString(snapshot.name) ? snapshot.name : snapshot.id}, `
  }, '')
  deleteNames = deleteNames.substring(0, deleteNames.length - 2)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteSnapshot({
      data: { snapshotID: deleteIDs },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${deleteNames} snapshots is deleted.`,
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="snapshot.delete_title" />}
      okText={<FormattedMessage id="delete" />}
      cancelText={<FormattedMessage id="cancel" />}
      selectedNames={deleteNames}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default SnapshotDelete
