import { ColumnType } from 'antd/es/table'
import { listBlockStorageService } from 'api'
import { Ligther, PageLayout, Table } from 'components'
import { BlockStorageService } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import { DateNowDiffMinute, dateSort, findInString } from 'utils'
import State from './state'
import Status from './status'

interface BlockStorageServiceListProps {}

const BlockStorageServiceList: React.FC<BlockStorageServiceListProps> = () => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [keyword, setKeyword] = useState<string>('')
  const [blockStorageServices, setBlockStorageServices] = useState<BlockStorageService[]>([])

  useEffect(() => {
    fetchBlockStorageServices()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchBlockStorageServices = async () => {
    setLoading(true)
    const end = (await listBlockStorageService({})) as BlockStorageService[]
    if (_isMounted.current) {
      setBlockStorageServices(end)
      setLoading(false)
    }
  }

  const columns: ColumnType<BlockStorageService>[] = [
    {
      key: 'binary',
      width: 300,
      ellipsis: true,
      dataIndex: 'binary',
      title: intl.formatMessage({ id: 'name' }),
      sorter: (a, b) => a.binary.localeCompare(b.binary),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      key: 'host',
      width: 200,
      dataIndex: 'host',
      title: intl.formatMessage({ id: 'host' }),
      ellipsis: true,
      sorter: (a, b) => a.host.localeCompare(b.host),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      key: 'zone',
      width: 200,
      dataIndex: 'zone',
      title: intl.formatMessage({ id: 'zone' }),
      ellipsis: true,
      sorter: (a, b) => a.zone.localeCompare(b.zone),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      key: 'status',
      width: 200,
      ellipsis: true,
      dataIndex: 'status',
      title: intl.formatMessage({ id: 'status' }),
      sorter: (a, b) => a.status.localeCompare(b.status),
      render: (value: string) => (
        <Status status={value}>
          <Ligther keywords={[keyword]} source={value} />
        </Status>
      ),
    },
    {
      key: 'state',
      width: 200,
      dataIndex: 'state',
      ellipsis: true,
      title: intl.formatMessage({ id: 'state' }),
      sorter: (a, b) => a.status.localeCompare(b.status),
      render: (value: string) => (
        <State state={value}>
          <Ligther keywords={[keyword]} source={value} />
        </State>
      ),
    },
    {
      key: 'updated_at',
      width: 200,
      ellipsis: true,
      dataIndex: 'updated_at',
      title: intl.formatMessage({ id: 'updated_at' }),
      sorter: (a, b) => dateSort(a.updated_at, b.updated_at),
      render: (value: Date) => {
        const tmpVal = DateNowDiffMinute(value)
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
  ]

  return (
    <PageLayout
      noCard
      loading={loading}
      title={intl.formatMessage({ id: 'system.block_storage_services' })}
      fetchAction={fetchBlockStorageServices}
    >
      <Table
        rowKey="id"
        loading={loading}
        columns={columns}
        dataCount={blockStorageServices.length}
        dataSource={
          keyword
            ? blockStorageServices.filter(
                (service) =>
                  findInString(service.binary, keyword) ||
                  findInString(service.host, keyword) ||
                  findInString(service.zone, keyword) ||
                  findInString(service.state, keyword) ||
                  findInString(service.status, keyword) ||
                  findInString(service.state, keyword) ||
                  findInString(DateNowDiffMinute(service.updated_at), keyword)
              )
            : blockStorageServices
        }
        onSearch={(value) => {
          setKeyword(value)
        }}
      />
    </PageLayout>
  )
}

export default BlockStorageServiceList
