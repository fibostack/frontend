import { InUseStatus, AvailableStatus, DownStatus } from 'configs'
import { Tag } from 'antd'
import React from 'react'

const State: React.FC<{ state: string }> = ({ state, children }) => {
  let color = InUseStatus
  switch (state) {
    case 'up':
      color = AvailableStatus
      break
    case 'down':
      color = DownStatus
      break
    default:
      break
  }
  return (
    <Tag color={color} className="capitalize">
      {children || state}
    </Tag>
  )
}

export default State
