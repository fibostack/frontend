import { ColumnType } from 'antd/es/table'
import { listService } from 'api'
import { PageLayout, Table, Ligther } from 'components'
import { Service, Endpoint } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import Highlighter from 'react-highlight-words'
import { findInString } from 'utils'

interface ServiceListProps {}

const ServiceList: React.FC<ServiceListProps> = () => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [keyword, setKeyword] = useState<string>('')
  const [services, setServices] = useState<Service[]>([])

  useEffect(() => {
    fetchEndpoints()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchEndpoints = async () => {
    setLoading(true)
    const ss = (await listService({})) as Service[]
    if (_isMounted.current) {
      setServices(ss)
      setLoading(false)
    }
  }

  const columns: ColumnType<Service>[] = [
    {
      key: 'name',
      width: 300,
      ellipsis: true,
      dataIndex: 'name',
      title: intl.formatMessage({ id: 'name' }),
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      key: 'type',
      width: 200,
      dataIndex: 'type',
      ellipsis: true,
      title: intl.formatMessage({ id: 'service' }),
      sorter: (a, b) => a.type.localeCompare(b.type),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      key: 'region',
      width: 200,
      dataIndex: 'region',
      ellipsis: true,
      title: intl.formatMessage({ id: 'region' }),
      sorter: (a, b) => a.region.localeCompare(b.region),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      width: 600,
      key: 'endpoints',
      dataIndex: 'endpoints',
      ellipsis: true,
      title: intl.formatMessage({ id: 'system.endpoints' }),
      render: (value?: Endpoint[]) => {
        if (value) value.sort((a, b) => a.interface.localeCompare(b.interface))
        return (
          <ul className="nostyle">
            {value &&
              value.map((item) => (
                <li key={item.url + item.interface}>
                  <strong className="capitalize">{item.interface}: </strong>
                  {keyword ? <Ligther keywords={[keyword]} source={item.url} /> : item.url}
                </li>
              ))}
          </ul>
        )
      },
    },
  ]

  return (
    <PageLayout noCard loading={loading} title={intl.formatMessage({ id: 'services' })} fetchAction={fetchEndpoints}>
      <Table
        rowKey="id"
        loading={loading}
        columns={columns}
        dataCount={services.length}
        dataSource={
          keyword
            ? services.filter(
                (service) =>
                  findInString(service.name, keyword) ||
                  findInString(service.type, keyword) ||
                  findInString(service.region, keyword) ||
                  service.endpoints.findIndex((end) => findInString(end.url, keyword)) >= 0
              )
            : services
        }
        onSearch={(value) => {
          setKeyword(value)
        }}
      />
    </PageLayout>
  )
}

export default ServiceList
