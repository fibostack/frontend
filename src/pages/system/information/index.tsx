import { AimOutlined, ApartmentOutlined, DatabaseOutlined, LaptopOutlined } from '@ant-design/icons'
import { Tabs } from 'antd'
import { PageLayout } from 'components'
import React from 'react'
import { FormattedMessage } from 'react-intl'
import BlockStorageService from './block_storage_service'
import ComputeService from './compute_service'
import Services from './services'
import NetworkAgents from './network_agents'

interface SystemInformationProps {}

const SystemInformationList: React.FC<SystemInformationProps> = () => {
  return (
    <PageLayout title={<FormattedMessage id="menu.system_information" />}>
      <Tabs defaultActiveKey="endpoints">
        <Tabs.TabPane
          tab={
            <span>
              <AimOutlined />
              <FormattedMessage id="services" />
            </span>
          }
          key="services"
        >
          <Services />
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <span>
              <LaptopOutlined />
              <FormattedMessage id="system.compute_services" />
            </span>
          }
          key="compute_services"
        >
          <ComputeService />
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <span>
              <DatabaseOutlined />
              <FormattedMessage id="system.block_storage_services" />
            </span>
          }
          key="block_storage_services"
        >
          <BlockStorageService />
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <span>
              <ApartmentOutlined />
              <FormattedMessage id="system.network_agents" />
            </span>
          }
          key="network_agents"
        >
          <NetworkAgents />
        </Tabs.TabPane>
      </Tabs>
    </PageLayout>
  )
}

export default SystemInformationList
