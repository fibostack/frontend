import { ColumnType } from 'antd/es/table'
import { listComputeService } from 'api'
import { Ligther, PageLayout, Table } from 'components'
import { ComputeService } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import { DateNowDiffMinute, findInString, dateSort } from 'utils'
import State from './state'
import Status from './status'

interface ComputeServiceListProps {}

const ComputeServiceList: React.FC<ComputeServiceListProps> = () => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [keyword, setKeyword] = useState<string>('')
  const [computeServices, setComputeServices] = useState<ComputeService[]>([])

  useEffect(() => {
    fetchComputeServices()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchComputeServices = async () => {
    setLoading(true)
    const end = (await listComputeService({})) as ComputeService[]
    if (_isMounted.current) {
      setComputeServices(end)
      setLoading(false)
    }
  }

  const columns: ColumnType<ComputeService>[] = [
    {
      key: 'binary',
      width: 300,
      ellipsis: true,
      dataIndex: 'binary',
      title: intl.formatMessage({ id: 'name' }),
      sorter: (a, b) => a.binary.localeCompare(b.binary),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      key: 'host',
      width: 200,
      dataIndex: 'host',
      title: intl.formatMessage({ id: 'host' }),
      ellipsis: true,
      sorter: (a, b) => a.host.localeCompare(b.host),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      key: 'zone',
      width: 200,
      dataIndex: 'zone',
      title: intl.formatMessage({ id: 'zone' }),
      ellipsis: true,
      sorter: (a, b) => a.zone.localeCompare(b.zone),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      key: 'status',
      width: 200,
      ellipsis: true,
      dataIndex: 'status',
      title: intl.formatMessage({ id: 'status' }),
      sorter: (a, b) => a.status.localeCompare(b.status),
      render: (value: string) => (
        <Status status={value}>
          <Ligther keywords={[keyword]} source={value} />
        </Status>
      ),
    },
    {
      key: 'state',
      width: 200,
      dataIndex: 'state',
      ellipsis: true,
      title: intl.formatMessage({ id: 'state' }),
      sorter: (a, b) => a.status.localeCompare(b.status),
      render: (value: string) => (
        <State state={value}>
          <Ligther keywords={[keyword]} source={value} />
        </State>
      ),
    },
    {
      key: 'updated_at',
      width: 200,
      dataIndex: 'updated_at',
      ellipsis: true,
      title: intl.formatMessage({ id: 'updated_at' }),
      sorter: (a, b) => dateSort(a.updated_at, b.updated_at),
      render: (value: Date) => {
        const tmpVal = DateNowDiffMinute(value)
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
  ]

  return (
    <PageLayout
      noCard
      loading={loading}
      title={intl.formatMessage({ id: 'system.compute_services' })}
      fetchAction={fetchComputeServices}
    >
      <Table
        rowKey="id"
        loading={loading}
        columns={columns}
        dataCount={computeServices.length}
        dataSource={
          keyword
            ? computeServices.filter(
                (service) =>
                  findInString(service.binary, keyword) ||
                  findInString(service.host, keyword) ||
                  findInString(service.zone, keyword) ||
                  findInString(service.state, keyword) ||
                  findInString(service.status, keyword) ||
                  findInString(service.state, keyword) ||
                  findInString(DateNowDiffMinute(service.updated_at), keyword)
              )
            : computeServices
        }
        onSearch={(value) => {
          setKeyword(value)
        }}
      />
    </PageLayout>
  )
}

export default ComputeServiceList
