import { ColumnType } from 'antd/es/table'
import { listNetworkAgent } from 'api'
import { BooleanStatus, PageLayout, Table, Ligther } from 'components'
import { NetworkAgent } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import { DateNowDiffMinute, findInString, booleanSort, dateSort } from 'utils'

interface NetworkAgentListProps {}

const NetworkAgentList: React.FC<NetworkAgentListProps> = () => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [keyword, setKeyword] = useState<string>('')
  const [networkAgents, setNetworkAgents] = useState<NetworkAgent[]>([])

  useEffect(() => {
    fetchNetworkAgents()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchNetworkAgents = async () => {
    setLoading(true)
    const end = (await listNetworkAgent({})) as NetworkAgent[]
    if (_isMounted.current) {
      setNetworkAgents(end)
      setLoading(false)
    }
  }

  const columns: ColumnType<NetworkAgent>[] = [
    {
      key: 'tagent_typeype',
      width: 200,
      dataIndex: 'agent_type',
      title: intl.formatMessage({ id: 'type' }),
      ellipsis: true,
      sorter: (a, b) => a.agent_type.localeCompare(b.agent_type),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      key: 'binary',
      width: 300,
      dataIndex: 'binary',
      title: intl.formatMessage({ id: 'name' }),
      ellipsis: true,
      sorter: (a, b) => a.binary.localeCompare(b.binary),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      key: 'host',
      width: 200,
      dataIndex: 'host',
      title: intl.formatMessage({ id: 'host' }),
      ellipsis: true,
      sorter: (a, b) => a.host.localeCompare(b.host),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      key: 'availability_zone',
      width: 200,
      dataIndex: 'availability_zone',
      title: intl.formatMessage({ id: 'zone' }),
      ellipsis: true,
      sorter: (a, b) => a.availability_zone.localeCompare(b.availability_zone),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      key: 'alive',
      width: 200,
      ellipsis: true,
      dataIndex: 'alive',
      title: intl.formatMessage({ id: 'status' }),
      sorter: (a, b) => booleanSort(a.alive, b.alive),
      render: (value: boolean) => <BooleanStatus status={value} />,
    },
    {
      key: 'admin_state_up',
      width: 200,
      ellipsis: true,
      dataIndex: 'admin_state_up',
      title: intl.formatMessage({ id: 'state' }),
      sorter: (a, b) => booleanSort(a.admin_state_up, b.admin_state_up),
      render: (value: boolean) => <BooleanStatus status={value} />,
    },
    {
      key: 'heartbeat_timestamp',
      width: 200,
      ellipsis: true,
      dataIndex: 'heartbeat_timestamp',
      title: intl.formatMessage({ id: 'updated_at' }),
      sorter: (a, b) => dateSort(a.heartbeat_timestamp, b.heartbeat_timestamp),
      render: (value: Date) => {
        const tmpVal = DateNowDiffMinute(value)
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
  ]

  return (
    <PageLayout
      noCard
      loading={loading}
      title={intl.formatMessage({ id: 'system.network_agents' })}
      fetchAction={fetchNetworkAgents}
    >
      <Table
        rowKey="id"
        loading={loading}
        columns={columns}
        dataCount={networkAgents.length}
        dataSource={
          keyword
            ? networkAgents.filter(
                (agent) =>
                  findInString(agent.agent_type, keyword) ||
                  findInString(agent.binary, keyword) ||
                  findInString(agent.host, keyword) ||
                  findInString(agent.availability_zone, keyword) ||
                  findInString(DateNowDiffMinute(agent.heartbeat_timestamp), keyword)
              )
            : networkAgents
        }
        onSearch={(value) => {
          setKeyword(value)
        }}
      />
    </PageLayout>
  )
}

export default NetworkAgentList
