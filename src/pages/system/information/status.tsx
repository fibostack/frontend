import { InUseStatus, AvailableStatus, DownStatus } from 'configs'
import { Tag } from 'antd'
import React from 'react'

const Status: React.FC<{ status: string }> = ({ status, children }) => {
  let color = InUseStatus
  switch (status) {
    case 'enabled':
      color = AvailableStatus
      break
    case 'disabled':
      color = DownStatus
      break
    default:
      break
  }
  return (
    <Tag color={color} className="capitalize">
      {children || status}
    </Tag>
  )
}

export default Status
