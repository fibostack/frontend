import { Col, Row } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listHypervisor, statisticsHypervisor } from 'api'
import { CircleProgress, Ligther, Loader, PageLayout, Table } from 'components'
import { Hypervisor, HypervisorTotal } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import { findInString, formatGB, formatMB } from 'utils'

interface HypervisorProps {}

const HypervisorList: React.FC<HypervisorProps> = () => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [hypervisors, setHypervisors] = useState<Hypervisor[]>([])
  const [totals, setTotals] = useState<HypervisorTotal>()
  const [keyword, setKeyword] = useState<string>('')

  useEffect(() => {
    fetchHypervisors()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchHypervisors = async () => {
    setLoading(true)
    const [totalRes, listRes] = await Promise.all([statisticsHypervisor({}), listHypervisor({})])
    const us = listRes as Hypervisor[]
    const ts = totalRes as HypervisorTotal
    if (_isMounted.current) {
      setTotals(ts)
      setHypervisors(us)
      setLoading(false)
    }
  }

  const columns: ColumnType<Hypervisor>[] = [
    {
      title: intl.formatMessage({ id: 'name' }),
      key: 'hypervisor_hostname',
      dataIndex: 'hypervisor_hostname',
      width: 300,
      ellipsis: true,
      sorter: (a, b) => a.hypervisor_hostname.localeCompare(b.hypervisor_hostname),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: intl.formatMessage({ id: 'type' }),
      dataIndex: 'hypervisor_type',
      key: 'hypervisor_type',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.hypervisor_type.localeCompare(b.hypervisor_type),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: intl.formatMessage({ id: 'vcpu' }),
      key: 'vcpu',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.vcpus_used - b.vcpus_used,
      render: (record: Hypervisor) => {
        const tmpVal = `${record.vcpus_used}/${record.vcpus}`
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
    {
      title: intl.formatMessage({ id: 'ram' }),
      key: 'ram',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.memory_mb_used - b.memory_mb_used,
      render: (record: Hypervisor) => {
        const tmpVal = `${formatMB(record.memory_mb_used)}/${formatMB(record.memory_mb)}`
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
    {
      title: intl.formatMessage({ id: 'storage' }),
      key: 'storage',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.local_gb_used - b.local_gb_used,
      render: (record: Hypervisor) => {
        const tmpVal = `${formatGB(record.local_gb_used)}/${formatGB(record.local_gb)}`
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
    {
      title: intl.formatMessage({ id: 'instance' }),
      key: 'instance',
      dataIndex: 'running_vms',
      width: 300,
      ellipsis: true,
      sorter: (a, b) => a.running_vms - b.running_vms,
      render: (value: number) => {
        const tmpVal = value.toString()
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
  ]

  return (
    <PageLayout
      loading={loading}
      title={intl.formatMessage({ id: 'menu.hypervisor' })}
      fetchAction={() => {
        fetchHypervisors()
      }}
    >
      <>
        <Loader loading={loading}>
          {totals && (
            <Row justify="center" gutter={48}>
              <Col>
                <CircleProgress used={totals.count} all={totals.count} title="menu.hypervisor" />
              </Col>
              <Col>
                <CircleProgress used={totals.vcpus_used} all={totals.vcpus} title="total_vcpu" />
              </Col>
              <Col>
                <CircleProgress
                  used={totals.memory_mb_used}
                  all={totals.memory_mb}
                  title="total_ram"
                  format={formatMB}
                />
              </Col>
              {/* <Col>
                <CircleProgress
                  used={totals.local_gb_used}
                  all={totals.local_gb}
                  title="total_storage"
                  format={formatGB}
                />
              </Col> */}
              <Col>
                <CircleProgress used={totals.running_vms} all={totals.running_vms} title="instance" />
              </Col>
            </Row>
          )}
        </Loader>
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          dataCount={hypervisors.length}
          dataSource={
            keyword
              ? hypervisors.filter(
                  (hypervisor) =>
                    findInString(hypervisor.hypervisor_hostname, keyword) ||
                    findInString(hypervisor.hypervisor_type, keyword) ||
                    findInString(`${hypervisor.vcpus_used}/${hypervisor.vcpus}`, keyword) ||
                    findInString(`${formatMB(hypervisor.memory_mb_used)}/${formatMB(hypervisor.memory_mb)}`, keyword) ||
                    findInString(`${formatGB(hypervisor.local_gb_used)}/${formatGB(hypervisor.local_gb)}`, keyword) ||
                    findInString(hypervisor.running_vms.toString(), keyword)
                )
              : hypervisors
          }
          onSearch={(value) => {
            setKeyword(value)
          }}
        />
      </>
    </PageLayout>
  )
}

export default HypervisorList
