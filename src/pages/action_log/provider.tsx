import Icon, { ExclamationCircleOutlined } from '@ant-design/icons'
import { Tooltip } from 'antd'
import { ReactComponent as AwsIcon } from 'assets/icons/amazon_web_service.svg'
import { ReactComponent as Openstack } from 'assets/icons/openstack.svg'
import { CloudProvider } from 'models'
import React from 'react'

interface Props {
  value: CloudProvider
}

const Provider: React.FC<Props> = ({ value, children }) => {
  const renderIcon = () => {
    switch (value) {
      case 'AWS':
        return <Icon component={() => <AwsIcon width={84} />} />
      case 'OPENSTACK':
        return <Icon component={() => <Openstack width={96} />} />
      default:
        return <ExclamationCircleOutlined />
    }
  }

  return (
    <Tooltip placement="top" title={children}>
      {renderIcon()}
    </Tooltip>
  )
}

export default Provider
