import {
  KeyOutlined,
  UserOutlined,
  FireOutlined,
  GoldOutlined,
  AuditOutlined,
  UnlockOutlined,
  BorderOutlined,
  CoffeeOutlined,
  ProjectOutlined,
  SettingOutlined,
  CompassOutlined,
  DatabaseOutlined,
  SwitcherOutlined,
  FileImageOutlined,
  MacCommandOutlined,
  EnvironmentOutlined,
  PropertySafetyOutlined,
} from '@ant-design/icons'
import { Tooltip } from 'antd'
import { LogService } from 'models'
import React from 'react'

interface Props {
  value: LogService
}

const Service: React.FC<Props> = ({ value, children }) => {
  const renderIcon = () => {
    switch (value) {
      case 'User':
        return <UserOutlined />
      case 'Network':
        return <GoldOutlined />
      case 'KeyPair':
        return <KeyOutlined />
      case 'Router':
        return <CompassOutlined />
      case 'Instance':
        return <FireOutlined />
      case 'Image':
        return <FileImageOutlined />
      case 'Volume':
        return <DatabaseOutlined />
      case 'Snapshot':
        return <SwitcherOutlined />
      case 'Security Group':
        return <PropertySafetyOutlined />
      case 'Floating IP':
        return <EnvironmentOutlined />
      case 'System':
        return <SettingOutlined />
      case 'Role':
        return <UnlockOutlined />
      case 'Credentials':
        return <AuditOutlined />
      case 'EC2':
        return <MacCommandOutlined />
      case 'Project':
        return <ProjectOutlined />
      case 'Permission':
        return <CoffeeOutlined />
      default:
        return <BorderOutlined />
    }
  }

  return (
    <Tooltip placement="top" title={value}>
      <span>
        {renderIcon()} {children || value}
      </span>
    </Tooltip>
  )
}

export default Service
