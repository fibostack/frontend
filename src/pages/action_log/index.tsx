/* eslint-disable camelcase */
import { Input, Select } from 'antd'
import { TablePaginationConfig } from 'antd/es/table'
import { ColumnType } from 'antd/lib/table'
import { listActionLog } from 'api'
import { Ligther, PageLayout, Table } from 'components'
import { ActionLog, CloudProvider, LogService } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import ReactJson from 'react-json-view'
import Provider from './provider'
import Service from './service'

const JsonViewer: React.FC<{ value: string; name: string }> = ({ value, name }) => {
  return (
    <ReactJson
      name={name}
      style={{ padding: 12 }}
      src={JSON.parse(value)}
      theme="ocean"
      displayDataTypes={false}
      iconStyle="square"
      displayObjectSize
      indentWidth={4}
      collapsed={1}
      collapseStringsAfterLength={150}
    />
  )
}

interface Props {}

export interface ActionLogResponse {
  logs: ActionLog[]
  total_logs: number
}

const ActionLogScreen: React.FC<Props> = () => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [fieldName, setFieldName] = useState<string>('event_name')
  const [keyword, setKeyword] = useState<string>('')
  const [sorter, setSorter] = useState<{ sortField: string; sortOrder: 'asc' | 'desc' | '' }>({
    sortField: '',
    sortOrder: '',
  })
  const [logs, setLogs] = useState<ActionLog[]>([])
  const [myPagination, setMyPagination] = useState<TablePaginationConfig>({
    current: 1,
    pageSize: 20,
    showSizeChanger: true,
  })
  const [loading, setLoading] = useState<boolean>(true)

  useEffect(() => {
    fetchActionLog({
      sortField: '',
      sortOrder: '',
      current: 1,
      pageSize: 20,
      fieldName: '',
      keyword: '',
    })
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchActionLog = async (params = {}) => {
    setLoading(true)
    const actionLogs = (await listActionLog({
      data: params,
    })) as ActionLogResponse
    if (_isMounted.current) {
      setLogs(actionLogs.logs || [])
      setMyPagination((old) => {
        return {
          ...old,
          total: actionLogs.total_logs,
        }
      })
      setLoading(false)
    }
  }

  const columns: ColumnType<ActionLog>[] = [
    {
      width: 110,
      title: intl.formatMessage({ id: 'provider' }),
      key: 'cloud_provider',
      dataIndex: 'cloud_provider',
      ellipsis: true,
      render: (value: CloudProvider) => (
        <Provider value={value}>
          <Ligther keywords={[keyword]} source={value} />
        </Provider>
      ),
    },
    {
      width: 150,
      title: intl.formatMessage({ id: 'service' }),
      key: 'service',
      dataIndex: 'service',
      ellipsis: true,
      sorter: (a, b) => a.service.localeCompare(b.service),
      render: (value: LogService) => (
        <Service value={value}>
          <Ligther keywords={[keyword]} source={value} />
        </Service>
      ),
    },
    {
      title: intl.formatMessage({ id: 'project' }),
      key: 'os_tenant_name',
      dataIndex: 'os_tenant_name',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.os_tenant_name.localeCompare(b.os_tenant_name),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: intl.formatMessage({ id: 'resource' }),
      key: 'resource_name',
      dataIndex: 'resource_name',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.resource_name.localeCompare(b.resource_name),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: intl.formatMessage({ id: 'event' }),
      key: 'event_name',
      dataIndex: 'event_name',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.event_name.localeCompare(b.event_name),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: intl.formatMessage({ id: 'email' }),
      key: 'email',
      dataIndex: 'email',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.email.localeCompare(b.email),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: intl.formatMessage({ id: 'error' }),
      key: 'error_msg',
      dataIndex: 'error_msg',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.error_msg.localeCompare(b.error_msg),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: intl.formatMessage({ id: 'ip' }),
      key: 'source_ip',
      dataIndex: 'source_ip',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.source_ip.localeCompare(b.source_ip),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: intl.formatMessage({ id: 'date' }),
      key: 'event_date',
      dataIndex: 'event_date',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.event_date.localeCompare(b.event_date),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
  ]

  return (
    <PageLayout title={intl.formatMessage({ id: 'menu.action_log' })}>
      <Table
        rowKey="id"
        loading={loading}
        columns={columns}
        dataCount={logs.length}
        pagination={myPagination}
        onChange={(pagination, _filters, sort) => {
          const tmpSort = sort as any
          let order: 'asc' | 'desc' | '' = ''
          switch (tmpSort.order) {
            case 'ascend':
              order = 'asc'
              break
            case 'descend':
              order = 'desc'
              break
            default:
              order = ''
              break
          }
          if (order) {
            setSorter({ sortField: tmpSort.field, sortOrder: order })
          } else {
            setSorter({ sortField: '', sortOrder: '' })
          }
          setMyPagination(pagination)
          fetchActionLog({
            fieldName,
            keyword,
            sortField: order ? tmpSort.field : '',
            sortOrder: order || '',
            ...pagination,
          })
        }}
        dataSource={logs}
        onSearch={() => {}}
        searchComponent={() => {
          return (
            <Input.Group compact>
              <Select value={fieldName} onChange={setFieldName} style={{ width: '40%' }}>
                <Select.Option value="cloud_provider">{intl.formatMessage({ id: 'provider' })}</Select.Option>
                <Select.Option value="service">{intl.formatMessage({ id: 'service' })}</Select.Option>
                <Select.Option value="os_tenant_name">{intl.formatMessage({ id: 'project' })}</Select.Option>
                <Select.Option value="resource_name">{intl.formatMessage({ id: 'resource' })}</Select.Option>
                <Select.Option value="event_name">{intl.formatMessage({ id: 'event' })}</Select.Option>
                <Select.Option value="email">{intl.formatMessage({ id: 'email' })}</Select.Option>
                <Select.Option value="error_msg">{intl.formatMessage({ id: 'error' })}</Select.Option>
                <Select.Option value="source_ip">{intl.formatMessage({ id: 'ip' })}</Select.Option>
                <Select.Option value="event_date">{intl.formatMessage({ id: 'date' })}</Select.Option>
              </Select>
              <Input.Search
                enterButton
                style={{ width: '60%' }}
                size="middle"
                placeholder={intl.formatMessage({ id: 'search' })}
                onSearch={(e) => {
                  setKeyword(e)
                  fetchActionLog({
                    fieldName,
                    keyword: e,
                    ...sorter,
                    ...myPagination,
                  })
                }}
              />
            </Input.Group>
          )
        }}
        expandable={{
          expandedRowRender: (record: ActionLog) => (
            <span className="flex col">
              {record.request_header && <JsonViewer value={record.request_header} name="header" />}
              {record.request_body && <JsonViewer value={record.request_body} name="body" />}
            </span>
          ),
        }}
      />
    </PageLayout>
  )
}

export default ActionLogScreen
