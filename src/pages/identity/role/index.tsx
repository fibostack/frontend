import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import { Descriptions, Tooltip, Button, Row, Col } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listRole } from 'api'
import { PageLayout, Table, Ligther } from 'components'
import { MenuAction, UserRole, UserRoleExtra } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import { findInString } from 'utils'
import CreateModal from './create'
import DetailModal from './detail'
import DeleteModal from './delete'
import UpdateModal from './update'

interface RoleProps {}

const RoleList: React.FC<RoleProps> = () => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [keyword, setKeyword] = useState<string>('')
  const [roles, setRoles] = useState<UserRole[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [updateVisible, setUpdateVisible] = useState<UserRole>()
  const [deleteVisible, setDeleteVisible] = useState<UserRole>()
  const [detailVisible, setDetailVisible] = useState<UserRole>()

  useEffect(() => {
    fetchRoles()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchRoles = async () => {
    setLoading(true)
    const us = (await listRole({})) as UserRole[]
    if (_isMounted.current) {
      setRoles(us)
      setLoading(false)
    }
  }

  const columns: ColumnType<UserRole>[] = [
    {
      title: intl.formatMessage({ id: 'name' }),
      key: 'name',
      dataIndex: 'name',
      width: 300,
      ellipsis: true,
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (value: string, role: UserRole) => (
        <Button
          type="link"
          onClick={() => {
            setDetailVisible(role)
          }}
        >
          <Ligther keywords={[keyword]} source={value} />
        </Button>
      ),
    },
    {
      title: intl.formatMessage({ id: 'action' }),
      key: 'action',
      width: 150,
      render: (_value, record) => (
        <Row gutter={8}>
          <Col>
            <Tooltip placement="topRight" title={intl.formatMessage({ id: 'update' })}>
              <Button
                type="primary"
                className="onlyIcon"
                icon={<EditOutlined />}
                onClick={() => {
                  setUpdateVisible(record)
                }}
              />
            </Tooltip>
          </Col>
          <Col>
            <Tooltip placement="topRight" title={intl.formatMessage({ id: 'delete' })}>
              <Button
                danger
                className="onlyIcon"
                icon={<DeleteOutlined />}
                onClick={() => {
                  setDeleteVisible(record)
                }}
              />
            </Tooltip>
          </Col>
        </Row>
      ),
    },
  ]

  return (
    <PageLayout
      loading={loading}
      title={intl.formatMessage({ id: 'roles' })}
      fetchAction={() => {
        fetchRoles()
      }}
      createAction={() => {
        setCreateVisible(true)
      }}
    >
      <>
        {createVisible && (
          <CreateModal
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchRoles()
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {!!updateVisible && (
          <UpdateModal
            role={updateVisible}
            visible={!!updateVisible}
            onOk={() => {
              setUpdateVisible(undefined)
              fetchRoles()
            }}
            onCancel={() => {
              setUpdateVisible(undefined)
            }}
          />
        )}
        {!!deleteVisible && (
          <DeleteModal
            role={deleteVisible}
            visible={!!deleteVisible}
            onOk={() => {
              setDeleteVisible(undefined)
              fetchRoles()
            }}
            onCancel={() => {
              setDeleteVisible(undefined)
            }}
          />
        )}
        {!!detailVisible && (
          <DetailModal
            role={detailVisible}
            visible={!!detailVisible}
            onOk={() => {
              setDetailVisible(undefined)
            }}
          />
        )}
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          dataCount={roles.length}
          dataSource={roles ? roles.filter((role) => findInString(role.name, keyword)) : roles}
          onSearch={(value) => {
            setKeyword(value)
          }}
        />
      </>
    </PageLayout>
  )
}

export default RoleList
