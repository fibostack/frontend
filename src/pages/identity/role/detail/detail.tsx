import React from 'react'
import { KeyValue } from 'components'
import { FormattedMessage } from 'react-intl'
import { UserRole } from 'models'
import { Descriptions } from 'antd'
import { DateFormat } from 'utils'

interface Props {
  role: UserRole
}

const Detail = ({ role }: Props) => {
  return (
    <Descriptions column={{ sm: 2, xs: 1 }}>
      <Descriptions.Item
        label={
          <strong>
            <FormattedMessage id="name" />
          </strong>
        }
      >
        {role.name || '-'}
      </Descriptions.Item>
      <Descriptions.Item
        label={
          <strong>
            <FormattedMessage id="create_at" />
          </strong>
        }
      >
        {DateFormat(role.created_at) || '-'}
      </Descriptions.Item>
    </Descriptions>
  )
}

export default Detail
