import { ColumnType } from 'antd/lib/table'
import { Ligther, PageLayout, Table } from 'components'
import { UserPermission, UserRole } from 'models'
import React, { useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { findInString } from 'utils'
import { Button } from 'antd'
import { EditOutlined } from '@ant-design/icons'
import UpdateModal from './update'

interface Props {
  role: UserRole
  fetchRole: Function
}

const Permission: React.FC<Props> = ({ role, fetchRole }) => {
  const [keyword, setKeyword] = useState<string>('')
  const [updateVisible, setUpdateVisible] = useState<boolean>(false)

  const columns: ColumnType<UserPermission>[] = [
    {
      title: <FormattedMessage id="name" />,
      key: 'name',
      dataIndex: 'entity',
      width: 300,
      ellipsis: true,
      sorter: (a, b) => a.entity.localeCompare(b.entity),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: <FormattedMessage id="api" />,
      key: 'api',
      width: 300,
      dataIndex: 'api',
      ellipsis: true,
      sorter: (a, b) => a.api.localeCompare(b.api),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
  ]

  return (
    <PageLayout
      noCard
      title={<FormattedMessage id="permissions" />}
      extraElement={
        <Button
          type="primary"
          icon={<EditOutlined />}
          onClick={() => {
            setUpdateVisible(true)
          }}
        >
          <FormattedMessage id="update" />
        </Button>
      }
    >
      <>
        {!!updateVisible && (
          <UpdateModal
            role={role}
            visible={updateVisible}
            onOk={() => {
              setUpdateVisible(false)
              fetchRole()
            }}
            onCancel={() => {
              setUpdateVisible(false)
            }}
          />
        )}
        <Table
          rowKey="id"
          columns={columns}
          dataCount={role.Permissions?.length}
          dataSource={
            keyword
              ? role.Permissions?.filter(
                  (permission) => findInString(permission.entity, keyword) || findInString(permission.api, keyword)
                )
              : role.Permissions
          }
          onSearch={(value) => {
            setKeyword(value)
          }}
        />
      </>
    </PageLayout>
  )
}

export default Permission
