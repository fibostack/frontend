import { Form, notification, Transfer, Spin } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { listPermission, updateRolePermission } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { useWindow } from 'hooks'
import { UserPermission, UserRole } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { calcModalWidth } from 'utils'

interface PermissionUpdateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  role: UserRole
}

const PermissionUpdate: React.FC<PermissionUpdateProps> = ({ visible, onOk, onCancel, role }) => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [width] = useWindow()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)
  const [allPermissions, setAllPermissions] = useState<UserPermission[]>([])

  useEffect(() => {
    fetchPermission()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchPermission = async () => {
    setLoading(true)
    const pms = (await listPermission({})) as UserPermission[]
    if (_isMounted.current) {
      setAllPermissions(pms)
      setLoading(false)
    }
  }

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.role_id = role.id
    const success = await updateRolePermission({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'role.permission_updated' }, { name: role.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(role.name)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      width={calcModalWidth(width)}
      formName="role_permission_update_form"
      okText={<FormattedMessage id="update" />}
      title={intl.formatMessage({ id: 'role.permission_update_title' }, { name: role.name })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Spin spinning={loading}>
        <Form
          form={form}
          layout="vertical"
          labelAlign="left"
          onFinish={handleFinish}
          name="role_permission_update_form"
          initialValues={{
            permission_ids: role.Permissions?.reduce<string[]>((acc, item) => [...acc, item.id.toString()], []),
          }}
        >
          <FormInput
            required
            hasLabel
            name="permission_ids"
            label={<FormattedMessage id="permissions" />}
            valuePropName="targetKeys"
          >
            <Transfer
              listStyle={{ width: '100%', height: '70vh' }}
              rowKey={(item) => item.id.toString()}
              dataSource={allPermissions as any}
              showSearch
              filterOption={(inputValue, option) => {
                const tmp = (option as unknown) as UserPermission
                return tmp.entity.indexOf(inputValue) > -1
              }}
              render={(item) => item.entity}
            />
          </FormInput>
        </Form>
      </Spin>
    </ModalForm>
  )
}

export default PermissionUpdate
