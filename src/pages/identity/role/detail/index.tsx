import { CodeOutlined, ReadOutlined } from '@ant-design/icons'
import { Tabs } from 'antd'
import { getRole } from 'api'
import { DetailModal, Loader } from 'components'
import { CloseAwaitMS } from 'configs'
import { useWindow } from 'hooks'
import { UserRole } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { calcModalWidth, isEmptyString } from 'utils'
import Detail from './detail'
import Permission from './permission'

interface UserRoleDetailProps {
  visible: boolean
  onOk: Function
  role: UserRole
}

const UserRoleDetail: React.FC<UserRoleDetailProps> = ({ visible, onOk, role }) => {
  const intl = useIntl()
  const [width] = useWindow()
  const _isMounted = useRef(true)
  const [vis, setVis] = useState<boolean>(visible)
  const [userRole, setUserRole] = useState<UserRole>()
  const [loading, setLoading] = useState<boolean>(true)

  useEffect(() => {
    fetchRole(role.id)
    return () => {
      _isMounted.current = false
    }
  }, [role])

  const fetchRole = async (id: number) => {
    setLoading(true)
    const ro = (await getRole({ data: { role_id: id } })) as UserRole
    if (_isMounted.current) {
      setUserRole(ro)
      setLoading(false)
    }
  }

  return (
    <DetailModal
      visible={vis}
      width={calcModalWidth(width)}
      title={
        <FormattedMessage
          id="role.detail_title"
          values={{
            name: isEmptyString(role.name) ? role.name : role.id,
          }}
        />
      }
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onOk()
        }, CloseAwaitMS)
      }}
    >
      <Tabs defaultActiveKey="detail" style={{ marginTop: -24 }}>
        <Tabs.TabPane
          tab={
            <>
              <ReadOutlined />
              {intl.formatMessage({ id: 'detail' })}
            </>
          }
          key="detail"
          className="p_t_1"
        >
          <Loader loading={loading && !userRole}>{!!userRole && <Detail role={userRole} />}</Loader>
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <>
              <CodeOutlined />
              {intl.formatMessage({ id: 'permission' })}
            </>
          }
          key="permission"
          className="p_t_1"
        >
          <Loader loading={loading && !userRole}>
            {!!userRole && (
              <Permission
                role={userRole}
                fetchRole={() => {
                  fetchRole(role.id)
                }}
              />
            )}
          </Loader>
        </Tabs.TabPane>
      </Tabs>
    </DetailModal>
  )
}

export default UserRoleDetail
