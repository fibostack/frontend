import { Form, notification, Row, Col } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { updateRole } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { UserRole } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

interface RoleUpdateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  role: UserRole
}

const RoleUpdate: React.FC<RoleUpdateProps> = ({ visible, onOk, onCancel, role }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.role_id = role.id
    const success = await updateRole({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${values.name} role is updated.`,
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'role.update_title' }, { name: role.name })}
      formName="role_update_form"
      okText={<FormattedMessage id="update" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form
        form={form}
        name="role_update_form"
        layout="vertical"
        labelAlign="left"
        onFinish={handleFinish}
        initialValues={{
          name: role.name,
        }}
      >
        <FormInput
          required
          hasLabel
          name="name"
          type="input"
          label={intl.formatMessage({ id: 'name' })}
          placeholder={intl.formatMessage({ id: 'name' })}
        />
      </Form>
    </ModalForm>
  )
}

export default RoleUpdate
