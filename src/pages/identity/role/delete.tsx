import { notification } from 'antd'
import { deleteRole } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { UserRole } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface RoleDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  role: UserRole
}

const RoleDelete: React.FC<RoleDeleteProps> = ({ visible, onOk, onCancel, role }) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [vis, setVis] = useState<boolean>(visible)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteRole({
      data: { role_id: role.id },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${role.name} role is deleted.`,
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'role.delete_title' })}
      selectedNames={role.name}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default RoleDelete
