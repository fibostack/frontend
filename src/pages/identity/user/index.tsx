import { DeleteOutlined } from '@ant-design/icons'
import { Descriptions } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listUser } from 'api'
import { BooleanStatus, Ligther, PageLayout, Table } from 'components'
import { MenuAction, User } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { booleanSort, DateFormat, dateSort, findInString } from 'utils'
import CreateModal from './create'
import DeleteModal from './delete'

interface UserProps {}

const UserList: React.FC<UserProps> = () => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [users, setUsers] = useState<User[]>([])
  const [keyword, setKeyword] = useState<string>('')
  const [selectedRows, setSelectedRows] = useState<User[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [deleteVisible, setDeleteVisible] = useState<boolean>(false)

  useEffect(() => {
    fetchUsers(true)
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchUsers = async (init = false) => {
    setLoading(true)
    const us = (await listUser({})) as User[]
    if (_isMounted.current) {
      setUsers(us)
      setLoading(false)
    }
  }

  const columns: ColumnType<User>[] = [
    {
      title: intl.formatMessage({ id: 'name' }),
      key: 'name',
      width: 300,
      ellipsis: true,
      sorter: (a, b) => a.firstname.localeCompare(b.firstname),
      render: (user: User) => {
        const tmpVal = `${user.firstname} ${user.lastname}`
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
    {
      title: intl.formatMessage({ id: 'email' }),
      dataIndex: 'email',
      key: 'email',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.email.localeCompare(b.email),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: intl.formatMessage({ id: 'phone' }),
      dataIndex: 'phone',
      key: 'phone',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.phone.localeCompare(b.phone),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} /> || '-',
    },
    {
      title: intl.formatMessage({ id: 'enable' }),
      dataIndex: 'is_active',
      key: 'is_active',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => booleanSort(a.is_active, b.is_active),
      render: (isActive: boolean) => <BooleanStatus status={isActive} />,
    },
    {
      title: intl.formatMessage({ id: 'last_login_date' }),
      dataIndex: 'last_login_date',
      key: 'last_login_date',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => dateSort(a.last_login_date, b.last_login_date),
      render: (value: Date) => {
        const tmpVal = DateFormat(value)
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
  ]

  const actions: MenuAction[] = [
    {
      name: intl.formatMessage({ id: 'delete' }),
      disabled: selectedRows.length === 0,
      icon: <DeleteOutlined />,
      action: () => {
        setDeleteVisible(true)
      },
    },
  ]

  return (
    <PageLayout
      loading={loading}
      title={intl.formatMessage({ id: 'users' })}
      fetchAction={() => {
        fetchUsers()
      }}
      actions={actions}
      createAction={() => {
        setCreateVisible(true)
      }}
    >
      <>
        {createVisible && (
          <CreateModal
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchUsers()
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {deleteVisible && selectedRows.length > 0 && (
          <DeleteModal
            users={selectedRows}
            visible={deleteVisible}
            onOk={() => {
              setDeleteVisible(false)
              fetchUsers()
            }}
            onCancel={() => {
              setDeleteVisible(false)
            }}
          />
        )}
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          onRowSelected={setSelectedRows}
          dataCount={users.length}
          dataSource={
            keyword
              ? users.filter(
                  (user) =>
                    findInString(`${user.firstname} ${user.lastname}`, keyword) ||
                    findInString(user.email, keyword) ||
                    findInString(user.phone, keyword) ||
                    findInString(DateFormat(user.last_login_date), keyword)
                )
              : users
          }
          onSearch={(value) => {
            setKeyword(value)
          }}
          expandable={{
            expandedRowRender: (record: User) => (
              <Descriptions size="small" layout="vertical" column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}>
                {record.projects?.map((project) => (
                  <Descriptions.Item
                    label={
                      <strong>
                        <FormattedMessage id="project" />
                      </strong>
                    }
                  >
                    {project.project_name || '-'} ({' '}
                    <strong>
                      <FormattedMessage id="roles" />:
                    </strong>{' '}
                    {project.role_name})
                  </Descriptions.Item>
                ))}
                {(!record.projects || record.projects.length === 0) && (
                  <Descriptions.Item
                    label={
                      <strong>
                        <FormattedMessage id="project" />
                      </strong>
                    }
                  >
                    -
                  </Descriptions.Item>
                )}
              </Descriptions>
            ),
          }}
        />
      </>
    </PageLayout>
  )
}

export default UserList
