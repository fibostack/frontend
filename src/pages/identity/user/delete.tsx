import { notification } from 'antd'
import { deleteUser } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { User } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface UserDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  users: User[]
}

const UserDelete: React.FC<UserDeleteProps> = ({ visible, onOk, onCancel, users }) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [vis, setVis] = useState<boolean>(visible)

  const deleteIDs = users.reduce<string[]>((acc: string[], user: User) => {
    return [...acc, user.os_user_id]
  }, [])

  let deleteNames = users.reduce<string>((acc: string, user: User) => {
    return `${acc}${user.email}, `
  }, '')
  deleteNames = deleteNames.substring(0, deleteNames.length - 2)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteUser({
      data: { os_user_ids: deleteIDs },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${deleteNames} user is deleted.`,
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'user.delete_title' })}
      selectedNames={deleteNames}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default UserDelete
