import { Form, notification } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createUser } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface UserCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
}

const UserCreate: React.FC<UserCreateProps> = ({ visible, onOk, onCancel }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await createUser({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${values.firstname} user is created.`,
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.firstname)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'user.create_title' })}
      formName="user_create_form"
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="user_create_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <FormInput
          required
          hasLabel
          name="firstname"
          type="input"
          label={intl.formatMessage({ id: 'firstname' })}
          placeholder={intl.formatMessage({ id: 'firstname' })}
        />
        <FormInput
          required
          hasLabel
          name="lastname"
          type="input"
          label={intl.formatMessage({ id: 'lastname' })}
          placeholder={intl.formatMessage({ id: 'lastname' })}
        />
        <FormInput
          required
          hasLabel
          name="email"
          type="email"
          label={intl.formatMessage({ id: 'email' })}
          placeholder={intl.formatMessage({ id: 'email' })}
        />
        <FormInput
          required
          hasLabel
          name="mobile_num"
          type="phone"
          label={intl.formatMessage({ id: 'phone' })}
          placeholder={intl.formatMessage({ id: 'phone' })}
        />
        <FormInput
          required
          hasLabel
          name="password"
          type="password"
          label={intl.formatMessage({ id: 'password' })}
          placeholder={intl.formatMessage({ id: 'password' })}
        />
        <FormInput
          required
          hasLabel
          name="confirm_password"
          type="confirm_password"
          dependencies={['password']}
          label={intl.formatMessage({ id: 'confirm_password' })}
          placeholder={intl.formatMessage({ id: 'confirm_password' })}
          rules={[
            ({ getFieldValue }) => ({
              validator(_rule, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve()
                }
                return Promise.reject(new Error('The passwords not match!'))
              },
            }),
          ]}
        />
      </Form>
    </ModalForm>
  )
}

export default UserCreate
