import { notification } from 'antd'
import { deletePermission } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { UserPermission } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface PermissionDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  permissions: UserPermission[]
}

const PermissionDelete: React.FC<PermissionDeleteProps> = ({ visible, onOk, onCancel, permissions }) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [vis, setVis] = useState<boolean>(visible)

  const deleteIDs = permissions.reduce<number[]>((acc: number[], permission: UserPermission) => {
    return [...acc, permission.id]
  }, [])

  let deleteNames = permissions.reduce<string>((acc: string, permission: UserPermission) => {
    return `${acc}${permission.entity}, `
  }, '')
  deleteNames = deleteNames.substring(0, deleteNames.length - 2)

  const onDelete = async () => {
    setLoading(true)
    const success = await deletePermission({
      data: { permission_ids: deleteIDs },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${deleteNames} permission is deleted.`,
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'permission.delete_title' })}
      selectedNames={deleteNames}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default PermissionDelete
