import { Form, notification } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createPermission } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface PermissionCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
}

const PermissionCreate: React.FC<PermissionCreateProps> = ({ visible, onOk, onCancel }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await createPermission({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${values.entity} permission is created.`,
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.entity)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'permission.create_title' })}
      formName="permission_create_form"
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form form={form} name="permission_create_form" layout="vertical" labelAlign="left" onFinish={handleFinish}>
        <FormInput
          required
          hasLabel
          name="entity"
          label={intl.formatMessage({ id: 'name' })}
          type="input"
          placeholder={intl.formatMessage({ id: 'name' })}
        />
        <FormInput
          required
          hasLabel
          name="api"
          label={intl.formatMessage({ id: 'api' })}
          type="input"
          placeholder={intl.formatMessage({ id: 'api' })}
        />
      </Form>
    </ModalForm>
  )
}

export default PermissionCreate
