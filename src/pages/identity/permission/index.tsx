import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import { Button, Tooltip } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listPermission } from 'api'
import { Ligther, PageLayout, Table } from 'components'
import { MenuAction, UserPermission } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import { DateFormat, dateSort, findInString } from 'utils'
import CreateModal from './create'
// import DeleteModal from './delete'
// import UpdateModal from './update'

interface PermissionProps {}

const PermissionList: React.FC<PermissionProps> = () => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [permissions, setPermissions] = useState<UserPermission[]>([])
  const [keyword, setKeyword] = useState<string>('')
  const [selectedRows, setSelectedRows] = useState<UserPermission[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  // const [updateVisible, setUpdateVisible] = useState<UserPermission>()
  // const [deleteVisible, setDeleteVisible] = useState<boolean>(false)

  useEffect(() => {
    fetchPermissions()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchPermissions = async () => {
    setLoading(true)
    const pers = (await listPermission({})) as UserPermission[]
    if (_isMounted.current) {
      setPermissions(pers)
      setLoading(false)
    }
  }

  const columns: ColumnType<UserPermission>[] = [
    {
      title: intl.formatMessage({ id: 'name' }),
      key: 'name',
      width: 300,
      ellipsis: true,
      dataIndex: 'entity',
      sorter: (a, b) => a.entity.localeCompare(b.entity),
      render: (value: string) => {
        return <Ligther keywords={[keyword]} source={value} />
      },
    },
    {
      title: intl.formatMessage({ id: 'api' }),
      dataIndex: 'api',
      key: 'api',
      width: 300,
      ellipsis: true,
      sorter: (a, b) => a.api.localeCompare(b.api),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: intl.formatMessage({ id: 'create_at' }),
      dataIndex: 'created_at',
      key: 'created_at',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => dateSort(a.created_at, b.created_at),
      render: (value: Date) => {
        const tmpVal = DateFormat(value)
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
    // {
    //   title: intl.formatMessage({ id: 'action' }),
    //   key: 'action',
    //   width: 150,
    //   render: (_value, record) => (
    //     <Tooltip placement="topRight" title={intl.formatMessage({ id: 'update' })}>
    //       <Button
    //         type="primary"
    //         className="onlyIcon"
    //         icon={<EditOutlined />}
    //         onClick={() => {
    //           setUpdateVisible(record)
    //         }}
    //       />
    //     </Tooltip>
    //   ),
    // },
  ]

  // const actions: MenuAction[] = [
  //   {
  //     name: intl.formatMessage({ id: 'delete' }),
  //     disabled: selectedRows.length === 0,
  //     icon: <DeleteOutlined />,
  //     action: () => {
  //       setDeleteVisible(true)
  //     },
  //   },
  // ]

  return (
    <PageLayout
      loading={loading}
      title={intl.formatMessage({ id: 'permissions' })}
      fetchAction={() => {
        fetchPermissions()
      }}
      // actions={actions}
      createAction={() => {
        setCreateVisible(true)
      }}
    >
      <>
        {createVisible && (
          <CreateModal
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchPermissions()
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {/* {deleteVisible && selectedRows.length > 0 && (
          <DeleteModal
            permissions={selectedRows}
            visible={deleteVisible}
            onOk={() => {
              setDeleteVisible(false)
              fetchPermissions()
            }}
            onCancel={() => {
              setDeleteVisible(false)
            }}
          />
        )}
        {!!updateVisible && (
          <UpdateModal
            visible={!!updateVisible}
            permission={updateVisible}
            onOk={() => {
              setUpdateVisible(undefined)
              fetchPermissions()
            }}
            onCancel={() => {
              setUpdateVisible(undefined)
            }}
          />
        )} */}
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          onRowSelected={setSelectedRows}
          dataCount={permissions.length}
          dataSource={
            keyword
              ? permissions.filter(
                  (permission) =>
                    findInString(permission.entity, keyword) ||
                    findInString(permission.api, keyword) ||
                    findInString(DateFormat(permission.created_at), keyword)
                )
              : permissions
          }
          onSearch={(value) => {
            setKeyword(value)
          }}
        />
      </>
    </PageLayout>
  )
}

export default PermissionList
