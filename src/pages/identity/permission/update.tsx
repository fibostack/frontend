import { Form, notification } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { updatePermission } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { UserPermission } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

interface PermissionUpdateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  permission: UserPermission
}

const PermissionUpdate: React.FC<PermissionUpdateProps> = ({ visible, onOk, onCancel, permission }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.permission_id = permission.id
    const success = await updatePermission({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: `${values.entity} permission is updated.`,
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.entity)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'permission.update_title' }, { name: permission.entity })}
      formName="permission_update_form"
      okText={<FormattedMessage id="update" />}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form
        form={form}
        name="permission_update_form"
        layout="vertical"
        labelAlign="left"
        onFinish={handleFinish}
        initialValues={{ entity: permission.entity, api: permission.api }}
      >
        <FormInput
          required
          hasLabel
          name="entity"
          label={intl.formatMessage({ id: 'name' })}
          type="input"
          placeholder={intl.formatMessage({ id: 'name' })}
        />
        <FormInput
          required
          hasLabel
          name="api"
          label={intl.formatMessage({ id: 'api' })}
          type="input"
          placeholder={intl.formatMessage({ id: 'api' })}
        />
      </Form>
    </ModalForm>
  )
}

export default PermissionUpdate
