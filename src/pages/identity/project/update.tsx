/* eslint-disable camelcase */
import { Form, notification, Divider, Row, Col } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { updateProject, getProject } from 'api'
import { FormInput, ModalForm, Loader } from 'components'
import { CloseAwaitMS } from 'configs'
import { Project } from 'models'
import React, { useState, useRef, useEffect } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'
import { toNamespacedPath } from 'path'

interface ProjectUpdateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  project: Project
}

const quotaFieldNames = [
  'instance',
  'cpu',
  'ram',
  'keypair',
  'volume',
  'snapshot',
  'volume_size',
  'external_ip',
  'security_group',
]

const ProjectUpdate: React.FC<ProjectUpdateProps> = ({ visible, onOk, onCancel, project }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const _isMounted = useRef(true)
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)
  const [fLoading, setFLoading] = useState(true)
  const [projectO, setProjectO] = useState<Project>()

  useEffect(() => {
    fetchProject(project.id)
    return () => {
      _isMounted.current = false
    }
  }, [project.id])

  const fetchProject = async (pID: string) => {
    setFLoading(true)
    const p = (await getProject({ data: { project_id: pID } })) as Project
    if (_isMounted.current) {
      setProjectO(p)
      setFLoading(false)
    }
  }

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.project_id = project.id
    const success = await updateProject({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'project.updated' }, { name: values.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(values.keyPairName)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'project.update_title' }, { name: project.name })}
      formName="project_update_form"
      okText={intl.formatMessage({ id: 'update' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Loader loading={fLoading}>
        {projectO && (
          <Form
            form={form}
            name="project_update_form"
            layout="vertical"
            labelAlign="left"
            onFinish={handleFinish}
            initialValues={{
              name: project.name,
              enabled: project.enabled,
              description: project.description,
              instance: projectO.compute_quota?.instances.limit,
              cpu: projectO.compute_quota?.cores.limit,
              ram: projectO.compute_quota?.ram.limit,
              keypair: projectO.compute_quota?.key_pairs.limit,
              volume: projectO.volume_quota?.volumes.limit,
              snapshot: projectO.volume_quota?.snapshots.limit,
              volume_size: projectO.volume_quota?.gigabytes.limit,
              external_ip: projectO.compute_quota?.floating_ips.limit,
              security_group: projectO.compute_quota?.security_groups.limit,
            }}
          >
            <FormInput
              required
              hasLabel
              name="name"
              type="input"
              placeholder={intl.formatMessage({ id: 'name' })}
              label={intl.formatMessage({ id: 'name' })}
            />
            <FormInput
              required
              hasLabel
              name="description"
              type="textarea"
              label={intl.formatMessage({ id: 'description' })}
              placeholder={intl.formatMessage({ id: 'description' })}
            />
            <Divider>
              <FormattedMessage id="dashboard.resource_quota" />
            </Divider>
            <Row gutter={8}>
              {quotaFieldNames.map((name) => (
                <Col xs={24} sm={24} md={12} key={name}>
                  <FormInput
                    required
                    hasLabel
                    name={name}
                    type="number"
                    className="w-fill"
                    label={intl.formatMessage({ id: name })}
                    placeholder={intl.formatMessage({ id: name })}
                  />
                </Col>
              ))}
            </Row>
          </Form>
        )}
      </Loader>
    </ModalForm>
  )
}

export default ProjectUpdate
