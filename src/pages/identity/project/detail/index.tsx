import { CodeOutlined, ReadOutlined } from '@ant-design/icons'
import { Tabs } from 'antd'
import { getProject } from 'api'
import { DetailModal, Loader } from 'components'
import { CloseAwaitMS } from 'configs'
import { useWindow } from 'hooks'
import { Project } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { calcModalWidth, isEmptyString } from 'utils'
import Detail from './detail'
import Members from './member'

interface ProjectDetailProps {
  visible: boolean
  onOk: Function
  project: Project
}

const ProjectDetail: React.FC<ProjectDetailProps> = ({ visible, onOk, project }) => {
  const intl = useIntl()
  const [width] = useWindow()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [vis, setVis] = useState<boolean>(visible)
  const [projectO, setProjectO] = useState<Project>()

  useEffect(() => {
    fetchProject(project.id)
    return () => {
      _isMounted.current = false
    }
  }, [project.id])

  const fetchProject = async (pID: string) => {
    setLoading(true)
    const p = (await getProject({ data: { project_id: pID } })) as Project
    if (_isMounted.current) {
      setProjectO(p)
      setLoading(false)
    }
  }

  return (
    <DetailModal
      visible={vis}
      width={calcModalWidth(width)}
      title={
        <FormattedMessage
          id="project.detail_title"
          values={{
            name: projectO && (isEmptyString(projectO.name) ? projectO.name : projectO.id),
          }}
        />
      }
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onOk()
        }, CloseAwaitMS)
      }}
    >
      <Loader loading={loading}>
        {projectO && (
          <Tabs defaultActiveKey="detail" style={{ marginTop: -24 }}>
            <Tabs.TabPane
              tab={
                <>
                  <ReadOutlined />
                  {intl.formatMessage({ id: 'detail' })}
                </>
              }
              key="detail"
              className="p_t_1"
            >
              <Detail project={projectO} />
            </Tabs.TabPane>
            <Tabs.TabPane
              tab={
                <>
                  <CodeOutlined />
                  {intl.formatMessage({ id: 'members' })}
                </>
              }
              key="members"
              className="p_t_1"
            >
              <Members project={projectO} />
            </Tabs.TabPane>
          </Tabs>
        )}
      </Loader>
    </DetailModal>
  )
}

export default ProjectDetail
