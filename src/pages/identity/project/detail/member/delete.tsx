import { notification } from 'antd'
import { deleteProjectMember } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { User } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { isEmptyString } from 'utils'

interface ProjectMemberDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  members: User[]
  projectID: string
}

const ProjectMemberDelete: React.FC<ProjectMemberDeleteProps> = ({ onOk, projectID, visible, onCancel, members }) => {
  const intl = useIntl()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const deleteIDs = members.reduce<string[]>((acc: string[], pl: User) => {
    return [...acc, pl.os_user_id]
  }, [])

  let deleteNames = members.reduce<string>((acc: string, pl: User) => {
    return `${acc}${isEmptyString(pl.lastname) ? pl.lastname : pl.id}, `
  }, '')
  deleteNames = deleteNames.substring(0, deleteNames.length - 2)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteProjectMember({
      data: { project_id: projectID, os_user_ids: deleteIDs },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'project.member_deleted' }, { name: deleteNames }),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={<FormattedMessage id="project.member_delete_title" />}
      selectedNames={deleteNames}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default ProjectMemberDelete
