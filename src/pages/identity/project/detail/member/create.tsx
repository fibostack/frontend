/* eslint-disable camelcase */
import { Col, Divider, Form, Input, notification, Row, Select } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createProjectMember } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { Project, User, UserRole } from 'models'
import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

interface MemberCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  project: Project
  roles: UserRole[]
  users: User[]
}

const MemberCreate: React.FC<MemberCreateProps> = ({ visible, onOk, onCancel, roles, users, project }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.project_id = project.id
    values.project_name = project.name
    values.email = users.find((user) => user.os_user_id === values.os_user_id)?.email || ''
    const success = await createProjectMember({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'project.member_created' }, { name: project.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(project.name)
      }, CloseAwaitMS)
    }
  }

  const quotaFields = [
    {
      name: 'instance',
      min: 0,
      max: (project.compute_quota?.instances.limit || 0) - (project.reserved_quota?.instance || 0),
    },
    {
      name: 'cpu',
      min: 0,
      max: (project.compute_quota?.cores.limit || 0) - (project.reserved_quota?.cpu || 0),
    },
    {
      name: 'ram',
      min: 0,
      max: (project.compute_quota?.ram.limit || 0) - (project.reserved_quota?.ram || 0),
    },
    {
      name: 'keypair',
      min: 0,
      max: (project.compute_quota?.key_pairs.limit || 0) - (project.reserved_quota?.keypair || 0),
    },
    {
      name: 'volume',
      min: 0,
      max: (project.volume_quota?.volumes.limit || 0) - (project.reserved_quota?.volume || 0),
    },
    {
      name: 'snapshot',
      min: 0,
      max: (project.volume_quota?.snapshots.limit || 0) - (project.reserved_quota?.snapshot || 0),
    },
    {
      name: 'volume_size',
      min: 0,
      max: (project.volume_quota?.gigabytes.limit || 0) - (project.reserved_quota?.volume_size || 0),
    },
    {
      name: 'external_ip',
      min: 0,
      max: (project.compute_quota?.floating_ips.limit || 0) - (project.reserved_quota?.external_ip || 0),
    },
    {
      name: 'security_group',
      min: 0,
      max: (project.compute_quota?.security_groups.limit || 0) - (project.reserved_quota?.security_group || 0),
    },
  ]

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      formName="project_member_create_form"
      title={intl.formatMessage({ id: 'project.member_create_title' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form
        form={form}
        layout="vertical"
        labelAlign="left"
        onFinish={handleFinish}
        name="project_member_create_form"
        initialValues={{
          instance: 3,
          cpu: 6,
          ram: 12288,
          keypair: 20,
          volume: 6,
          snapshot: 6,
          volume_size: 300,
          external_ip: 3,
          security_group: 3,
        }}
      >
        <FormInput required hasLabel name="os_user_id" label={<FormattedMessage id="member" />}>
          <Select placeholder={<FormattedMessage id="member" />}>
            {users.map((user) => {
              return (
                <Select.Option key={user.firstname + user.os_user_id} value={user.os_user_id}>
                  {user.firstname}
                </Select.Option>
              )
            })}
          </Select>
        </FormInput>
        <FormInput required hasLabel name="role_id" label={<FormattedMessage id="role" />}>
          <Select placeholder={<FormattedMessage id="role" />}>
            {roles.map((role) => {
              return (
                <Select.Option key={role.name + role.id} value={role.id}>
                  {role.name}
                </Select.Option>
              )
            })}
          </Select>
        </FormInput>
        <Divider>
          <FormattedMessage id="dashboard.resource_quota" />
        </Divider>
        <Row gutter={8}>
          {quotaFields.map((item) => (
            <Col xs={24} sm={24} md={12} key={item.name}>
              <FormInput
                required
                hasLabel
                showLimits
                name={item.name}
                min={item.min}
                max={item.max}
                label={intl.formatMessage({ id: item.name })}
                placeholder={intl.formatMessage({ id: item.name })}
                type="number"
                className="w-fill"
              />
            </Col>
          ))}
        </Row>
      </Form>
    </ModalForm>
  )
}

export default MemberCreate
