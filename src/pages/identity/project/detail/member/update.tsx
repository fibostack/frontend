/* eslint-disable camelcase */
import { Col, Divider, Form, notification, Row, Select } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { getProjectUserQuota, updateProjectMember } from 'api'
import { FormInput, Loader, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import { User, UserQuotaset, UserRole, Project } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

interface ProjectMemberUpdateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  user: User
  project: Project
  roles: UserRole[]
}

const ProjectMemberUpdate: React.FC<ProjectMemberUpdateProps> = ({ visible, onOk, project, onCancel, user, roles }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const _isMounted = useRef(true)
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)
  const [fLoading, setFLoading] = useState(true)
  const [quota, setQuota] = useState<UserQuotaset>()

  useEffect(() => {
    fetchDatas(project.id, user.os_user_id)
    return () => {
      _isMounted.current = false
    }
  }, [project.id, user.os_user_id])

  const fetchDatas = async (pID: string, uID: string) => {
    setFLoading(true)
    const upq = (await getProjectUserQuota({ data: { project_id: pID, os_user_id: uID } })) as UserQuotaset
    if (_isMounted.current) {
      setQuota(upq)
      setFLoading(false)
    }
  }

  const handleFinish = async (values: Store) => {
    setLoading(true)
    values.project_id = project.id
    values.os_user_id = user.os_user_id
    values.quota_id = quota?.id || -1
    const success = await updateProjectMember({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'project.member_updated' }, { name: user.lastname }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  const quotaFields = [
    {
      name: 'instance',
      min: quota?.instances || 0,
      max:
        (project.compute_quota?.instances.limit || 0) -
        (project.reserved_quota?.instance || 0) +
        ((quota && quota.instances) || 0),
    },
    {
      name: 'cpu',
      min: quota?.cpu || 0,
      max: (project.compute_quota?.cores.limit || 0) - (project.reserved_quota?.cpu || 0) + ((quota && quota.cpu) || 0),
    },
    {
      name: 'ram',
      min: quota?.ram || 0,
      max: (project.compute_quota?.ram.limit || 0) - (project.reserved_quota?.ram || 0) + ((quota && quota.ram) || 0),
    },
    {
      name: 'keypair',
      min: quota?.keypair || 0,
      max:
        (project.compute_quota?.key_pairs.limit || 0) -
        (project.reserved_quota?.keypair || 0) +
        ((quota && quota.keypair) || 0),
    },
    {
      name: 'volume',
      min: quota?.volume || 0,
      max:
        (project.volume_quota?.volumes.limit || 0) -
        (project.reserved_quota?.volume || 0) +
        ((quota && quota.volume) || 0),
    },
    {
      name: 'snapshot',
      min: quota?.snapshot || 0,
      max:
        (project.volume_quota?.snapshots.limit || 0) -
        (project.reserved_quota?.snapshot || 0) +
        ((quota && quota.snapshot) || 0),
    },
    {
      name: 'volume_size',
      min: quota?.volume_size || 0,
      max:
        (project.volume_quota?.gigabytes.limit || 0) -
        (project.reserved_quota?.volume_size || 0) +
        ((quota && quota.volume_size) || 0),
    },
    {
      name: 'external_ip',
      min: quota?.external_ip || 0,
      max:
        (project.compute_quota?.floating_ips.limit || 0) -
        (project.reserved_quota?.external_ip || 0) +
        ((quota && quota.external_ip) || 0),
    },
    {
      name: 'security_group',
      min: quota?.security_group || 0,
      max:
        (project.compute_quota?.security_groups.limit || 0) -
        (project.reserved_quota?.security_group || 0) +
        ((quota && quota.security_group) || 0),
    },
  ]

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'project.member_update_title' }, { name: user.lastname })}
      formName="project_member_role_update_form"
      okText={intl.formatMessage({ id: 'update' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Loader loading={fLoading}>
        {quota && (
          <Form
            form={form}
            layout="vertical"
            name="project_member_role_update_form"
            labelAlign="left"
            onFinish={handleFinish}
            initialValues={{
              role_id: user.projects?.find((item) => item.project_id === project.id)?.role_id,
              instance: quota.instances,
              ...quota,
            }}
          >
            <FormInput
              required
              hasLabel
              name="role_id"
              label={intl.formatMessage({ id: 'role' })}
              className="form_last_item"
            >
              <Select placeholder={intl.formatMessage({ id: 'role' })}>
                {roles.map((role) => {
                  return (
                    <Select.Option key={role.name + role.id} value={role.id}>
                      {role.name}
                    </Select.Option>
                  )
                })}
              </Select>
            </FormInput>
            <Divider>
              <FormattedMessage id="dashboard.resource_quota" />
            </Divider>
            <Row gutter={8}>
              {quotaFields.map((item) => (
                <Col xs={24} sm={24} md={12} key={item.name}>
                  <FormInput
                    required
                    hasLabel
                    showLimits
                    name={item.name}
                    min={item.min}
                    max={item.max}
                    type="number"
                    className="w-fill"
                    label={intl.formatMessage({ id: item.name })}
                    placeholder={intl.formatMessage({ id: item.name })}
                  />
                </Col>
              ))}
            </Row>
          </Form>
        )}
      </Loader>
    </ModalForm>
  )
}

export default ProjectMemberUpdate
