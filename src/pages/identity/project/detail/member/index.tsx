/* eslint-disable camelcase */
import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import { Button, Tooltip } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listRole, listUser } from 'api'
import { BooleanStatus, Ligther, PageLayout, Table } from 'components'
import { MenuAction, Project, User, UserRole } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { booleanSort, findInString } from 'utils'
import CreateModal from './create'
import DeleteModal from './delete'
import Detail from './detail'
import UpdateModal from './update'

interface UserProps {
  project: Project
}

const UserList: React.FC<UserProps> = ({ project }) => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [users, setUsers] = useState<User[]>([])
  const [roles, setRoles] = useState<UserRole[]>([])
  const [keyword, setKeyword] = useState<string>('')
  const [selectedRows, setSelectedRows] = useState<User[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [deleteVisible, setDeleteVisible] = useState<boolean>(false)
  const [updateVisible, setUpdateVisible] = useState<User>()

  useEffect(() => {
    fetchDatas(true)
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchDatas = async (init = false) => {
    setLoading(true)
    const [userResponse, roleResponse] = await Promise.all([listUser({}), init && listRole({})])
    const us = userResponse as User[]
    if (_isMounted.current) {
      setUsers(us)
      if (init) {
        const rs = roleResponse as UserRole[]
        setRoles(rs)
      }
      setLoading(false)
    }
  }

  const columns: ColumnType<User>[] = [
    {
      title: intl.formatMessage({ id: 'name' }),
      key: 'name',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.firstname.localeCompare(b.firstname),
      render: (user: User) => {
        const tmpVal = `${user.firstname} ${user.lastname}`
        return <Ligther keywords={[keyword]} source={tmpVal} />
      },
    },
    {
      title: intl.formatMessage({ id: 'email' }),
      dataIndex: 'email',
      key: 'email',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => a.email.localeCompare(b.email),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: intl.formatMessage({ id: 'role' }),
      key: 'role',
      width: 200,
      ellipsis: true,
      render: (user: User) => {
        const tmpVal = user.projects?.find((item) => item.project_id === project.id)
        if (tmpVal) {
          return keyword ? <Ligther keywords={[keyword]} source={tmpVal.role_name || ''} /> : tmpVal.role_name
        }
        return '-'
      },
    },
    {
      title: intl.formatMessage({ id: 'enable' }),
      dataIndex: 'is_active',
      key: 'is_active',
      width: 200,
      ellipsis: true,
      sorter: (a, b) => booleanSort(a.is_active, b.is_active),
      render: (isActive: boolean) => <BooleanStatus status={isActive} />,
    },
    {
      title: <FormattedMessage id="action" />,
      key: 'action',
      width: 80,
      render: (_value, record) => (
        <Tooltip placement="topRight" title={<FormattedMessage id="update" />}>
          <Button
            type="primary"
            className="onlyIcon"
            icon={<EditOutlined />}
            onClick={() => {
              setUpdateVisible(record)
            }}
          />
        </Tooltip>
      ),
    },
  ]

  const actions: MenuAction[] = [
    {
      name: <FormattedMessage id="delete" />,
      disabled: selectedRows.length === 0,
      icon: <DeleteOutlined />,
      action: () => {
        setDeleteVisible(true)
      },
    },
  ]

  const members = users.filter((user) =>
    user.projects ? user.projects.findIndex((p) => p.project_id === project.id) !== -1 : false
  )

  return (
    <PageLayout
      noCard
      loading={loading}
      title={intl.formatMessage({ id: 'members' })}
      fetchAction={() => {
        fetchDatas()
      }}
      createAction={() => {
        setCreateVisible(true)
      }}
      actions={actions}
    >
      <>
        {createVisible && (
          <CreateModal
            roles={roles}
            users={users.filter((user) =>
              user.projects ? user.projects.findIndex((p) => p.project_id === project.id) === -1 : true
            )}
            project={project}
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchDatas()
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {selectedRows.length > 0 && deleteVisible && (
          <DeleteModal
            projectID={project.id}
            visible={deleteVisible}
            members={selectedRows}
            onOk={() => {
              setDeleteVisible(false)
              setSelectedRows([])
              fetchDatas()
            }}
            onCancel={() => {
              setDeleteVisible(false)
              setSelectedRows([])
            }}
          />
        )}
        {!!updateVisible && (
          <UpdateModal
            project={project}
            roles={roles}
            user={updateVisible}
            visible={!!updateVisible}
            onOk={() => {
              setUpdateVisible(undefined)
              fetchDatas()
            }}
            onCancel={() => {
              setUpdateVisible(undefined)
            }}
          />
        )}
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          onRowSelected={setSelectedRows}
          dataCount={members.length}
          dataSource={
            keyword
              ? members.filter(
                  (member) =>
                    findInString(`${member.firstname} ${member.lastname}`, keyword) ||
                    findInString(member.email, keyword) ||
                    findInString(
                      member.projects?.find((item) => item.project_id === project.id)?.role_name || '',
                      keyword
                    )
                )
              : members
          }
          onSearch={(value) => {
            setKeyword(value)
          }}
          expandable={{
            expandedRowRender: (record: User) => <Detail projectID={project.id} user={record} />,
          }}
        />
      </>
    </PageLayout>
  )
}

export default UserList
