/* eslint-disable camelcase */
import { Descriptions } from 'antd'
import { getProjectUserQuota } from 'api'
import { Loader } from 'components'
import { User, UserQuotaset } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { formatGB, formatMB } from 'utils'

interface Props {
  user: User
  projectID: string
}

const MemberDetail: React.FC<Props> = ({ user, projectID }) => {
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState<boolean>(false)
  const [quota, setQuota] = useState<UserQuotaset>()

  useEffect(() => {
    fetchDatas(projectID, user.os_user_id)
    return () => {
      _isMounted.current = false
    }
  }, [projectID, user.os_user_id])

  const fetchDatas = async (pID: string, uID: string) => {
    setLoading(true)
    const upq = (await getProjectUserQuota({ data: { project_id: pID, os_user_id: uID } })) as UserQuotaset
    if (_isMounted.current) {
      setQuota(upq)
      setLoading(false)
    }
  }

  return (
    <Loader loading={loading}>
      <Descriptions size="small" layout="vertical" column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="instance" />
            </strong>
          }
        >
          {quota?.instances || '-'}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="cpu" />
            </strong>
          }
        >
          {`${quota?.cpu} vCPU` || '-'}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="ram" />
            </strong>
          }
        >
          {formatMB(quota?.ram || 0) || '-'}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="keypair" />
            </strong>
          }
        >
          {quota?.keypair || '-'}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="volume" />
            </strong>
          }
        >
          {quota?.volume || '-'}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="snapshot" />
            </strong>
          }
        >
          {quota?.snapshot || '-'}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="volume_size" />
            </strong>
          }
        >
          {formatGB(quota?.volume_size || 0) || '-'}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="external_ip" />
            </strong>
          }
        >
          {quota?.external_ip || '-'}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="security_group" />
            </strong>
          }
        >
          {quota?.security_group || '-'}
        </Descriptions.Item>
      </Descriptions>
    </Loader>
  )
}

export default MemberDetail
