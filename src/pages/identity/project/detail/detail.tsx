/* eslint-disable camelcase */
import { Descriptions, Divider } from 'antd'
import { BooleanStatus } from 'components'
import { Project } from 'models'
import React from 'react'
import { FormattedMessage } from 'react-intl'
import { formatGB, formatMB } from 'utils'

interface Props {
  project: Project
}

const Detail = ({ project }: Props) => {
  return (
    <>
      <Descriptions column={{ sm: 2, xs: 1 }}>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="name" />
            </strong>
          }
        >
          {project.name || '-'}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="id" />
            </strong>
          }
        >
          {project.id || '-'}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="project.domain_id" />
            </strong>
          }
        >
          {project.domain_id || '-'}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="enabled" />
            </strong>
          }
        >
          <BooleanStatus status={project.enabled} />
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="description" />
            </strong>
          }
        >
          {project.description || '-'}
        </Descriptions.Item>
      </Descriptions>
      <Divider>
        <FormattedMessage id="dashboard.resource_quota" />
      </Divider>
      <Descriptions column={{ sm: 2, xs: 1 }}>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="instance" />
            </strong>
          }
        >
          {`${project.reserved_quota?.instance} / ${project.compute_quota?.instances.limit}`}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="cpu" />
            </strong>
          }
        >
          {`${project.reserved_quota?.cpu}vCPU / ${project.compute_quota?.cores.limit}vCPU`}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="ram" />
            </strong>
          }
        >
          {`${formatMB(project.reserved_quota?.ram || 0)} / ${formatMB(project.compute_quota?.ram.limit || 0)}`}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="keypair" />
            </strong>
          }
        >
          {`${project.reserved_quota?.keypair} / ${project.compute_quota?.key_pairs.limit}`}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="volume" />
            </strong>
          }
        >
          {`${project.reserved_quota?.volume} / ${project.volume_quota?.volumes.limit}`}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="snapshot" />
            </strong>
          }
        >
          {`${project.reserved_quota?.snapshot} / ${project.volume_quota?.snapshots.limit}`}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="volume_size" />
            </strong>
          }
        >
          {`${formatGB(project.reserved_quota?.volume_size || 0)} / ${formatGB(
            project.volume_quota?.gigabytes.limit || 0
          )}`}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="external_ip" />
            </strong>
          }
        >
          {`${project.reserved_quota?.external_ip} / ${project.compute_quota?.floating_ips.limit}`}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <strong>
              <FormattedMessage id="security_group" />
            </strong>
          }
        >
          {`${project.reserved_quota?.security_group} / ${project.compute_quota?.security_groups.limit}`}
        </Descriptions.Item>
      </Descriptions>
    </>
  )
}

export default Detail
