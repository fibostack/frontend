import { notification } from 'antd'
import { deleteProject } from 'api'
import { DeleteModal } from 'components'
import { CloseAwaitMS } from 'configs'
import { Project } from 'models'
import React, { useState } from 'react'
import { useIntl } from 'react-intl'

interface ProjectDeleteProps {
  visible: boolean
  onOk: Function
  onCancel: Function
  projects: Project[]
}

const ProjectDelete: React.FC<ProjectDeleteProps> = ({ visible, onOk, onCancel, projects }) => {
  const intl = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [vis, setVis] = useState<boolean>(visible)

  const deleteIDs = projects.reduce<string[]>((acc: string[], project: Project) => {
    return [...acc, project.id]
  }, [])

  let deleteNames = projects.reduce<string>((acc: string, project: Project) => {
    return `${acc}${project.name}, `
  }, '')
  deleteNames = deleteNames.substring(0, deleteNames.length - 2)

  const onDelete = async () => {
    setLoading(true)
    const success = await deleteProject({
      data: { project_ids: deleteIDs },
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'project.deleted' }, { name: deleteNames }),
      })
      setVis(false)
      setTimeout(() => {
        onOk()
      }, CloseAwaitMS)
    }
  }

  return (
    <DeleteModal
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'project.delete_title' })}
      okText={intl.formatMessage({ id: 'delete' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      selectedNames={deleteNames}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
      onDelete={() => {
        onDelete()
      }}
    />
  )
}

export default ProjectDelete
