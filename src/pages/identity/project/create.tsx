import { Form, notification, Divider, Row, Col } from 'antd'
import { Store } from 'antd/lib/form/interface'
import { createProject } from 'api'
import { FormInput, ModalForm } from 'components'
import { CloseAwaitMS } from 'configs'
import React, { useState, ReactNode } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'

interface ProjectCreateProps {
  visible: boolean
  onOk: Function
  onCancel: Function
}

const quotaFieldNames = [
  'instance',
  'cpu',
  'ram',
  'keypair',
  'volume',
  'snapshot',
  'volume_size',
  'external_ip',
  'security_group',
]

const ProjectCreate: React.FC<ProjectCreateProps> = ({ visible, onOk, onCancel }) => {
  const intl = useIntl()
  const [form] = Form.useForm()
  const [vis, setVis] = useState<boolean>(visible)
  const [loading, setLoading] = useState<boolean>(false)

  const handleFinish = async (values: Store) => {
    setLoading(true)
    const success = await createProject({
      data: values,
    })
    setLoading(false)
    if (success) {
      notification.success({
        message: intl.formatMessage({ id: 'successful' }),
        description: intl.formatMessage({ id: 'project.created' }, { name: values.name }),
      })
      form.resetFields()
      setVis(false)
      setTimeout(() => {
        onOk(success.id)
      }, CloseAwaitMS)
    }
  }

  return (
    <ModalForm
      visible={vis}
      loading={loading}
      title={intl.formatMessage({ id: 'project.create_title' })}
      formName="project_create_form"
      okText={intl.formatMessage({ id: 'create' })}
      cancelText={intl.formatMessage({ id: 'cancel' })}
      onCancel={() => {
        setVis(false)
        setTimeout(() => {
          onCancel()
        }, CloseAwaitMS)
      }}
    >
      <Form
        form={form}
        name="project_create_form"
        layout="vertical"
        labelAlign="left"
        onFinish={handleFinish}
        initialValues={{
          instance: 30,
          cpu: 60,
          ram: 122880,
          keypair: 200,
          volume: 60,
          snapshot: 60,
          volume_size: 3000,
          external_ip: 30,
          security_group: 30,
        }}
      >
        <FormInput
          required
          hasLabel
          name="name"
          type="input"
          placeholder={intl.formatMessage({ id: 'name' })}
          label={intl.formatMessage({ id: 'name' })}
        />
        <FormInput
          required
          hasLabel
          name="description"
          type="textarea"
          label={intl.formatMessage({ id: 'description' })}
          placeholder={intl.formatMessage({ id: 'description' })}
        />
        <Divider>
          <FormattedMessage id="dashboard.resource_quota" />
        </Divider>
        <Row gutter={8}>
          {quotaFieldNames.map((name) => (
            <Col xs={24} sm={24} md={12} key={name}>
              <FormInput
                required
                hasLabel
                name={name}
                type="number"
                className="w-fill"
                label={intl.formatMessage({ id: name })}
                placeholder={intl.formatMessage({ id: name })}
              />
            </Col>
          ))}
        </Row>
      </Form>
    </ModalForm>
  )
}

export default ProjectCreate
