import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import { Button, Tooltip } from 'antd'
import { ColumnType } from 'antd/es/table'
import { listProject } from 'api'
import { BooleanStatus, Ligther, PageLayout, Table } from 'components'
import { MenuAction, Project } from 'models'
import React, { useEffect, useRef, useState } from 'react'
import { useIntl } from 'react-intl'
import { findInString, booleanSort } from 'utils'
import CreateModal from './create'
import DeleteModal from './delete'
import DetailModal from './detail'
import UpdateModal from './update'

interface ProjectProps {}

const ProjectList: React.FC<ProjectProps> = () => {
  const intl = useIntl()
  const _isMounted = useRef(true)
  const [loading, setLoading] = useState(true)
  const [keyword, setKeyword] = useState<string>('')
  const [projects, setProjects] = useState<Project[]>([])
  const [selectedRows, setSelectedRows] = useState<Project[]>([])
  const [createVisible, setCreateVisible] = useState<boolean>(false)
  const [updateVisible, setUpdateVisible] = useState<Project>()
  const [deleteVisible, setDeleteVisible] = useState<boolean>(false)
  const [detailVisible, setDetailVisible] = useState<Project>()

  useEffect(() => {
    fetchProjects()
    return () => {
      _isMounted.current = false
    }
  }, [])

  const fetchProjects = async () => {
    setLoading(true)
    const ps = (await listProject({})) as Project[]
    if (_isMounted.current) {
      setProjects(ps.filter((p) => p.name !== 'userdefault'))
      setLoading(false)
    }
  }

  const columns: ColumnType<Project>[] = [
    {
      key: 'name',
      width: 300,
      title: intl.formatMessage({ id: 'name' }),
      ellipsis: true,
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (project: Project) => {
        const tmpVal = project.name
        return (
          <Button
            type="link"
            onClick={() => {
              setDetailVisible(project)
            }}
          >
            <Ligther keywords={[keyword]} source={tmpVal} />
          </Button>
        )
      },
    },
    {
      title: intl.formatMessage({ id: 'description' }),
      dataIndex: 'description',
      key: 'description',
      width: 400,
      ellipsis: true,
      sorter: (a, b) => a.description.localeCompare(b.description),
      render: (value: string) => <Ligther keywords={[keyword]} source={value} />,
    },
    {
      title: intl.formatMessage({ id: 'enabled' }),
      dataIndex: 'enabled',
      key: 'enabled',
      width: 100,
      ellipsis: true,
      sorter: (a, b) => booleanSort(a.enabled, b.enabled),
      render: (value: boolean) => <BooleanStatus status={value} />,
    },
    {
      title: intl.formatMessage({ id: 'action' }),
      key: 'action',
      width: 80,
      render: (_value, record) => (
        <Tooltip placement="topRight" title={intl.formatMessage({ id: 'update' })}>
          <Button
            type="primary"
            className="onlyIcon"
            icon={<EditOutlined />}
            onClick={() => {
              setUpdateVisible(record)
            }}
          />
        </Tooltip>
      ),
    },
  ]

  const actions: MenuAction[] = [
    {
      name: intl.formatMessage({ id: 'delete' }),
      disabled: selectedRows.length === 0,
      icon: <DeleteOutlined />,
      action: () => {
        setDeleteVisible(true)
      },
    },
  ]

  return (
    <PageLayout
      loading={loading}
      title={intl.formatMessage({ id: 'projects' })}
      fetchAction={fetchProjects}
      actions={actions}
      createAction={() => {
        setCreateVisible(true)
      }}
    >
      <>
        {createVisible && (
          <CreateModal
            visible={createVisible}
            onOk={() => {
              setCreateVisible(false)
              fetchProjects()
            }}
            onCancel={() => {
              setCreateVisible(false)
            }}
          />
        )}
        {!!updateVisible && (
          <UpdateModal
            project={updateVisible}
            visible={!!updateVisible}
            onOk={() => {
              setUpdateVisible(undefined)
              fetchProjects()
            }}
            onCancel={() => {
              setUpdateVisible(undefined)
            }}
          />
        )}
        {deleteVisible && selectedRows.length > 0 && (
          <DeleteModal
            projects={selectedRows}
            visible={deleteVisible}
            onOk={() => {
              setDeleteVisible(false)
              fetchProjects()
            }}
            onCancel={() => {
              setDeleteVisible(false)
            }}
          />
        )}
        {!!detailVisible && (
          <DetailModal
            project={detailVisible}
            visible={!!detailVisible}
            onOk={() => {
              setDetailVisible(undefined)
            }}
          />
        )}
        <Table
          rowKey="id"
          loading={loading}
          columns={columns}
          onRowSelected={setSelectedRows}
          dataCount={projects.length}
          dataSource={
            projects
              ? projects.filter(
                  (project) => findInString(project.name, keyword) || findInString(project.description, keyword)
                )
              : projects
          }
          onSearch={(value) => {
            setKeyword(value)
          }}
        />
      </>
    </PageLayout>
  )
}

export default ProjectList
