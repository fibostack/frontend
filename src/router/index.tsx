import { Loading } from 'components'
import { ConnectedRouter } from 'connected-react-router'
import { History } from 'history'
import Layout from 'layouts'
import { NotAuthorized, NotFound } from 'pages/errors'
import React from 'react'
import Loadable from 'react-loadable'
import { Redirect, Route, Switch } from 'react-router-dom'

const LoadableLoader = (loader: any) =>
  Loadable({
    loader,
    delay: false,
    loading: () => <Loading fill={false} />,
  })

interface RouteInterface {
  path: string
  component: React.FC | React.ComponentClass
  exact: boolean
}

const routes: RouteInterface[] = [
  {
    path: '/auth/signin',
    component: LoadableLoader(() => import('pages/signin')),
    exact: true,
  },
  {
    path: '/dashboard',
    component: LoadableLoader(() => import('pages/dashboard')),
    exact: true,
  },
  {
    path: '/fg-compute/instances',
    component: LoadableLoader(() => import('pages/compute/instance')),
    exact: true,
  },
  {
    path: '/fg-compute/instance/create',
    component: LoadableLoader(() => import('pages/compute/instance/create')),
    exact: true,
  },
  {
    path: '/fg-compute/instance/:id',
    component: LoadableLoader(() => import('pages/compute/instance/detail')),
    exact: true,
  },
  {
    path: '/fg-compute/key-pairs',
    component: LoadableLoader(() => import('pages/compute/key_pair')),
    exact: true,
  },
  {
    path: '/fg-compute/images',
    component: LoadableLoader(() => import('pages/compute/image')),
    exact: true,
  },
  {
    path: '/fg-compute/flavors',
    component: LoadableLoader(() => import('pages/compute/flavor')),
    exact: true,
  },
  {
    path: '/fg-storage/volumes',
    component: LoadableLoader(() => import('pages/storage/volume')),
    exact: true,
  },
  {
    path: '/fg-storage/snapshots',
    component: LoadableLoader(() => import('pages/storage/snapshot')),
    exact: true,
  },
  {
    path: '/fg-network/networks',
    component: LoadableLoader(() => import('pages/network/network')),
    exact: true,
  },
  {
    path: '/fg-network/routers',
    component: LoadableLoader(() => import('pages/network/router')),
    exact: true,
  },
  {
    path: '/fg-network/security-groups',
    component: LoadableLoader(() => import('pages/network/security_group')),
    exact: true,
  },
  {
    path: '/fg-network/security-groups/:id',
    component: LoadableLoader(() => import('pages/network/security_group/rules')),
    exact: true,
  },
  {
    path: '/fg-network/floating-ips',
    component: LoadableLoader(() => import('pages/network/floating_ip')),
    exact: true,
  },
  {
    path: '/fg-identity/projects',
    component: LoadableLoader(() => import('pages/identity/project')),
    exact: true,
  },
  {
    path: '/fg-identity/users',
    component: LoadableLoader(() => import('pages/identity/user')),
    exact: true,
  },
  {
    path: '/fg-identity/roles',
    component: LoadableLoader(() => import('pages/identity/role')),
    exact: true,
  },
  {
    path: '/fg-identity/permissions',
    component: LoadableLoader(() => import('pages/identity/permission')),
    exact: true,
  },
  {
    path: '/fg-system/information',
    component: LoadableLoader(() => import('pages/system/information')),
    exact: true,
  },
  {
    path: '/fg-system/hypervisor',
    component: LoadableLoader(() => import('pages/system/hypervisor')),
    exact: true,
  },
  {
    path: '/fg-action_log',
    component: LoadableLoader(() => import('pages/action_log')),
    exact: true,
  },
]

interface RouterProps {
  history: History
}

const Router: React.FC<RouterProps> = ({ history }) => {
  return (
    <ConnectedRouter history={history}>
      <Layout>
        <Switch>
          <Route exact path="/" render={() => <Redirect to="/dashboard" />} />
          {routes.map((route) => (
            <Route key={route.path} path={route.path} component={route.component} exact={route.exact} />
          ))}
          <Route exact path="403" component={NotAuthorized} />
          <Route component={NotFound} />
        </Switch>
      </Layout>
    </ConnectedRouter>
  )
}

export default Router
