/* eslint-disable camelcase */
// #region [Import]
import { notification } from 'antd'
import axios, { AxiosRequestConfig, Method } from 'axios'
import { store as Redux } from 'index'
import { BaseResponse } from 'models'
// #endregion

// #region [BaseRequest]
interface BaseRequestProps {
  url: string
  method: Method
  params?: Object
  data?: Object
  onError?: Function
}

const catchError = (err: any) => {
  if (err.response) {
    if (err.response.status === 401) {
      localStorage.removeItem('token')
      window.location.href = '/'
    } else if (err.response.status === 403) {
      notification.warning({
        message: 'Танд хандах эрх байхгүй байна.',
        description: '',
      })
    }
  } else if (err.message === 'Network Error') {
    notification.info({
      message: 'Алдаа гарлаа. Дараа дахин оролдоно уу',
      description: '',
    })
  } else {
    notification.error({
      message: err.name,
      description: err.message,
    })
  }
}

export const BaseRequest = async ({ onError, ...props }: BaseRequestProps) => {
  const { os_tenant_id, ipAddress } = Redux.getState().UserReducer
  const token = localStorage.getItem('token')
  const locale = localStorage.getItem('app.settings.locale')
  axios.defaults.headers.common.Accept = 'application/json'
  axios.defaults.headers.common['Access-Control-Allow-Headers'] = '*'
  axios.defaults.headers.common['Multi-Language'] = locale || 'en'
  axios.defaults.headers.common['Content-Type'] = 'application/json'
  axios.defaults.headers.common.Accept = 'application/json'
  if (token) axios.defaults.headers.common.Authorization = token
  if (ipAddress) axios.defaults.headers.common.SourceIP = ipAddress
  if (os_tenant_id) axios.defaults.headers.common['OS-Tenant-ID'] = os_tenant_id
  const config: AxiosRequestConfig = {
    baseURL: process.env.REACT_APP_BACK_URL,
    ...props,
  }
  try {
    const responseInstance = await axios(config)
    const response = responseInstance.data as BaseResponse
    if (response.status_code !== 0) {
      catchError(new Error(response.error_msg))
      return null
    }
    return response.body || false
  } catch (err) {
    catchError(err)
    if (onError) {
      onError()
    }
    return null
  }
}

export const freshAxios = axios.create()
// #endregion

// #region [Props]
interface GetProps {
  onError?: Function
}

interface PostProps {
  data: Object
  onError?: Function
}

interface PutProps {
  data: Object
  onError?: Function
}

interface DeleteProps {
  data: Object
  onError?: Function
}
// #endregion

// #region [QuotaSet]
export const getQuotaSet = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'system/projectLimit',
    method: 'GET',
    ...props,
  })
  return response || {}
}
// #endregion

// #region [Instance]
export const actionLogInstance = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'instance/action',
    method: 'POST',
    ...props,
  })
  return response || []
}

export const consoleLogInstance = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'instance/showConsole',
    method: 'POST',
    ...props,
  })
  return response
}

export const listInstance = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'instance/list',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const getInstance = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'instance/get',
    method: 'POST',
    ...props,
  })
  return response || {}
}

export const updateInstance = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'instance/resize',
    method: 'PUT',
    ...props,
  })
  return !!response
}

export const revertUpdateInstance = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'instance/revertResize',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const confirmUpdateInstance = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'instance/confirmResize',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const createInstance = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'instance/create',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const deleteInstance = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'instance/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}

export const startInstance = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'instance/start',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const onWAFInstance = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'instance/on_waf',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const offWAFInstance = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'instance/off_waf',
    method: 'DELETE',
    ...props,
  })
  return !!response
}

export const stopInstance = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'instance/stop',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const resumeInstance = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'instance/resume',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const suspendInstance = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'instance/suspend',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const rebootInstance = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'instance/reboot',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const remoteInstance = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'instance/console',
    method: 'POST',
    ...props,
  })
  return response
}

export const listAvailabilityZones = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'instance/listAZ',
    method: 'GET',
    ...props,
  })
  return response
}

export const listImage = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'image/listImage',
    method: 'GET',
    ...props,
  })
  return response
}
// #endregion

// #region [Flavor]
export const listFlavor = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'flavor/list',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const createFlavor = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'flavor/create',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const deleteFlavor = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'flavor/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}
// #endregion

// #region [Keypair]
export const listKeypair = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'keypair/list',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const createKeypair = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'keypair/create',
    method: 'POST',
    ...props,
  })
  return response
}

export const deleteKeypair = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'keypair/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}

export const importKeypair = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'keypair/import',
    method: 'POST',
    ...props,
  })
  return !!response
}
// #endregion

// #region [ImageService]
export const listImageService = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'imageService/listImages',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const listImageServiceData = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'imageService/listImageData',
    method: 'GET',
    ...props,
  })
  return response
}

export const createImageService = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'imageService/createImage',
    method: 'POST',
    ...props,
  })
  return !!response
}
// #endregion

// #region [Volume]
export const listVolume = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'volume/list',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const createVolume = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'volume/create',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const deleteVolume = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'volume/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}

export const extendVolume = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'volume/extend',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const snapshotVolume = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'volume/createSnapshot',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const attachVolume = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'volume/attach',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const detachVolume = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'volume/detach',
    method: 'POST',
    ...props,
  })
  return !!response
}
// #endregion

// #region [Snapshot]
export const listSnapshot = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'snapshot/list',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const deleteSnapshot = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'snapshot/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}
// #endregion

// #region [SecurityGroup]
export const listSecurityGroup = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'security_group/list',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const getSecurityGroup = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'security_group/get',
    method: 'POST',
    ...props,
  })
  return response || {}
}

export const createSecurityGroup = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'security_group/create',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const updateSecurityGroup = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'security_group/update',
    method: 'PUT',
    ...props,
  })
  return !!response
}

export const deleteSecurityGroup = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'security_group/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}
// #endregion

// #region [SecurityGroupRule]
export const createSecurityGroupRule = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'security_group/rule/create',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const deleteSecurityGroupRule = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'security_group/rule/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}
// #endregion

// #region [FloatingIP]
export const listFloatingIP = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'network/listFloatingIPs',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const createFloatingIP = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'network/createFloatingIP',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const deleteFloatingIP = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'network/deleteFloatingIP',
    method: 'DELETE',
    ...props,
  })
  return !!response
}
// #endregion

// #region [Project]
export const listProject = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'projects/list',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const getProject = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'projects/get',
    method: 'POST',
    ...props,
  })
  return response || {}
}

export const getProjectUserQuota = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'projects/user/quota',
    method: 'POST',
    ...props,
  })
  return response || {}
}

export const createProject = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'projects/create',
    method: 'POST',
    ...props,
  })
  return response
}

export const updateProject = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'projects/update',
    method: 'PUT',
    ...props,
  })
  return !!response
}

export const deleteProject = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'projects/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}

// #region [ProjectMember]
export const createProjectMember = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'projects/member/create',
    method: 'POST',
    ...props,
  })
  return response
}

export const updateProjectMember = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'projects/member/update',
    method: 'PUT',
    ...props,
  })
  return !!response
}

export const deleteProjectMember = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'projects/member/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}
// #endregion
// #endregion

// #region [Role]
export const listRole = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'roles/list',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const getRole = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'roles/get',
    method: 'POST',
    ...props,
  })
  return response || {}
}

export const createRole = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'roles/create',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const updateRole = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'roles/update',
    method: 'PUT',
    ...props,
  })
  return !!response
}

export const updateRolePermission = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'roles/update/permission',
    method: 'PUT',
    ...props,
  })
  return !!response
}

export const deleteRole = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'roles/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}
// #endregion

// #region [Permission]
export const listPermission = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'permissions/list',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const createPermission = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'permissions/create',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const updatePermission = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'permissions/update',
    method: 'PUT',
    ...props,
  })
  return !!response
}

export const deletePermission = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'permissions/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}
// #endregion

// #region [User]
export const listUser = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'user/list',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const createUser = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'user/create',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const updateUser = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'user/update',
    method: 'PUT',
    ...props,
  })
  return !!response
}

export const deleteUser = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'user/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}

export const listUserProjects = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'user/projects',
    method: 'GET',
    ...props,
  })
  return response || []
}
// #endregion

// #region [Network]
export const listNetwork = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'network/list',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const getNetwork = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'network/get',
    method: 'POST',
    ...props,
  })
  return response || {}
}

export const createNetwork = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'network/create',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const deleteNetwork = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'network/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}
export const updateNetwork = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'network/update',
    method: 'PUT',
    ...props,
  })
  return !!response
}
// #endregion

// #region [Subnet]
export const listAllSubnet = async () => {
  const response = await BaseRequest({
    url: 'network/listSubnetFromPrivate',
    method: 'GET',
  })
  return response || []
}

export const getSubnet = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'network/subnet/get',
    method: 'POST',
    ...props,
  })
  return response || {}
}

export const createSubnet = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'network/subnet/create',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const deleteSubnet = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'network/subnet/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}
export const updateSubnet = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'network/subnet/update',
    method: 'PUT',
    ...props,
  })
  return !!response
}
// #endregion

// #region [Port]
export const listPort = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'network/port/list',
    method: 'POST',
    ...props,
  })
  return response || []
}

export const getPort = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'network/port/get',
    method: 'POST',
    ...props,
  })
  return response || {}
}

export const createPort = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'network/port/create',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const deletePort = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'network/port/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}
export const updatePort = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'network/port/update',
    method: 'PUT',
    ...props,
  })
  return !!response
}
// #endregion

// #region [Router]
export const listRouter = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'network/router/list',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const getRouter = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'network/router/get',
    method: 'POST',
    ...props,
  })
  return response || {}
}

export const createRouter = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'network/router/create',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const deleteRouter = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'network/router/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}

export const updateRouter = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'network/router/update',
    method: 'PUT',
    ...props,
  })
  return !!response
}
// #endregion

// #region [Interface]
export const listInterface = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'network/router/interface/list',
    method: 'POST',
    ...props,
  })
  return response || []
}

export const listSubnet = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'network/subnet/list',
    method: 'POST',
    ...props,
  })
  return response || []
}

export const getInterface = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'network/interface/get',
    method: 'POST',
    ...props,
  })
  return response || {}
}

export const createInterface = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'network/router/addInterfaceSubnet',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const deleteInterface = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'network/router/removeInterfacePort',
    method: 'DELETE',
    ...props,
  })
  return !!response
}
export const updateInterface = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'network/interface/update',
    method: 'PUT',
    ...props,
  })
  return !!response
}

export const associateFIP = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'network/associateFIP',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const disassociatedFIP = async (props: PutProps) => {
  const response = await BaseRequest({
    url: 'network/disassociatedFIP',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const listIntanceInterfaces = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'interface/listServerInterface',
    method: 'POST',
    ...props,
  })
  return response || {}
}

export const getIntanceInterface = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'interface/getServerInterface',
    method: 'POST',
    ...props,
  })
  return response || {}
}

export const attachInterface = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'interface/createServerInterface',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const detachInterface = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'interface/deleteServerInterface',
    method: 'DELETE',
    ...props,
  })
  return !!response
}
// #endregion

// #region [SystemInformation]
export const listService = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'system/services',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const listComputeService = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'system/compute/service',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const listBlockStorageService = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'system/blockstorage/service',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const listNetworkAgent = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'system/network/agent',
    method: 'GET',
    ...props,
  })
  return response || []
}
// #endregion

// #region [Hyervisor]
export const listHypervisor = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'hypervisor/list',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const showHypervisor = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'hypervisor/show',
    method: 'POST',
    ...props,
  })
  return response || []
}

export const statisticsHypervisor = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'hypervisor/statistics',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const tenantUsageHypervisor = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'hypervisor/tenantUsage',
    method: 'POST',
    ...props,
  })
  return response || []
}

export const showUptimeHypervisor = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'hypervisor/showUptime',
    method: 'POST',
    ...props,
  })
  return response || []
}
// #endregion

// #region [ActionLog]
export const listActionLog = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'system/actionLog',
    method: 'POST',
    ...props,
  })
  return response || []
}
// #endregion

// #region [Object storage]
// #region [Bucket]
export const listBucket = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'objectstorages3/bucket/list',
    method: 'POST',
    ...props,
  })
  return response || []
}

export const listCloudBucket = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'noobaa/bucket/list',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const createCloudBucket = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'noobaa/bucket',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const createBucket = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'objectstorages3/bucket/create',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const getBucketPolicy = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'objectstorages3/bucket/get/policy',
    method: 'POST',
    ...props,
  })
  return response
}

export const setBucketPolicy = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'objectstorages3/bucket/set/policy',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const deleteBucket = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'objectstorages3/bucket/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}

export const checkAccessCloudBucket = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'noobaa/account/check-nbi',
    method: 'GET',
    ...props,
  })
  return response || false
}

export const setupCloudBucket = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'noobaa/account',
    method: 'POST',
    ...props,
  })
  return !!response
}

// #region [Cloud connection]
export const listCloudConnection = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'noobaa/account/connection',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const createCloudConnection = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'noobaa/account/add-connection',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const checkCloudConnection = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'noobaa/account/check-connection',
    method: 'POST',
    ...props,
  })
  return response
}
// #endregion

// #region [Cloud resource]
export const listCloudResource = async (props: GetProps) => {
  const response = await BaseRequest({
    url: 'noobaa/pool',
    method: 'GET',
    ...props,
  })
  return response || []
}

export const createCloudResource = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'noobaa/pool/cloud',
    method: 'POST',
    ...props,
  })
  return !!response
}

export const getCloudBuckets = async (props: GetProps, name: string) => {
  const response = await BaseRequest({
    url: `noobaa/bucket/cloud/${name}`,
    method: 'GET',
    ...props,
  })
  return response || []
}
// #endregion
// #endregion

// #region [Object]
export const listObject = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'objectstorages3/bucket/object/list',
    method: 'POST',
    ...props,
  })
  return response || []
}

export const createObject = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'objectstorages3/bucket/object/putpresinedurl',
    method: 'POST',
    ...props,
  })
  return response as { file_name: string; presign_url: string }[]
}

export const getObject = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'objectstorages3/bucket/object/get',
    method: 'POST',
    ...props,
  })
  return response || {}
}

export const linkObject = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'objectstorages3/bucket/object/link',
    method: 'POST',
    ...props,
  })
  return response
}

export const deleteObject = async (props: DeleteProps) => {
  const response = await BaseRequest({
    url: 'objectstorages3/bucket/object/delete',
    method: 'DELETE',
    ...props,
  })
  return !!response
}

export const getObjectParts = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'noobaa/object/parts',
    method: 'POST',
    ...props,
  })
  return response || {}
}

export const copyObject = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'objectstorages3/bucket/object/copy',
    method: 'POST',
    ...props,
  })
  return !!response
}
// #endregion

// #region [Folder]
export const createFolder = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'objectstorages3/bucket/folder/create',
    method: 'POST',
    ...props,
  })
  return !!response
}
// #endregion

// #region [api_key]
export const getAPIkey = async () => {
  const response = await BaseRequest({
    url: 'objectstorages3/api',
    method: 'GET',
  })
  return response
}

export const generateAPIkey = async (props: PostProps) => {
  const response = await BaseRequest({
    url: 'objectstorages3/api',
    method: 'POST',
    ...props,
  })
  return !!response
}

// #endregion
