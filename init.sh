echo 'Getting public IP'
IP="$(dig +short myip.opendns.com @resolver1.opendns.com)"
CHECK="$(echo $IP | grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}')"

if [ -z "$CHECK" ]
then
  ADDRESS="127.0.0.1"
else
  ADDRESS=$CHECK
fi

echo $ADDRESS

rm /opt/stack/fibostack/frontend/.env
cat > /opt/stack/fibostack/frontend/.env <<EOL
REACT_APP_BACK_URL=http://$ADDRESS:8081/api/v1/
REACT_APP_REMOTE_URL=http://$ADDRESS:6080/
EOL

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

source ~/.bashrc
command -v nvm
nvm install v12.19.0

npm i -g yarn
yarn
yarn build

sudo apt-get update
sudo apt-get install nginx -y

sudo mv /opt/stack/fibostack/frontend/nginx.conf /etc/nginx/sites-available/default

sudo service nginx start
sudo service nginx restart
